import { combineReducers } from 'redux';
import { reducer as form } from 'redux-form';
import { polyglotReducer as polyglot } from 'redux-polyglot';
import {reducer as notifications} from 'react-notification-system-redux';
import { page } from './page';
import { user } from './user';
import { search } from './search';
import { result } from './result';
import { book } from './book';
import { quote } from './quote';
import { company } from './company';
import { currency } from './currency';
import { shipment } from './shipment';
import { message } from './message';

export default combineReducers({
  form,
  notifications,
  page,
  user,
  company,
  polyglot,
  search,
  result,
  book,
  quote,
  currency,
  shipment,
  message
})
