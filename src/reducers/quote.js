import { POST_QUOTE, RECEIVE_POST_QUOTE, REJECT_POST_QUOTE,
         FETCH_QUOTES, RECEIVE_FETCH_QUOTES, REJECT_FETCH_QUOTES,
         DELETE_QUOTE, RECEIVE_DELETE_QUOTE, REJECT_DELETE_QUOTE } from '../actions';

const initialState = {
  isFetching : false,
  quotes : [],
  byCreatedAt : [],
  pageState : null
}

export const quote = (state = initialState,action) => {
  switch(action.type) {
    case POST_QUOTE :
      return Object.assign({}, state, {
        isFetching : action.isFetching
      })
    case RECEIVE_POST_QUOTE :
      return Object.assign({}, state, {
        isFetching : action.isFetching,
      });
    case FETCH_QUOTES :
      return Object.assign({}, state, {
        isFetching : action.isFetching
      });
    case RECEIVE_FETCH_QUOTES :
      const newQuotes = action.payload.results
                          .reduce( (quotesArr, quoteObj) => {
                            return quotesArr.concat(quoteObj);
                          }, state.quotes)
                          .filter((elem, pos, arr) => {
                            return arr.findIndex( arrObj => {
                              return arrObj.createdAt === elem.createdAt
                            }) === pos;
                          }).sort((a,b) => {
                            return new Date(b.createdAt) - new Date(a.createdAt);
                          });
      const newQuotesByCreatedAt = action.payload.results
                          .reduce( (quoteCreatedAtArr, quoteObj) => {
                            return quoteCreatedAtArr.concat(quoteObj.createdAt);
                          }, state.byCreatedAt)
                          .filter((elem, pos, arr) => {
                            return arr.indexOf(elem) === pos;
                          }).sort((a,b) => {
                            return new Date(b) - new Date(a);
                          });;
      return Object.assign({}, state, {
        isFetching : action.isFetching,
        quotes : newQuotes,
        byCreatedAt : newQuotesByCreatedAt,
        pageState : action.payload.pageState
      })
    case DELETE_QUOTE :
      return Object.assign({}, state, {
        isFetching : action.isFetching
      })
    case RECEIVE_DELETE_QUOTE :
      const cleanQuotes = state.quotes
                          .filter( quoteObj => quoteObj.createdAt !== action.payload.response.createdAt);
      const cleanQuotesByCreatedAt = state.byCreatedAt
                          .filter( id => id !== action.payload.response.createdAt);
      return Object.assign({}, state, {
        quotes : cleanQuotes,
        byCreatedAt : cleanQuotesByCreatedAt,
        isFetching : action.isFetching
      })
    case REJECT_POST_QUOTE:
    case REJECT_FETCH_QUOTES:
    case REJECT_DELETE_QUOTE:
    default :
      return state;
  }
}
