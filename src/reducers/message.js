import _ from 'lodash';
import {
  RECEIVE_POST_SHIPMENT_MESSAGE,RECEIVE_UPDATE_NOTIFICATIONS_SHIPMENT_MESSAGE,
  FETCH_SHIPMENT_MESSAGES_HEAD, RECEIVE_FETCH_SHIPMENT_MESSAGES_HEAD, REJECT_FETCH_SHIPMENT_MESSAGES_HEAD,
  FETCH_SHIPMENT_MESSAGES_BODY, RECEIVE_FETCH_SHIPMENT_MESSAGES_BODY, REJECT_FETCH_SHIPMENT_MESSAGES_BODY,
  RECEIVE_POST_UPDATE_SHIPMENT_DOCUMENT,RECEIVE_FETCH_SUBSCRIBERS_BY_SHIPMENT,
  RECEIVE_FETCH_UNREAD_NOTIFICATIONS,
} from '../actions';
import {
  RECEIVE_WEBSOCKET_JOINED_ROOM,
  RECEIVE_WEBSOCKET_NEW_SUBSCRIBER,
  UPDATE_SHIPMENT_MESSAGE_FROM_WEBSOCKET} from '../actions/websocket'

const initialState = {
  isFetchingMessageHead : {},
  isFetchingMessageBody : {},
  errorFetchingMessageHead : {},
  errorFetchingMessageBody : {},
  messageHead : {},
  messageBody : {},
  notifications : {},
  subscribersByChannel : {},
  subscribedChannels : []
}

export const message = (state=initialState, action) => {
  switch(action.type) {
    case RECEIVE_WEBSOCKET_JOINED_ROOM :
        return Object.assign({}, state, {
          subscribedChannels : state.subscribedChannels.concat(action.payload.join)
        })
    case RECEIVE_POST_UPDATE_SHIPMENT_DOCUMENT :
        return Object.assign({}, state, {
          messageHead : Object.assign({}, state.messageHead, {
            [action.payload.response.message.shipmentId] : (state.messageHead.hasOwnProperty(action.payload.response.message.shipmentId) ?
              [...state.messageHead[action.payload.response.message.shipmentId]].concat(action.payload.response.message.messageDateBucket)
              : [].concat(action.payload.response.message.messageDateBucket)).filter((elem, pos, arr) => {
              return arr.indexOf(elem) === pos;
            })
          }),
          messageBody : Object.assign({}, state.messageBody, {
            [`${action.payload.response.message.shipmentId}_${action.payload.response.message.messageDateBucket}`] : state.messageBody.hasOwnProperty(`${action.payload.response.message.shipmentId}_${action.payload.response.message.messageDateBucket}`) ?
              [...state.messageBody[`${action.payload.response.message.shipmentId}_${action.payload.response.message.messageDateBucket}`]].concat(action.payload.response.message)
              : [].concat(action.payload.response.message),
          })
        });
    case UPDATE_SHIPMENT_MESSAGE_FROM_WEBSOCKET :
        return Object.assign({}, state, {
          messageHead : Object.assign({}, state.messageHead, {
            [action.payload.response.message.shipmentId] : (state.messageHead.hasOwnProperty(action.payload.response.message.shipmentId)
              ? [...state.messageHead[action.payload.response.message.shipmentId]].concat(action.payload.response.message.messageDateBucket)
              : [].concat(action.payload.response.message.messageDateBucket)).filter((elem, pos, arr) => {
                return arr.indexOf(elem) === pos;
              })
          }),
          messageBody : Object.assign({}, state.messageBody, {
            [`${action.payload.response.message.shipmentId}_${action.payload.response.message.messageDateBucket}`] : state.messageBody.hasOwnProperty(`${action.payload.response.message.shipmentId}_${action.payload.response.message.messageDateBucket}`)
              ? [...state.messageBody[`${action.payload.response.message.shipmentId}_${action.payload.response.message.messageDateBucket}`]].concat(action.payload.response.message)
              : [].concat(action.payload.response.message)
          }),
          notifications : Object.assign({}, state.notifications, {
            [action.payload.response.notification.shipmentId] : state.notifications.hasOwnProperty(action.payload.response.notification.shipmentId)
            ? Object.assign({}, state.notifications[action.payload.response.notification.shipmentId], {
              items : Object.assign({}, state.notifications[action.payload.response.notification.shipmentId].items, {
                        [action.payload.response.notification.createdAt] : action.payload.response.notification
                      }),
              documentTypeLength : action.payload.type === 'document'
                ? state.notifications[action.payload.response.notification.shipmentId].documentTypeLength + 1
                : state.notifications[action.payload.response.notification.shipmentId].documentTypeLength,

              messageTypeLength : state.notifications[action.payload.response.notification.shipmentId].messageTypeLength + 1
            })
            : Object.assign({}, {
              items : {
                [action.payload.response.notification.createdAt] : action.payload.response.notification
              },
              documentTypeLength : action.payload.type === 'document'
              ? 1
              : 0,
              messageTypeLength : 1
            })
          })
        });
    case RECEIVE_POST_SHIPMENT_MESSAGE :
        return Object.assign({}, state, {
          messageHead : Object.assign({}, state.messageHead, {
            [action.payload.response.shipmentId] : (state.messageHead.hasOwnProperty(action.payload.response.shipmentId) ?
              [...state.messageHead[action.payload.response.shipmentId]].concat(action.payload.response.messageDateBucket) : [].concat(action.payload.response.messageDateBucket)).filter((elem, pos, arr) => {
                return arr.indexOf(elem) === pos;
              })
          }),
          messageBody : Object.assign({}, state.messageBody, {
            [`${action.payload.response.shipmentId}_${action.payload.response.messageDateBucket}`] : state.messageBody.hasOwnProperty(`${action.payload.response.shipmentId}_${action.payload.response.messageDateBucket}`) ?
              [...state.messageBody[`${action.payload.response.shipmentId}_${action.payload.response.messageDateBucket}`]].concat(action.payload.response)
              : [].concat(action.payload.response)
          }),
          notifications : state.notifications.hasOwnProperty(action.payload.response.shipmentId)
          ? Object.assign({}, state.notifications, {
            [action.payload.response.shipmentId] : Object.assign({}, state.notifications[action.payload.response.shipmentId], {
              items : _.reduce(state.notifications[action.payload.response.shipmentId].items, (items, item) => {
                items[item.createdAt] = Object.assign({}, item, {isRead : true})
                return items
              },{}),
            documentTypeLength : 0,
            messageTypeLength : 0,
            })
          })
          : Object.assign({}, state.notifications)
        });
    case RECEIVE_UPDATE_NOTIFICATIONS_SHIPMENT_MESSAGE :
      return Object.assign({}, state, {
        notifications : state.notifications.hasOwnProperty(action.payload.shipmentId)
        ? Object.assign({}, state.notifications, {
          [action.payload.shipmentId] : Object.assign({}, state.notifications[action.payload.shipmentId], {
            items : _.reduce(state.notifications[action.payload.shipmentId].items, (items, item) => {
              items[item.createdAt] = Object.assign({}, item, {isRead : true})
              return items
            },{}),
            documentTypeLength : 0,
            messageTypeLength : 0,
          })
        })
        : Object.assign({}, state.notifications)
      })
    case RECEIVE_WEBSOCKET_NEW_SUBSCRIBER :
      return Object.assign({}, state, {
        subscribersByChannel : Object.assign({},state.subscribersByChannel,{
          [action.payload.response.shipmentId] : state.subscribersByChannel.hasOwnProperty(action.payload.response.shipmentId)
           ? Object.assign({},state.subscribersByChannel[action.payload.response.shipmentId],{
             [action.payload.response.userId] : action.payload.response
           })
           : Object.assign({},{
             [action.payload.response.userId] : action.payload.response
           })
        })
      })
    case RECEIVE_FETCH_SUBSCRIBERS_BY_SHIPMENT :
      return Object.assign({}, state, {
        subscribersByChannel : Object.assign({}, state.subscribersByChannel, {
          [action.payload.shipmentId] : _.mapKeys(action.payload.response, 'userId')
        })
      })
    case FETCH_SHIPMENT_MESSAGES_HEAD :
      return Object.assign({}, state, {
        isFetchingMessageHead : Object.assign({}, state.isFetchingMessageHead,{
          [action.payload.shipmentId] : action.isFetching
        }),
        errorFetchingMessageHead : Object.assign({}, state.errorFetchingMessageHead, {
            [action.payload.shipmentId] : ''
        })
      });
    case RECEIVE_FETCH_SHIPMENT_MESSAGES_HEAD :
      return Object.assign({}, state, {
        isFetchingMessageHead : Object.assign({}, state.isFetchingMessageHead,{
          [action.payload.shipmentId] : action.isFetching
          }),
        messageHead : Object.assign({}, state.messageHead, {
            [action.payload.shipmentId] : _.reduce( action.payload.response, (messageDateBucketsByShipmentId, messageHead) => {
              return messageDateBucketsByShipmentId.concat(messageHead.messageDateBucket);
              },[]).filter((elem, pos, arr) => {
                return arr.indexOf(elem) === pos;
              })
          })
      });
    case REJECT_FETCH_SHIPMENT_MESSAGES_HEAD :
    return Object.assign({}, state, {
      isFetchingMessageHead : Object.assign({}, state.isFetchingMessageHead, {
          [action.payload.shipmentId] : action.isFetching
      }),
      errorFetchingMessageHead : Object.assign({}, state.errorFetchingMessageHead, {
          [action.payload.shipmentId] : action.payload.xhr.response !== null ? action.payload.xhr.response.message : action.payload.xhr.response
      })
    });
    case FETCH_SHIPMENT_MESSAGES_BODY :
      return Object.assign({}, state, {
        isFetchingMessageBody : Object.assign({}, state.isFetchingMessageBody, {
          [`${action.payload.shipmentId}_${action.payload.messageDateBucket}`] : action.isFetching
        }),
        errorFetchingMessageBody : Object.assign({}, state.errorFetchingMessageBody, {
          [`${action.payload.shipmentId}_${action.payload.messageDateBucket}`] : ''
        })
      });
    case RECEIVE_FETCH_SHIPMENT_MESSAGES_BODY :
      return Object.assign({}, state, {
        isFetchingMessageBody : Object.assign({}, state.isFetchingMessageBody, {
          [`${action.payload.shipmentId}_${action.payload.messageDateBucket}`] : action.isFetching
        }),
        messageBody : Object.assign({}, state.messageBody, {
          [`${action.payload.shipmentId}_${action.payload.messageDateBucket}`] : action.payload.response
        })
      });
    case REJECT_FETCH_SHIPMENT_MESSAGES_BODY :
      return Object.assign({}, state, {
        isFetchingMessageBody : Object.assign({}, state.isFetchingMessageBody, {
          [`${action.payload.shipmentId}_${action.payload.messageDateBucket}`] : action.isFetching
        }),
        errorFetchingMessageBody : Object.assign({}, state.errorFetchingMessageBody, {
          [`${action.payload.shipmentId}_${action.payload.messageDateBucket}`] : action.payload.xhr.response !== null ? action.payload.xhr.response.message : action.payload.xhr.response
        })
      });
    case RECEIVE_FETCH_UNREAD_NOTIFICATIONS :
      const notifications = action.payload.response.reduce( (notifications, notification) => {
        const {shipmentId, createdAt, topic} = notification
        notifications[shipmentId] = notifications.hasOwnProperty(shipmentId)
        ? Object.assign({},notifications[shipmentId],{
            items : Object.assign({}, notifications[shipmentId].items, {
              [createdAt] : notification
            }),
            messageTypeLength : notifications[shipmentId].messageTypeLength + 1,
            documentTypeLength : topic === 'NEW_DOCUMENT'
                                  ? notifications[shipmentId].documentTypeLength + 1
                                  : notifications[shipmentId].documentTypeLength
          })
        : Object.assign({},{
            items : {
              [createdAt] : notification
            },
            messageTypeLength : 1,
            documentTypeLength : topic === 'NEW_DOCUMENT' ? 1 : 0
          })
        return notifications
      },{})
      return Object.assign({}, state, {
        notifications : Object.assign({}, state.notifications, notifications)
      })
    default :
      return state
  }
}
