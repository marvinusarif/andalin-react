import _ from 'lodash';
import { OPEN_LOGIN_MODAL, CLOSE_LOGIN_MODAL,
         OPEN_INVITATION_MODAL, CLOSE_INVITATION_MODAL,
         OPEN_LOGOUT_MODAL, CLOSE_LOGOUT_MODAL,
         OPEN_UPLOADFILE_MODAL, CLOSE_UPLOADFILE_MODAL,
         OPEN_RESET_PASSWORD_MODAL, CLOSE_RESET_PASSWORD_MODAL,
         RECEIVE_POST_USER_LOGIN, REJECT_POST_USER_LOGIN,
         OPEN_REGISTER_MODAL, CLOSE_REGISTER_MODAL,
         OPEN_CONTACT_MODAL, CLOSE_CONTACT_MODAL,
         OPEN_UPDATE_SHIPPER_CONSIGNEE_MODAL, CLOSE_UPDATE_SHIPPER_CONSIGNEE_MODAL,
         OPEN_UPDATE_ORDER_REQUEST_MODAL, CLOSE_UPDATE_ORDER_REQUEST_MODAL,
         OPEN_UPDATE_COMMODITIES_MODAL, CLOSE_UPDATE_COMMODITIES_MODAL,
         OPEN_UPDATE_CARRIER_MODAL, CLOSE_UPDATE_CARRIER_MODAL,
         OPEN_UPDATE_ORDER_STAT_MODAL, CLOSE_UPDATE_ORDER_STAT_MODAL,
         OPEN_UPDATE_FREIGHT_MODAL, CLOSE_UPDATE_FREIGHT_MODAL,
         OPEN_UPDATE_ORDER_PRICE_MODAL, CLOSE_UPDATE_ORDER_PRICE_MODAL,
         OPEN_UPLOADFILE_SHIPMENT_DOC_MODAL, CLOSE_UPLOADFILE_SHIPMENT_DOC_MODAL,
         POST_USER_REGISTER, RECEIVE_POST_USER_REGISTER, REJECT_POST_USER_REGISTER,
         RESET_VERIFICATION_EMAIL_TIMER, REDUCE_VERIFICATION_EMAIL_TIMER,
         FETCH_ALL_COUNTRIES, RECEIVE_ALL_COUNTRIES,
         POST_VERIFICATION_EMAIL, RECEIVE_POST_VERIFICATION_EMAIL, REJECT_POST_VERIFICATION_EMAIL,
         POST_CONTACT, RECEIVE_POST_CONTACT, REJECT_POST_CONTACT,
         VERIFY_USER, RECEIVE_VERIFY_USER, REJECT_VERIFY_USER,
         RESET_PASSWORD_MAIL, RECEIVE_RESET_PASSWORD_MAIL, REJECT_RESET_PASSWORD_MAIL,
         RESET_PASSWORD, RECEIVE_RESET_PASSWORD, REJECT_RESET_PASSWORD,
         POST_COMPANY_USERS_INVITE_MAIL, RECEIVE_POST_COMPANY_USERS_INVITE_MAIL, REJECT_POST_COMPANY_USERS_INVITE_MAIL,
         POST_COMPANY_USERS_JOIN, RECEIVE_POST_COMPANY_USERS_JOIN, REJECT_POST_COMPANY_USERS_JOIN,
         UPDATE_COMPANY_DOCS, RECEIVE_UPDATE_COMPANY_DOCS, REJECT_UPDATE_COMPANY_DOCS, UPDATE_PROGRESS_BAR_UPLOAD_COMPANY_DOCS,
         UPDATE_USER_PHOTO, RECEIVE_UPDATE_USER_PHOTO, REJECT_UPDATE_USER_PHOTO,
         UPDATE_SHIPMENT_SHIPPER_CONSIGNEE, RECEIVE_UPDATE_SHIPMENT_SHIPPER_CONSIGNEE, REJECT_UPDATE_SHIPMENT_SHIPPER_CONSIGNEE,
         UPDATE_SHIPMENT_ORDER_STAT, RECEIVE_UPDATE_SHIPMENT_ORDER_STAT, REJECT_UPDATE_SHIPMENT_ORDER_STAT,
         UPDATE_SHIPMENT_COMMODITIES, RECEIVE_UPDATE_SHIPMENT_COMMODITIES, REJECT_UPDATE_SHIPMENT_COMMODITIES,
         UPDATE_SHIPMENT_CARRIER, RECEIVE_UPDATE_SHIPMENT_CARRIER, REJECT_UPDATE_SHIPMENT_CARRIER,
         UPDATE_SHIPMENT_FREIGHT, RECEIVE_UPDATE_SHIPMENT_FREIGHT, REJECT_UPDATE_SHIPMENT_FREIGHT,
         UPDATE_SHIPMENT_ORDER_REQUEST, RECEIVE_UPDATE_SHIPMENT_ORDER_REQUEST, REJECT_UPDATE_SHIPMENT_ORDER_REQUEST,
         UPDATE_SHIPMENT_ORDER_PRICE, RECEIVE_UPDATE_SHIPMENT_ORDER_PRICE, REJECT_UPDATE_SHIPMENT_ORDER_PRICE,
         POST_UPDATE_SHIPMENT_DOCUMENT, RECEIVE_POST_UPDATE_SHIPMENT_DOCUMENT, REJECT_POST_UPDATE_SHIPMENT_DOCUMENT, UPDATE_PROGRESS_BAR_UPLOAD_SHIPMENT_DOCS,
         FETCH_SHIPMENT_BANK_ACC, RECEIVE_FETCH_SHIPMENT_BANK_ACC, REJECT_FETCH_SHIPMENT_BANK_ACC,
         POST_SHIPMENT_MESSAGE, RECEIVE_POST_SHIPMENT_MESSAGE, REJECT_POST_SHIPMENT_MESSAGE,

       } from '../actions';
const initialState = {
  modal : {
    login : {
      isOpen : false,
      error : ''
    },
    invite : {
      isOpen : false,
      isFetching : false,
      isSuccessful : false
    },
    register : {
      isOpen : false,
      isFetching : false,
      isSuccessful : false,
      user : {
        userId : null,
        companyId : null,
        userEmail : null,
        userFullname : null
      },
      mail : {
        isFetching : false,
        emailSent : 3,
        timer : null
      },
      form : {},
      formValue : {
        countries : [],
        isFetching : false
      }
    },
    contact : {
      isOpen : false,
      isFetching : false,
      isSuccessful : false,
      message : ''
    },
    forgotPassword : {
      isOpen : false,
      isFetching : false,
      isSuccessful : false,
      message : ''
    },
    logout : {
      isOpen : false
    },
    uploadFile : {
      isOpen : false,
      isFetching : false,
      isSuccessful : false,
      percentCompleted : 0,
      message : ''
    },
    shipmentDetailShipperConsignee : {
      shipmentId : null,
      isOpen : false,
      isFetching : false,
      isSuccessful : false,
      message : ''
    },
    shipmentDetailRequest : {
      shipmentId : null,
      isOpen : false,
      isFetching : false,
      isSuccessful : false,
      message : ''
    },
    shipmentDetailCommodities : {
      shipmentId : null,
      isOpen : false,
      isFetching : false,
      isSuccessful : false,
      message : ''
    },
    shipmentDetailOrderStat : {
      shipmentId : null,
      isOpen : false,
      isFetching : false,
      isSuccessful : false,
      message : '',
      bankAcc : []
    },
    shipmentDetailCarrier : {
      shipmentId : null,
      isOpen : false,
      isFetching : false,
      isSuccessful : false,
      message : ''
    },
    shipmentDetailFreight : {
      shipmentId : null,
      isOpen : false,
      isFetching : false,
      isSuccessful : false,
      message : ''
    },
    shipmentDetailPrice : {
      shipmentId : null,
      isOpen : false,
      isFetching : false,
      isSuccessful : false,
      message : ''
    },
    shipmentDocumentUploadFile : {
      shipmentId : null,
      isOpen : false,
      isFetching : false,
      isSuccessful : false,
      percentCompleted : 0,
      message : ''
    }
  },
  verify : {
    isFetching : false,
    isSuccessful : false,
    message : ''
  },
  invitation : {
    isFetching : false,
    isSuccessful : false,
    message : ''
  },
  uploadPhoto : {
    isFetching : false,
    isSuccessful : false,
    message : ''
  },
  postShipmentMessage : {
    isFetching : false,
    message : ''
  }
}
export const page = (state=initialState,action) => {
  switch(action.type) {
    case OPEN_LOGIN_MODAL :
    case CLOSE_LOGIN_MODAL :
      return Object.assign({}, state, {
        modal : Object.assign({}, state.modal, {
            login : {
              ...state.modal.login,
              isOpen : action.isOpen,
              error : ''
            }
        })
      });
    case OPEN_INVITATION_MODAL :
    case CLOSE_INVITATION_MODAL :
      return Object.assign({}, state, {
        modal : Object.assign({}, state.modal, {
            invite : {
              ...state.modal.invite,
              isOpen : action.isOpen,
              isFetching : action.isFetching,
              isSuccessful : action.isSuccessful
            }
        })
      });
    case OPEN_UPLOADFILE_MODAL :
    case CLOSE_UPLOADFILE_MODAL :
      return Object.assign({}, state, {
        modal : Object.assign({}, state.modal, {
            uploadFile : {
              ...state.modal.uploadFile,
              isOpen : action.isOpen,
              isFetching : action.isFetching,
              isSuccessful : action.isSuccessful,
              message : ''
            }
        })
      });
    case OPEN_UPLOADFILE_SHIPMENT_DOC_MODAL :
    case CLOSE_UPLOADFILE_SHIPMENT_DOC_MODAL :
        return Object.assign({}, state, {
          modal : Object.assign({}, state.modal, {
              shipmentDocumentUploadFile : {
                ...state.modal.uploadFile,
                shipmentId : action.payload.shipmentId,
                isOpen : action.isOpen,
                isFetching : action.isFetching,
                isSuccessful : action.isSuccessful,
                message : ''
              }
          })
        });
    case OPEN_LOGOUT_MODAL :
    case CLOSE_LOGOUT_MODAL :
      return Object.assign({}, state, {
        modal : Object.assign({}, state.modal, {
          logout : {
            isOpen : action.isOpen
          }
        })
      });
      case OPEN_RESET_PASSWORD_MODAL :
      case CLOSE_RESET_PASSWORD_MODAL :
        return Object.assign({}, state, {
          modal : Object.assign({}, state.modal, {
              forgotPassword : {
                ...state.modal.forgotPassword,
                isOpen : action.isOpen,
                isFetching : action.isFetching,
                isSuccessful : action.isSuccessful,
                message : ''
              }
          })
        });
      case OPEN_REGISTER_MODAL :
      case CLOSE_REGISTER_MODAL :
        return Object.assign({}, state, {
          modal : Object.assign({}, state.modal, {
              register : {
                ...state.modal.register,
                isOpen : action.isOpen
              }
          })
        });
      case OPEN_CONTACT_MODAL :
      case CLOSE_CONTACT_MODAL :
        return Object.assign({}, state, {
          modal : Object.assign({}, state.modal, {
              contact : {
              ...state.modal.contact,
              isOpen : action.isOpen,
              isSuccessful : action.isSuccessful
              }
          })
        });
    case RECEIVE_POST_USER_LOGIN :
      return Object.assign({}, state, {
        modal : Object.assign({}, state.modal, {
            login : {
              ...state.modal.login,
              isOpen : action.isOpen,
              error : ''
            }
        })
      });
    case REJECT_POST_USER_LOGIN :
      return Object.assign({}, state, {
        modal : Object.assign({}, state.modal, {
          login : {
            ...state.modal.login,
            error : action.payload.xhr.response !== null ? action.payload.xhr.response.message : action.payload.xhr.response
          }
        })
      })
    case POST_USER_REGISTER :
      return Object.assign({}, state, {
        modal : Object.assign({}, state.modal, {
          register : {
            ...state.modal.register,
            isFetching : action.isFetching,
            isSuccessful : action.isSuccessful,
            form : action.payload.form
          }
        })
      })
    case RECEIVE_POST_USER_REGISTER :
      return Object.assign({}, state, {
        modal : Object.assign({}, state.modal, {
          register : {
            ...state.modal.register,
            isFetching : action.isFetching,
            isSuccessful : action.isSuccessful,
            user : {
              userId : action.payload.response.userId,
              userEmail : action.payload.response.userEmail,
              companyId : action.payload.response.companyId,
              userFullname : action.payload.response.userFullname
            },
            mail : {
              ...state.modal.register.mail,
              emailSent : state.modal.register.mail.emailSent - 1
            }
          }
        })
      })
    case REJECT_POST_USER_REGISTER :
      return Object.assign({}, state, {
        modal : Object.assign({}, state.modal, {
          register : {
            ...state.modal.register,
            isFetching : action.isFetching,
            isSuccessful : action.isSuccessful
          }
        })
      });
    case POST_VERIFICATION_EMAIL :
    case RECEIVE_POST_VERIFICATION_EMAIL :
    case REJECT_POST_VERIFICATION_EMAIL :
      return Object.assign({}, state, {
        modal : Object.assign({}, state.modal, {
          register : {
            ...state.modal.register,
            mail : {
              ...state.modal.register.mail,
              isFetching : action.isFetching,
              emailSent : action.payload.response ? state.modal.register.mail.emailSent - 1 : state.modal.register.mail.emailSent
            }
          }
        })
      });
    case RESET_VERIFICATION_EMAIL_TIMER :
      return Object.assign({}, state, {
        modal : Object.assign({}, state.modal, {
          register : {
            ...state.modal.register,
            mail : {
              ...state.modal.register.mail,
              timer : action.timer
            }
          }
        })
      });
    case REDUCE_VERIFICATION_EMAIL_TIMER :
      return Object.assign({}, state, {
        modal : Object.assign({}, state.modal, {
          register : {
            ...state.modal.register,
            mail : {
              ...state.modal.register.mail,
              timer : state.modal.register.mail.timer - 1
            }
          }
        })
      });
    case FETCH_ALL_COUNTRIES :
      return Object.assign({}, state, {
        modal : Object.assign({}, state.modal, {
            register : {
              ...state.modal.register,
              formValue : {
                ...state.modal.register.formValue,
                isFetching : action.isFetching
              }
            }
        })
      });
    case RECEIVE_ALL_COUNTRIES :
      const countries = action.payload.response.reduce( (countries, country) => {
        const { code, name } = country;
        return countries.concat({
          value : code,
          label : name
        });
      }, [])
      return Object.assign({}, state, {
        modal : Object.assign({}, state.modal, {
            register : {
              ...state.modal.register,
              formValue : {
                countries,
                isFetching : action.isFetching
              }
            }
        })
      });
    case POST_CONTACT :
      return Object.assign({}, state, {
        modal : Object.assign({}, state.modal, {
          contact : {
            ...state.modal.contact,
            isFetching : action.isFetching,
            message : ''
          }
        })
      });
    case RECEIVE_POST_CONTACT :
      return Object.assign({}, state, {
        modal : Object.assign({}, state.modal, {
          contact : {
            ...state.modal.contact,
            isFetching : action.isFetching,
            isSuccessful : action.isSuccessful
          }
        })
      });
    case REJECT_POST_CONTACT :
      return Object.assign({}, state, {
        modal : Object.assign({}, state.modal, {
          contact : {
            ...state.modal.contact,
            isFetching : action.isFetching,
            isSuccessful : action.isSuccessful,
            message : action.payload.xhr.response !== null ? action.payload.xhr.response.message : action.payload.xhr.response
          }
        })
      });
    case VERIFY_USER :
    case RECEIVE_VERIFY_USER :
      return Object.assign({}, state, {
        verify : {
          isFetching : action.isFetching,
          isSuccessful : action.isSuccessful,
          message : ''
        }
      });
    case REJECT_VERIFY_USER :
      return Object.assign({}, state, {
        verify : {
          isFetching : action.isFetching,
          isSuccessful : action.isSuccessful,
          message : action.payload.xhr.response !== null ? action.payload.xhr.response.message : action.payload.xhr.response
        }
      })
    case RESET_PASSWORD :
    case RECEIVE_RESET_PASSWORD :
    case RESET_PASSWORD_MAIL :
    case RECEIVE_RESET_PASSWORD_MAIL :
      return Object.assign({}, state, {
        modal : Object.assign({}, state.modal, {
            forgotPassword : {
              ...state.modal.forgotPassword,
              isSuccessful : action.isSuccessful,
              isFetching : action.isFetching,
              message : ''
            }
        })
      });
    case REJECT_RESET_PASSWORD :
    case REJECT_RESET_PASSWORD_MAIL :
      return Object.assign({}, state, {
        modal : Object.assign({}, state.modal, {
            forgotPassword : {
              ...state.modal.forgotPassword,
              isSuccessful : action.isSuccessful,
              message : action.payload.xhr.response !== null ? action.payload.xhr.response.message : action.payload.xhr.response
            }
        })
      });
    case POST_COMPANY_USERS_INVITE_MAIL :
    case RECEIVE_POST_COMPANY_USERS_INVITE_MAIL :
      return Object.assign({}, state, {
        modal : Object.assign({}, state.modal, {
          invite : {
            ...state.modal.invite,
            isSuccessful : action.isSuccessful,
            isFetching  : action.isFetching,
            message : ''
          }
        })
      });
    case REJECT_POST_COMPANY_USERS_INVITE_MAIL :
      return Object.assign({}, state, {
        modal : Object.assign({}, state.modal, {
          invite : {
            ...state.modal.invite,
            isSuccessful : action.isSuccessful,
            isFetching  : action.isFetching,
            message : action.payload.xhr.response !== null ? action.payload.xhr.response.message : action.payload.xhr.response
          }
        })
      });
    case POST_COMPANY_USERS_JOIN :
    case RECEIVE_POST_COMPANY_USERS_JOIN :
      return Object.assign({}, state, {
        invitation : {
          isFetching : action.isFetching,
          isSuccessful : action.isSuccessful,
          message : ''
        }
      });
    case REJECT_POST_COMPANY_USERS_JOIN :
      return Object.assign({}, state, {
        invitation : {
          isFetching : action.isFetching,
          isSuccessful : action.isSuccessful,
          message : action.payload.xhr.response !== null ? action.payload.xhr.response.message : action.payload.xhr.response
        }
      });

    case UPDATE_COMPANY_DOCS :
    case RECEIVE_UPDATE_COMPANY_DOCS :
      return Object.assign({}, state, {
        modal : Object.assign({}, state.modal, {
          uploadFile : {
              ...state.modal.uploadFile,
              isFetching : action.isFetching,
              isSuccessful : action.isSuccessful,
              percentCompleted : 0,
              message : ''
            }
        })
      });
    case UPDATE_PROGRESS_BAR_UPLOAD_COMPANY_DOCS :
      return Object.assign({}, state, {
          modal : Object.assign({}, state.modal, {
            uploadFile : {
                ...state.modal.uploadFile,
                percentCompleted : action.payload.percentCompleted
              }
          })
        });
    case REJECT_UPDATE_COMPANY_DOCS :
      return Object.assign({}, state, {
        modal : Object.assign({}, state.modal, {
          uploadFile : {
              ...state.modal.uploadFile,
              isFetching : action.isFetching,
              isSuccessful : action.isSuccessful,
              message : action.payload.xhr.response !== null ? action.payload.xhr.response.message : action.payload.xhr.response
            }
        })
      });
    case UPDATE_USER_PHOTO :
    case RECEIVE_UPDATE_USER_PHOTO :
      return Object.assign({}, state, {
        uploadPhoto : {
          isFetching : action.isFetching,
          isSuccessful : action.isSuccessful,
          message : ''
        }
      });
    case REJECT_UPDATE_USER_PHOTO :
      return Object.assign({}, state, {
        uploadPhoto : {
          isFetching : action.isFetching,
          isSuccessful : action.isSuccessful,
          message : action.payload.xhr.response !== null ? action.payload.xhr.response.message : action.payload.xhr.response
        }
      })
    case CLOSE_UPDATE_SHIPPER_CONSIGNEE_MODAL :
    case OPEN_UPDATE_SHIPPER_CONSIGNEE_MODAL :
    return Object.assign({}, state, {
      modal : Object.assign({}, state.modal, {
        shipmentDetailShipperConsignee : Object.assign({}, state.modal.shipmentDetailShipperConsignee, {
          shipmentId : action.payload.shipmentId,
          isOpen : action.isOpen,
          isFetching : action.isFetching,
          isSuccessful : false,
          message : ''
        })
      })
    });
    case REJECT_UPDATE_SHIPMENT_SHIPPER_CONSIGNEE :
      return Object.assign({}, state, {
        modal : Object.assign({}, state.modal, {
          shipmentDetailShipperConsignee : Object.assign({}, state.modal.shipmentDetailShipperConsignee, {
            shipmentId : action.payload.shipmentId,
            isFetching : action.isFetching,
            isSuccessful : action.isSuccessful,
            message : action.payload.xhr.response !== null ? action.payload.xhr.response.message : action.payload.xhr.response
          })
        })
      });
    case UPDATE_SHIPMENT_SHIPPER_CONSIGNEE :
    case RECEIVE_UPDATE_SHIPMENT_SHIPPER_CONSIGNEE :
      return Object.assign({}, state, {
        modal : Object.assign({}, state.modal, {
          shipmentDetailShipperConsignee : Object.assign({}, state.modal.shipmentDetailShipperConsignee, {
            shipmentId : action.payload.shipmentId,
            isFetching : action.isFetching,
            isSuccessful : action.isSuccessful,
            message : ''
          })
        })
      });

    case OPEN_UPDATE_ORDER_REQUEST_MODAL :
    case CLOSE_UPDATE_ORDER_REQUEST_MODAL :
      return Object.assign({}, state, {
        modal : Object.assign({}, state.modal, {
          shipmentDetailRequest : Object.assign({}, state.modal.shipmentDetailRequest, {
            shipmentId : action.payload.shipmentId,
            isOpen : action.isOpen,
            isFetching : action.isFetching,
            isSuccessful : false,
            message : ''
          })
        })
      });
    case REJECT_UPDATE_SHIPMENT_ORDER_REQUEST :
      return Object.assign({}, state, {
        modal : Object.assign({}, state.modal, {
          shipmentDetailRequest : Object.assign({}, state.modal.shipmentDetailRequest, {
            shipmentId : action.payload.shipmentId,
            isFetching : action.isFetching,
            isSuccessful : action.isSuccessful,
            message : action.payload.xhr.response !== null ? action.payload.xhr.response.message : action.payload.xhr.response
          })
        })
      });
    case UPDATE_SHIPMENT_ORDER_REQUEST :
    case RECEIVE_UPDATE_SHIPMENT_ORDER_REQUEST :
      return Object.assign({}, state, {
        modal : Object.assign({}, state.modal, {
          shipmentDetailRequest : Object.assign({}, state.modal.shipmentDetailRequest, {
            shipmentId : action.payload.shipmentId,
            isFetching : action.isFetching,
            isSuccessful : action.isSuccessful,
            message : ''
          })
        })
      });
    case CLOSE_UPDATE_COMMODITIES_MODAL :
    case OPEN_UPDATE_COMMODITIES_MODAL :
      return Object.assign({}, state, {
        modal : Object.assign({}, state.modal, {
          shipmentDetailCommodities : Object.assign({}, state.modal.shipmentDetailCommodities, {
            shipmentId : action.payload.shipmentId,
            isOpen : action.isOpen,
            isFetching : action.isFetching,
            isSuccessful : false,
            message : ''
          })
        })
      });
    case REJECT_UPDATE_SHIPMENT_COMMODITIES :
      return Object.assign({}, state, {
        modal : Object.assign({}, state.modal, {
          shipmentDetailCommodities : Object.assign({}, state.modal.shipmentDetailCommodities, {
            shipmentId : action.payload.shipmentId,
            isFetching : action.isFetching,
            isSuccessful : action.isSuccessful,
            message : action.payload.xhr.response !== null ? action.payload.xhr.response.message : action.payload.xhr.response
          })
        })
      });
    case UPDATE_SHIPMENT_COMMODITIES :
    case RECEIVE_UPDATE_SHIPMENT_COMMODITIES :
      return Object.assign({}, state, {
        modal : Object.assign({}, state.modal, {
          shipmentDetailCommodities : Object.assign({}, state.modal.shipmentDetailCommodities, {
            shipmentId : action.payload.shipmentId,
            isFetching : action.isFetching,
            isSuccessful : action.isSuccessful,
            message : ''
          })
        })
      });
    case CLOSE_UPDATE_CARRIER_MODAL :
    case OPEN_UPDATE_CARRIER_MODAL :
      return Object.assign({}, state, {
        modal : Object.assign({}, state.modal, {
          shipmentDetailCarrier : Object.assign({}, state.modal.shipmentDetailCarrier, {
            shipmentId : action.payload.shipmentId,
            isOpen : action.isOpen,
            isFetching : action.isFetching,
            isSuccessful : false,
            message : ''
          })
        })
      });
    case REJECT_UPDATE_SHIPMENT_CARRIER :
    return Object.assign({}, state, {
      modal : Object.assign({}, state.modal, {
        shipmentDetailCarrier : Object.assign({}, state.modal.shipmentDetailCarrier, {
          shipmentId : action.payload.shipmentId,
          isFetching : action.isFetching,
          isSuccessful : action.isSuccessful,
          message : action.payload.xhr.response !== null ? action.payload.xhr.response.message : action.payload.xhr.response
        })
      })
    });
    case UPDATE_SHIPMENT_CARRIER :
    case RECEIVE_UPDATE_SHIPMENT_CARRIER :
      return Object.assign({}, state, {
        modal : Object.assign({}, state.modal, {
          shipmentDetailCarrier : Object.assign({}, state.modal.shipmentDetailCarrier, {
            shipmentId : action.payload.shipmentId,
            isFetching : action.isFetching,
            isSuccessful : action.isSuccessful,
            message : ''
          })
        })
      });
    case OPEN_UPDATE_ORDER_STAT_MODAL :
    case CLOSE_UPDATE_ORDER_STAT_MODAL :
      return Object.assign({}, state, {
        modal : Object.assign({}, state.modal, {
          shipmentDetailOrderStat : Object.assign({}, state.modal.shipmentDetailOrderStat, {
            shipmentId : action.payload.shipmentId,
            isOpen : action.isOpen,
            isFetching : action.isFetching,
            isSuccessful : false,
            message : ''
          })
        })
      });
    case FETCH_SHIPMENT_BANK_ACC :
      return Object.assign({}, state, {
        modal : Object.assign({}, state.modal, {
          shipmentDetailOrderStat : Object.assign({}, state.modal.shipmentDetailOrderStat, {
            isFetching : action.isFetching
          })
        })
      });
    case RECEIVE_FETCH_SHIPMENT_BANK_ACC :
      const bankAcc = _.reduce(action.payload.response, (bankAcc, bank) => {
        return bankAcc.concat({
          label : `${bank.bank} - ${bank.name} - ${bank.number}`,
          value : `${bank.bankId}`
        })
      },[]);
      return Object.assign({}, state, {
        modal : Object.assign({}, state.modal, {
          shipmentDetailOrderStat : Object.assign({}, state.modal.shipmentDetailOrderStat, {
            bankAcc : bankAcc,
            isFetching : action.isFetching
          })
        })
      });
    case REJECT_FETCH_SHIPMENT_BANK_ACC :
      return Object.assign({}, state, {
        modal : Object.assign({}, state.modal, {
          shipmentDetailOrderStat : Object.assign({}, state.modal.shipmentDetailOrderStat, {
            isFetching : action.isFetching,
            message : action.payload.xhr.response !== null ? action.payload.xhr.response.message : action.payload.xhr.response
          })
        })
      });
    case REJECT_UPDATE_SHIPMENT_ORDER_STAT :
      return Object.assign({}, state, {
        modal : Object.assign({}, state.modal, {
          shipmentDetailOrderStat : Object.assign({}, state.modal.shipmentDetailOrderStat, {
            shipmentId : action.payload.shipmentId,
            isFetching : action.isFetching,
            isSuccessful : action.isSuccessful,
            message : action.payload.xhr.response !== null ? action.payload.xhr.response.message : action.payload.xhr.response
          })
        })
      });
    case UPDATE_SHIPMENT_ORDER_STAT :
    case RECEIVE_UPDATE_SHIPMENT_ORDER_STAT :
    return Object.assign({}, state, {
      modal : Object.assign({}, state.modal, {
        shipmentDetailOrderStat : Object.assign({}, state.modal.shipmentDetailOrderStat, {
          shipmentId : action.payload.shipmentId,
          isFetching : action.isFetching,
          isSuccessful : action.isSuccessful,
          message : ''
        })
      })
    });
    case OPEN_UPDATE_FREIGHT_MODAL :
    case CLOSE_UPDATE_FREIGHT_MODAL :
      return Object.assign({}, state, {
        modal : Object.assign({}, state.modal, {
          shipmentDetailFreight : Object.assign({}, state.modal.shipmentDetailFreight, {
            shipmentId : action.payload.shipmentId,
            isOpen : action.isOpen,
            isFetching : action.isFetching,
            isSuccessful : false,
            message : ''
          })
        })
      });
    case REJECT_UPDATE_SHIPMENT_FREIGHT :
      return Object.assign({}, state, {
        modal : Object.assign({}, state.modal, {
          shipmentDetailFreight : Object.assign({}, state.modal.shipmentDetailFreight, {
            shipmentId : action.payload.shipmentId,
            isFetching : action.isFetching,
            isSuccessful : action.isSuccessful,
            message : action.payload.xhr.response !== null ? action.payload.xhr.response.message : action.payload.xhr.response
          })
        })
      });
    case UPDATE_SHIPMENT_FREIGHT :
    case RECEIVE_UPDATE_SHIPMENT_FREIGHT :
      return Object.assign({}, state, {
        modal : Object.assign({}, state.modal, {
          shipmentDetailFreight : Object.assign({}, state.modal.shipmentDetailFreight, {
            shipmentId : action.payload.shipmentId,
            isFetching : action.isFetching,
            isSuccessful : action.isSuccessful,
            message : ''
          })
        })
      });

    case OPEN_UPDATE_ORDER_PRICE_MODAL :
    case CLOSE_UPDATE_ORDER_PRICE_MODAL :
      return Object.assign({}, state, {
        modal : Object.assign({}, state.modal, {
          shipmentDetailPrice : Object.assign({}, state.modal.shipmentDetailPrice, {
            shipmentId : action.payload.shipmentId,
            isOpen : action.isOpen,
            isFetching : action.isFetching,
            isSuccessful : false,
            message : ''
          })
        })
      });
    case UPDATE_SHIPMENT_ORDER_PRICE :
    case RECEIVE_UPDATE_SHIPMENT_ORDER_PRICE :
    return Object.assign({}, state, {
      modal : Object.assign({}, state.modal, {
        shipmentDetailPrice : Object.assign({}, state.modal.shipmentDetailPrice, {
          shipmentId : action.payload.shipmentId,
          isFetching : action.isFetching,
          isSuccessful : action.isSuccessful,
          message : ''
        })
      })
    });
    case REJECT_UPDATE_SHIPMENT_ORDER_PRICE :
      return Object.assign({}, state, {
        modal : Object.assign({}, state.modal, {
          shipmentDetailPrice : Object.assign({}, state.modal.shipmentDetailPrice, {
            shipmentId : action.payload.shipmentId,
            isFetching : action.isFetching,
            isSuccessful : action.isSuccessful,
            message : action.payload.xhr.response !== null ? action.payload.xhr.response.message : action.payload.xhr.response
          })
        })
      });
    case RECEIVE_POST_UPDATE_SHIPMENT_DOCUMENT :
    case POST_UPDATE_SHIPMENT_DOCUMENT :
    return Object.assign({}, state, {
      modal : Object.assign({}, state.modal, {
        shipmentDocumentUploadFile : Object.assign({}, state.modal.shipmentDocumentUploadFile, {
          shipmentId : action.payload.shipmentId,
          isFetching : action.isFetching,
          isSuccessful : action.isSuccessful,
          message : ''
        })
      })
    });
    case REJECT_POST_UPDATE_SHIPMENT_DOCUMENT :
      return Object.assign({}, state, {
        modal : Object.assign({}, state.modal, {
          shipmentDocumentUploadFile : Object.assign({}, state.modal.shipmentDocumentUploadFile, {
            shipmentId : action.payload.shipmentId,
            isFetching : action.isFetching,
            isSuccessful : action.isSuccessful,
            message : action.payload.xhr.response !== null ? action.payload.xhr.response.message : action.payload.xhr.response
          })
        })
      });
    case UPDATE_PROGRESS_BAR_UPLOAD_SHIPMENT_DOCS :
        return Object.assign({}, state, {
            modal : Object.assign({}, state.modal, {
              shipmentDocumentUploadFile : {
                  ...state.modal.shipmentDocumentUploadFile,
                  percentCompleted : action.payload.percentCompleted
                }
            })
          });
    case POST_SHIPMENT_MESSAGE :
    case RECEIVE_POST_SHIPMENT_MESSAGE :
      return Object.assign({}, state, {
        postShipmentMessage : {
          isFetching : action.isFetching,
          message : ''
        }
      });
    case REJECT_POST_SHIPMENT_MESSAGE :
      return Object.assign({}, state, {
        postShipmentMessage : {
          isFetching : action.isFetching,
          message : action.payload.xhr.response !== null ? action.payload.xhr.response.message : action.payload.xhr.response
        }
      });
    default :
      return state;
  }
}
