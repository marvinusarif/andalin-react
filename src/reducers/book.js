import { POST_BOOK_VALIDATE_QUOTE, RECEIVE_POST_BOOK_VALIDATE_QUOTE, REJECT_POST_BOOK_VALIDATE_QUOTE,
         POST_BOOK_ORDER, RECEIVE_POST_BOOK_ORDER, REJECT_POST_BOOK_ORDER } from '../actions';

const initialState = {
  isValidating : false,
  isBooking : false,
  isValidatingSuccessful : false,
  isBookingSuccessful : false,
  query : {
    shipmentLoad : 'lcl',
    form : {}
  },
  quote : {},
  bookValidity : false,
  quoteValidity : false
}

export const book = (state = initialState,action) => {
  switch(action.type) {
    case POST_BOOK_VALIDATE_QUOTE :
      return Object.assign({}, state, {
        isValidating : action.isValidating,
        isValidatingSuccessful : action.isSuccessful,
        isBooking : false,
        isBookingSuccessful : false,
        quote : action.payload.quote,
        query : Object.assign({}, state.query, action.payload.query),
        quoteValidity : action.payload.validity,
        bookValidity : false
      });
    case RECEIVE_POST_BOOK_VALIDATE_QUOTE :
      return Object.assign({}, state, {
        quote : action.payload.quote,
        query : action.payload.query,
        isValidating : action.isValidating,
        isValidatingSuccessful : action.isSuccessful,
        quoteValidity : action.payload.validity
      });
    case REJECT_POST_BOOK_VALIDATE_QUOTE :
      return Object.assign({}, state, {
        isValidating : action.isValidating,
        isValidatingSuccessful : action.isSuccessful
      });
    case POST_BOOK_ORDER :
      return Object.assign({}, state, {
        isBooking : action.isBooking,
        isBookingSuccessful : action.isSuccessful,
        bookValidity : false
      })
    case RECEIVE_POST_BOOK_ORDER :
      return Object.assign({}, state, {
        isBooking : action.isBooking,
        isBookingSuccessful : action.isSuccessful,
        quote : initialState.quote,
        query : Object.assign({}, initialState.query),
        isValidating : initialState.isValidating,
        isValidatingSuccessful : false,
        quoteValidity : initialState.quoteValidity,
        bookValidity : action.payload.response.validity
      })
    case REJECT_POST_BOOK_ORDER :
      return Object.assign({}, state, {
        isBooking : action.isBooking,
        isBookingSuccessful : action.isSuccessful,
        bookValidity : false
      })
    default :
      return state;
  }
}
