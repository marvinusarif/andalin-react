import { OPEN_LOGIN_MODAL, CLOSE_LOGIN_MODAL,
         POST_USER_LOGIN, RECEIVE_POST_USER_LOGIN, REJECT_POST_USER_LOGIN,
         FETCH_USER, RECEIVE_FETCH_USER, REJECT_FETCH_USER,
         UPDATE_USER, RECEIVE_UPDATE_USER, REJECT_UPDATE_USER,
         RECEIVE_UPDATE_USER_PHOTO,
         USER_LOGOUT } from '../actions';

const initialState = {
  isFetching : false,
  profile : {
    username : '',
    fullname : '',
    gender : '',
    photo : '',
    codeusertype : '',
    phone : '',
    email : '',
  },
  session : {
    isValid : false,
    loggedAt : null,
    token : null
  }
}

export const user = (state = initialState, action) => {
  switch(action.type) {
    case OPEN_LOGIN_MODAL :
    case CLOSE_LOGIN_MODAL :
    case POST_USER_LOGIN :
    case FETCH_USER :
    case UPDATE_USER:
      return Object.assign({}, state, {
        isFetching : action.isFetching
      });
    case REJECT_FETCH_USER:
    case REJECT_POST_USER_LOGIN :
    case REJECT_UPDATE_USER:
      return Object.assign({}, state, {
        isFetching : action.isFetching
      });
    case RECEIVE_POST_USER_LOGIN :
      return Object.assign({}, state, {
        isFetching : action.isFetching,
        profile : action.payload.response.profile,
        session : action.payload.response.session
      });
    case RECEIVE_UPDATE_USER :
    case RECEIVE_FETCH_USER :
    case RECEIVE_UPDATE_USER_PHOTO :
      return Object.assign({}, state, {
        isFetching : action.isFetching,
        profile : Object.assign({}, state.profile, action.payload.response)
      })
    case USER_LOGOUT :
      return initialState;
    default :
      return state;

  }
}
