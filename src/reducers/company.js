import { RECEIVE_POST_USER_LOGIN,
         FETCH_COMPANY, RECEIVE_FETCH_COMPANY, REJECT_FETCH_COMPANY,
         FETCH_COMPANY_USERS, RECEIVE_FETCH_COMPANY_USERS, REJECT_FETCH_COMPANY_USERS,
         FETCH_COMPANY_DOCS, RECEIVE_FETCH_COMPANY_DOCS, REJECT_FETCH_COMPANY_DOCS,
         UPDATE_COMPANY, RECEIVE_UPDATE_COMPANY, REJECT_UPDATE_COMPANY,
         UPDATE_COMPANY_USERS, RECEIVE_UPDATE_COMPANY_USERS, REJECT_UPDATE_COMPANY_USERS,
         UPDATE_COMPANY_DOCS, RECEIVE_UPDATE_COMPANY_DOCS, REJECT_UPDATE_COMPANY_DOCS} from '../actions/';

const getCompTp =(codeusertype) => {
  if(codeusertype === 'USRS' || codeusertype === 'USRA'){
    return 'USER';
  }else if(codeusertype === 'VDRA' || codeusertype === 'VDRS'){
    return 'VENDOR';
  }else{
    return 'ANDALIN'
  }
}

const initialState = {
  isFetching : false,
  profile : {
    name : '',
    address : '',
    phone : '',
    fax : '',
    email : '',
    country : '',
    industry : '',
    description : '',
    type : ''
  },
  documents : {
    npwp : false,
    siup : false,
    tdp : false,
    ktp : false,
    passport : false,
    license  : false,
    nikexport : false,
    nikimport : false
  },
  users : []
}

export const company = (state=initialState,action) => {
  switch(action.type){
    case FETCH_COMPANY :
    case FETCH_COMPANY_USERS :
    case FETCH_COMPANY_DOCS :
    case UPDATE_COMPANY :
    case UPDATE_COMPANY_DOCS :
    case UPDATE_COMPANY_USERS :
      return Object.assign({}, state, {
        isFetching : action.isFetching,
      });
    case RECEIVE_FETCH_COMPANY :
    case RECEIVE_UPDATE_COMPANY :
      return Object.assign({}, state, {
        isFetching : action.isFetching,
        profile : action.payload.response
      });
    case RECEIVE_FETCH_COMPANY_USERS :
    case RECEIVE_UPDATE_COMPANY_USERS :
      return Object.assign({}, state, {
        isFetching : action.isFetching,
        users : action.payload.response
      })
    case RECEIVE_FETCH_COMPANY_DOCS :
        return Object.assign({}, state, {
          isFetching : action.isFetching,
          documents : action.payload.response
        });
    case RECEIVE_UPDATE_COMPANY_DOCS :
      return Object.assign({}, state, {
        isFetching : action.isFetching,
        documents : Object.assign({}, state.documents, action.payload.response)
      });
    case REJECT_FETCH_COMPANY :
    case REJECT_FETCH_COMPANY_USERS :
    case REJECT_FETCH_COMPANY_DOCS :
    case REJECT_UPDATE_COMPANY :
    case REJECT_UPDATE_COMPANY_DOCS :
    case REJECT_UPDATE_COMPANY_USERS :
      return Object.assign({}, state, {
        isFetching : action.isFetching
      });
    case RECEIVE_POST_USER_LOGIN :
      return Object.assign({}, state, {
        profile : Object.assign({}, initialState.profile, {
          type : getCompTp(action.payload.response.profile.codeusertype)
        })
      })
    default :
      return state;
  }
}
