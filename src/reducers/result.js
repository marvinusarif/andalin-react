import { RESULT_SORT, POST_QUERY, RECEIVE_POST_QUERY, SWITCH_CURRENCY } from '../actions';

const initialState = {
    isFetching : false,
    quotes : [],
    sortBy : 'orderPriceTotal',
    summary : {
      price : { max : 0, min : 0 },
      modes :   [],
      ratings : [],
      vendors : [],
      shippingLines : []
    },
    form : {
        currency : 'USD',
        price : 0,
        modes :   [],
        ratings : [],
        vendors : [],
        shippingLines : []
    }
}
export const result = (state = initialState,action) => {
  switch(action.type){
    case RESULT_SORT :
      return Object.assign({},state,{
        sortBy : action.payload.sortBy
      });
    case POST_QUERY :
      return Object.assign({}, state, {
        isFetching : action.isFetching
      });
    case RECEIVE_POST_QUERY :
      const modes = action.payload.response
                    .reduce( (temp, quote) => {
                      return temp.concat([quote.orderFreight]);
                    },[]).filter((elem, pos, arr) => {
                      return arr.indexOf(elem) === pos;
                    });
      const ratings = action.payload.response
                    .reduce( (temp, quote) => {
                      return temp.concat([quote.vendorRating]);
                    },[]).filter((elem, pos, arr) => {
                      return arr.indexOf(elem) === pos;
                    }).sort( (a,b) => {return b-a});
      const vendors = action.payload.response
                    .reduce( (temp, quote) => {
                      return temp.concat([quote.vendorName]);
                    },[]).filter((elem, pos, arr) => {
                      return arr.indexOf(elem) === pos;
                    });
      const shippingLines = action.payload.response
                    .reduce( (temp, quote) => {
                      return temp.concat([quote.orderShippingLine]);
                    },[]).filter((elem, pos, arr) => {
                      return arr.indexOf(elem) === pos;
                    });
      const step = 10;
      const max = Math.floor( (Math.ceil(Math.max
                  .apply(Math,action.payload.response
                    .map((res) => {return res.orderPriceTotal;}))) + step) / step)*step;
      const min = Math.ceil(Math.min
                  .apply(Math,action.payload.response
                    .map((res) => {return res.orderPriceTotal;})));

      return Object.assign({}, state, {
          isFetching : action.isFetching,
          quotes : action.payload.response,
          summary : { price : { max, min, step },
                      modes,
                      ratings,
                      vendors,
                      shippingLines
                    },
          form : Object.assign({}, state.form, {
            modes : modes.map(mode => true),
            price : max,
            ratings : ratings.map(rating => true),
            vendors : vendors.map( vendor => true),
            shippingLines : shippingLines.map( shippingLine => true)
          })
      });
    case SWITCH_CURRENCY :
      return Object.assign({}, state, {
        form : Object.assign({}, state.form, {
          currency : action.payload.currency
        })
      })
    default :
      return state
  }
}
