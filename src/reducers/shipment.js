import { FETCH_SHIPMENTS, RECEIVE_FETCH_SHIPMENTS, REJECT_FETCH_SHIPMENTS,
         FETCH_SHIPMENT_DETAIL, RECEIVE_FETCH_SHIPMENT_DETAIL, REJECT_FETCH_SHIPMENT_DETAIL,
         FETCH_SHIPMENT_PRICE, RECEIVE_FETCH_SHIPMENT_PRICE, REJECT_FETCH_SHIPMENT_PRICE,
         FETCH_SHIPMENT_CARRIER, RECEIVE_FETCH_SHIPMENT_CARRIER, REJECT_FETCH_SHIPMENT_CARRIER,
         FETCH_SHIPMENT_DOCUMENT, RECEIVE_FETCH_SHIPMENT_DOCUMENT, REJECT_FETCH_SHIPMENT_DOCUMENT,
         FETCH_SHIPMENT_HISTORY, RECEIVE_FETCH_SHIPMENT_HISTORY, REJECT_FETCH_SHIPMENT_HISTORY,
         UPDATE_SHIPMENT_SHIPPER_CONSIGNEE, RECEIVE_UPDATE_SHIPMENT_SHIPPER_CONSIGNEE, REJECT_UPDATE_SHIPMENT_SHIPPER_CONSIGNEE,
         UPDATE_SHIPMENT_ORDER_STAT, RECEIVE_UPDATE_SHIPMENT_ORDER_STAT, REJECT_UPDATE_SHIPMENT_ORDER_STAT,
         UPDATE_SHIPMENT_COMMODITIES, RECEIVE_UPDATE_SHIPMENT_COMMODITIES, REJECT_UPDATE_SHIPMENT_COMMODITIES,
         UPDATE_SHIPMENT_CARRIER, RECEIVE_UPDATE_SHIPMENT_CARRIER, REJECT_UPDATE_SHIPMENT_CARRIER,
         UPDATE_SHIPMENT_FREIGHT, RECEIVE_UPDATE_SHIPMENT_FREIGHT, REJECT_UPDATE_SHIPMENT_FREIGHT,
         UPDATE_SHIPMENT_ORDER_REQUEST, RECEIVE_UPDATE_SHIPMENT_ORDER_REQUEST, REJECT_UPDATE_SHIPMENT_ORDER_REQUEST,
         UPDATE_SHIPMENT_ORDER_PRICE, RECEIVE_UPDATE_SHIPMENT_ORDER_PRICE, REJECT_UPDATE_SHIPMENT_ORDER_PRICE,
         POST_UPDATE_SHIPMENT_DOCUMENT, RECEIVE_POST_UPDATE_SHIPMENT_DOCUMENT, REJECT_POST_UPDATE_SHIPMENT_DOCUMENT
       } from '../actions';
import { RECEIVE_WEBSOCKET_BOOK_SHIPMENT,
         RECEIVE_WEBSOCKET_UPDATE_SHIPMENT_SHIPPER_CONSIGNEE,
         RECEIVE_WEBSOCKET_UPDATE_SHIPMENT_ORDER_STAT,
         RECEIVE_WEBSOCKET_UPDATE_SHIPMENT_COMMODITIES,
         RECEIVE_WEBSOCKET_UPDATE_SHIPMENT_CARRIER,
         RECEIVE_WEBSOCKET_UPDATE_SHIPMENT_REQUESTS,
         RECEIVE_WEBSOCKET_UPDATE_SHIPMENT_FREIGHTS,
         RECEIVE_WEBSOCKET_UPDATE_SHIPMENT_ORDER_PRICE,
         RECEIVE_WEBSOCKET_UPDATE_SHIPMENT_DOCUMENTS } from '../actions/websocket'

import _ from 'lodash';

const initialState = {
  isFetching : false,
  isFetchingDetail : {},
  isFetchingPrice : {},
  isFetchingCarrier : {},
  isFetchingDocument : {},
  isFetchingHistory : {},
  shipment : {},
  shipmentDetail : {},
  shipmentCarrier : {},
  shipmentPrice : {},
  shipmentDocument : {},
  shipmentHistory : {},
  shipmentBankAcc : []
}

export const shipment = (state=initialState, action) => {
  switch(action.type){

    case FETCH_SHIPMENTS :
      return Object.assign({}, state, {
        isFetching : action.isFetching
      });
    case RECEIVE_WEBSOCKET_BOOK_SHIPMENT :
      return Object.assign({}, state, {
        shipment : Object.assign({}, state.shipment, {
          [action.payload.response.result.shipmentId] : action.payload.response.result
        })
      });
    case RECEIVE_FETCH_SHIPMENTS :
      const shipments = _.mapKeys(action.payload.response.results, 'shipmentId');
      return Object.assign({}, state, {
        isFetching : action.isFetching,
        shipment : Object.assign({}, state.shipment, shipments)
        });
    case REJECT_FETCH_SHIPMENTS :
      return Object.assign({}, state, {
        isFetching : action.isFetching
      })
    case FETCH_SHIPMENT_DETAIL :
      return Object.assign({}, state, {
        isFetchingDetail : Object.assign({}, state.isFetchingDetail, {
          [action.payload.shipmentId] : action.isFetching
      })
    });

    case RECEIVE_FETCH_SHIPMENT_DETAIL :
      return Object.assign({}, state, {
        isFetchingDetail : Object.assign({}, state.isFetchingDetail, {
          [action.payload.shipmentId] : action.isFetching
        }),
        shipmentDetail : Object.assign({}, state.shipmentDetail, {
          [action.payload.shipmentId] : action.payload.response
        })
      });
    case REJECT_FETCH_SHIPMENT_DETAIL :
      return Object.assign({}, state, {
        isFetchingDetail : Object.assign({}, state.isFetchingDetail, {
          [action.payload.shipmentId] : action.isFetching
        })
      });

    case FETCH_SHIPMENT_PRICE :
        return Object.assign({}, state, {
          isFetchingPrice : Object.assign({}, state.isFetchingPrice, {
            [action.payload.shipmentId] : action.isFetching
        })
      });
    case RECEIVE_FETCH_SHIPMENT_PRICE :
        return Object.assign({}, state, {
          isFetchingPrice : Object.assign({}, state.isFetchingPrice, {
            [action.payload.shipmentId] : action.isFetching
          }),
          shipmentPrice : Object.assign({}, state.shipmentPrice, {
            [action.payload.shipmentId] : action.payload.response
          })
        });
    case REJECT_FETCH_SHIPMENT_PRICE :
        return Object.assign({}, state, {
          isFetchingPrice : Object.assign({}, state.isFetchingPrice, {
            [action.payload.shipmentId] : action.isFetching
          })
        });

    case FETCH_SHIPMENT_DOCUMENT :
        return Object.assign({}, state, {
          isFetchingDocument : Object.assign({}, state.isFetchingDocument, {
            [action.payload.shipmentId] : action.isFetching
        })
        });
    case RECEIVE_FETCH_SHIPMENT_DOCUMENT :
        return Object.assign({}, state, {
          isFetchingDocument : Object.assign({}, state.isFetchingDocument, {
            [action.payload.shipmentId] : action.isFetching
          }),
          shipmentDocument : Object.assign({}, state.shipmentDocument, {
            [action.payload.shipmentId] : action.payload.response
          })
        });
    case REJECT_FETCH_SHIPMENT_DOCUMENT :
          return Object.assign({}, state, {
            isFetchingDocument : Object.assign({}, state.isFetchingDocument, {
              [action.payload.shipmentId] : action.isFetching
            })
          });

    case FETCH_SHIPMENT_CARRIER :
        return Object.assign({}, state, {
          isFetchingCarrier : Object.assign({}, state.isFetchingCarrier, {
            [action.payload.shipmentId] : action.isFetching
        })
      });
    case RECEIVE_FETCH_SHIPMENT_CARRIER :
        return Object.assign({}, state, {
          isFetchingCarrier : Object.assign({}, state.isFetchingCarrier, {
            [action.payload.shipmentId] : action.isFetching
          }),
          shipmentCarrier : Object.assign({}, state.shipmentCarrier, {
            [action.payload.shipmentId] : action.payload.response
          })
      });
    case REJECT_FETCH_SHIPMENT_CARRIER :
        return Object.assign({}, state, {
          isFetchingCarrier : Object.assign({}, state.isFetchingCarrier, {
            [action.payload.shipmentId] : action.isFetching
          })
      });
    case FETCH_SHIPMENT_HISTORY :
        return Object.assign({}, state, {
          isFetchingHistory : Object.assign({}, state.isFetchingHistory, {
            [action.payload.shipmentId] : action.isFetching
        })
      });
    case RECEIVE_FETCH_SHIPMENT_HISTORY :
        return Object.assign({}, state, {
          isFetchingHistory : Object.assign({}, state.isFetchingHistory, {
            [action.payload.shipmentId] : action.isFetching
          }),
          shipmentHistory : Object.assign({}, state.shipmentHistory, {
            [action.payload.shipmentId] : action.payload.response
          })
      });
    case REJECT_FETCH_SHIPMENT_HISTORY :
        return Object.assign({}, state, {
          isFetchingHistory : Object.assign({}, state.isFetchingHistory, {
            [action.payload.shipmentId] : action.isFetching
          })
      });
    case UPDATE_SHIPMENT_SHIPPER_CONSIGNEE :
        return Object.assign({}, state, {
          isFetchingDetail : Object.assign({}, state.isFetchingDetail, {
            [action.payload.shipmentId] : action.isFetching
          })
        });
    case RECEIVE_WEBSOCKET_UPDATE_SHIPMENT_SHIPPER_CONSIGNEE :
        if(state.shipmentDetail.hasOwnProperty(action.payload.response.shipmentId)){
          return Object.assign({}, state, {
            shipmentDetail : Object.assign({}, state.shipmentDetail, {
              [action.payload.response.shipmentId] : Object.assign({}, state.shipmentDetail[action.payload.response.shipmentId], action.payload.response)
            })
          });
        } else {
          return state
        }
    case RECEIVE_UPDATE_SHIPMENT_SHIPPER_CONSIGNEE :
       return Object.assign({}, state, {
         isFetchingDetail : Object.assign({}, state.isFetchingDetail, {
           [action.payload.shipmentId] : action.isFetching
         }),
         shipmentDetail : Object.assign({}, state.shipmentDetail, {
           [action.payload.shipmentId] : Object.assign({}, state.shipmentDetail[action.payload.shipmentId], action.payload.response)
         })
       });
    case REJECT_UPDATE_SHIPMENT_SHIPPER_CONSIGNEE :
      return Object.assign({}, state, {
        isFetchingDetail : Object.assign({}, state.isFetchingDetail, {
          [action.payload.shipmentId] : action.isFetching
        })
      });
    case UPDATE_SHIPMENT_ORDER_STAT :
        return Object.assign({}, state, {
          isFetchingDetail : Object.assign({}, state.isFetchingDetail, {
          [action.payload.shipmentId] : action.isFetching
        })
      });
    case RECEIVE_WEBSOCKET_UPDATE_SHIPMENT_ORDER_STAT :
      if(state.shipment.hasOwnProperty(action.payload.response.shipmentId) && state.shipmentDetail.hasOwnProperty(action.payload.response.shipmentId)){
        return Object.assign({}, state, {
           shipment : Object.assign({}, state.shipment, {
             [action.payload.response.shipmentId] : Object.assign({}, state.shipment[action.payload.response.shipmentId], {
               orderStat : action.payload.response.orderStat
             })
           }),
           shipmentDetail : Object.assign({}, state.shipmentDetail, {
             [action.payload.response.shipmentId] : Object.assign({}, state.shipmentDetail[action.payload.response.shipmentId], action.payload.response)
           })
       });
     } else {
       return state
     }
    case RECEIVE_UPDATE_SHIPMENT_ORDER_STAT:
      return Object.assign({}, state, {
         isFetchingDetail : Object.assign({}, state.isFetchingDetail, {
           [action.payload.shipmentId] : action.isFetching
         }),
         shipment : Object.assign({}, state.shipment, {
           [action.payload.shipmentId] : Object.assign({}, state.shipment[action.payload.shipmentId], {
             orderStat : action.payload.response.orderStat
           })
         }),
         shipmentDetail : Object.assign({}, state.shipmentDetail, {
           [action.payload.shipmentId] : Object.assign({}, state.shipmentDetail[action.payload.shipmentId], action.payload.response)
         })
      });
    case REJECT_UPDATE_SHIPMENT_ORDER_STAT :
        return Object.assign({}, state, {
          isFetchingDetail : Object.assign({}, state.isFetchingDetail, {
            [action.payload.shipmentId] : action.isFetching
          })
      });
    case UPDATE_SHIPMENT_COMMODITIES :
        return Object.assign({}, state, {
          isFetchingDetail : Object.assign({}, state.isFetchingDetail, {
            [action.payload.shipmentId] : action.isFetching
          })
      });
    case RECEIVE_WEBSOCKET_UPDATE_SHIPMENT_COMMODITIES :
      if(state.shipmentDetail.hasOwnProperty(action.payload.response.shipmentId)){
        return Object.assign({}, state, {
         shipmentDetail : Object.assign({}, state.shipmentDetail, {
           [action.payload.response.shipmentId] : Object.assign({}, state.shipmentDetail[action.payload.response.shipmentId], action.payload.response)
         })
        });
      } else {
        return state
      }
    case RECEIVE_UPDATE_SHIPMENT_COMMODITIES :
        return Object.assign({}, state, {
         isFetchingDetail : Object.assign({}, state.isFetchingDetail, {
           [action.payload.shipmentId] : action.isFetching
          }),
         shipmentDetail : Object.assign({}, state.shipmentDetail, {
           [action.payload.shipmentId] : Object.assign({}, state.shipmentDetail[action.payload.shipmentId], action.payload.response)
         })
      });
    case REJECT_UPDATE_SHIPMENT_COMMODITIES :
        return Object.assign({}, state, {
        isFetchingDetail : Object.assign({}, state.isFetchingDetail, {
          [action.payload.shipmentId] : action.isFetching
        })
      });
    case UPDATE_SHIPMENT_CARRIER:
        return Object.assign({}, state, {
          isFetchingDetail : Object.assign({}, state.isFetchingDetail, {
            [action.payload.shipmentId] : action.isFetching
          })
      });
    case RECEIVE_WEBSOCKET_UPDATE_SHIPMENT_CARRIER :
      if(state.shipmentDetail.hasOwnProperty(action.payload.response.shipmentId)){
        return Object.assign({}, state, {
          shipmentDetail : Object.assign({}, state.shipmentDetail, {
           [action.payload.response.shipmentId] : Object.assign({}, state.shipmentDetail[action.payload.response.shipmentId], action.payload.response)
          })
        });
      } else {
        return state
      }
    case RECEIVE_UPDATE_SHIPMENT_CARRIER :
        return Object.assign({}, state, {
          isFetchingDetail : Object.assign({}, state.isFetchingDetail, {
           [action.payload.shipmentId] : action.isFetching
          }),
          shipmentDetail : Object.assign({}, state.shipmentDetail, {
           [action.payload.shipmentId] : Object.assign({}, state.shipmentDetail[action.payload.shipmentId], action.payload.response)
          })
        });
    case REJECT_UPDATE_SHIPMENT_CARRIER :
        return Object.assign({}, state, {
          isFetchingDetail : Object.assign({}, state.isFetchingDetail, {
            [action.payload.shipmentId] : action.isFetching
        })
      });
    case UPDATE_SHIPMENT_FREIGHT:
        return Object.assign({}, state, {
          isFetchingDetail : Object.assign({}, state.isFetchingDetail, {
            [action.payload.shipmentId] : action.isFetching
          })
      });
    case RECEIVE_WEBSOCKET_UPDATE_SHIPMENT_FREIGHTS :
      if(state.shipmentDetail.hasOwnProperty(action.payload.response.shipmentId)){
        return Object.assign({}, state, {
          shipmentDetail : Object.assign({}, state.shipmentDetail, {
           [action.payload.response.shipmentId] : Object.assign({}, state.shipmentDetail[action.payload.response.shipmentId], {
             freights : action.payload.response.freights
           })
          })
        });
      } else {
        return state
      }
    case RECEIVE_UPDATE_SHIPMENT_FREIGHT :
        return Object.assign({}, state, {
          isFetchingDetail : Object.assign({}, state.isFetchingDetail, {
           [action.payload.shipmentId] : action.isFetching
          }),
          shipmentDetail : Object.assign({}, state.shipmentDetail, {
           [action.payload.shipmentId] : Object.assign({}, state.shipmentDetail[action.payload.shipmentId], {
             freights : action.payload.response
           })
          })
      });
    case REJECT_UPDATE_SHIPMENT_FREIGHT :
        return Object.assign({}, state, {
          isFetchingDetail : Object.assign({}, state.isFetchingDetail, {
            [action.payload.shipmentId] : action.isFetching
        })
      });
  case UPDATE_SHIPMENT_ORDER_REQUEST:
      return Object.assign({}, state, {
          isFetchingDetail : Object.assign({}, state.isFetchingDetail, {
            [action.payload.shipmentId] : action.isFetching
          })
      });
  case RECEIVE_WEBSOCKET_UPDATE_SHIPMENT_REQUESTS :
    if(state.shipmentDetail.hasOwnProperty(action.payload.response.shipmentId)){
      return Object.assign({}, state, {
          shipmentDetail : Object.assign({}, state.shipmentDetail, {
             [action.payload.response.shipmentId] : Object.assign({}, state.shipmentDetail[action.payload.response.shipmentId], {
             requests : action.payload.response.requests
            })
          })
      });
    }else{
      return state
    }
  case RECEIVE_UPDATE_SHIPMENT_ORDER_REQUEST :
      return Object.assign({}, state, {
          isFetchingDetail : Object.assign({}, state.isFetchingDetail, {
             [action.payload.shipmentId] : action.isFetching
          }),
          shipmentDetail : Object.assign({}, state.shipmentDetail, {
             [action.payload.shipmentId] : Object.assign({}, state.shipmentDetail[action.payload.shipmentId], {
             requests : action.payload.response
            })
          })
      });
  case REJECT_UPDATE_SHIPMENT_ORDER_REQUEST :
      return Object.assign({}, state, {
          isFetchingDetail : Object.assign({}, state.isFetchingDetail, {
            [action.payload.shipmentId] : action.isFetching
        })
      });

  case UPDATE_SHIPMENT_ORDER_PRICE:
      return Object.assign({}, state, {
          isFetchingPrice : Object.assign({}, state.isFetchingPrice, {
            [action.payload.shipmentId] : action.isFetching
          })
      });
  case RECEIVE_WEBSOCKET_UPDATE_SHIPMENT_ORDER_PRICE :
      if(state.shipmentDetail.hasOwnProperty(action.payload.response.shipmentId)){
        return Object.assign({}, state, {
            shipmentDetail : Object.assign({}, state.shipmentDetail, {
              [action.payload.response.shipmentId] : Object.assign({}, state.shipmentDetail[action.payload.response.shipmentId], {
                bookPriceTotal : action.payload.response.bookPriceTotal
              })
            }),
            shipmentPrice : Object.assign({}, state.shipmentPrice, {
              [action.payload.response.shipmentId] : Object.assign({}, state.shipmentPrice[action.payload.response.shipmentId], action.payload.response.shipmentPrice)
          })
        });
      } else {
        return Object.assign({}, state, {
            shipmentPrice : Object.assign({}, state.shipmentPrice, {
              [action.payload.response.shipmentId] : Object.assign({}, state.shipmentPrice[action.payload.response.shipmentId], action.payload.response.shipmentPrice)
          })
        });
      }
  case RECEIVE_UPDATE_SHIPMENT_ORDER_PRICE :
      return Object.assign({}, state, {
          isFetchingPrice : Object.assign({}, state.isFetchingPrice, {
            [action.payload.shipmentId] : action.isFetching
          }),
          shipmentDetail : Object.assign({}, state.shipmentDetail, {
            [action.payload.shipmentId] : Object.assign({}, state.shipmentDetail[action.payload.shipmentId], {
              bookPriceTotal : action.payload.response.bookPriceTotal
            })
          }),
          shipmentPrice : Object.assign({}, state.shipmentPrice, {
            [action.payload.shipmentId] : Object.assign({}, state.shipmentPrice[action.payload.shipmentId], action.payload.response.shipmentPrice)
        })
      });
    case REJECT_UPDATE_SHIPMENT_ORDER_PRICE :
        return Object.assign({}, state, {
          isFetchingPrice : Object.assign({}, state.isFetchingPrice, {
            [action.payload.shipmentId] : action.isFetching
          })
        });
    case POST_UPDATE_SHIPMENT_DOCUMENT:
        return Object.assign({}, state, {
          isFetchingDocument : Object.assign({}, state.isFetchingDocument, {
            [action.payload.shipmentId] : action.isFetching
          })
      });
    case RECEIVE_WEBSOCKET_UPDATE_SHIPMENT_DOCUMENTS :
      if(state.shipmentDocument.hasOwnProperty(action.payload.response.shipmentId)){
        return Object.assign({}, state, {
          shipmentDocument : Object.assign({}, state.shipmentDocument, {
            [action.payload.response.shipmentId] : Object.assign({}, state.shipmentDocument[action.payload.response.shipmentId], action.payload.response.documents)
          })
        });
      }else{
        return state
      }
    case RECEIVE_POST_UPDATE_SHIPMENT_DOCUMENT :
        return Object.assign({}, state, {
          isFetchingDocument : Object.assign({}, state.isFetchingDocument, {
              [action.payload.shipmentId] : action.isFetching
          }),
          shipmentDocument : Object.assign({}, state.shipmentDocument, {
            [action.payload.shipmentId] : Object.assign({}, state.shipmentDocument[action.payload.shipmentId], action.payload.response.documents)
          })
      });
      case REJECT_POST_UPDATE_SHIPMENT_DOCUMENT :
        return Object.assign({}, state, {
          isFetchingDocument : Object.assign({}, state.isFetchingDocument, {
            [action.payload.shipmentId] : action.isFetching
          })
      });
    default :
      return state;
  }
}
