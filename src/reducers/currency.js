import { SWITCH_CURRENCY, FETCH_CURRENCY_RATE, RECEIVE_FETCH_CURRENCY_RATE, REJECT_FETCH_CURRENCY_RATE } from '../actions';

const initialState = {
  select : 'USD',
  rates : {},
  isFetching : false
}

export const currency = (state=initialState,action) => {
  switch(action.type){
    case SWITCH_CURRENCY :
      return Object.assign({}, state, {
        select : action.payload.currency
      })
    case FETCH_CURRENCY_RATE :
      return Object.assign({}, state, {
        isFetching : action.isFetching
      })
    case RECEIVE_FETCH_CURRENCY_RATE :
      const rates = action.payload.response.reduce( (rates, _rate) => {
        if( !rates.hasOwnProperty(_rate.base) ) {
          rates[_rate.base] = {}
        }
        return Object.assign({}, rates, {
          [_rate.base] : Object.assign({}, rates[_rate.base], {
            [_rate.quote] : {
              rate : _rate.rate
            }
          })
        });
      }, {});
      return Object.assign({}, state, {
        rates : rates,
        isFetching : action.isFetching
      })
    case REJECT_FETCH_CURRENCY_RATE :
      return Object.assign({}, state, {
        isFetching : action.isFetching
      })
    default :
      return state;
  }
}
