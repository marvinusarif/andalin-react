import { SEARCH_SWITCH_TAB,
          FETCH_COUNTRIES_DESTINATION, RECEIVE_COUNTRIES_DESTINATION,
          FETCH_COUNTRIES_ORIGIN, RECEIVE_COUNTRIES_ORIGIN,
          FETCH_CITIES_DESTINATION, RECEIVE_CITIES_DESTINATION,
          FETCH_CITIES_ORIGIN, RECEIVE_CITIES_ORIGIN,
          POST_QUERY, RECEIVE_POST_QUERY, REJECT_POST_QUERY } from '../actions';

const initialState = {
  activeTab : 'lcl',
  formValue : {
    countries : {
      origin : [],
      destination : []
    },
    cities : {
      origin : {},
      destination  : {}
    }
  },
  query : {
    shipmentLoad : 'lcl',
    form : {}
  }
}
export const search = (state = initialState,action) => {
  switch(action.type){
    case SEARCH_SWITCH_TAB :
      return Object.assign({}, state, {
        activeTab : action.payload.activeTab
      });

    case RECEIVE_COUNTRIES_DESTINATION :
    const destination = action.payload.response
                        .reduce( (destination, country)=> {
                          return destination.concat({ label : country.name, value : country.code })
                        },[])
      return Object.assign({}, state, {
        formValue : {
          ...state.formValue,
          countries : {
            ...state.formValue.countries,
            destination
          }
        }
      });
    case RECEIVE_COUNTRIES_ORIGIN :
    const origin = action.payload.response
                    .reduce( (origin, country)=> {
                      return origin.concat({ label : country.name, value : country.code })
                    },[])
      return Object.assign({}, state, {
        formValue : {
          ...state.formValue,
          countries : {
            ...state.formValue.countries,
            origin
          }
        }
      });
    case RECEIVE_CITIES_DESTINATION :
      const newCitiesDestination = action.payload.response
                          .reduce( (cities, city) => {
                            const _city = {
                              value : city.code,
                              label : city.name
                            }
                            cities[city.codecountry] = cities[city.codecountry] ? cities[city.codecountry].concat(_city) : [_city];
                            return cities;
                          }, {});
      return Object.assign({}, state, {
        formValue : {
          ...state.formValue,
          cities : {
            ...state.formValue.cities,
            destination : Object.assign({}, state.formValue.cities.destination, newCitiesDestination)
        }
      }});
    case RECEIVE_CITIES_ORIGIN :
      const newCitiesOrigin = action.payload.response
                          .reduce( (cities, city) => {
                            const _city = {
                              value : city.code,
                              label : city.name
                            }
                            cities[city.codecountry] = cities[city.codecountry] ? cities[city.codecountry].concat(_city) : [_city];
                            return cities;
                          }, {});
      return Object.assign({}, state, {
        formValue : {
          ...state.formValue,
          cities : {
            ...state.formValue.cities,
            origin : Object.assign({}, state.formValue.cities.origin, newCitiesOrigin)
        }
      }});
    case FETCH_COUNTRIES_DESTINATION :
    case FETCH_COUNTRIES_ORIGIN :
    case FETCH_CITIES_DESTINATION :
    case FETCH_CITIES_ORIGIN :
      return state;

    case POST_QUERY :
      return Object.assign({}, state, {
        query : action.payload.query
      });
    case RECEIVE_POST_QUERY :
    case REJECT_POST_QUERY :
    default :
      return state
  }
}
