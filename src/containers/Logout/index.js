import React, { Component } from 'react';
import { Row, Col, Button as ButtonStrap } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { getP } from 'redux-polyglot';
import { userLogout } from '../../actions/user';
import { closeLogoutModal } from '../../actions/page';

class Logout extends Component {
  constructor(props){
    super(props);
    this._handleCancel = this._handleCancel.bind(this);
    this._handleLogout = this._handleLogout.bind(this);
  }
  _handleCancel(){
    this.props.closeLogoutModal();
  }
  _handleLogout(){
    this.props.userLogout();
    this.props.closeLogoutModal();
    this.props.history.push('/');
  }
  render(){
    const { p } = this.props;
    return (
      <div>
        <Row>
          <Col md={{size:10, offset : 1}} tag="h3">
            {p.tc('title')}
          </Col>
        </Row>
        <Row>
          <Col md={{size:10, offset : 1}} tag="h5">
            {p.tc('description')}
          </Col>
        </Row>
        <Row>
          <Col md={{size:5, offset : 1}}>
            <ButtonStrap block onClick={this._handleCancel} color="primary">
              {p.tc('action.cancel')}
            </ButtonStrap>
          </Col>
          <Col md={{size:5}}>
            <ButtonStrap block onClick={this._handleLogout} color="danger">
              {p.tc('action.logout')}
            </ButtonStrap>
          </Col>
        </Row>
      </div>
    )
  }
}
const mapStateToProps = (state,ownProps) => {
  const p = getP(state, { polyglotScope : 'logout'})
  return {
    p
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    userLogout,
    closeLogoutModal
  }, dispatch)
}
Logout = connect(mapStateToProps,mapDispatchToProps)(Logout)
export default withRouter(Logout)
