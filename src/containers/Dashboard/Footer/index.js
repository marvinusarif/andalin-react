import React,{Component} from 'react';
import { Container, Row } from 'reactstrap';

class Footer extends Component {
  render(){
    return(
          <Container className="container-fluid container-fixed-lg footer">
            <Row className="copyright sm-text-center">
              <p className="small no-margin pull-left sm-pull-reset">
                <span className="hint-text">Copyright © 2017 </span>
                <span className="hint-text"> PT Eximku Teknologi Indonesia</span>.
                <span className="hint-text"> All rights reserved.</span>
                <span className="sm-block">
                  <a className="m-l-10 m-r-10" href="https://www.andalin.com/termcondition">Terms of use</a> |
                  <a className="m-l-10" href="https://www.andalin.com/privacypolicy">Privacy Policy</a>
                </span>
              </p>
              <p className="small no-margin pull-right sm-pull-reset">
              </p>
            <div className="clearfix"></div>
          </Row>
        </Container>
    )
  }
}

export default Footer;
