export const validate = (values, props) => {
  const { p } = props;
  const errors = {};
  if(!values.messageBody){
    errors.messageBody = p.tc('message.validation.required');
  }
  return errors;
}
