import React, { Component } from 'react';
import { Row, Col } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';
import { getP } from 'redux-polyglot';
import { TextField, Button } from '../../../../components/Bootstrap/Forms/';
import { postShipmentMessage} from '../../../../actions/message';
import { openPostUpdateShipmentDocs } from '../../../../actions/page';
import { validate } from './Validation';

class MessagesForm extends Component {
  constructor(props){
    super(props);
    this._handlePostUpdateShipmentDocs = this._handlePostUpdateShipmentDocs.bind(this);
    this._submitForm = this._submitForm.bind(this);
  }
  _handlePostUpdateShipmentDocs(shipmentId){
    this.props.openPostUpdateShipmentDocs(shipmentId);
  }
  _submitForm(form){
    const { shipmentId, orderNumber } = this.props;
    const submittedValue = Object.assign({}, form, {
      shipmentId,
      orderNumber
    })
    this.props.postShipmentMessage(submittedValue);
    this.props.reset();
  }
  render(){
    const { p, handleSubmit, submitting, pristine, isFetching, shipmentId, orderNumber} = this.props;
    return(
      <div>
          <form onSubmit={handleSubmit(this._submitForm.bind(this))}>
            <Row className="m-t-20">
              <Col md="9">
                <Field
                  label={`${p.tu('message.label')}${orderNumber}`}
                  className="form-control input-sm"
                  name='messageBody'
                  placeholder={p.tc('message.placeholder')}
                  component={TextField}
                  required='required'/>
              </Col>
              <Col md="1" className="text-center">
                <Button
                  style={{margin : '30px 0px'}}
                  className="btn btn-primary btn-block"
                  type="button"
                  disabled={submitting}
                  onClick={this._handlePostUpdateShipmentDocs.bind(this,shipmentId)}
                  value={``}
                  icon={'paperclip'}
                />
              </Col>
              <Col md="2" className="text-center">
                <Button
                  style={{margin : '30px 0px'}}
                  className="btn btn-primary btn-block"
                  type="submit"
                  disabled={submitting || pristine }
                  value={isFetching ? p.tc('message.action.posting') :  p.tc('message.action.post')}/>
              </Col>
            </Row>
          </form>
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  const { message, page : { postShipmentMessage : { isFetching } } } = state;
  const p = getP(state, { polyglotScope : 'shipment.detail'});

  return {
    isFetching,
    message,
    p
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    openPostUpdateShipmentDocs,
    postShipmentMessage
  }, dispatch)
}

MessagesForm = reduxForm({
  enableReinitialize : true,
  validate
})(MessagesForm)

MessagesForm = connect(mapStateToProps,mapDispatchToProps)(MessagesForm)

export default MessagesForm
