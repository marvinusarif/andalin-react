import Moment from 'moment';
import _ from 'lodash';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Row, Col, Card } from 'reactstrap';
import { fetchShipmentMessagesHead, postSubscribeChannel, updateNotifications, fetchSubscribersByShipment} from '../../../actions/message';
import { ReactLoader } from '../../../components/Dashboard/Loader/';
import { Button } from '../../../components/Bootstrap/Forms/';
import MessagePerDay from './MessagePerDay';
import { getP } from 'redux-polyglot';

class Message extends Component {
  constructor(props){
    super(props);
    this.messageEnd = null;
    this._scrollToBottom = this._scrollToBottom.bind(this);
    this._handleSubscribe = this._handleSubscribe.bind(this);
  }
  _scrollToBottom(){
    const node = ReactDOM.findDOMNode(this.messageEnd);
    node.scrollTop = node.scrollHeight;
  }
  _handleSubscribe(){
    const { shipmentId, orderNumber } = this.props;
    this.props.postSubscribeChannel(shipmentId, orderNumber);
  }
  componentDidMount(){
    const { orderNumber, shipmentId, subscribedChannels, notifications, subscribersByChannel } = this.props;
    if(subscribedChannels.includes(orderNumber)){
      if(notifications.hasOwnProperty(shipmentId)){
        this.props.updateNotifications(shipmentId, _.values(notifications[shipmentId].items))
      }
      this.props.fetchShipmentMessagesHead({orderNumber, shipmentId});
      this._scrollToBottom();
    }
    if(!subscribersByChannel.hasOwnProperty(shipmentId)){
      this.props.fetchSubscribersByShipment(shipmentId,orderNumber);
    }
  }
  componentDidUpdate(){
    const { subscribedChannels, orderNumber } = this.props;
    if(subscribedChannels.includes(orderNumber)){
      this._scrollToBottom();
    }
  }
  render(){
    const { p, messageHead, shipmentId, orderNumber, isFetchingMessageHead, subscribedChannels, subscribersByChannel, vendorName } = this.props;
    if(!subscribedChannels.includes(orderNumber)) {
      return (
        <Card>
          <Row>
            <Col>
              <Row>
                <Col md={{size : 8, offset :2}} tag="h5">
                  { subscribersByChannel.hasOwnProperty(shipmentId) && Object.keys(subscribersByChannel[shipmentId]).length > 0
                    ? p.tc('subscribers.title', { smart_count : Object.keys(subscribersByChannel[shipmentId]).length })
                    : p.tc('subscribers.empty') }
                    <br/>
                </Col>
              </Row>
              <Row>
                <Col md={{size : 8, offset :2}} tag="h5">
                  { p.tc('subscribers.description', { talkTo : vendorName ? vendorName : 'customer' }) }
                </Col>
              </Row>
              <Row>
                <Col md={{size : 6, offset :3}}>
                  <Button
                    style={{margin : '30px 0px'}}
                    className="btn btn-primary btn-block"
                    type="button"
                    onClick={this._handleSubscribe.bind(this)}
                    value={`${p.tc('action.subscribe')} ${orderNumber}`}/>
                </Col>
              </Row>
            </Col>
          </Row>
        </Card>
      )
    }
    if( isFetchingMessageHead[shipmentId]){
        return (
            <ReactLoader message={`loading message of order #${orderNumber}`}/>
          )
    }
    return (
          <div style={{width: '100%', height : '300px', overflow : 'scroll', overflowX : 'hidden'}} ref={(el) => { this.messageEnd = el } }>
          { _.map(messageHead[shipmentId], (head, index) => {
            return (
              <Row key={`messageHead${shipmentId}_${index}`}>
                <Col md="12" className="text-center" tag="h5">
                  {Moment(head,'YYYYMMDD').format('DD MMMM YYYY')}
                </Col>
                <Col md="12">
                  <MessagePerDay shipmentId={shipmentId} orderNumber={orderNumber} messageDateBucket={head}/>
                </Col>
              </Row>
            )
          })}
        </div>
    )
  }
}
const mapStateToProps = (state,ownProps) => {
  const { message : { messageHead, messageBody, isFetchingMessageHead, subscribedChannels, notifications, subscribersByChannel} } = state;
  const p = getP(state, {polyglotScope : 'message'})
  return {
    p,
    subscribersByChannel,
    notifications,
    subscribedChannels,
    isFetchingMessageHead,
    messageHead,
    messageBody,
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    fetchSubscribersByShipment,
    fetchShipmentMessagesHead,
    postSubscribeChannel,
    updateNotifications
  }, dispatch)
}
export default connect(mapStateToProps,mapDispatchToProps)(Message);
