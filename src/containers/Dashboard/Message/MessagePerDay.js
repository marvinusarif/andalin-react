import Moment from 'moment';
import _ from 'lodash';
import React, { Component } from 'react';
import { Row, Col, Button as ButtonStrap } from 'reactstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getP } from 'redux-polyglot';
import { downloadShipmentDoc } from '../../../actions/shipment';
import { fetchShipmentMessagesBody } from '../../../actions/message';

class MessagePerDay extends Component {
  constructor(props){
    super(props);
    this._handleDownloadShipmentDoc = this._handleDownloadShipmentDoc.bind(this);
  }

  _handleDownloadShipmentDoc({shipmentId, orderNumber, fileCategory, fileId, extFileName, saveAsFileName}){
    this.props.downloadShipmentDoc({shipmentId, orderNumber, fileCategory, fileId, extFileName, saveAsFileName})
  }

  componentDidMount(){
    const { shipmentId, messageBody, orderNumber, messageDateBucket, messageHead } = this.props;
    const messageHeadTotalLength = messageHead[shipmentId].length;
    if(!messageBody.hasOwnProperty(`${shipmentId}_${messageDateBucket}`)){
      this.props.fetchShipmentMessagesBody({shipmentId, orderNumber, messageDateBucket});
    } else {
      if(messageHead[shipmentId][messageHeadTotalLength-1] === messageDateBucket){ //fetch only the last
        this.props.fetchShipmentMessagesBody({shipmentId, orderNumber, messageDateBucket});
      }
    }
  }
  render(){
    const { p, messageBody, shipmentId, messageDateBucket, isFetchingMessageBody, user, orderNumber, shipment } = this.props;
    if(isFetchingMessageBody[`${shipmentId}_${messageDateBucket}`]){
      return (
        <Row>
          <Col> loading ... </Col>
        </Row>
      )
    }
    return (
      <div>
        {_.map( messageBody[`${shipmentId}_${messageDateBucket}`], (message, index) => {
          return (
            <Row key={`Message${shipmentId}_${messageDateBucket}${index}`}>,
              <Col md="12" className={`${user.profile.username === message.sender.username ? 'text-right' : 'text-left' }`}>
                <small className={`${ user.profile.username === message.sender.username ? 'text-primary' : '' }`} style={{ fontWeight : '800px', fontSize : '11px'}}> {message.sender.username} ({message.sender.comptp === 'VENDOR' ? shipment[shipmentId].vendorName : message.sender.comptp === 'USER' ? shipment[shipmentId].shipperCompany : 'Andalin' }) - </small>
                <small className="text-muted" style={{ fontSize : '10px'}}>{Moment(message.createdAt, 'YYYY-MM-DD HH:mm:ss Z').fromNow()}</small>
              </Col>
              <Col md="12" className={`${ user.profile.username === message.sender.username ? 'text-primary text-right' : 'text-left' }`} style={{fontSize : '12px' }}> {message.body} <br/>
                { message.attachment && ([
                  <small key={`fileDesc_${message.messageId}`}>{p.tc('file_description')} : {message.attachment.fileDesc}</small>,
                  <br key={`brFileDesc_${message.messageId}`}/>,
                  <ButtonStrap
                    key={`donwload_${message.messageId}`}
                    className="btn btn-primary"
                    size="xs"
                    onClick={this._handleDownloadShipmentDoc.bind(this,{shipmentId,orderNumber,fileId : message.attachment.fileId,fileCategory : message.attachment.type.split('/')[0], saveAsFileName : `${orderNumber.split('/').join('-')}_${message.attachment.type.split('/')[0]}`, extFileName : message.attachment.type.split('/')[1] })}>
                    <span className="fa fa-save"></span> {p.tc('action.download', { fileName : message.attachment.type.split('/')[0] })}
                  </ButtonStrap>,
                  <br key={`br_${message.messageId}`}/>
                ])}

              </Col>
            </Row>)
        })}
      </div>
    )
  }
}

const mapStateToProps = (state,ownProps) => {
  const { message : { messageBody,isFetchingMessageBody, messageHead }, user, shipment : { shipment } } = state;
  const p = getP(state, { polyglotScope : 'shipment.detail.message'})
  return {
    shipment,
    p,
    user,
    isFetchingMessageBody,
    messageHead,
    messageBody
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    fetchShipmentMessagesBody,
    downloadShipmentDoc
  }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(MessagePerDay);
