import React, { Component } from 'react';
import { NotificationItem,NotificationItemScrollable, NotificationFooter } from '../../../../components/Dashboard/Header/Notification';

class Notifications extends Component {
  render(){
    return (
      <div>
        {/*<!-- START NOTIFICATION LIST -->*/}
        <ul className="hidden-md-down notification-list no-margin hidden-sm-down b-grey b-r no-style p-l-30 p-r-20">
          <li className="p-r-10 inline">
            <div className="dropdown">
              <a id="notification-center" className="header-icon pg pg-world" data-toggle="dropdown">
                <span className="bubble"></span>
              </a>
              {/*<!-- START Notification Dropdown -->*/}
              <div className="dropdown-menu notification-toggle" role="menu" aria-labelledby="notification-center">
                {/*<!-- START Notification -->*/}
                <div className="notification-panel">
                  {/*<!-- START Notification Body-->*/}
                  <div className="notification-body scrollable">
                    <NotificationItem/>
                    <NotificationItemScrollable/>
                  </div>
                  {/*<!-- END Notification Body-->*/}
                  <NotificationFooter />
                </div>
                {/*<!-- END Notification -->*/}
              </div>
              {/*<!-- END Notification Dropdown -->*/}
            </div>
          </li>
        </ul>
        {/*<!-- END NOTIFICATIONS LIST -->*/}
      </div>
    )
  }
}

export default Notifications
