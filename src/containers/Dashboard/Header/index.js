import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import UserInfo from './UserInfo';
import Logo from '../../../assets/img/logo_2x.png';

class Header extends Component {
  render(){
    return (
      <div>
        {/*<!-- START PAGE HEADER WRAPPER -->*/}
        {/*<!-- START HEADER -->*/}
        <div className="header">
          {/*<!-- START MOBILE SIDEBAR TOGGLE -->*/}
          <button href="#" className="btn-link toggle-sidebar hidden-lg-up pg pg-menu" data-toggle="sidebar"> </button>
          {/*<!-- END MOBILE SIDEBAR TOGGLE -->*/}
          <div style={{background : '#ebebeb'}}>
            <div className="brand inline" style={{background : '#ebebeb'}}>
              <img src={Logo} alt="logo" width="78" height="22"/>
            </div>
          </div>
          <div className="d-flex align-items-center" style={{ width : '40%', margin : '0px 100px'}} >
            <div className="col-md-2">
            </div>
            <div className="col-md-10">
              <UserInfo />
            </div>
          </div>
        </div>
        {/*<!-- END HEADER -->*/}
        {/*<!-- END PAGE HEADER WRAPPER -->*/}
      </div>
    )
  }
}
const mapStateToProps = (state, ownProps) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({

  },dispatch)
}
export default connect(mapStateToProps,mapDispatchToProps)(Header);
