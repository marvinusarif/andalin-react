import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { getP } from 'redux-polyglot';
import { DropDownCustom } from '../../../../components/Bootstrap/dropdown';
import { openLoginModal, openLogoutModal, openRegisterModal, openContactModal } from '../../../../actions/page';
import LanguageInfo from '../LanguageInfo';
const userOptions = [{
  label : 'user_options.settings',
  icon : 'cog',
  action : 'settings'
},{
  label : 'user_options.help',
  icon : 'comment',
  action : 'help'
},{
  thread : true,
  label : 'user_options.logout',
  icon : 'power-off',
  action : 'logout'
}]

const newUserOptions = [{
  label : 'user_options.login',
  icon : '',
  action : 'login'
},{
  label : 'user_options.signup',
  icon : '',
  action : 'signup'
},{
  thread : true,
  label : 'user_options.help',
  icon : 'comment',
  action : 'help'
}]

class UserInfo extends Component {
  constructor(props){
    super(props);
    this.state = {
      openProfileIsOpen : false,
      openProfileActive : !this.props.user.session.isValid ? newUserOptions[0].label : userOptions[0].label
    }
    this._toggle = this._toggle.bind(this);
    this._handleUserOptions = this._handleUserOptions.bind(this);
  }
  _toggle(){
    this.setState({
      openProfileIsOpen : !this.state.openProfileIsOpen
    })
  }
  componentWillReceiveProps(nextProps){
    this.setState({
      openProfileActive : !nextProps.user.session.isValid ? newUserOptions[0].label : userOptions[0].label
    });
  }
  _handleUserOptions({label, action}){
    this.setState({
      openProfileActive : label
    })
    if(action === 'login'){
      this.props.openLoginModal();
    }else if(action === 'signup'){
      this.props.openRegisterModal();
    }else if(action === 'help'){
      this.props.openContactModal();
    }else if(action === 'logout'){
      this.props.openLogoutModal();
    }else if(action === 'settings'){
      this.props.history.push('/settings');
    }
  }

  render(){
    const { user : { profile, session }, p } = this.props;
    const { openProfileIsOpen, openProfileActive } = this.state;
    return (
      <div style={{bottom: '0px'}} className="row clearfix">
        <div className="col-md-6 text-right">
          { session.isValid && (
            <small className="semi-bold">{p.tc('hello', { fullname : profile.fullname }) }</small>)}
        </div>
        <div className="col-md-3 text-right">
          <LanguageInfo />
        </div>
        <div className="col-md-3">
          <DropDownCustom
            p={p}
            icon={session.isValid ? `cog` : ``}
            label={openProfileActive}
            isOpen={openProfileIsOpen}
            toggle={this._toggle.bind(this)}
            options={session.isValid ? userOptions : newUserOptions }
            onItemClick={this._handleUserOptions.bind(this)}/>
        </div>
        {/*<!-- START User Info-->*/}


        {/*<!-- END User Info-->*/}
      </div>
    )
  }
}

const mapStateToProps = (state,prevState) => {
  const { user } = state;
  const p = getP(state, { polyglotScope : 'header.userinfo'});
  return {
    user,
    p
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    openLoginModal,
    openLogoutModal,
    openRegisterModal,
    openContactModal,
  }, dispatch);
}
UserInfo = connect(mapStateToProps,mapDispatchToProps)(UserInfo);

export default withRouter(UserInfo);
