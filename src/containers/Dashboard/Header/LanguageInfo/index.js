import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { setLanguage } from '../../../../actions/lang';
import { DropDown } from '../../../../components/Bootstrap/dropdown';

const languageOptions = [{
  label : 'English',
  action : 'en'
},{
  label : 'Bahasa Indonesia',
  action : 'idn'
}]

class LanguageInfo extends Component {
  constructor(props){
    super(props);
    this.state = {
      langDropdownIsOpen : false,
      langActive : this.props.polyglot.locale ? this.props.polyglot.locale : 'English'
    }
  }
  _toggle(){
    this.setState({
      langDropdownIsOpen: !this.state.langDropdownIsOpen,
    });
  }
  _handleLanguageItem(lang){
    this.setState({
      langActive : lang.label
    })
    this.props.setLanguage(lang.action);
  }
  componentWillMount(){
    !this.props.polyglot.locale && this._handleLanguageItem(languageOptions[0])
  }
  render(){
    const { langDropdownIsOpen, langActive } = this.state;
    return (
                <DropDown
                  label={langActive}
                  isOpen={langDropdownIsOpen}
                  toggle={this._toggle.bind(this)}
                  options={languageOptions}
                  onItemClick={this._handleLanguageItem.bind(this)}/>
    )
  }
}
const mapStateToProps = (state,ownProps) => {
  const { polyglot } = state;
  return {
    polyglot
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    setLanguage
  },dispatch)
}
export default connect(mapStateToProps,mapDispatchToProps)(LanguageInfo);
