import React,{Component} from 'react';
import { Link } from 'react-router-dom';

class Quickview extends Component {
  render(){
    return (
      <div>
        {/*<!--START QUICKVIEW -->*/}
            <div id="quickview" className="quickview-wrapper" data-pages="quickview">
          {/*<!-- Nav tabs -->*/}
          <ul className="nav nav-tabs">
            <li className="" data-target="#quickview-notes" data-toggle="tab">
              <Link to="#">Notes</Link>
            </li>
            <li data-target="#quickview-alerts" data-toggle="tab">
              <Link to="#">Alerts</Link>
            </li>
            <li className="active" data-target="#quickview-chat" data-toggle="tab">
              <Link to="#">Chat</Link>
            </li>
          </ul>
          <Link to="#" className="btn-link quickview-toggle" data-toggle-element="#quickview" data-toggle="quickview"><i className="pg-close"></i></Link>
          {/*<!-- Tab panes -->*/}
          <div className="tab-content">
            {/*<!-- BEGIN Notes !-->*/}
            <div className="tab-pane no-padding" id="quickview-notes">
              <div className="view-port clearfix quickview-notes" id="note-views">
                {/*<!-- BEGIN Note List !-->*/}
                <div className="view list" id="quick-note-list">
                  <div className="toolbar clearfix">
                    <ul className="pull-right ">
                      <li>
                        <Link to="#" className="delete-note-link"><i className="fa fa-trash-o"></i></Link>
                      </li>
                      <li>
                        <Link to="#" className="new-note-link" data-navigate="view" data-view-port="#note-views" data-view-animation="push"><i className="fa fa-plus"></i></Link>
                      </li>
                    </ul>
                    <button className="btn-remove-notes btn btn-xs btn-block hide"><i className="fa fa-times"></i> Delete</button>
                  </div>
                  <ul>
                    {/*<!-- BEGIN Note Item !-->*/}
                    <li data-noteid="1">
                      <div className="left">
                        {/*<!-- BEGIN Note Action !-->*/}
                        <div className="checkbox check-warning no-margin">
                          <input id="qncheckbox1" type="checkbox" value="1"/>
                          <label htmlFor="qncheckbox1"></label>
                        </div>
                        {/*<!-- END Note Action !-->*/}
                        {/*<!-- BEGIN Note Preview Text !-->*/}
                        <p className="note-preview">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                        {/*<!-- BEGIN Note Preview Text !-->*/}
                      </div>
                      {/*<!-- BEGIN Note Details !-->*/}
                      <div className="right pull-right">
                        {/*<!-- BEGIN Note Date !-->*/}
                        <span className="date">12/12/14</span>
                        <Link to="#" data-navigate="view" data-view-port="#note-views" data-view-animation="push"><i className="fa fa-chevron-right"></i></Link>
                        {/*<!-- END Note Date !-->*/}
                      </div>
                      {/*<!-- END Note Details !-->*/}
                    </li>
                    {/*<!-- END Note List !-->*/}
                  </ul>
                </div>
                {/*<!-- END Note List !-->*/}
                <div className="view note" id="quick-note">
                  <div>s
                    <ul className="toolbar">
                      <li><Link to="#" className="close-note-link"><i className="pg-arrow_left"></i></Link>
                      </li>
                      <li><Link to="#" data-action="Bold" className="fs-12"><i className="fa fa-bold"></i></Link>
                      </li>
                      <li><Link to="#" data-action="Italic" className="fs-12"><i className="fa fa-italic"></i></Link>
                      </li>
                      <li><Link to="#" className="fs-12"><i className="fa fa-link"></i></Link>
                      </li>
                    </ul>
                    <div className="body">
                      <div>
                        <div className="top">
                          <span>21st april 2014 2:13am</span>
                        </div>
                        <div className="content">
                          <div className="quick-note-editor full-width full-height js-input" contentEditable="true"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/*<!-- END Notes !-->*/}
            {/*<!-- BEGIN Alerts !-->*/}
            <div className="tab-pane no-padding" id="quickview-alerts">
              <div className="view-port clearfix" id="alerts">
                {/*<!-- BEGIN Alerts View !-->*/}
                <div className="view bg-white">
                  {/*<!-- BEGIN View Header !-->*/}
                  <div className="navbar navbar-default navbar-sm">
                    <div className="navbar-inner">
                      {/*<!-- BEGIN Header Controler !-->*/}
                      <Link to="#" className="inline action p-l-10 link text-master" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                        <i className="pg-more"></i>
                      </Link>
                      {/*<!-- END Header Controler !-->*/}
                      <div className="view-heading">
                        Notications
                      </div>
                      {/*<!-- BEGIN Header Controler !-->*/}
                      <Link to="#" className="inline action p-r-10 pull-right link text-master">
                        <i className="pg-search"></i>
                      </Link>
                      {/*<!-- END Header Controler !-->*/}
                    </div>
                  </div>
                  {/*<!-- END View Header !-->*/}
                  {/*<!-- BEGIN Alert List !-->*/}
                  <div data-init-list-view="ioslist" className="list-view boreded no-top-border">
                    {/*<!-- BEGIN List Group !-->*/}
                    <div className="list-view-group-container">
                      {/*<!-- BEGIN List Group Header!-->*/}
                      <div className="list-view-group-header text-uppercase">
                        Calendar
                      </div>
                      {/*<!-- END List Group Header!-->*/}
                      <ul>
                        {/*<!-- BEGIN List Group Item!-->*/}
                        <li className="alert-list">
                          {/*<!-- BEGIN Alert Item Set Animation using data-view-animation !-->*/}
                          <Link to="#" className="align-items-center" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                            <p className="">
                              <span className="text-warning fs-10"><i className="fa fa-circle"></i></span>
                            </p>
                            <p className="p-l-10 overflow-ellipsis fs-12">
                              <span className="text-master">David Nester Birthday</span>
                            </p>
                            <p className="p-r-10 ml-auto fs-12 text-right">
                              <span className="text-warning">Today <br/></span>
                              <span className="text-master">All Day</span>
                            </p>
                          </Link>
                          {/*<!-- END Alert Item!-->*/}
                          {/*<!-- BEGIN List Group Item!-->*/}
                        </li>
                        {/*<!-- END List Group Item!-->*/}
                      </ul>
                    </div>
                    {/*<!-- END List Group !-->*/}
                    <div className="list-view-group-container">
                      {/*<!-- BEGIN List Group Header!-->*/}
                      <div className="list-view-group-header text-uppercase">
                        Social
                      </div>
                      {/*<!-- END List Group Header!-->*/}
                      <ul>
                        {/*<!-- BEGIN List Group Item!-->*/}
                        <li className="alert-list">
                          {/*<!-- BEGIN Alert Item Set Animation using data-view-animation !-->*/}
                          <Link to="#" className="p-t-10 p-b-10 align-items-center" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                            <p className="">
                              <span className="text-complete fs-10"><i className="fa fa-circle"></i></span>
                            </p>
                            <p className="col overflow-ellipsis fs-12 p-l-10">
                              <span className="text-master link">Jame Smith commented on your status<br/></span>
                              <span className="text-master">“Perfection Simplified - Company Revox"</span>
                            </p>
                          </Link>
                          {/*<!-- END Alert Item!-->*/}
                        </li>
                        {/*<!-- END List Group Item!-->*/}
                      </ul>
                    </div>
                    <div className="list-view-group-container">
                      {/*<!-- BEGIN List Group Header!-->*/}
                      <div className="list-view-group-header text-uppercase">
                        Sever Status
                      </div>
                      {/*<!-- END List Group Header!-->*/}
                      <ul>
                        {/*<!-- BEGIN List Group Item!-->*/}
                        <li className="alert-list">
                          {/*<!-- BEGIN Alert Item Set Animation using data-view-animation !-->*/}
                          <Link to="#" className="p-t-10 p-b-10 align-items-center" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                            <p className="">
                              <span className="text-danger fs-10"><i className="fa fa-circle"></i></span>
                            </p>
                            <p className="col overflow-ellipsis fs-12 p-l-10">
                              <span className="text-master link">12:13AM GTM, 10230, ID:WR174s<br/></span>
                              <span className="text-master">Server Load Exceeted. Take action</span>
                            </p>
                          </Link>
                          {/*<!-- END Alert Item!-->*/}
                        </li>
                        {/*<!-- END List Group Item!-->*/}
                      </ul>
                    </div>
                  </div>
                  {/*<!-- END Alert List !-->*/}
                </div>
                {/*<!-- EEND Alerts View !-->*/}
              </div>
            </div>
            {/*<!-- END Alerts !-->*/}
            <div className="tab-pane active no-padding" id="quickview-chat">
              <div className="view-port clearfix" id="chat">
                <div className="view bg-white">
                  {/*<!-- BEGIN View Header !-->*/}
                  <div className="navbar navbar-default">
                    <div className="navbar-inner">
                      {/*<!-- BEGIN Header Controler !-->*/}
                      <Link to="#" className="inline action p-l-10 link text-master" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                        <i className="pg-plus"></i>
                      </Link>
                      {/*<!-- END Header Controler !-->*/}
                      <div className="view-heading">
                        Chat List
                        <div className="fs-11">Show All</div>
                      </div>
                      {/*<!-- BEGIN Header Controler !-->*/}
                      <Link to="#" className="inline action p-r-10 pull-right link text-master">
                        <i className="pg-more"></i>
                      </Link>
                      {/*<!-- END Header Controler !-->*/}
                    </div>
                  </div>
                  {/*<!-- END View Header !-->*/}
                  <div data-init-list-view="ioslist" className="list-view boreded no-top-border">
                    <div className="list-view-group-container">
                      <div className="list-view-group-header text-uppercase">
                        a</div>
                      <ul>
                        {/*<!-- BEGIN Chat User List Item  !-->*/}
                        <li className="chat-user-list clearfix">
                          <Link data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" className="" to="#">
                            <span className="thumbnail-wrapper d32 circular bg-success">
                                <img width="34" height="34" data-src-retina="assets/img/profiles/1x.jpg" data-src="assets/img/profiles/1.jpg" alt="" src="assets/img/profiles/1.jpg" className="col-top"/>
                            </span>
                            <p className="p-l-10 ">
                              <span className="text-master">ava flores</span>
                              <span className="block text-master hint-text fs-12">Hello there</span>
                            </p>
                          </Link>
                        </li>
                        {/*<!-- END Chat User List Item  !-->*/}
                      </ul>
                    </div>
                    <div className="list-view-group-container">
                      <div className="list-view-group-header text-uppercase">b</div>
                      <ul>
                        {/*<!-- BEGIN Chat User List Item  !-->*/}
                        <li className="chat-user-list clearfix">
                          <Link data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" className="" to="#">
                            <span className="thumbnail-wrapper d32 circular bg-success">
                                <img width="34" height="34" data-src-retina="assets/img/profiles/1x.jpg" data-src="assets/img/profiles/1.jpg" alt="" src="assets/img/profiles/1.jpg" className="col-top"/>
                            </span>
                            <p className="p-l-10 ">
                              <span className="text-master">bella mccoy</span>
                              <span className="block text-master hint-text fs-12">Hello there</span>
                            </p>
                          </Link>
                        </li>
                        {/*<!-- END Chat User List Item  !-->*/}
                        {/*<!-- BEGIN Chat User List Item  !-->*/}
                        <li className="chat-user-list clearfix">
                          <Link data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" className="" to="#">
                            <span className="thumbnail-wrapper d32 circular bg-success">
                                <img width="34" height="34" data-src-retina="assets/img/profiles/1x.jpg" data-src="assets/img/profiles/1.jpg" alt="" src="assets/img/profiles/1.jpg" className="col-top"/>
                            </span>
                            <p className="p-l-10 ">
                              <span className="text-master">bob stephens</span>
                              <span className="block text-master hint-text fs-12">Hello there</span>
                            </p>
                          </Link>
                        </li>
                        {/*<!-- END Chat User List Item  !-->*/}
                      </ul>
                    </div>

                  </div>
                </div>
                {/*<!-- BEGIN Conversation View  !-->*/}
                <div className="view chat-view bg-white clearfix">
                  {/*<!-- BEGIN Header  !-->*/}
                  <div className="navbar navbar-default">
                    <div className="navbar-inner">
                      <Link to="#" className="link text-master inline action p-l-10 p-r-10" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                        <i className="pg-arrow_left"></i>
                      </Link>
                      <div className="view-heading">
                        John Smith
                        <div className="fs-11 hint-text">Online</div>
                      </div>
                      <Link to="#" className="link text-master inline action p-r-10 pull-right ">
                        <i className="pg-more"></i>
                      </Link>
                    </div>
                  </div>
                  {/*<!-- END Header  !-->*/}
                  {/*<!-- BEGIN Conversation  !-->*/}
                  <div className="chat-inner" id="my-conversation">
                    {/*<!-- BEGIN From Me Message  !-->*/}
                    <div className="message clearfix">
                      <div className="chat-bubble from-me">
                        Hello there
                      </div>
                    </div>
                    {/*<!-- END From Me Message  !-->*/}
                    {/*<!-- BEGIN From Them Message  !-->*/}
                    <div className="message clearfix">
                      <div className="profile-img-wrapper m-t-5 inline">
                        <img width="34" height="34" data-src-retina="assets/img/profiles/1x.jpg" data-src="assets/img/profiles/1.jpg" alt="" src="assets/img/profiles/1.jpg" className="col-top"/>
                      </div>
                      <div className="chat-bubble from-them">
                        Hey
                      </div>
                    </div>
                    {/*<!-- END From Them Message  !-->*/}
                    {/*<!-- BEGIN From Me Message  !-->*/}
                    <div className="message clearfix">
                      <div className="chat-bubble from-me">
                        Did you check out Pages framework ?
                      </div>
                    </div>
                    {/*<!-- END From Me Message  !-->*/}
                    {/*<!-- BEGIN From Me Message  !-->*/}
                    <div className="message clearfix">
                      <div className="chat-bubble from-me">
                        Its an awesome chat
                      </div>
                    </div>
                    {/*<!-- END From Me Message  !-->*/}
                    {/*<!-- BEGIN From Them Message  !-->*/}
                    <div className="message clearfix">
                      <div className="profile-img-wrapper m-t-5 inline">
                        <img width="34" height="34" data-src-retina="assets/img/profiles/1x.jpg" data-src="assets/img/profiles/1.jpg" alt="" src="assets/img/profiles/1.jpg" className="col-top"/>
                      </div>
                      <div className="chat-bubble from-them">
                        Yea
                      </div>
                    </div>
                    {/*<!-- END From Them Message  !-->*/}
                  </div>
                  {/*<!-- BEGIN Conversation  !-->*/}
                  {/*<!-- BEGIN Chat Input  !-->*/}
                  <div className="b-t b-grey bg-white clearfix p-l-10 p-r-10">
                    <div className="row">
                      <div className="col-1 p-t-15">
                        <Link to="#" className="link text-master"><i className="fa fa-plus-circle"></i></Link>
                      </div>
                      <div className="col-8 no-padding">
                        <input type="text" className="form-control chat-input" data-chat-input="" data-chat-conversation="#my-conversation" placeholder="Say something"/>
                      </div>
                      <div className="col-2 link text-master m-l-10 m-t-15 p-l-10 b-l b-grey col-top">
                        <Link to="#" className="link text-master"><i className="pg-camera"></i></Link>
                      </div>
                    </div>
                  </div>
                  {/*<!-- END Chat Input  !-->*/}
                </div>
                {/*<!-- END Conversation View  !-->*/}
              </div>
            </div>
          </div>
        </div>
        {/*<!-- END QUICKVIEW-->*/}
      </div>
    )
  }
}

export default Quickview;
