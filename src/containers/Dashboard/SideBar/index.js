import _ from 'lodash';
import React, {Component} from 'react';
import { connect } from 'react-redux';
import SideBarItems from '../../../components/Dashboard/SideBar';
import Logo2x from '../../../assets/img/logo_2x.png';

const menuItems = {
  home : {
    label : 'menu.home',
    detail : '',
    url : '/home',
    icon : "home",
    requiredLogin : true,
  },
  new_quotes : {
    label : 'menu.new_search',
    detail : "",
    url : "/search",
    icon : "search",
    requiredLogin : false
  },
  quotes : {
    label : "menu.quotes",
    detail : "",
    url : "/quotes",
    icon : "folder",
    requiredLogin : true
  },
  shipments : {
    label : "menu.shipments",
    detail : "",
    url : "/shipments",
    icon : "ship",
    requiredLogin : true
  }, /*
  message : {
    label : "menu.messages",
    detail : "",
    url : "/messages",
    icon : "comment",
    requiredLogin : true
  }, */
  settings : {
    label : "menu.settings",
    detail : "",
    url : "/settings",
    icon : "cog",
    requiredLogin : true
  }
}
class SideBar extends Component {

  render(){
    const { user : { session } } = this.props;
    let displayMenuItems = menuItems;
    if(!session.isValid){
      displayMenuItems = _.filter(menuItems, menuItem => menuItem.requiredLogin === false)
    }

    return (
      <div>
        {/*<!-- BEGIN SIDEBAR -->*/}
            {/*<!-- BEGIN SIDEBPANEL-->*/}
        <nav className="page-sidebar" data-pages="sidebar">
          {/*<!-- BEGIN SIDEBAR MENU TOP TRAY CONTENT-->*/}
          <div className="sidebar-overlay-slide from-top" id="appMenu">

          </div>
          {/*<!-- END SIDEBAR MENU TOP TRAY CONTENT-->*/}
          {/*<!-- BEGIN SIDEBAR MENU HEADER-->*/}
          <div className="sidebar-header" style={{background : '#ebebeb'}}>
            <img src={Logo2x} alt="logo" className="brand" width="78" height="22"/>
          </div>
          {/*<!-- END SIDEBAR MENU HEADER-->*/}
          {/*<!-- START SIDEBAR MENU -->*/}
          <div className="sidebar-menu m-t-20">
            {/*<!-- BEGIN SIDEBAR MENU ITEMS-->*/}
            <SideBarItems items={displayMenuItems}/>
          </div>
          {/*<!-- END SIDEBAR MENU -->*/}
        </nav>
        {/*<!-- END SIDEBAR -->*/}
      </div>
    )
  }
}
const mapStateToProps = (state, ownProps) => {
  const { user } = state;
  return {
    user
  }
}
export default connect(mapStateToProps, null)(SideBar)
