import React from 'react';
import { Card, CardHeader, CardBlock, Row, Col } from 'reactstrap';
import { Field } from 'redux-form';
import translate from 'redux-polyglot/translate';
import { SliderField, SelectField } from '../../../../components/Bootstrap/Forms/';
import { currencyOptions} from '../../Search/Forms/index';

const PriceSelector = (props) => {
  const { priceMin, priceMax, priceStep, p, currency, currencyBase, onChangeSelect} = props;
  return (
    <Card>
      <CardHeader tag="h6">
        {p.tc('title')}
      </CardHeader>
      <CardBlock>
          <Row>
            <Col md="12">
              <Field
                label={p.tu('currency')}
                className="form-control input-sm"
                name='currency'
                onChange={onChangeSelect}
                items={currencyOptions}
                component={SelectField}
                required='required'/>
            </Col>
          </Row>
          <Row>
            <Col md="12">
              { Object.keys(currency.rates).length > 0 && (
                <small style={{fontSize:'9px'}}>*rates {currencyBase} 1 =  {currency.select} {currency.rates[currencyBase][currency.select].rate}</small>
              )}
            </Col>
          </Row>
          <Row>
            <Col md={{size : 10, offset : 1}}>
              <Field
                max={priceMax}
                min={priceMin}
                step={priceStep}
                currency={currency}
                currencyBase = {currencyBase}
                name="price"
                component={SliderField}/>
            </Col>
          </Row>
      </CardBlock>
  </Card>
  )
}

export default translate({polyglotScope : 'result.filter.price'})(PriceSelector)
