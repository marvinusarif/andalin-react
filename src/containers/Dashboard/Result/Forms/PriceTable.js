import React from 'react';
import { Table } from 'reactstrap';
import NumberFormat from 'react-number-format';
import _ from 'lodash';

const PriceTableHeader = ({p}) => {
  return (
    <tr>
      <th style={{fontSize : '10px'}} className="bg-transparent">
        {p.tu('display.detail.fee_code')}
      </th>
      <th style={{fontSize : '10px'}} className="bg-transparent">
        {p.tu('display.detail.fee_desc')}
      </th>
      <th style={{fontSize : '10px'}} className="bg-transparent">
        {p.tu('display.detail.fee_units')}
      </th>
      <th style={{fontSize : '10px'}} className="bg-transparent">
        {p.tu('display.detail.fee_uom')}
      </th>
      <th style={{fontSize : '10px'}} className="bg-transparent">
        {p.tu('display.detail.fee_price')}
      </th>
      <th style={{fontSize : '10px'}} className="bg-transparent">
        {p.tu('display.detail.fee_pricesum')}
      </th>
    </tr>
  )
}
const getUnits = (priceComponent) => {
  const { priceUOM, volume, weight, qty, price } = priceComponent;
  if(priceUOM === 'V'){
    return <NumberFormat value={volume} displayType={'text'} thousandSeparator={true} />;
  }else if(priceUOM === 'W'){
    return <NumberFormat value={weight} displayType={'text'} thousandSeparator={true} />;
  }else if(priceUOM === 'CONT'){
    return <NumberFormat value={qty} displayType={'text'} thousandSeparator={true} />;
  }else if(priceUOM === 'ALL') {
    return <NumberFormat value={qty} displayType={'text'} thousandSeparator={true} />;
  }else{
    if(volume * price > weight * price){
      return <NumberFormat value={volume} displayType={'text'} thousandSeparator={true} />
    }else{
      return <NumberFormat value={weight} displayType={'text'} thousandSeparator={true} />
    }
  }
}

export const PriceConversion = ({price,currency, currencyBase}) => {
  const { rates, select } = currency;
  let prefix = '';
  let value = 0;
  let toFixed = 0;
  if(currencyBase !== select ){
    prefix = select;
    value = price * rates[currencyBase][select].rate;
  }else{
    prefix = currencyBase;
    value = price;
  }
  if(select === 'IDR'){
    toFixed = 0;
  } else {
    toFixed = 2;
  }
  return (
    <NumberFormat value={value.toFixed(toFixed)} displayType={'text'} thousandSeparator={true} prefix={`${prefix} `} />
  )
}
const getPriceUOM = (p, priceComponent) => {
  const { priceUOM } = priceComponent;
  if(priceUOM === 'V'){
    return p.tc('display.detail.uom_options.volume');
  }else if(priceUOM === 'W'){
    return p.tc('display.detail.uom_options.weight');
  }else if(priceUOM === 'CONT'){
    return p.tc('display.detail.uom_options.container');
  }else if(priceUOM === 'ALL'){
    return p.tc('display.detail.uom_options.shipment');
  }else{
    return p.tc('display.detail.uom_options.volumetric');
  }
}
export const PriceTableFreightCharges = ({p, quote, currency}) => {
  const { orderPriceFreight, orderPriceDocument, orderPriceInsurance, orderPriceCurrencyCode } = quote;
  let totalSubAmountFreight = 0;
  let totalSubAmountDoc = 0;
  let totalSubAmountInsurance = 0;
  const pricelistFreight = _.reduce(orderPriceFreight, (combinedPricelistPerServiceType, pricelistPerServiceType) =>{
    const combinedPricelistPerPriceGroup = _.reduce(pricelistPerServiceType, (combinedPricelistPerPriceGroup, pricelistPerPriceGroup) => {
      const combinedPricelistPerPriceCode = _.reduce(pricelistPerPriceGroup, (combinedPricelistPerPriceCode, pricelistPerPriceCode) => {
        return combinedPricelistPerPriceCode.concat(pricelistPerPriceCode);
      },[]);
      return combinedPricelistPerPriceGroup.concat(combinedPricelistPerPriceCode);
    }, []);
    return combinedPricelistPerServiceType.concat(combinedPricelistPerPriceGroup);
  },[]);

  const pricelistDoc = _.reduce(orderPriceDocument, (combinedPricelistPerServiceType, pricelistPerServiceType) => {
    const combinedPricelistPerPriceCode = _.reduce(pricelistPerServiceType, (combinedPricelistPerPriceCode, pricelistPerPriceCode) => {
      return combinedPricelistPerPriceCode.concat(pricelistPerPriceCode);
    },[])
    return combinedPricelistPerServiceType.concat(combinedPricelistPerPriceCode);
  }, []);
  let goodsValueCurrency = _.reduce(orderPriceInsurance, (goodsValueCurrency,insurance) => {
    return insurance.goodsValueCurrency;
  },'')
  return (
    <Table className="bg-transparent">
      <thead>
        <PriceTableHeader p={p}/>
      </thead>
      <tbody>
        { _.map(pricelistFreight, (price) => {
            totalSubAmountFreight = totalSubAmountFreight + price.priceSum;
                return (<tr key={`${price.serviceType}_${price.codeGroupPriceFreight}_${price.codePriceFreight}`} style={{lineHeight : '10px'}}>
                  <td style={{fontSize : '10px'}} className="bg-transparent">{price.codePriceFreight}{price.serviceType ? ` : ${price.serviceType} ` : ''}</td>
                  <td style={{fontSize : '10px'}} className="bg-transparent">{price.descPriceFreight}</td>
                  <td style={{fontSize : '10px'}} className="bg-transparent">{getUnits(price)}</td>
                  <td style={{fontSize : '10px'}} className="bg-transparent">{getPriceUOM(p, price)}</td>
                  <td style={{fontSize : '10px'}} className="bg-transparent">
                      <NumberFormat value={price.price.toFixed(2)} displayType={'text'} thousandSeparator={true} prefix={`${orderPriceCurrencyCode} `} />
                  </td>
                  <td style={{fontSize : '10px'}} className="bg-transparent">
                      <PriceConversion price={price.priceSum} currency={currency} currencyBase={orderPriceCurrencyCode}/>
                      { price.priceMin > price.priceSum ? `*${p.tc('display.detail.minimum_charge')}` : ''}
                  </td>
                </tr>)
              })}
        { pricelistFreight.length > 0 && (
                <tr>
                  <td colSpan="3" className="bg-transparent"></td>
                  <td colSpan="2" style={{fontSize : '10px', fontWeight : '900'}} className="text-right bg-transparent">{p.tu('display.detail.subamount.freight')}</td>
                  <td style={{fontSize : '10px', fontWeight : '900'}} className="bg-transparent">
                    <PriceConversion price={totalSubAmountFreight} currency={currency} currencyBase={orderPriceCurrencyCode}/></td>
                </tr>
              )
          }
        { _.map(pricelistDoc, (price) => {
          totalSubAmountDoc = totalSubAmountDoc + price.priceSum;
              return (<tr key={`${price.serviceType}_${price.codeGroupPriceDoc}_${price.codePriceDoc}`} style={{lineHeight : '10px'}}>
                <td style={{fontSize : '10px'}} className="bg-transparent">{price.codePriceDoc}{price.serviceType ? ` : ${price.serviceType} ` : ''}</td>
                <td style={{fontSize : '10px'}} className="bg-transparent">{price.descPriceDoc}</td>
                <td style={{fontSize : '10px'}} className="bg-transparent">{getUnits(price)}</td>
                <td style={{fontSize : '10px'}} className="bg-transparent">{getPriceUOM(p, price)}</td>
                <td style={{fontSize : '10px'}} className="bg-transparent">
                  <NumberFormat value={price.price.toFixed(2)} displayType={'text'} thousandSeparator={true} prefix={`${orderPriceCurrencyCode} `} />
                </td>
                <td style={{fontSize : '10px'}} className="bg-transparent">
                  <PriceConversion price={price.priceSum} currency={currency} currencyBase={orderPriceCurrencyCode}/>
                  { price.priceMin > price.priceSum ? `*${p.tc('display.detail.minimum_charge')}` : ''}
                </td>
              </tr>)
        })}
        { pricelistDoc.length > 0 && (
                <tr>
                  <td colSpan="3" className="bg-transparent"></td>
                  <td colSpan="2" style={{fontSize : '10px', fontWeight : '900'}} className="text-right bg-transparent">{p.tu('display.detail.subamount.document')}</td>
                  <td style={{fontSize : '10px', fontWeight : '900'}} className="bg-transparent">
                    <PriceConversion price={totalSubAmountDoc} currency={currency} currencyBase={orderPriceCurrencyCode}/>
                  </td>
                </tr>
                  )
          }
          { _.map(orderPriceInsurance, (price, insuranceCode) => {
              totalSubAmountInsurance = totalSubAmountInsurance + price.priceSum;
                  return (<tr key={`${price.serviceType}_${price.insuranceCode}`} style={{lineHeight : '10px'}}>
                    <td style={{fontSize : '10px'}} className="bg-transparent">{price.codePriceInsurance}</td>
                    <td style={{fontSize : '10px'}} className="bg-transparent">{p.tc('display.detail.insurance')}</td>
                    <td style={{fontSize : '10px'}} className="bg-transparent">{price.insurancePcg * 100}</td>
                    <td style={{fontSize : '10px'}} className="bg-transparent">%</td>
                    <td style={{fontSize : '10px'}} className="bg-transparent">
                      <NumberFormat value={price.goodsValue.toFixed(2)} displayType={'text'} thousandSeparator={true} prefix={`${price.goodsValueCurrency} `} />
                    </td>
                    <td style={{fontSize : '10px'}} className="bg-transparent">
                      <PriceConversion price={price.priceSum} currency={currency} currencyBase={price.goodsValueCurrency}/>
                    </td>
                    </tr>)
                })}
          { Object.keys(orderPriceInsurance).length > 0 && (
                  <tr>
                    <td colSpan="3" className="bg-transparent"></td>
                    <td colSpan="2" style={{fontSize : '10px', fontWeight : '900'}} className="text-right bg-transparent">{p.tu('display.detail.subamount.insurance')}</td>
                    <td style={{fontSize : '10px', fontWeight : '900'}} className="bg-transparent">
                      <PriceConversion price={totalSubAmountInsurance} currency={currency} currencyBase={goodsValueCurrency}/>
                    </td>
                  </tr>)
            }
      </tbody>
    </Table>)
}
export const PriceTableSummary = ({p, quote, currency}) => {
  const { orderNote, orderNoteDetailFreight, orderPriceTotal, orderPriceCurrencyCode } = quote;
  const combinedOrderNotes = _.reduce(orderNote, (combinedOrderNotes, orderNote) => {
    const combinedNotePerServiceType = _.reduce(orderNote, (combinedNotePerServiceType, notePerServiceType) => {
      const notesPerServiceType = notePerServiceType.split('\n')
      const parsedNotesPerServiceType = _.reduce(notesPerServiceType, (parsedNotesPerServiceType, note) => {
          return parsedNotesPerServiceType.concat(note)
        },[])
      return combinedNotePerServiceType.concat(parsedNotesPerServiceType);
    },[]);
    return combinedOrderNotes.concat(combinedNotePerServiceType);
  },[]).filter((elem, pos, arr) => {
    return arr.indexOf(elem) === pos;
  });
  const combinedOrderNotesDetail = _.reduce(orderNoteDetailFreight, (combinedOrderNotesDetail, orderNoteDetail) => {
    const combinedNoteDetailPerServiceType = _.reduce(orderNoteDetail, (combinedNoteDetailPerServiceType, noteDetailPerServiceType) => {
      return combinedNoteDetailPerServiceType.concat(noteDetailPerServiceType);
    },[]);
    return combinedOrderNotesDetail.concat(combinedNoteDetailPerServiceType);
  },[]).filter((elem, pos, arr) => {
    return arr.indexOf(elem) === pos;
  });
  return(
    <Table className="bg-transparent">
      <thead>
        <tr>
          <th colSpan="6" className="bg-transparent">
            {p.tu('display.detail.notes')}
          </th>
        </tr>
      </thead>
      <tbody>
        {combinedOrderNotes.map( (orderNote, index) => {
          return(<tr key={index}>
            <td colSpan="6" style={{fontSize : '10px'}} className="bg-transparent">
              {orderNote !=='' ? p.tc('display.detail.notes_description', {notes : orderNote}) : ''}
            </td>
          </tr>
        )})}
        {combinedOrderNotesDetail.map( (orderNoteDetail, index) => {
          return (<tr  key={index}>
            <td colSpan="6" style={{fontSize : '10px'}} className="bg-transparent">
              {orderNoteDetail !=='' ? p.tc('display.detail.notes_description', {notes : orderNoteDetail}) : ''}
            </td>
          </tr>)
        })}
        {
           (combinedOrderNotesDetail.length < 1 && combinedOrderNotes.length < 1) && (
           <tr>
             <td colSpan="6" style={{fontSize : '10px', fontWeight : '900'}} className="text-center bg-transparent">{p.tu('display.detail.notes_empty')}</td>
           </tr>
         )
        }
        <tr>
          <td colSpan="4" style={{fontSize : '10px', fontWeight : '900', width : '80%'}} className="text-right bg-transparent">
            {p.tu('display.detail.summary')}
          </td>
          <td colSpan="2" style={{fontSize : '10px', fontWeight : '900'}} className="bg-transparent">
            <PriceConversion price={orderPriceTotal} currency={currency} currencyBase={orderPriceCurrencyCode}/>
          </td>
        </tr>
      </tbody>
    </Table>)
}
export const PriceTableOriginDestinationCharges= ({p, quote, currency, cityCode, originOrDestination }) => {
  const { orderPriceCustom, orderPriceTrucking, orderPriceCurrencyCode } = quote;
  let pricelistCustomOrigin = [];
  let pricelistCustomDestination = [];
  let pricelistTruckingOrigin = [];
  let pricelistTruckingDestination = [];
  let totalSubAmountCustomOrigin = 0;
  let totalSubAmountCustomDestination = 0;
  let totalSubAmountTruckingOrigin = 0;
  let totalSubAmountTruckingDestination = 0;
  if(originOrDestination === 'origin'){
    if(orderPriceCustom.hasOwnProperty(cityCode)){
      pricelistCustomOrigin = _.reduce(orderPriceCustom[cityCode], (combinedPricelistPerServiceType, pricelistPerServiceType) => {
        const combinedPricelistPerPriceCode = _.reduce(pricelistPerServiceType, (combinedPricelistPerPriceCode, pricelistPerPriceCode) => {
          return combinedPricelistPerPriceCode.concat(pricelistPerPriceCode);
        },[])
        return combinedPricelistPerServiceType.concat(combinedPricelistPerPriceCode);
      }, []);
    }
    if(orderPriceTrucking.hasOwnProperty(cityCode)){
      pricelistTruckingOrigin = _.reduce(orderPriceTrucking[cityCode], (combinedPricelistPerServiceType, pricelistPerServiceType) => {
        const combinedPricelistPerPriceCode = _.reduce(pricelistPerServiceType, (combinedPricelistPerPriceCode, pricelistPerPriceCode) => {
          return combinedPricelistPerPriceCode.concat(pricelistPerPriceCode);
        },[])
        return combinedPricelistPerServiceType.concat(combinedPricelistPerPriceCode);
      }, []);
    }
  }else{
    if(orderPriceCustom.hasOwnProperty(cityCode)){
      pricelistCustomDestination = _.reduce(orderPriceCustom[cityCode], (combinedPricelistPerServiceType, pricelistPerServiceType) => {
        const combinedPricelistPerPriceCode = _.reduce(pricelistPerServiceType, (combinedPricelistPerPriceCode, pricelistPerPriceCode) => {
          return combinedPricelistPerPriceCode.concat(pricelistPerPriceCode);
        },[])
        return combinedPricelistPerServiceType.concat(combinedPricelistPerPriceCode);
      }, []);
    }
    if(orderPriceTrucking.hasOwnProperty(cityCode)){
      pricelistTruckingDestination = _.reduce(orderPriceTrucking[cityCode], (combinedPricelistPerServiceType, pricelistPerServiceType) => {
        const combinedPricelistPerPriceCode = _.reduce(pricelistPerServiceType, (combinedPricelistPerPriceCode, pricelistPerPriceCode) => {
          return combinedPricelistPerPriceCode.concat(pricelistPerPriceCode);
        },[])
        return combinedPricelistPerServiceType.concat(combinedPricelistPerPriceCode);
      }, []);
    }
  }
  return (
    <Table className="bg-transparent">
      <thead>
        <PriceTableHeader p={p}/>
      </thead>
      <tbody>
        {
           (originOrDestination === 'origin' && pricelistCustomOrigin.length < 1 && pricelistTruckingOrigin.length < 1) && (
           <tr>
             <td colSpan="6" style={{fontSize : '10px', fontWeight : '900'}} className="text-center bg-transparent">{p.tu('display.detail.fee_empty', { place : cityCode})}</td>
           </tr>
         )
        }
        {
           (originOrDestination === 'destination' && pricelistCustomDestination.length < 1 && pricelistTruckingDestination.length  < 1)  && (
           <tr>
             <td colSpan="6" style={{fontSize : '10px', fontWeight : '900'}} className="text-center bg-transparent">{p.tu('display.detail.fee_empty', { place : cityCode})}</td>
           </tr>
         )
        }
        { _.map(pricelistTruckingOrigin, (price) => {
            totalSubAmountTruckingOrigin = totalSubAmountTruckingOrigin + price.priceSum;
                return (<tr key={`${price.serviceType}_${price.codeGroupPriceTrucking}_${price.codePriceTrucking}`} style={{lineHeight : '10px'}}>
                  <td style={{fontSize : '10px'}} className="bg-transparent">{price.codePriceTrucking }{price.serviceType ? ` : ${price.serviceType} ` : ''}</td>
                  <td style={{fontSize : '10px'}} className="bg-transparent">{price.descPriceTrucking }</td>
                  <td style={{fontSize : '10px'}} className="bg-transparent">{getUnits(price)}</td>
                  <td style={{fontSize : '10px'}} className="bg-transparent">{getPriceUOM(p, price)}</td>
                  <td style={{fontSize : '10px'}} className="bg-transparent">
                    <NumberFormat value={price.price.toFixed(2)} displayType={'text'} thousandSeparator={true} prefix={`${orderPriceCurrencyCode} `} />
                  </td>
                  <td style={{fontSize : '10px'}} className="bg-transparent">
                    <PriceConversion price={price.priceSum} currency={currency} currencyBase={orderPriceCurrencyCode}/>
                    { price.priceMin > price.priceSum ? `*${p.tc('display.detail.minimum_charge')}` : ''}</td>
                </tr>)
              })}
          { pricelistTruckingOrigin.length > 0 && (
            <tr>
              <td colSpan="3" className="bg-transparent"></td>
              <td colSpan="2" style={{fontSize : '10px', fontWeight : '900'}} className="text-right bg-transparent">{p.tu('display.detail.subamount.trucking_origin')}</td>
              <td style={{fontSize : '10px', fontWeight : '900'}} className="bg-transparent">
                <PriceConversion price={totalSubAmountTruckingOrigin} currency={currency} currencyBase={orderPriceCurrencyCode}/>
              </td>
            </tr>)
          }
        {_.map(pricelistCustomOrigin, (price) => {
            totalSubAmountCustomOrigin = totalSubAmountCustomOrigin + price.priceSum;
                return (<tr key={`${price.serviceType}_${price.codeGroupPriceCustom}_${price.codePriceCustom}`} style={{lineHeight : '10px'}}>
                  <td style={{fontSize : '10px'}} className="bg-transparent">{price.codePriceCustom }{price.serviceType ? ` : ${price.serviceType} ` : ''}</td>
                  <td style={{fontSize : '10px'}} className="bg-transparent">{price.descPriceCustom }</td>
                  <td style={{fontSize : '10px'}} className="bg-transparent">{getUnits(price)}</td>
                  <td style={{fontSize : '10px'}} className="bg-transparent">{getPriceUOM(p, price)}</td>
                  <td style={{fontSize : '10px'}} className="bg-transparent">
                    <NumberFormat value={price.price.toFixed(2)} displayType={'text'} thousandSeparator={true} prefix={`${orderPriceCurrencyCode} `} />
                  </td>
                  <td style={{fontSize : '10px'}} className="bg-transparent">
                    <PriceConversion price={price.priceSum} currency={currency} currencyBase={orderPriceCurrencyCode}/>
                    { price.priceMin > price.priceSum ? `*${p.tc('display.detail.minimum_charge')}` : ''}</td>
                </tr>)
              })}
            { pricelistCustomOrigin.length > 0 && (
                <tr>
                  <td colSpan="3" className="bg-transparent"></td>
                  <td colSpan="2" style={{fontSize : '10px', fontWeight : '900'}} className="text-right bg-transparent">{p.tu('display.detail.subamount.custom_origin')}</td>
                  <td style={{fontSize : '10px', fontWeight : '900'}} className="bg-transparent">
                    <PriceConversion price={totalSubAmountCustomOrigin} currency={currency} currencyBase={orderPriceCurrencyCode}/></td>
                </tr>)
          }
        {_.map(pricelistCustomDestination, (price) => {
            totalSubAmountCustomDestination = totalSubAmountCustomDestination + price.priceSum;
                return (<tr key={`${price.serviceType}_${price.codeGroupPriceCustom}_${price.codePriceCustom}`} style={{lineHeight : '10px'}}>
                  <td style={{fontSize : '10px'}} className="bg-transparent">{price.codePriceCustom }{price.serviceType ? ` : ${price.serviceType} ` : ''}</td>
                  <td style={{fontSize : '10px'}} className="bg-transparent">{price.descPriceCustom }</td>
                  <td style={{fontSize : '10px'}} className="bg-transparent">{getUnits(price)}</td>
                  <td style={{fontSize : '10px'}} className="bg-transparent">{getPriceUOM(p, price)}</td>
                  <td style={{fontSize : '10px'}} className="bg-transparent">
                    <NumberFormat value={price.price.toFixed(2)} displayType={'text'} thousandSeparator={true} prefix={`${orderPriceCurrencyCode} `} />
                  </td>
                  <td style={{fontSize : '10px'}} className="bg-transparent">
                    <PriceConversion price={price.priceSum} currency={currency} currencyBase={orderPriceCurrencyCode}/>
                    { price.priceMin > price.priceSum ? `*${p.tc('display.detail.minimum_charge')}` : ''}</td>
                </tr>)
              })}
            { pricelistCustomDestination.length > 0 && (
                <tr>
                  <td colSpan="3" className="bg-transparent"></td>
                  <td colSpan="2" style={{fontSize : '10px', fontWeight : '900'}} className="text-right bg-transparent">{p.tu('display.detail.subamount.custom_destination')}</td>
                  <td style={{fontSize : '10px', fontWeight : '900'}} className="bg-transparent">
                    <PriceConversion price={totalSubAmountCustomDestination} currency={currency} currencyBase={orderPriceCurrencyCode}/>
                  </td>
                </tr>)
          }
        {_.map(pricelistTruckingDestination, (price) => {
            totalSubAmountTruckingDestination = totalSubAmountTruckingDestination + price.priceSum;
                return (<tr key={`${price.serviceType}_${price.codeGroupPriceTrucking}_${price.codePriceTrucking}`} style={{lineHeight : '10px'}}>
                  <td style={{fontSize : '10px'}} className="bg-transparent">{price.codePriceTrucking }{price.serviceType ? ` : ${price.serviceType} ` : ''}</td>
                  <td style={{fontSize : '10px'}} className="bg-transparent">{price.descPriceTrucking }</td>
                  <td style={{fontSize : '10px'}} className="bg-transparent">{getUnits(price)}</td>
                  <td style={{fontSize : '10px'}} className="bg-transparent">{getPriceUOM(p, price)}</td>
                  <td style={{fontSize : '10px'}} className="bg-transparent">
                    <NumberFormat value={price.price.toFixed(2)} displayType={'text'} thousandSeparator={true} prefix={`${orderPriceCurrencyCode} `} />
                  </td>
                  <td style={{fontSize : '10px'}} className="bg-transparent">
                    <PriceConversion price={price.priceSum} currency={currency} currencyBase={orderPriceCurrencyCode}/>
                    { price.priceMin > price.priceSum ? `*${p.tc('display.detail.minimum_charge')}` : ''}</td>
                </tr>)
              })}
          { pricelistTruckingDestination.length > 0 && (
                <tr>
                  <td colSpan="3" className="bg-transparent"></td>
                  <td colSpan="2" style={{fontSize : '10px', fontWeight : '900'}} className="text-right bg-transparent">{p.tu('display.detail.subamount.trucking_destination')}</td>
                  <td style={{fontSize : '10px', fontWeight : '900'}} className="bg-transparent">
                    <PriceConversion price={totalSubAmountTruckingDestination} currency={currency} currencyBase={orderPriceCurrencyCode}/>
                  </td>
                </tr>)
          }
      </tbody>
    </Table>
  )
}
