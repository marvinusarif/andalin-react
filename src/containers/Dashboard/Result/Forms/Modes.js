import React from 'react';
import { Card, CardHeader, CardBlock, Row, Col } from 'reactstrap';
import { Field, FieldArray } from 'redux-form';
import translate from 'redux-polyglot/translate';
import { CheckBox } from '../../../../components/Bootstrap/Forms/';

const renderModesList = (props) => {
  const { fields, modeOptions, p } = props;
  return ( <div>
    { fields.map( (mode,index) => {
    return (
      <Row key={index}>
        <Col md="12">
          <Field
              label={p.tc(`${modeOptions[index]}`.toLowerCase())}
              className="form-control input-sm"
              value={`${modeOptions[index]}`}
              name={`${mode}`}
              component={CheckBox}/>
        </Col>
      </Row>
    )})} </div>)
}

const ModesSelector = (props) => {
  const { modeOptions,p } = props;
  return (
    <Card>
      <CardHeader tag="h6">
        {p.tc('title')}
      </CardHeader>
      <CardBlock>
        <FieldArray name="modes" p={p} modeOptions={modeOptions} component={renderModesList}/>
      </CardBlock>
    </Card>)
}

export default translate({polyglotScope : 'result.filter.modes'})(ModesSelector)
