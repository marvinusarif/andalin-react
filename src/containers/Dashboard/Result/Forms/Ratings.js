import React from 'react';
import { Card, CardHeader, CardBlock, Row, Col } from 'reactstrap';
import { Field, FieldArray } from 'redux-form';
import translate from 'redux-polyglot/translate';
import { CheckBox } from '../../../../components/Bootstrap/Forms/';
import StarRatingComponent from 'react-star-rating-component';

const renderRatingsList = (props) => {
  const { fields, ratingOptions } = props;
  return ( <div>
    { fields.map( (rating,index) => {
    return (
      <Row key={index}>
        <Col md="1">
          <Field
              className="form-control input-sm"
              value={`${ratingOptions[index]}`}
              name={`${rating}`}
              component={CheckBox}/>
        </Col>
        <Col md="10">
          <StarRatingComponent
            name='vendorRating' /* name of the radio input, it is required */
            value={parseInt(ratingOptions[index],0)} /* number of selected icon (`0` - none, `1` - first) */
            starCount={5} /* number of icons in rating, default `5` */
            emptyStarColor={`#cdd6ec`}
            editing={false} /* is component available for editing, default `true` */
        />
        </Col>
      </Row>
    )})} </div>)
}

export const RatingsSelector = (props) => {
  const { ratingOptions, p } = props;
  return (
    <Card>
      <CardHeader tag="h6">
        {p.tc('title')}
      </CardHeader>
      <CardBlock>
        <FieldArray name="ratings" ratingOptions={ratingOptions} component={renderRatingsList}/>
      </CardBlock>
    </Card>)
}

export default translate({ polyglotScope : 'result.filter.ratings'})(RatingsSelector)
