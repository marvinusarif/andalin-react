import _ from 'lodash';
import React from 'react';
import translate from 'redux-polyglot/translate';
import { Card, Row, Col, Button } from 'reactstrap';

const SearchSummary = (props) => {
  const { query, p, renderServiceType, backToSearch } = props;
  let totalShipment = { qty : null, volume : null, weight : null };
  let displayFCLShipment;
  if(query.shipmentLoad === 'lcl' && Object.keys(query.form).length > 0){
    const { loadsLCL } = query.form;
    totalShipment = {
            qty : loadsLCL.reduce( (totalQty,shipment) => {
                    return totalQty += shipment.qty ? parseInt(shipment.qty,10) : 0;
                  }, 0),
            volume : loadsLCL.reduce( (totalVolume, shipment) => {
                    let volume = parseInt(shipment.size.length_,10) * parseInt(shipment.size.width,10) * parseInt(shipment.size.height,10) / 1000000 * parseInt(shipment.qty,10)
                    volume = shipment.size.unit === 'in' ? volume / 35.3147 : volume
                    return totalVolume += !isNaN(volume) ? parseInt(Math.ceil(volume),10) : 0;
                  }, 0),
            weight : loadsLCL.reduce( (totalWeight, shipment) => {
                    let weight = parseInt(shipment.weight,10) * parseInt(shipment.qty,10)
                    weight = shipment.weightUnit === 'lbs' ? weight / 2.20462 : weight
                    return totalWeight += !isNaN(weight) ? parseInt(Math.ceil(weight),10) : 0;
                  }, 0)
          }
  }else if(query.shipmentLoad === 'fcl' && Object.keys(query.form).length > 0){
    const { loadsFCL } = query.form;
    totalShipment = {
            qty : loadsFCL.reduce( (totalQty, shipment) => {
                  return totalQty += shipment.qty ? parseInt(shipment.qty,10) : 0;
                  }, 0),
            summary : loadsFCL.reduce( (totalVolume, shipment) => {
                    const { qty, size, type, weight } = shipment;
                    totalVolume[size] = !totalVolume.hasOwnProperty(size) ? {} : totalVolume[size];
                    totalVolume[size][type] = !totalVolume[size].hasOwnProperty(type) ? {} : totalVolume[size][type];
                    totalVolume[size][type][weight]= totalVolume[size][type][weight] ? (totalVolume[size][type][weight] + qty ) : qty;
                    return totalVolume
                  },{})
    }
    displayFCLShipment = _.map(totalShipment.summary, (size, sizeKey) => (
      _.map(size, (type, typeKey) => (
         _.map(type, (qty, weightKey) => (
           [<br/>, p.tu(`form.load.fcl.volume.${sizeKey}`, { smart_count : qty, type: p.tu(`form.load.fcl.type.${typeKey}`), weight : p.tu(`form.load.fcl.weight.${weightKey}`)})]
        ))
    ))));
  }
  return (
    <Card block>
      <Row>
        <Col md="2" className="bold">
          { p.tc('form.service') }
        </Col>
        <Col>
          { renderServiceType(query) }
        </Col>
      </Row>
      <Row>
        <Col md="2" className="bold">
          { p.tc('form.route.title')}
        </Col>
        <Col>
          { p.tu('form.route.origin', {origin_city : query.form.originCity, origin_country : query.form.originCountry})}
          {` `}<span className="fa fa-arrow-right"></span> {` `}
          { p.tu('form.route.destination', {destination_city : query.form.destinationCity, destination_country : query.form.destinationCountry})}
          <Button
            className="m-l-10"
            size="xs"
            outline
            color="success"
            onClick={backToSearch}>{p.tc('form.action.back_to_search')}</Button>
        </Col>
      </Row>
      <Row>
        <Col md="2" className="bold">
          { p.tc('form.load.title')}
        </Col>
        <Col>
          { query.shipmentLoad === 'lcl' && ([
            `${p.tu('form.load.lcl.qty', { smart_count : totalShipment.qty})}, `,
            `${p.tu('form.load.lcl.volume', { smart_count : totalShipment.volume})}, `,
            `${p.tu('form.load.lcl.weight', { smart_count : totalShipment.weight})}`])}
          { query.shipmentLoad === 'fcl' && ([
            `${p.tu('form.load.fcl.qty', { smart_count : totalShipment.qty })} `,
            displayFCLShipment
        ])}

        </Col>
      </Row>
      <Row>
        <Col md="2" className="bold">
          { p.tc('form.date.title')}
        </Col>
        <Col>
          { query.form.datePickup}
        </Col>
      </Row>
      {(query.form.insurance || query.form.custom) && (
        <Row>
          <Col md="2" className="bold">
            { p.tc('form.optional_services.title')}
          </Col>
          <Col>
            {query.form.insurance && (
              `${p.tu('form.optional_services.insurance')}`
            )}
            {' '}
            {query.form.custom && (
              `${p.tu('form.optional_services.custom')}`
            )}
          </Col>
        </Row>
      )}
    </Card>
  )
}

export default translate({polyglotScope : 'result'})(SearchSummary)
