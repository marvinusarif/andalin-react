import React from 'react';
import { Card, CardHeader, CardBlock, Row, Col } from 'reactstrap';
import { Field, FieldArray } from 'redux-form';
import translate from 'redux-polyglot/translate';
import { CheckBox } from '../../../../components/Bootstrap/Forms/';

const renderVendorsList = (props) => {
  const { fields, vendorOptions } = props;
  return ( <div>
    { fields.map( (vendor,index) => {
    return (
      <Row key={index}>
        <Col md="12">
          <Field
              label={`${vendorOptions[index]}`}
              className="form-control input-sm"
              value={`${vendorOptions[index]}`}
              name={`${vendor}`}
              component={CheckBox}/>
        </Col>
      </Row>
    )})} </div>)
}

const VendorsSelector = (props) => {
  const { vendorOptions, p } = props;
  return (
    <Card>
      <CardHeader tag="h6">
        {p.tc('title')}
      </CardHeader>
      <CardBlock>
        <FieldArray name="vendors" vendorOptions={vendorOptions} component={renderVendorsList}/>
      </CardBlock>
    </Card>)
}

export default translate({ polyglotScope : 'result.filter.providers'})(VendorsSelector)
