import React from 'react';
import { Card, CardHeader, CardBlock, Row, Col } from 'reactstrap';
import { Field, FieldArray } from 'redux-form';
import translate from 'redux-polyglot/translate';
import { CheckBox } from '../../../../components/Bootstrap/Forms/';

const renderShippingLineList = (props) => {
  const { fields, shippingLineOptions } = props;
  return ( <div>
    { fields.map( (shippingLine,index) => {
    return (
      <Row key={index}>
        <Col md="12">
          <Field
              label={`${shippingLineOptions[index]}`}
              className="form-control input-sm"
              value={`${shippingLineOptions[index]}`}
              name={`${shippingLine}`}
              component={CheckBox}/>
        </Col>
      </Row>
    )})} </div>)
}

const ShippingLinesSelector = (props) => {
  const { shippingLineOptions, p } = props;
  return (
    <Card>
      <CardHeader tag="h6">
        {p.tc('title')}
      </CardHeader>
      <CardBlock>
        <FieldArray name="shippingLines" shippingLineOptions={shippingLineOptions} component={renderShippingLineList}/>
      </CardBlock>
    </Card>)
}

export default translate({ polyglotScope : 'result.filter.shipping_lines'})(ShippingLinesSelector)
