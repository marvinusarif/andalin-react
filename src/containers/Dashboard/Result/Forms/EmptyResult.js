import React from 'react';
import { Link } from 'react-router-dom';
import { Container, Card, CardBlock } from 'reactstrap';

export const EmptyResult = (props) => {
  const { p } = props;
  return (
    <Container>
      <Card>
        <CardBlock className="text-center">
          {p.tc('empty_result')}
          <br/>
          <br/>
          <Link to="/search" className="btn btn-success">{p.tc('action.back_to_search')}</Link>
        </CardBlock>
      </Card>
    </Container>
  )
}
