import React, { Component } from 'react';
import { Container, Card, CardBlock, Table, Row, Col, Button as ButtonStrap } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { reduxForm, formValueSelector } from 'redux-form';
import { withRouter } from 'react-router-dom';
import { getP } from 'redux-polyglot';
import StarRatingComponent from 'react-star-rating-component';
import { Button } from '../../../components/Bootstrap/Forms/';
import { ReactLoader } from '../../../components/Dashboard/Loader/';
import SearchSummary from './Forms/SearchSummary';
import PriceSelector from './Forms/Price';
import VendorsSelector from './Forms/Vendors';
import RatingsSelector from './Forms/Ratings';
import ModesSelector from './Forms/Modes';
import ShippingLinesSelector from './Forms/ShippingLines';
import { EmptyResult } from './Forms/EmptyResult';
import { PriceTableOriginDestinationCharges, PriceTableFreightCharges, PriceTableSummary, PriceConversion } from './Forms/PriceTable';
import { sortResultBy } from '../../../actions/result';
import { postQuote } from '../../../actions/quote';
import { postBookValidateQuote } from '../../../actions/book';
import { fetchCurrencyRate, switchCurrency } from '../../../actions/currency';

const categories = [{
  label : 'sortBy.best_deal',
  field : 'vendorRating'
},{
  label : 'sortBy.fastest',
  field : 'orderTransittimeMax'
},{
  label : 'sortBy.cheapest',
  field : 'orderPriceTotal'
}]
class ResultForm extends Component {
  constructor(){
    super();
    this._renderSortBy = this._renderSortBy.bind(this);
    this._formSubmit = this._formSubmit.bind(this);
    this._handleSave= this._handleSave.bind(this);
    this._handleChangeCurrency = this._handleChangeCurrency.bind(this);
    this.state = {
      quotesOpen : []
    }
  }
  componentWillMount(){
    this.props.fetchCurrencyRate();
  }
  componentDidMount(){
    const { result : { quotes }, isFetching } = this.props;
  if(quotes.length < 1 && !isFetching){
      this.props.history.push('/search')
    }
  }
  _handlePriceComponent(selectedQuote){
    if(!this.state.quotesOpen.includes(selectedQuote)) {
      this.setState( (prevState) => ({
        quotesOpen : prevState.quotesOpen.concat(selectedQuote)
      }))
    }else{
      this.setState( (prevState) => ({
        quotesOpen : prevState.quotesOpen.filter(quote => quote !== selectedQuote)
      }))
    }
  }
  _renderSortBy(){
    const { result : {sortBy}, sortResultBy, p} = this.props;
    return categories.map((category,index) => (
      <ButtonStrap key={index} color="link" className={ sortBy === category.field  ? 'text-primary p-l-50 bold' : 'text-muted p-l-50' }
        onClick={ () => sortResultBy(category.field) }> {p.tc(category.label)} </ButtonStrap>
    ))
  }
  _compareFilter(arrForm, arrVal){
    return arrForm ? arrForm.map( (e,i) => {
              return e === true ? arrVal[i] : '' })
           .filter(e => e !== '') : [];
  }
  _handleSave(selectedQuote){
    const { query } = this.props;
    this.props.postQuote({quote : selectedQuote, query});
  }
  _formSubmit(selectedQuote){
    const { query } = this.props;
    this.props.history.push('/book');
    this.props.postBookValidateQuote({ quote : selectedQuote, query});
  }
  _renderServiceType({form}){
    const { originArea, destinationArea } = form;
    const originServiceType = originArea === 'fob' ? 'PORT' : 'DOOR';
    const destinationServiceType = destinationArea === 'fob' ? 'PORT' : 'DOOR';
    return `${originServiceType} TO ${destinationServiceType}`;
  }
  _backToSearch(){
    this.props.history.push('/search');
  }
  _handleChangeCurrency(e){
    const { target : { value }} = e;
    this.props.switchCurrency(value);
  }
  render(){
    const { quotesOpen } = this.state;
    const { handleSubmit, reset, submitting, isFetching, query, currency, formValueSelect : { price, modes, vendors, ratings, shippingLines }, result : { quotes, sortBy, summary, form }, p } = this.props;
    let quoteOptions = quotes.length > 0 ? quotes : [];
    if(quotes.length > 0 ) {
      const allowedModes = this._compareFilter(modes,summary.modes);
      const allowedVendors = this._compareFilter(vendors,summary.vendors);
      const allowedRatings = this._compareFilter(ratings,summary.ratings);
      const allowedShippingLines = this._compareFilter(shippingLines, summary.shippingLines);
      quoteOptions = quotes.filter( e => price >= e.orderPriceTotal)
                      .filter( e => allowedModes.includes(e.orderFreight))
                      .filter( e => allowedVendors.includes(e.vendorName))
                      .filter( e => allowedRatings.includes(e.vendorRating))
                      .filter( e => allowedShippingLines.includes(e.orderShippingLine))
                      .sort( (a,b) => {
                        return sortBy !== 'vendorRating' ? (a[sortBy] - b[sortBy]) : ( b[sortBy] - a[sortBy])
                      });
    }
    if(isFetching) {
      return <ReactLoader message={p.tc('loading')}/>
    }
    return (
        <Container>
          <Row>
            <Card>
              <CardBlock>
                <form onSubmit={handleSubmit(this._formSubmit.bind(this))}>
                <Row>
                  <Col md="12">
                    <SearchSummary
                      query={query}
                      renderServiceType={this._renderServiceType.bind(this)}
                      backToSearch={this._backToSearch.bind(this)}/>
                  </Col>
                </Row>
                { quotes.length < 1 ? (
                    <Row>
                      <Col md="12">
                        <EmptyResult p={p}/>
                      </Col>
                    </Row>
                ) : (
                <Row>
                  <Col md="3">
                    <Card block>
                      <Button
                        className="btn btn-danger"
                        type="button"
                        onClick={reset}
                        value={p.tu('filter.reset')}/>
                      <small className="text-primary text-center">{p.tc('filter.result_shown', { smart_count : quoteOptions.length})}</small>
                    </Card>
                    <PriceSelector
                      currency={currency}
                      currencyBase='USD'
                      onChangeSelect={this._handleChangeCurrency}
                      priceStep={summary.price.step ? summary.price.step : 1}
                      priceMin={summary.price.min ? summary.price.min : 0}
                      priceMax={summary.price.max ? summary.price.max : 1}/>
                    <VendorsSelector vendorOptions={summary.vendors}/>
                    <RatingsSelector ratingOptions={summary.ratings}/>
                    <ModesSelector modeOptions={summary.modes}/>
                    <ShippingLinesSelector shippingLineOptions={summary.shippingLines}/>
                  </Col>
                  <Col md="9">
                      <Card block>
                        <Row>
                          <Col md="2" className="bold">
                            {p.tc('sortBy.title')}
                          </Col>
                          <Col>
                            {this._renderSortBy()}
                          </Col>
                        </Row>
                      </Card>
                      <Card block>
                        <Table responsive>
                          <thead>
                            <tr>
                              <th style={{width : '20%'}}>
                                {p.tc('display.header.service_providers')}
                              </th>
                              <th colSpan="4">
                                {p.tc('display.header.services')}
                              </th>
                              <th style={{width : '20%'}}>
                                {p.tu('display.header.price')}
                              </th>
                            </tr>
                          </thead>
                          <tbody>
                        { quoteOptions.map( (quote,index) => {
                          //const orderRoute = (quote.orderDirect) ?  `EXPRESS` : `${quote.orderPol} > ${quote.orderPod}`;
                          const orderRoute = '';
                          return (
                            [ <tr key={index}>
                              <td>
                                <p><img src={`/img/${quote.vendorImage}`} className="img-responsive-width" style={{ height : '50px'}} alt={quote.vendorName}/></p>
                                <small className="text-success">{quote.vendorName}</small>
                                <br/>
                                <StarRatingComponent
                                  name='vendorRating' /* name of the radio input, it is required */
                                  value={parseInt(quote.vendorRating,0)} /* number of selected icon (`0` - none, `1` - first) */
                                  starCount={5} /* number of icons in rating, default `5` */
                                  emptyStarColor={`#cdd6ec`}
                                  editing={false} /* is component available for editing, default `true` */
                              />
                                <br/>
                                { quote.vendorReviews && (
                                    <small className="text-muted">{quote.vendorReviews} {p.tc('display.content.reviews')}</small>
                                )}
                              </td>
                              <td colSpan="4">
                                <p className="semi-bold">
                                  { quote.orderFreight !== "OCEAN" ? <span className="fa fa-plane"></span> : <span className="fa fa-ship"></span>}
                                  { ` ` }
                                  { `${quote.orderOriginCity.toUpperCase()}`} >
                                  { orderRoute && (
                                    <div><strong> {orderRoute.toUpperCase()} </strong> > </div>
                                  ) }
                                  {` ${quote.orderDestinationCity.toUpperCase()}`}
                                </p>
                                <small><strong>{p.tc('display.content.delivery_estimate') }</strong> {quote.orderTransittimeMin} - {quote.orderTransittimeMax} {p.t('display.content.days')} </small>
                                <br/>
                                <small><strong>{p.tc('display.content.valid_until')}</strong> {quote.orderValidUntil}</small>
                                <br />
                                <small><strong>{p.tc('display.content.shipping_line')}</strong> {quote.orderShippingLine ? quote.orderShippingLine : p.tc('display.content.unknown')}</small>
                              </td>
                              <td className="text-center">
                                <h5 className="bold">
                                  <PriceConversion price={quote.orderPriceTotal} currency={currency} currencyBase={quote.orderPriceCurrencyCode}/>
                                </h5>
                                <Button
                                  className="btn btn-success btn-sm"
                                  onClick={handleSubmit(this._formSubmit.bind(this,quote))}
                                  type="submit"
                                  disabled={submitting}
                                  value={p.tu('display.content.action.book')}/>
                                <br/>
                                <br/>
                                <ButtonStrap size="xs" color="link" onClick={this._handleSave.bind(this,quote)}>{p.tu('display.content.action.save')}</ButtonStrap>
                                <br/>
                                <ButtonStrap color="link" className="text-primary" size="xs" onClick={this._handlePriceComponent.bind(this,quote.quoteId)}>{p.tc('display.content.action.price_detail')} <span className={`fa fa-${quotesOpen.includes(quote) ? 'arrow-circle-up' : 'arrow-circle-down' }`}></span> </ButtonStrap>
                              </td>
                          </tr>,
                          quotesOpen.includes(quote.quoteId) && ([
                            <tr>
                              <td colSpan="6" style={{background: '#c8d9ed'}}>
                                {quote.orderOriginCountryCode},{quote.orderOriginCityCode} {`${ quote.orderPol ? `> ${quote.orderPol}` : ''}`}
                              </td>
                            </tr>,
                            <tr>
                              <td colSpan="6" style={{background: '#f4f4f4'}}>
                                <PriceTableOriginDestinationCharges p={p} currency={currency} quote={quote} cityCode={quote.orderOriginCityCode} originOrDestination={'origin'}/>
                              </td>
                            </tr>,
                            <tr>
                              <td colSpan="6" style={{background: '#c8d9ed'}}>
                                {!quote.orderPol && !quote.orderPod ? (
                                  `${quote.orderOriginCityCode}, ${quote.orderOriginCountryCode} > ${quote.orderDestinationCityCode}, ${quote.orderDestinationCountryCode}`
                                ) : (
                                  `${quote.orderOriginCityCode}, ${quote.orderPol} > Port : ${quote.orderPod}, ${quote.orderOriginCityCode}`
                                )}
                              </td>
                            </tr>,
                            <tr>
                              <td colSpan="6" style={{background: '#f4f4f4'}}>
                                <PriceTableFreightCharges currency={currency} p={p} quote={quote}/>
                              </td>
                            </tr>,
                            <tr>
                              <td colSpan="6" style={{background: '#c8d9ed'}}>
                                {`${ quote.orderPol ? `${quote.orderPol} >` : ''}`} {quote.orderDestinationCountryCode}, {quote.orderDestinationCityCode}
                              </td>
                            </tr>,
                            <tr>
                              <td colSpan="6" style={{background: '#f4f4f4'}}>
                                <PriceTableOriginDestinationCharges p={p} currency={currency} quote={quote} cityCode={quote.orderDestinationCityCode} originOrDestination={'destination'}/>
                              </td>
                            </tr>,
                            <tr>
                              <td colSpan="6" style={{background: '#c8d9ed'}}>
                                {p.tu('display.detail.summary')}
                              </td>
                            </tr>,
                            <tr>
                              <td colSpan="6" style={{background: '#f4f4f4'}}>
                                <PriceTableSummary p={p} quote={quote} currency={currency}/>
                              </td>
                            </tr>
                          ])])
                        })}

                      </tbody>
                    </Table>
                    </Card>
                  </Col>
                </Row>
              )}
              </form>
            </CardBlock>
          </Card>
        </Row>
      </Container>)
  }
}
const selector = formValueSelector("ResultForm");

const mapStateToProps = (state,prevState) => {
  const { search, result, quote, currency } = state;
  const { isFetching } = result;
  const { query } = search;
  const _initialValues = {
    currency : 'USD',
    modes : [],
    price : 1,
    ratings : [],
    vendors : []
  }
  const p = getP(state, { polyglotScope : 'result'})
  const formValues = result.quotes.length > 0 ? result.form : _initialValues;
  return {
    formValueSelect : selector(state, 'price', 'vendors', 'ratings', 'modes', 'shippingLines'),
    initialValues : formValues,
    isFetching,
    query,
    quote,
    result,
    currency,
    p
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    sortResultBy,
    postQuote,
    postBookValidateQuote,
    fetchCurrencyRate,
    switchCurrency
  }, dispatch)
}

ResultForm = reduxForm({
  form : 'ResultForm',
  enableReinitialize : true,
  keepDirty :
})(ResultForm)

ResultForm = connect(mapStateToProps,mapDispatchToProps)(ResultForm)

export default withRouter(ResultForm)
