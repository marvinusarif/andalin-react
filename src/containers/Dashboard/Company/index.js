import { Container, TabContent, TabPane, Nav, NavItem, NavLink, Card, CardBlock, Row, Col } from 'reactstrap';
import classnames from 'classnames';
import React, {Component} from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getP } from 'redux-polyglot';
import CompanyInfoForm from './Forms/CompanyInfo';
import CompanyUsersForm from './Forms/CompanyUsers';
import CompanyDocuments from './Forms/CompanyDocuments';
import UserForm from '../User';

class Company extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab : 'company'
    }
    this._toggle = this._toggle.bind(this);
  }
  componentWillMount(){
    !this.props.user.session.isValid && this.props.history.push('/search');
  }
  _toggle(tab) {
    this.setState({
      activeTab : tab
    })
  }
  render(){
    const { p } = this.props;
    const { activeTab } = this.state;
    return(
      <Container>
        <Row>
          <Card>
            <CardBlock>
                <Nav tabs className='nav nav-tabs nav-tabs-simple'>
                  <NavItem>
                    <NavLink
                      className={classnames({ active : activeTab === 'company'})}
                      onClick={() => {this._toggle('company');}}>
                        {p.t('tabs.company')}
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink
                      className={classnames({ active : activeTab === 'companyDocuments'})}
                      onClick={() => {this._toggle('companyDocuments');}}>
                        {p.t('tabs.company_documents')}
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink
                      className={classnames({ active : activeTab === 'companyUsers'})}
                      onClick={() => {this._toggle('companyUsers');}}>
                        {p.t('tabs.company_users')}
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink
                      className={classnames({ active : activeTab === 'user'})}
                      onClick={() => {this._toggle('user');}}>
                        {p.t('tabs.user')}
                    </NavLink>
                  </NavItem>
                </Nav>
                <TabContent activeTab={activeTab}>
                  <TabPane tabId='company'>
                    <Row className="row column-seperation">
                      <Col sm="12">
                        <CompanyInfoForm/>
                      </Col>
                    </Row>
                  </TabPane>
                  <TabPane tabId='companyDocuments'>
                    <Row className="row column-seperation">
                      <Col sm="12">
                        <CompanyDocuments/>
                      </Col>
                    </Row>
                  </TabPane>
                  <TabPane tabId='companyUsers'>
                    <Row className="row column-seperation">
                      <Col sm="12">
                        <CompanyUsersForm/>
                      </Col>
                    </Row>
                  </TabPane>
                  <TabPane tabId='user'>
                    <Row className="row column-seperation">
                      <Col sm="12">
                        <UserForm/>
                      </Col>
                    </Row>
                  </TabPane>
                </TabContent>
            </CardBlock>
          </Card>
        </Row>
      </Container>
    )
  }
}

const mapStateToProps = (state,ownProps) => {
  const { user } = state;
  const p = getP(state, { polyglotScope : 'settings'} );
  return {
    user,
    p
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({

  }, dispatch)
}

Company = connect(mapStateToProps,mapDispatchToProps)(Company)

export default withRouter(Company)
