import React, { Component } from 'react';
import { Row, Col, Button as ButtonStrap } from 'reactstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { reduxForm, Field } from 'redux-form';
import { getP } from 'redux-polyglot';
import Logo from '../../../../assets/img/logo.png';
import { SelectFieldReact, FileInputField, Button } from '../../../../components/Bootstrap/Forms';
import { closeUploadFileModal } from '../../../../actions/page';
import { updateCompanyDocs } from '../../../../actions/company';
import { fileTypeOptions } from './';

class CompanyDocumentsForm extends Component {
  constructor(props){
    super(props);
    this.state = {
      fileType : null
    }
    this._submitForm = this._submitForm.bind(this);
    this._closeUploadFileModal = this._closeUploadFileModal.bind(this);
  }
  _closeUploadFileModal(){
    this.props.closeUploadFileModal();
  }
  _submitForm(form){
    this.setState({
      fileType : form.documentType
    });
    let formData = new FormData();
        formData.append('documentType', form.documentType)
        formData.append('fileDocument', form.fileDocument)
    this.props.updateCompanyDocs(formData);
  }
  render(){
    const { p,err, handleSubmit, submitting, pristine, uploadFile : { isFetching, percentCompleted, isSuccessful, message } } = this.props;
    const displayFileTypeOptions = fileTypeOptions.map(opt => ({
      label : p.tu(`type.${opt.label}`),
      value : opt.value
    }));
    if(isFetching && !isSuccessful) {
      return (
        <Row className="m-t-20">
          <Col md={{size:10, offset:1}} className="text-center">
            {p.tc('form.loading')}
            <br/>
            {percentCompleted}%
          </Col>
        </Row>
      )
    }
    if(!isFetching && isSuccessful){
      return (
      <div>
        <Row className="m-t-20">
          <Col md={{size : 4, offset : 1}} className="p-t-10">
            <img src={Logo} alt="logo"/>
          </Col>
          <Col md={{size: 2, offset : 3}}>
            <ButtonStrap outline onClick={this._closeUploadFileModal}><i className="fa fa-remove"></i> {p.tc('form.close')} </ButtonStrap>
          </Col>
        </Row>
        <Row>
          <Col md={{size : 10, offset: 1}} tag="h6" className="text-center">
              {p.tc('form.success', { fileType : this.state.fileType })}
          </Col>
        </Row>
      </div>
      )
    }
    return (
      <form onSubmit={handleSubmit(this._submitForm)}>
      <Row className="m-t-20">
        <Col md={{size : 5, offset : 1}} className="p-t-10">
          <img src={Logo} alt="logo"/>
        </Col>
        <Col md={{size: 2, offset : 3}}>
          <ButtonStrap outline onClick={this._closeUploadFileModal}><i className="fa fa-remove"></i> {p.tc('form.close')} </ButtonStrap>
        </Col>
      </Row>
      <Row>
        <Col md={{size:10, offset : 1}} tag="h3">
          {p.tc('form.title')}
        </Col>
      </Row>
      <Row>
        <Col md={{size:10, offset : 1}}>
          <small>{p.tc('form.description')}</small>
        </Col>
      </Row>
      <Row className='m-t-10'>
        <Col md={{size:10, offset : 1}}>
          <Field
            label={p.tu('type.label')}
            name='documentType'
            items={displayFileTypeOptions}
            placeholder={p.tc('type.placeholder')}
            clearable={false}
            simpleValue={true}
            component={SelectFieldReact}
            required='required'/>
        </Col>
      </Row>
      <Row>
        <Col md={{size:10, offset : 1}}>
          <Field
            label={p.tu('form.file_input.label')}
            className="form-control input-sm"
            name='fileDocument'
            component={FileInputField}
            required='required'/>
        </Col>
      </Row>
      { message && (
        <Row>
          <Col md={{size:10, offset : 1}} className="text-center">
            <small className="text-danger text-center">{err.tc(`${message}`)}</small>
          </Col>
        </Row>
      )}
      <Row>
        <Col md={{ size : 6, offset : 3}} className="text-center">
          <Button
              style={{margin : '20px 0px'}}
              className="btn btn-primary btn-block"
              type="submit"
              disabled={submitting || pristine || isFetching }
              value={isFetching ? p.tc('action.uploading') :  p.tc('action.upload')}/>
        </Col>
      </Row>
    </form>
    )
  }
}

CompanyDocumentsForm = reduxForm({
  form : 'CompanyDocumentsForm',
  enableReinitialize : true
})(CompanyDocumentsForm)

const mapStateToProps = (state,ownProps) => {
  const { page : { modal : { uploadFile }}} = state;
  const err = getP(state, {polyglotScope : 'error'});
  const p = getP(state, {polyglotScope : 'settings.docs' });
  return {
    uploadFile,
    p,
    err
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    closeUploadFileModal,
    updateCompanyDocs
  }, dispatch)
}
export default connect(mapStateToProps,mapDispatchToProps)(CompanyDocumentsForm)
