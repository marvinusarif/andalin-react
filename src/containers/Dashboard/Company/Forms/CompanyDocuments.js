import _ from 'lodash';
import React, { Component } from 'react';
import { Row, Col, Table, Button as ButtonStrap } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { getP } from 'redux-polyglot';
import { fetchCompanyDocs, downloadCompanyDoc} from '../../../../actions/company';
import { openUploadFileModal } from '../../../../actions/page';
import { Button } from '../../../../components/Bootstrap/Forms';

class CompanyDocuments extends Component {
  constructor(props){
    super(props);
    this.state = {
      editable : false
    };
    this._handleDownloadFile = this._handleDownloadFile.bind(this);
    this._handleUploadFileModal = this._handleUploadFileModal.bind(this);
  }
  componentDidMount(){
    const { user : { profile : { codeusertype }}} = this.props;
    this.setState({
      editable : codeusertype === 'SADM' || codeusertype === 'USRA' ? true : false
    })
  }
  componentWillMount(){
    const { user } = this.props;
    if(user.session.isValid) {
      this.setState({ editable : true })
      this.props.fetchCompanyDocs();
    }
  }
  _handleDownloadFile(saveAsFileName){
    const extFileName = this.props.company.documents[saveAsFileName];
    this.props.downloadCompanyDoc(saveAsFileName,extFileName);
  }
  _handleUploadFileModal(){
    this.props.openUploadFileModal();
  }
  render(){
    const { p, company } = this.props;
    const { editable } = this.state;
    return(
      <div>
      <Row>
        <Col md="12">
          <Table hover>
            <thead>
              <tr>
                <th>
                  {p.tc('type.title')}
                </th>
                <th>
                  {p.tc('uploaded.title')}
                </th>
                <th>

                </th>
            </tr>
          </thead>
          <tbody>
            {_.map(company.documents, (doc,key) => {
              return (
                <tr key={key}>
                  <td>
                    {p.tu(`type.options.${key}`)}
                  </td>
                  <td>
                    {doc ? p.tc('uploaded.options.exists') : p.tc('uploaded.options.not_exists')}
                  </td>
                  <td>
                    { doc && (
                      <div>
                      <ButtonStrap color="link" onClick={this._handleDownloadFile.bind(this,key)}>{p.tc('action.download')}</ButtonStrap>
                    </div>
                    )}
                  </td>
                </tr>
              )
            })}
          </tbody>
          </Table>
        </Col>
      </Row>
      <Row>
        <Col md="2">
          <Button type="button" onClick={this._handleUploadFileModal} value={p.tc('action.new_upload')} disabled={!editable} className="btn btn-block btn-primary"/>
        </Col>
      </Row>
    </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  const { company, user, page : { modal : { uploadFile }} } = state;
  const p = getP(state, { polyglotScope : 'settings.docs'});
  return {
    user,
    company,
    uploadFile,
    p
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    fetchCompanyDocs,
    downloadCompanyDoc,
    openUploadFileModal
  }, dispatch)
}

CompanyDocuments = connect(mapStateToProps,mapDispatchToProps)(CompanyDocuments)

export default withRouter(CompanyDocuments)
