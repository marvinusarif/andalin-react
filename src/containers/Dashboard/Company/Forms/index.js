import { industryOptions as IndustryOptions } from '../../../Register/Forms';

export const codeusertypeOptionsUser = [{
  label : 'options.USRA',
  value : 'USRA'
},{
  label : 'options.USRS',
  value : 'USRS'
}]
export const codeusertypeOptionsVendor = [{
  label : 'options.VDRA',
  value : 'VDRA'
},{
  label : 'options.VDRS',
  value : 'VDRS'
}]
export const codeusertypeOptionsAdm = [{
  label : 'options.SADM',
  value : 'SADM'
}]
export const fileTypeOptions = [{
  label : 'options.ktp',
  value : 'ktp'
},{
  label : 'options.npwp',
  value : 'npwp'
},{
  label : 'options.siup',
  value : 'siup'
},{
  label : 'options.tdp',
  value : 'tdp'
},{
  label : 'options.passport',
  value : 'passport'
},{
  label : 'options.license',
  value : 'license'
},{
  label : 'options.nikimport',
  value : 'nikimport'
},{
  label : 'options.nikexport',
  value : 'nikexport'
}]

export const industryOptions = IndustryOptions;
