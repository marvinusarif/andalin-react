import React, { Component } from 'react';
import { Row, Col, Table } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { reduxForm, FieldArray, Field } from 'redux-form';
import { withRouter } from 'react-router-dom';
import { getP } from 'redux-polyglot';
import { SelectField, CheckBox, Button } from '../../../../components/Bootstrap/Forms/';
import { fetchCompanyUsers, updateCompanyUsers } from '../../../../actions/company';
import { openInvitationModal } from '../../../../actions/page';
import { codeusertypeOptionsVendor, codeusertypeOptionsUser, codeusertypeOptionsAdm } from './';

const renderCompanyUsers = (props) => {
  const { fields, p, users, type, editable, currentUser, disabled, isFetching} = props;
  let displayCodeusertypeOptions;
  if(type === 'VENDOR'){
    displayCodeusertypeOptions = codeusertypeOptionsVendor.map(usertype => ({
      label : p.tc(usertype.label),
      value : usertype.value
    }))
  }else if(type === 'USER'){
    displayCodeusertypeOptions = codeusertypeOptionsUser.map(usertype => ({
      label : p.tc(usertype.label),
      value : usertype.value
    }))
  }else{
    displayCodeusertypeOptions = codeusertypeOptionsAdm.map(usertype => ({
      label : p.tc(usertype.label),
      value : usertype.value
    }))
  }
  return (
    <tbody>
      {
        fields.map( (companyUser, index) => (
          <tr key={index}>
            <td>{users[index].username}</td>
            <td>{users[index].gender}</td>
            <td>{users[index].fullname}</td>
            <td><Field
              className="form-control input-sm"
              name={`${companyUser}.codeusertype`}
              items={displayCodeusertypeOptions}
              component={SelectField}
              disabled={!editable || users[index].username === currentUser.profile.username ? true : false }
              required='required'/>
            </td>
            <td>
              <Field
                name={`${companyUser}.statverif`}
                component={CheckBox}
                disabled={!editable || users[index].username === currentUser.profile.username ? true : false }
              />
            </td>
          </tr>))
      }
      <tr>
        <td colSpan="4">
        </td>
        <td>
          <Button type="submit" value={isFetching ? p.tc('action.updating') :  p.tc('action.update_users')} disabled={disabled} className="btn btn-block btn-primary"/>
        </td>
      </tr>
    </tbody>
  )
}
class CompanyUsersForm extends Component {
  constructor(props){
    super(props);
    this.state = {
      editable : false
    }
    this._submitForm = this._submitForm.bind(this);
    this._handleInviteUser = this._handleInviteUser.bind(this);
  }
  _handleInviteUser(){
    this.props.openInvitationModal();
  }
  componentDidMount(){
    const { user : { profile : { codeusertype }}} = this.props;
    this.setState({
      editable : codeusertype === 'SADM' || codeusertype === 'USRA' ? true : false
    })
  }
  componentWillMount(){
    const { user } = this.props;
    if(user.session.isValid) {
      this.setState({ editable : true });
      this.props.fetchCompanyUsers();
    }
  }
  _submitForm(form){
    this.props.updateCompanyUsers(form);
  }
  render(){
    const { p, user, company, handleSubmit, submitting, pristine } = this.props;
    const { editable } = this.state;
    return(
      <form onSubmit={handleSubmit(this._submitForm.bind(this))}>
        <Row>
          <Col md="12">
            <Table>
              <thead>
              <tr>
                <th>
                  {p.tu('username')}
                </th>
                <th>
                  {p.tu('gender')}
                </th>
                <th>
                  {p.tu('fullname')}
                </th>
                <th>
                  {p.tu('codeusertype')}
                </th>
                <th>
                  {p.tu('statverif')}
                </th>
              </tr>
              </thead>
              <FieldArray name="companyUsers"
                      p={p}
                      disabled={submitting || pristine}
                      component={renderCompanyUsers}
                      users={company.users}
                      currentUser={user}
                      isFetching={company.isFetching}
                      type={company.profile.type}
                      editable={editable}/>
          </Table>
          </Col>
        </Row>
        <Row>
          <Col md="2">
            <Button type="button" onClick={this._handleInviteUser} value={p.tc('action.invite')} disabled={!editable} className="btn btn-block btn-primary"/>
          </Col>
        </Row>
    </form>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  const { user, company } = state;
  const p = getP(state, { polyglotScope : 'settings.users'});
  const _initialValues = {
    companyUsers : company.users.length > 0 ? company.users : []
  }
  return {
    user,
    company,
    p,
    initialValues : _initialValues
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    fetchCompanyUsers,
    updateCompanyUsers,
    openInvitationModal
  }, dispatch)
}

CompanyUsersForm = reduxForm({
  form : 'CompanyUsersForm',
  enableReinitialize : true
})(CompanyUsersForm)

CompanyUsersForm = connect(mapStateToProps,mapDispatchToProps)(CompanyUsersForm)

export default withRouter(CompanyUsersForm)
