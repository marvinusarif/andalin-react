export const validateCompanyInfo = (values, { p }) => {
  const errors = {}
  if(!values.companyName){
    errors.companyName = p.tc('validation.required');
  }
  if(!values.companyAddress){
    errors.companyAddress = p.tc('validation.required');
  }
  if(!values.companyEmail){
    errors.companyEmail = p.tc('validation.required');
  }else if(!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.companyEmail)){
    errors.companyEmail = p.tc('validation.email_invalid');
  }
  if(!values.companyName){
    errors.companyName = p.tc('validation.required');
  }
  if(!values.companyPhone){
    errors.companyPhone = p.tc('validation.required');
  }
  if(!values.companyFax){
    errors.companyFax = p.tc('validation.required');
  }
  if(!values.companyCountry){
    errors.companyCountry = p.tc('validation.required');
  }
  if(!values.companyIndustry){
    errors.companyIndustry = p.tc('validation.required');
  }
  if(!values.companyDescription){
    errors.companyDescription = p.tc('validation.required');
  }
  return errors;
}

export const validateCompanyDocuments = (values, { p }) => {
  const errors = {}
  if(!values.documents){
    errors.documents = p.tc('form.validation.required')
  }
}
