import React, { Component } from 'react';
import { Row, Col } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';
import { withRouter } from 'react-router-dom';
import { getP } from 'redux-polyglot';
import { Button, TextField, TextArea, SelectFieldReact } from '../../../../components/Bootstrap/Forms/';
import { industryOptions } from './';
import { fetchCompany, updateCompany } from '../../../../actions/company';
import { fetchAllCountries } from '../../../../actions/page';
import { validateCompanyInfo } from './Validation';

class CompanyInfoForm extends Component {
  constructor(props){
    super(props);
    this.state = {
      editable : false
    }
    this._submitForm = this._submitForm.bind(this);
  }
  componentDidMount(){
    const { user : { profile : { codeusertype }}} = this.props;
    this.setState({
      editable : codeusertype === 'SADM' || codeusertype === 'USRA' ? true : false
    })
  }
  componentWillMount(){
    const { user, formValue } = this.props;
    if(user.session.isValid) {
      this.setState({ editable : true })
      this.props.fetchCompany();
    }
    if(formValue.countries.length < 1){
      this.props.fetchAllCountries();
    }
  }
  _submitForm(form){
    this.props.updateCompany(form);
  }
  render(){
    const { p, formValue, handleSubmit, isFetching, submitting, pristine, reset } = this.props;
    const { editable } = this.state;
    const displayIndustryOptions = industryOptions.map(opt => ({
      label : p.tc(`company_industry.${opt.label}`),
      value : opt.value
    }));
    return(
      <form onSubmit={handleSubmit(this._submitForm.bind(this))}>
        <Row>
          <Col md="8">
            <Field
              label={p.tu('company_name.label')}
              className="form-control input-sm"
              name='companyName'
              placeholder={p.tc('company_name.placeholder')}
              component={TextField}
              disabled={!editable}
              required='required'/>
          </Col>
          <Col md="4">
            <Field
              label={p.tu('company_industry.label')}
              name='companyIndustry'
              placeholder={p.tc('company_industry.placeholder')}
              clearable={false}
              simpleValue={true}
              items={displayIndustryOptions}
              component={SelectFieldReact}
              disabled={!editable}
              required='required'/>
          </Col>
        </Row>
        <Row>
          <Col md="8">
            <Field
              label={p.tu('company_address.label')}
              className="form-control input-sm"
              name='companyAddress'
              placeholder={p.tc('company_address.placeholder')}
              component={TextField}
              disabled={!editable}
              required='required'/>
          </Col>
          <Col md="4">
            <Field
              label={p.tu('company_country.label')}
              placeholder={p.tc('company_country.placeholder')}
              name='companyCountry'
              clearable={false}
              simpleValue={true}
              items={formValue.countries}
              component={SelectFieldReact}
              disabled={!editable}
              required='required'/>
          </Col>
        </Row>
        <Row>
          <Col md="4">
            <Field
              label={p.tu('company_email.label')}
              className="form-control input-sm"
              name='companyEmail'
              placeholder={p.tc('company_email.placeholder')}
              component={TextField}
              disabled={!editable}
              required='required'/>
          </Col>
          <Col md="4">
            <Field
              label={p.tu('company_phone.label')}
              className="form-control input-sm"
              name='companyPhone'
              placeholder={p.tc('company_phone.placeholder')}
              component={TextField}
              disabled={!editable}
              required='required'/>
          </Col>
          <Col md="4">
            <Field
              label={p.tu('company_fax.label')}
              className="form-control input-sm"
              name='companyFax'
              placeholder={p.tc('company_fax.placeholder')}
              component={TextField}
              disabled={!editable}
              required='required'/>
          </Col>
        </Row>
        <Row>
          <Col md="8">
            <Field
              label={p.tu('company_description.label')}
              className="form-control input-sm"
              name='companyDescription'
              placeholder={p.tc('company_description.placeholder')}
              component={TextArea}
              disabled={!editable}
              required='required'/>
          </Col>
        </Row>
        <Row>
          <Col md="8"></Col>
          <Col md="2" className="text-center">
            <Button
              style={{margin : '20px 0px'}}
              className="btn btn-danger btn-block"
              type="button"
              onClick={reset}
              value={p.tc('action.reset')}/>
          </Col>
          <Col md="2" className="text-center">
            <Button
              style={{margin : '20px 0px'}}
              className="btn btn-primary btn-block"
              type="submit"
              disabled={submitting || pristine || !editable}
              value={ isFetching ? p.tc('action.updating') :  p.tc('action.update_button')}/>
          </Col>
        </Row>
      </form>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  const { user, company : { isFetching, profile } , page : { modal : { register : { formValue }}}} = state;
  const _initialValues = profile ? {
    companyName : profile.name,
    companyAddress : profile.address,
    companyPhone : profile.phone,
    companyFax : profile.fax,
    companyEmail : profile.email,
    companyCountry : profile.country,
    companyIndustry : profile.industry,
    companyDescription : profile.description
  } : {};
  const p = getP(state, { polyglotScope : 'register'});
  return {
    user,
    formValue,
    isFetching,
    p,
    initialValues : _initialValues
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    fetchAllCountries,
    fetchCompany,
    updateCompany
  }, dispatch)
}

CompanyInfoForm = reduxForm({
  form : 'CompanyInfoForm',
  enableReinitialize : true,
  validate : validateCompanyInfo
})(CompanyInfoForm)

CompanyInfoForm = connect(mapStateToProps,mapDispatchToProps)(CompanyInfoForm)

export default withRouter(CompanyInfoForm)
