import React, { Component } from 'react';
import { Switch, Route, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Main from './Main';
import Header from './Header';
import Footer from './Footer';
import Breadcrumb from './Breadcrumb';
import Quickview from './Quickview';
import SearchForm from './Search';
import ResultForm from './Result';
import BookForm from './Book';
import Quote from './Quote';
import Shipment from './Shipment';
import Company from './Company';
import Verify from './Verify';
import ResetPasswordForm from './ResetPassword';
import InvitationForm from './Invitation';

class Dashboard extends Component {

  render(){
    return (
      <div>
        {/*<!-- START PAGE-CONTAINER -->*/}
        <div className="page-container">
          <Header />
          {/*<!-- START PAGE CONTENT WRAPPER -->*/}
          <div className="page-content-wrapper full-height">
            {/*<!-- START PAGE CONTENT -->*/}
            <div className="content">
              <div className="container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
                <Breadcrumb/>
                <Switch>
                  <Route exact path="/user/verify" component={Verify}/>
                  <Route exact path="/user/resetPassword" component={ResetPasswordForm}/>
                  <Route exact path="/user/invitation" component={InvitationForm}/>
                  <Route exact path="/result" component={ResultForm}/>
                  <Route exact path="/book" component={BookForm}/>
                  <Route exact path="/quotes" component={Quote}/>
                  <Route exact path="/shipments" component={Shipment}/>
                  <Route exact path="/settings" component={Company}/>
                  <Route exact path="/home" component={Main}/>
                  <Route path="/" component={SearchForm}/>
                </Switch>
              </div>
            </div>
            {/*<!-- END PAGE CONTENT -->*/}
          <Footer/>
        </div>
        {/*<!-- END PAGE CONTENT WRAPPER -->*/}
      </div>
      {/*<!-- END PAGE CONTAINER -->*/}
      <Quickview/>
      </div>

    )
  }
}
const mapStateToProps = (state,ownProps) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({

  }, dispatch)
}

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(Dashboard))
