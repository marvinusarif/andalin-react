import React, { Component } from 'react';
import { Row, Col, Button as ButtonStrap } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';
import { withRouter } from 'react-router-dom';
import { getP } from 'redux-polyglot';
import { SelectField, TextField, Button } from '../../../components/Bootstrap/Forms/';
import { postCompanyUsersJoin } from '../../../actions/company';
import { openLoginModal } from '../../../actions/page';
import { sexOptions } from './Forms/';
import { validate } from './Forms/Validation';
import * as queryString from 'query-string';

class InvitationForm extends Component {
  constructor(props){
    super(props);
    this._submitForm = this._submitForm.bind(this);
    this._handleLogin = this._handleLogin.bind(this);
  }
  _handleLogin(){
    this.props.openLoginModal();
  }
  _submitForm(form){
    if(queryString.parse(this.props.location.search)){
      const registrationCode = queryString.parse(this.props.location.search).code;
      if(registrationCode){
        const submitValue = {
          ...form,
          registrationCode
        }
        this.props.postCompanyUsersJoin(submitValue);
      }
    }
  }
  render(){
    const { p, pt, handleSubmit, submitting, pristine, invitation : { isFetching, isSuccessful } } = this.props;
    const displaySexOptions = sexOptions.map(opt => ({
      label : p.tc(`sex.${opt.label}`),
      value : opt.value
    }));
    if(isFetching && !isSuccessful){
      return (
        <Row>
          <Col md={{offset : 1, size : 10}} className="text-center">
            { pt.tc('loading') }
          </Col>
        </Row>
      )
    }else if(!isFetching && isSuccessful){
      return (
        <div>
          <Row>
            <Col md={{size : 10, offset: 1}} tag="h6">
              {pt.tc('success')}
            </Col>
          </Row>
          <Row>
            <Col md={{size : 2, offset : 1}}>
              <ButtonStrap color="primary" onClick={this._handleLogin}> { pt.tc('action.login') }</ButtonStrap>
            </Col>
          </Row>
        </div>
      )
    }
    return(
      <form onSubmit={handleSubmit(this._submitForm.bind(this))}>
        <Row>
          <Col md={{offset : 1, size : 11}} tag="h3">
            {pt.tc('title')}
          </Col>
        </Row>
        <Row>
          <Col md={{offset : 1, size : 11}} tag="h6">
            {pt.tc('description')}
          </Col>
        </Row>
        <Row>
          <Col md={{ offset : 1, size : 10}}>
            <Row>
              <Col md="6">
                <Field
                  label={p.tu('fullname.label')}
                  className="form-control input-sm"
                  name='userFullname'
                  placeholder={p.tc('fullname.placeholder')}
                  component={TextField}
                  required='required'/>
              </Col>
              <Col md="2">
                <Field
                  label={p.tu('sex.label')}
                  className="form-control input-sm"
                  name='userSex'
                  items={displaySexOptions}
                  component={SelectField}
                  required='required'/>
              </Col>
            </Row>
            <Row>
              <Col md="4">
                <Field
                  label={p.tu('phone.label')}
                  className="form-control input-sm"
                  name='userPhone'
                  placeholder={p.tc('phone.placeholder')}
                  component={TextField}
                  required='required'/>
              </Col>
              <Col md="4">
                <Field
                  label={p.tu('job.label')}
                  className="form-control input-sm"
                  name='userJob'
                  placeholder={p.tc('job.placeholder')}
                  component={TextField}
                  required='required'/>
              </Col>
            </Row>
            <Row>
              <Col md="4">
                <Field
                  label={p.tu('password_change.label')}
                  type='password'
                  className="form-control input-sm"
                  name='userPassword'
                  placeholder={p.tc('password_change.placeholder')}
                  component={TextField}
                  required='required'/>
              </Col>
              <Col md="4">
                <Field
                  label={p.tu('password_confirm.label')}
                  type='password'
                  className="form-control input-sm"
                  name='userPasswordConfirm'
                  placeholder={p.tc('password_confirm.placeholder')}
                  component={TextField}
                  required='required'/>
              </Col>
              </Row>
              <Row>
                <Col md="3" className="text-center">
                  <Button
                    style={{margin : '20px 0px'}}
                    className="btn btn-primary btn-block"
                    type="submit"
                    disabled={submitting || pristine }
                    value={pt.tc('action.submit')}/>
                </Col>
              </Row>
            </Col>
        </Row>

  </form>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  const { page : { invitation }} = state;
  const p = getP(state, { polyglotScope : 'register'});
  const pt = getP(state, { polyglotScope : 'invitation'});
  return {
    p,
    pt,
    invitation,
    initialValues : { userSex : 'M' }
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    postCompanyUsersJoin,
    openLoginModal
  }, dispatch)
}

InvitationForm = reduxForm({
  form : 'invitationForm',
  enableReinitialize : true,
  validate
})(InvitationForm)

InvitationForm = connect(mapStateToProps,mapDispatchToProps)(InvitationForm)

export default withRouter(InvitationForm)
