import Moment from 'moment';
import { Container, TabContent, TabPane, Nav, NavItem, NavLink, Card, CardBlock, Row, Col } from 'reactstrap';
import classnames from 'classnames';
import React, {Component} from 'react';
import { withRouter } from 'react-router-dom';
import { reduxForm, formValueSelector } from 'redux-form';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getP } from 'redux-polyglot';
import { switchTab, postQuery } from '../../../actions/search';
import { openLoginModal } from '../../../actions/page';
import { ButtonV } from '../../../components/Bootstrap/Forms/'
import FCLInfo from './Forms/FCLInfo.js';
import LCLInfo from './Forms/LCLInfo.js';
import PickupInfo from './Forms/PickupInfo';
import DeliverInfo from './Forms/DeliverInfo';
import OptionalServices from './Forms/OptionalServices';
import { validate } from './Forms/Validation';

class SearchForm extends Component {
  constructor(props) {
    super(props);
    this._toggle = this._toggle.bind(this);
    this._submitForms = this._submitForms.bind(this);
  }
  _toggle(tab) {
    if (this.props.search.activeTab !== tab) {
      this.props.switchTab(tab);
    }
  }
  _submitForms(form){
    const submitValues = {
      shipmentLoad : this.props.search.activeTab,
      form
    }
    this.props.postQuery(submitValues);
    if(this.props.user.session.isValid){
      this.props.history.push('/result');
    }else{
      this.props.openLoginModal();
    }
  }

  render(){
    const { handleSubmit, submitting, formValueSelect, p } = this.props;
    const { originArea, originCountry, destinationArea, destinationCountry, loadsFCL, loadsLCL, insurance, custom } = formValueSelect;

    return(
      <Container>
        <Row>
          <Card>
            <CardBlock>
              <form onSubmit={handleSubmit(this._submitForms.bind(this))}>
                <Nav tabs className='nav nav-tabs nav-tabs-simple'>
                  <NavItem>
                    <NavLink
                      className={classnames({ active : this.props.search.activeTab === 'lcl'})}
                      onClick={() => {this._toggle('lcl');}}>
                        {p.t('tabs.boxes')}
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink
                      className={classnames({ active : this.props.search.activeTab === 'fcl'})}
                      onClick={() => {this._toggle('fcl');}}>
                        {p.t('tabs.containers')}
                    </NavLink>
                  </NavItem>
                </Nav>
                <TabContent activeTab={this.props.search.activeTab}>
                  <TabPane tabId='lcl'>
                    <Row className="row column-seperation">
                      <Col sm="12">
                        <LCLInfo loadsLCL={loadsLCL}/>
                      </Col>
                    </Row>
                  </TabPane>
                  <TabPane tabId='fcl'>
                    <Row className="row column-seperation">
                      <Col sm="12">
                        <FCLInfo loadsFCL={loadsFCL}/>
                      </Col>
                    </Row>
                  </TabPane>
                </TabContent>
                  <PickupInfo originArea={originArea} originCountry={originCountry ? originCountry : _initialValues.originCountry}/>
                  <DeliverInfo destinationArea={destinationArea} destinationCountry={destinationCountry ? destinationCountry : _initialValues.destinationCountry}/>

                  <Row className="row clearfix">
                    <Col md="10">
                      <OptionalServices insurance={insurance} custom={custom}/>
                    </Col>
                    <Col md="2">
                      <ButtonV
                        className="btn btn-success"
                        type="submit"
                        icon="search"
                        disabled={submitting}
                        value={p.tc('form.action.search_button')}/>
                    </Col>
                  </Row>
              </form>
            </CardBlock>
          </Card>
        </Row>
      </Container>
    )
  }
}

const selector = formValueSelector("SearchForm");
const _initialValues = {
  originArea : 'fob',
  originCountry : 'IDN',
  originCity : 'JKT',
  datePickup : Moment().format('DD/MM/YY'),
  destinationArea : 'fob',
  destinationCountry : 'SGP',
  destinationCity : 'SIN',
  loadsLCL : [{ size : { length_ : 10, width : 10, height : 10, unit : 'cm'} , qty : 1, type: 'box', weight : 10, weightUnit : 'kg'}],
  loadsFCL : [{ size: 'ft20', qty: 1, type: 'dry', weight: 'standard' }],
  insurance : "",
  goodsValueCurrency : 'IDR',
  goodsValue : '0',
  dangerousGoods : "",
  custom : ""
}
const mapStateToProps = (state,ownProps) => {
  const formValues = Object.keys(state.search.query.form).length > 0 ? state.search.query.form : _initialValues;
  const { search, user} = state;
  const p = getP(state, { polyglotScope: 'search' });
  return {
    search,
    user,
    p,
    formValueSelect : selector(state, 'originArea', 'originCountry', 'destinationArea', 'destinationCountry', 'loadsFCL', 'loadsLCL', 'insurance', 'custom'),
    initialValues : formValues
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
      switchTab,
      postQuery,
      openLoginModal
      }, dispatch)
}


SearchForm = reduxForm({
  form : 'SearchForm',
  enableReinitialize : true,
  destroyOnUnmount : true,
  forceUnregisterOnUnmount : true,
  validate
})(SearchForm)

SearchForm = connect(mapStateToProps,mapDispatchToProps)(SearchForm)

export default withRouter(SearchForm);
