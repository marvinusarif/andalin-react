import React from 'react';
import { fclOptions, containerTypeOptions, containerWeightOptions, containerTypeOptionsDryOnly } from './index'
import { Row, Col } from 'reactstrap';
import { Field, FieldArray } from 'redux-form';
import translate from 'redux-polyglot/translate';
import { min, number } from './Validation';
import { TextField, Button, SelectField } from '../../../../components/Bootstrap/Forms/'


const renderFCLLoads = (props) => {
  const { fields, loadsFCL, p } = props;
  let totalShipment = {};
  if(loadsFCL){
    totalShipment = {
      qty : loadsFCL.reduce( (total,shipment) => {
              return total += shipment.qty ? parseInt(shipment.qty,10) : 0;
            }, 0)
    }
  }
  const convertedFCLOptions = fclOptions.reduce( (types,type) => {
    const { value, label } = type;
    return types.concat({ value, label : p.tu(label)});
  }, []);
  const convertedContainerTypeOptions = containerTypeOptions.reduce( (types,type) => {
    const { value, label } = type;
    return types.concat([{ value, label : p.tu(label)}]);
  }, []);
  const convertedContainerTypeOptionsDryOnly = containerTypeOptionsDryOnly.reduce( (types,type) => {
    const { value, label } = type;
    return types.concat([{ value, label : p.tu(label)}]);
  }, [])
  const convertedContainerWeightUnitOptions = containerWeightOptions.reduce( (weights,weight) => {
    const { value, label } = weight;
    return weights.concat([{ value, label : p.tu(label)}]);
  }, []);
  return ( <div>
            <Row>
              <Col md="12">
                { fields.map( (load, index) => (
                        <Row className="form-group" key={index} style={{height : '75px'}}>
                            <Col md="1">
                              <h4 style={{padding: '22px 30px'}}><strong>{`#${index+1}`}</strong></h4>
                            </Col>
                            <Col md="1">
                              <Field
                                className="form-control input-sm"
                                type="number"
                                label={p.tu('qty')}
                                name={`${load}.qty`}
                                normalize={(val) => (min(number(val)))}
                                component={TextField}
                                required='required'/>
                            </Col>
                            <Col md="4">
                              <Field
                                label={p.tu('size')}
                                className="form-control input-sm"
                                name={`${load}.size`}
                                items={convertedFCLOptions}
                                component={SelectField}
                                required='required'/>
                            </Col>
                            <Col md="2">
                              <Field
                                label={p.tu('type')}
                                className="form-control input-sm"
                                name={`${load}.type`}
                                items={loadsFCL ? (
                                  loadsFCL[index].size !== 'hc40' ? convertedContainerTypeOptions : convertedContainerTypeOptionsDryOnly ) : (
                                  convertedContainerTypeOptions
                                  ) }
                                component={SelectField}
                                required="required"/>
                            </Col>
                            <Col md="2">
                              <Field
                                label={p.tu('weight')}
                                className="form-control input-sm"
                                name={`${load}.weight`}
                                items={convertedContainerWeightUnitOptions}
                                component={SelectField}
                                required="required"/>
                            </Col>
                            <Col>
                              <Col className="form-group" style={{padding : '35px 5px'}}>
                              { fields.length > 1 ? (
                                <Button
                                  className="btn btn-danger btn-xs"
                                  icon="remove"
                                  onClick={() => fields.remove(index)}/>
                              ) : (
                                <Button
                                  className="btn btn-info btn-xs"
                                  icon="remove"/>
                              )}
                            </Col>
                          </Col>
                      </Row>
                    ))}
              </Col>
            </Row>
            <Row>
              <Col md="8">
                <Button
                  className="btn btn-primary btn-sm"
                  onClick={() => fields.push({size: 'ft20', qty: 1, type : 'dry', weight : 'standard'})}
                  value={p.tc('add_load')}
                  icon="plus"/>
              </Col>
              <Col md="4">
                <p className="text text-primary">{p.tu('total_shipment', {smart_count:  totalShipment.qty})}</p>
              </Col>
            </Row></div>
          )
}

const FCLInfo = (props) => {
  const { loadsFCL, p } = props
  return (
    <div>
      <h5>{p.tu('title')}</h5>
      <FieldArray name="loadsFCL" p={p} component={renderFCLLoads} loadsFCL={loadsFCL}/>
    </div>
  )
}

export default translate({ polyglotScope : 'search.form.fcl' })(FCLInfo)
