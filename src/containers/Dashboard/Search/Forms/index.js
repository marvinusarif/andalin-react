export const fclOptions = [{
    value : 'ft20',
    label : 'sizeOptions.ft20'
  },{
    value : 'ft40',
    label : 'sizeOptions.ft40'
  },{
    value : 'hc40',
    label : 'sizeOptions.hc40'
  }]
export const lclOptions = [{
    value : 'box',
    label : 'typeOptions.boxes'
  },{
    value : 'pallet',
    label : 'typeOptions.pallets'
  }]

export const containerTypeOptions = [{
    value : 'dry',
    label : 'typeOptions.dry'
  },{
    value : 'reefer',
    label : 'typeOptions.reefer'
  },{
    value : 'ventilate',
    label : 'typeOptions.ventilate'
  }]
export const containerTypeOptionsDryOnly = [{
    value : 'dry',
    label : 'typeOptions.dry'
  }]
export const containerWeightOptions = [{
    value : 'standard',
    label : 'weightOptions.standard'
  },{
    value : 'overweight',
    label : 'weightOptions.overweight'
  }]
export const sizeUnitOptions = [{
    value : 'cm',
    label : 'cm'
  },{
    value : 'in',
    label : 'in'
  }]

export const weightUnitOptions = [{
  value : 'kg',
  label : 'kg'
},{
  value : 'lbs',
  label : 'lbs'
}]

export const areaOptions = [
  {
    value : 'factory',
    label : 'areaOptions.factory.label'
  },{
    value : 'business',
    label : 'areaOptions.business.label'
  },{
    value : 'residential',
    label : 'areaOptions.residential.label'
  },{
    value : 'fob',
    label : 'areaOptions.fob.label'
  }]

export const currencyOptions = [
    { value : 'USD',
      label : 'USD (US$)' },
    { value : 'IDR',
      label : 'IDR (Rp)'}
  ]
