import React from 'react';
import { Row, Col } from 'reactstrap';
import { Field } from 'redux-form';
import { SelectField, CheckBox, NumberField } from '../../../../components/Bootstrap/Forms/';
import { currencyOptions} from './index';
import translate from 'redux-polyglot/translate';

export const OptionalServices = (props) => {
  const { insurance, p} = props;
  return (
    <Row>
      <Col md="7">
        <h5>{p.tc('optional_services.title')}</h5>
        <Row className="row clearfix">
          <Col md="4">
            <Field name="insurance"
              label={p.tc('optional_services.insurance')}
              component={CheckBox}/>
          </Col>
          <Col md="8">
            <Field name="dangerousGoods"
              label={p.tc('optional_services.dangerous_goods')}
              component={CheckBox}/>
          </Col>
        </Row>
        { insurance && (
        <Row>
          <Col md="3">
            <Field
              label={p.tu('optional_services.currency')}
              className="form-control input-sm"
              name='goodsValueCurrency'
              items={currencyOptions}
              component={SelectField}
              required='required'/>
          </Col>
          <Col md="6">
            <Field
              className="form-control"
              label={p.tu('optional_services.goods_value.label')}
              placeholder={p.tc('optional_services.goods_value.placeholder')}
              name="goodsValue"
              component={NumberField}
              required='required'/>
          </Col>
        </Row>
        )}
      </Col>
      <Col md="5">
        <h5>{p.tc('custom_clearance.title')}</h5>
        <Row>
          <Col md="5">
            <Field name="custom"
              label={p.tc('custom_clearance.custom_brokerage')}
              component={CheckBox}/>
          </Col>
        </Row>
    </Col>
  </Row>)
}

export default translate({ polyglotScope : 'search.form'}) (OptionalServices)
