import React from 'react';
import translate from 'redux-polyglot/translate';
import { lclOptions, weightUnitOptions, sizeUnitOptions} from './index'
import { Row, Col } from 'reactstrap';
import { Field, FieldArray } from 'redux-form';
import { min, number } from './Validation';
import { TextField, Button, SelectField } from '../../../../components/Bootstrap/Forms/'

const renderLCLLoads = (props) => {
  const { fields, loadsLCL, p } = props;
  let totalShipment = {};
  if(loadsLCL){
    totalShipment = {
            qty : loadsLCL.reduce( (totalQty,shipment) => {
                    return totalQty += shipment.qty ? parseInt(shipment.qty,10) : 0;
                  }, 0),
            volume : loadsLCL.reduce( (totalVolume, shipment) => {
                    let volume = parseFloat(shipment.size.length_,2) * parseFloat(shipment.size.width,2) * parseFloat(shipment.size.height,2) / 1000000 * parseInt(shipment.qty,10)
                    volume = shipment.size.unit === 'in' ? volume / 35.3147 : volume
                    return totalVolume += !isNaN(volume) ? parseInt(Math.ceil(volume),10) : 0;
            }, 0),
            weight : loadsLCL.reduce( (totalWeight, shipment) => {
                    let weight = parseFloat(shipment.weight,2) * parseInt(shipment.qty,10)
                    weight = shipment.weightUnit === 'lbs' ? weight / 2.20462 : weight
                    return totalWeight += !isNaN(weight) ? parseFloat(Math.ceil(weight),2) : 0;
            }, 0)
          }
  }
  const convertedLCLOptions = lclOptions.reduce( (types,type) => {
    const { value, label } = type;
    return types.concat({ value, label : p.tu(label)});
  }, []);
  const convertedSizeUnitOptions = sizeUnitOptions.reduce( (sizes,size) => {
    const { value, label } = size;
    return sizes.concat([{ value, label : p.tu(label)}]);
  }, [])
  const convertedWeightUnitOptions = weightUnitOptions.reduce( (weights,weight) => {
    const { value, label } = weight;
    return weights.concat([{ value, label : p.tu(label)}]);
  }, [])

  return ( <div><Row>
            <Col md="12">
              { fields.map( (load, index) => (
                      <Row className="form-group" key={index} style={{height : '75px'}}>
                          <Col md="1">
                            <h4 style={{padding: '22px 30px'}}><strong>{`#${index+1}`}</strong></h4>
                          </Col>
                          <Col md="2">
                            <Field
                              label={p.tu('type')}
                              className="form-control input-sm"
                              name={`${load}.type`}
                              items={convertedLCLOptions}
                              component={SelectField}
                              required='required'/>
                          </Col>
                          <Col md="1">
                            <Field
                              className="form-control input-sm"
                              type="number"
                              label={p.tu('qty')}
                              name={`${load}.qty`}
                              component={TextField}
                              required='required'/>
                          </Col>
                          <Col md="4" className="form-group-attached">
                            <Row >
                              <Col md="3">
                                <Field
                                  className="form-control input-sm"
                                  type="number"
                                  label={p.tu('length_')}
                                  name={`${load}.size.length_`}
                                  component={TextField}
                                  required='required'/>
                              </Col>
                              <Col md="3">
                                <Field
                                  className="form-control input-sm"
                                  type="number"
                                  label={p.tu('width')}
                                  name={`${load}.size.width`}
                                  component={TextField}
                                  required='required'/>
                              </Col>
                              <Col md="3">
                                <Field
                                  className="form-control input-sm"
                                  type="number"
                                  label="height"
                                  name={`${load}.size.height`}
                                  component={TextField}
                                  required='required'/>
                              </Col>
                              <Col md="3">
                                <Field
                                  label={p.tu('size')}
                                  className="form-control input-sm"
                                  name={`${load}.size.unit`}
                                  items={convertedSizeUnitOptions}
                                  component={SelectField}
                                  required='required'/>
                              </Col>
                            </Row>
                          </Col>
                          <Col md="2" className="form-group-attached">
                            <Row>
                              <Col md="8">
                                <Field
                                  className="form-control input-sm"
                                  type="number"
                                  label={p.tu('weight')}
                                  name={`${load}.weight`}
                                  component={TextField}
                                  required='required'/>
                              </Col>
                              <Col md="4">
                                <Field
                                  label={p.tu('unit')}
                                  className="form-control input-sm"
                                  name={`${load}.weightUnit`}
                                  items={convertedWeightUnitOptions}
                                  component={SelectField}
                                  required="required"/>
                              </Col>
                            </Row>
                          </Col>
                          <Col md="2" style={{padding : '30px 0px'}} >
                            <Row>
                              <Col md="8">
                                { loadsLCL ? (
                                  <small className="text-primary" style={{fontSize: '10px', fontWeight: '500'}}>
                                    {loadsLCL[index].qty} { p.tu('unit') }, { ` `}
                                    {Math.ceil(loadsLCL[index].size.length_ * loadsLCL[index].size.width * loadsLCL[index].size.height / 1000000 * loadsLCL[index].qty)} {` `}
                                    {loadsLCL[index].size.unit==='cm' ? p.tu('cbm') : p.tu('cft')}, { ` `}
                                    {isNaN(loadsLCL[index].weight) || !loadsLCL[index].weight ? '0' : loadsLCL[index].weight * loadsLCL[index].qty } {` `}
                                    {loadsLCL[index].weightUnit === 'kg' ? p.tu('kg') : p.tu('lbs')}
                                  </small>
                                ) : '' }
                              </Col>
                              <Col md="4">
                                { fields.length > 1 ? (
                                  <Button
                                    className="btn btn-danger btn-xs"
                                    icon="remove"
                                    onClick={() => fields.remove(index)}/>
                                ) : (
                                  <Button
                                    className="btn btn-info btn-xs"
                                    icon="remove"/>
                                )}
                              </Col>
                            </Row>
                          </Col>
                    </Row>
                  ))}
            </Col>
            </Row>
            <Row>
              <Col md="8">
                <Button
                  className="btn btn-primary btn-sm"
                  onClick={() => fields.push({type:'box', qty:1, size:{length_ : 10, width: 10, height: 10, unit: 'cm'}, weight: 1, weightUnit : 'kg'})}
                  value={p.tc('add_load')}
                  icon="plus"/>
              </Col>
              <Col md="4">
                <p className="text text-primary">
                  {p.tu('total_shipment.title')}
                  {p.tu('total_shipment.qty', {smart_count:  totalShipment.qty})},
                  {p.tu('total_shipment.volume', {smart_count : totalShipment.volume})},
                  {p.tu('total_shipment.weight', {smart_count : totalShipment.weight})}
                </p>
              </Col>
            </Row></div>
          )
}
const LCLInfo = (props) => {
  const { loadsLCL,p } = props
  return (
    <div>
      <h5>{p.tu('title')}</h5>
      <FieldArray name="loadsLCL" p={p} component={renderLCLLoads} loadsLCL={loadsLCL}/>
    </div>
  )
}

export default translate({ polyglotScope : 'search.form.lcl'}) (LCLInfo)
