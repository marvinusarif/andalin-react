import React, { Component } from 'react';
import { areaOptions } from './index';
import { Row, Col } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Field } from 'redux-form';
import { SelectFieldReact, SelectField } from '../../../../components/Bootstrap/Forms/'
import { fetchCountriesDestination, fetchCitiesDestination } from '../../../../actions/search';
import { getP } from 'redux-polyglot';

class DeliverInfo extends Component {
  componentDidMount(){
    const { destinationCountry } = this.props;
    if(this.props.countries.destination.length < 1){
      this.props.fetchCountriesDestination();
    }
    if(!this.props.cities.destination[destinationCountry]){
      this.props.fetchCitiesDestination(destinationCountry);
    }
  }
  componentWillReceiveProps(nextProps){
    if(nextProps.destinationCountry !== this.props.destinationCountry) {
      if(!this.props.cities.destination[nextProps.destinationCountry]){
        this.props.fetchCitiesDestination(nextProps.destinationCountry);
      }
    }
  }
  render(){
    const { destinationArea, countries, cities, destinationCountry, p } = this.props;
    const citiesDisplay = cities.destination[destinationCountry] ? cities.destination[destinationCountry] : null;
    const areaDesc = destinationArea ? areaOptions.filter( area => area.value === destinationArea).pop().value : 'fob';
    const truckingCustomDesc = destinationArea === 'fob' ? 'exclude_trucking_custom' : 'include_trucking_custom';
    const convertedAreaOptions = areaOptions.reduce( (areas,area) => {
      const { value, label } = area;
      return areas.concat([{ value, label : p.tc(label) }])
    },[])
    return (
      <Row>
        <Col md="1" style={{padding : '10px 15px'}}>
          <h5 className="my-auto">{p.tc('deliver.title')}</h5>
        </Col>
        <Col md="3">
            <Field
              label={p.tc('deliver.to')}
              className="form-control input-sm"
              name='destinationArea'
              items={convertedAreaOptions}
              component={SelectField}
              required='required'/>
            <small className="text-primary" style={{margin : '0px 5px',fontSize : '11px', fontWeight : '400'}}>{ p.tc(`areaOptions.${areaDesc}.description`)}</small> <br/>
            <small className="text-primary" style={{margin : '0px 5px', fontSize : '9px', fontWeight : '600' }}>{ p.tc(`areaOptions.${truckingCustomDesc}`)}</small>
          </Col>
          <Col md="2">
            <Field
              label={p.tc('deliver.destination_country.label')}
              placeholder={p.tc(`${countries.destination.length > 0 ? 'deliver.destination_country.placeholder' : 'deliver.destination_country.loading'}`)}
              name='destinationCountry'
              clearable={false}
              simpleValue={true}
              items={countries.destination}
              component={SelectFieldReact}
              required='required'/>
          </Col>
          <Col md="3">
              <Field
                label={p.tc('deliver.destination_city.label') }
                placeholder={p.tc(`${citiesDisplay ? 'deliver.destination_city.placeholder' : 'deliver.destination_city.loading'}`)}
                name='destinationCity'
                clearable={false}
                simpleValue={true}
                items={citiesDisplay}
                component={SelectFieldReact}
                required='required'/>
          </Col>
          <Col md="3">

          </Col>
        </Row>
    )
  }
}
const mapStateToProps = (state, prevProps) => {
  const { search : { formValue : { countries, cities} } } = state;
  const p = getP(state, { polyglotScope: 'search.form'});
  return {
    countries,
    cities,
    p
  }
}
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    fetchCountriesDestination,
    fetchCitiesDestination,
  },dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(DeliverInfo)
