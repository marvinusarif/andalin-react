export const number = (value) => {
  if (typeof value === 'string') {
   value = value.replace(/[^0-9]+/g, '');
   return parseFloat(value);
 }
 return "NaN"
}
export const min = (number, minNumber=1) => {
  return number < minNumber || isNaN(number) ? minNumber : number;
}

export const validate = (values, props) => {
  const { p, search } = props;

  const errors = {}
  if(!values.datePickup) {
    errors.datePickup = p.tc('validation.required');
  }
  if(!values.destinationCountry) {
    errors.destinationCountry = p.tc('validation.required');
  }
  if(!values.destinationCity) {
    errors.destinationCity = p.tc('validation.required');
  }
  if(!values.originCountry) {
    errors.originCountry = p.tc('validation.required');
  }
  if(!values.originCity) {
    errors.originCity = p.tc('validation.required');
  }
  if(values.insurance){
    if(!values.goodsValue){
      errors.goodsValue = p.tc('validation.required');
    }
  }
  if(values.custom){
    if(!values.commodities){
      errors.commodities = p.tc('validation.required');
    }
  }
  if(search.activeTab === 'lcl'){
    if(!values.loadsLCL || !values.loadsLCL.length){
        errors.loadsLCL = { _error : 'At least one load must be entered'}
    }else{
      const loadsLCLArrayErrors = [];
      values.loadsLCL.forEach((load,loadIndex) => {
        const loadsLCLErrors = {size :{length_ : null, width: null, height: null, unit : null}}
        if(!load.qty || load.qty < 1) {
          loadsLCLErrors.qty = p.tc('validation.required');
          if(parseInt(load.qty,10) < 1) { loadsLCLErrors.qty = 'Must be greater than 0' }
          loadsLCLArrayErrors[loadIndex] = loadsLCLErrors;
        }
        if(!load.size.length_ || load.size.length_ < 0.001) {
          loadsLCLErrors.size.length_ = p.tc('validation.required');
          if(parseFloat(load.size.length_,2) < 0.001) { loadsLCLErrors.size.length_ = 'Must be greater than 0' }
          loadsLCLArrayErrors[loadIndex] = loadsLCLErrors;
        }
        if(!load.size.width || load.size.width < 0.001) {
          loadsLCLErrors.size.width = p.tc('validation.required');
          if(parseFloat(load.size.width,2) < 0.001) { loadsLCLErrors.size.width = 'Must be greater than 0' }
          loadsLCLArrayErrors[loadIndex] = loadsLCLErrors;
        }
        if(!load.size.height || load.size.height < 0.001) {
          loadsLCLErrors.size.height = p.tc('validation.required');
          if(parseFloat(load.size.height,2) < 0.001) { loadsLCLErrors.size.height = 'Must be greater than 0' }
          loadsLCLArrayErrors[loadIndex] = loadsLCLErrors;
        }
        if(!load.weight || load.weight < 0.001) {
          loadsLCLErrors.weight = p.tc('validation.required');
          if(parseFloat(load.weight,2) < 0.001) { loadsLCLErrors.weight = 'Must be greater than 0' }
          loadsLCLArrayErrors[loadIndex] = loadsLCLErrors;
        }
        return loadsLCLErrors;
      })
      if(loadsLCLArrayErrors.length) {
        errors.loadsLCL = loadsLCLArrayErrors;
      }
    }
  }else{
    if(!values.loadsFCL || !values.loadsFCL.length){
      errors.loadsFCL = { _error : 'At least one load must be entered'}
    }else{
      const loadsFCLArrayErrors = []
      values.loadsFCL.forEach((load,loadIndex) => {
        const loadsFCLErrors = {}
        if(!load.qty || load.qty < 1) {
          loadsFCLErrors.qty = p.tc('validation.required');
          if(parseInt(load.qty,10) < 1) { loadsFCLErrors.qty = 'Must be greater than 0' }
          loadsFCLArrayErrors[loadIndex] = loadsFCLErrors;
        }
        return loadsFCLErrors;
      })
      if(loadsFCLArrayErrors.length) {
        errors.loadsFCL = loadsFCLArrayErrors;
      }
    }
  }
  return errors
}
