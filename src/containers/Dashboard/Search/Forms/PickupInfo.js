import Moment from 'moment';
import React, { Component } from 'react';
import { areaOptions } from './index'
import { Row, Col } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Field } from 'redux-form';
import { getP } from 'redux-polyglot';
import { SelectField, DateField, SelectFieldReact } from '../../../../components/Bootstrap/Forms/'
import { fetchCountriesOrigin, fetchCitiesOrigin } from '../../../../actions/search';

class PickupInfo extends Component {
  componentDidMount(){
    const { originCountry } = this.props;
    if(this.props.countries.origin.length < 1){
      this.props.fetchCountriesOrigin();
    }
    if(!this.props.cities.origin[originCountry]){
        this.props.fetchCitiesOrigin(originCountry);
      }
  }
  componentWillReceiveProps(nextProps){
    if(nextProps.originCountry !== this.props.originCountry) {
      if(!this.props.cities.origin[nextProps.originCountry]){
        this.props.fetchCitiesOrigin(nextProps.originCountry);
      }
    }
  }
  render(){
    const { originArea, originCountry, countries, cities, p } = this.props;
    const citiesDisplay = cities.origin[originCountry] ? cities.origin[originCountry] : null;
    const areaDesc = originArea ? areaOptions.filter( area => area.value === originArea).pop().value : 'fob';
    const truckingCustomDesc = originArea === 'fob' ? 'exclude_trucking_custom' : 'include_trucking_custom';
    const convertedAreaOptions = areaOptions.reduce( (areas,area) => {
      const { value, label } = area;
      areas.push({ value, label : p.tc(label) })
      return areas
    },[])
    return (
      <Row>
        <Col md="1" style={{padding : '10px 15px'}}>
          <h5>{p.tc('pickup.title')}</h5>
        </Col>
        <Col md="3">
          <Field
            label={p.tu('pickup.from')}
            className="form-control input-sm"
            name='originArea'
            items={convertedAreaOptions}
            component={SelectField}
            required='required'/>
            <small className="text-primary" style={{margin : '0px 5px', fontSize : '11px', fontWeight : '400' }}>{ p.tc(`areaOptions.${areaDesc}.description`)}</small><br/>
            <small className="text-primary" style={{margin : '0px 5px', fontSize : '9px', fontWeight : '600' }}>{ p.tc(`areaOptions.${truckingCustomDesc}`)}</small>
        </Col>
        <Col md="2">
          <Field
            label={p.tc('pickup.origin_country.label')}
            placeholder={p.tc(`${countries.origin.length > 0 ? 'pickup.origin_country.placeholder' :'pickup.origin_country.loading'}`)}
            name='originCountry'
            clearable={false}
            simpleValue={true}
            items={countries.origin}
            component={SelectFieldReact}
            required='required'/>
        </Col>
        <Col md="3">
          <Field
            label={p.tc('pickup.origin_city.label')}
            placeholder={p.tc(`${ citiesDisplay ? 'pickup.origin_city.placeholder' : 'pickup.origin_city.loading'}`)}
            name='originCity'
            clearable={false}
            simpleValue={true}
            items={citiesDisplay}
            component={SelectFieldReact}
            required='required'/>
        </Col>
        <Col md="3">
          <Field
            className="form-control"
            label={p.tu('pickup.date')}
            name="datePickup"
            minDate={Moment()}
            maxDate={Moment().add(3,"months")}
            component={DateField}
            required='required'/>
        </Col>
      </Row>
    )
  }
}
const mapStateToProps = (state, prevProps) => {
  const { search : { formValue : { countries, cities }}} = state;
  const p = getP(state, { polyglotScope : 'search.form'});
  return {
    countries,
    cities,
    p
  }
}
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    fetchCountriesOrigin,
    fetchCitiesOrigin,
  },dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(PickupInfo)
