import React, { Component } from 'react';
import Logo from '../../../../assets/img/logo.png';
import { Row, Col, Button as ButtonStrap } from 'reactstrap';
import { reduxForm, FieldArray, Field } from 'redux-form';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { TextField, HiddenField } from '../../../../components/Bootstrap/Forms/';
import { closeUpdateFreightModal } from '../../../../actions/page';
import { updateShipmentFreight } from '../../../../actions/shipment';
import { ReactLoader } from '../../../../components/Dashboard/Loader/';
import { getP } from 'redux-polyglot';
import { validationFreight } from './Validation';

const renderFreights = (props) => {
  const {fields, p } = props;
  return (
    <div>
      { fields.map( (freight,index) => {
        return (
          <Row key={index}>
            <Col md="1">
              <Field
                name={`${freight}.freightId`}
                component={HiddenField}/>
              <Field
                name={`${freight}.additionalInfo`}
                component={HiddenField}/>
              <Field
                className="form-control input-sm"
                type="text"
                label={p.tu('freights.qty')}
                name={`${freight}.qty`}
                component={TextField}
                disabled={true}
                required='required'/>
            </Col>
            <Col md="2">
              <Field
                className="form-control input-sm"
                type="text"
                label={p.tu('freights.weight')}
                name={`${freight}.weight`}
                disabled={true}
                component={TextField}
                required='required'/>
            </Col>
            <Col md="2">
              <Field
                className="form-control input-sm"
                type="text"
                label={p.tu('freights.volume')}
                name={`${freight}.volume`}
                disabled={true}
                component={TextField}
                required='required'/>
            </Col>
            <Col md="3">
              <Field
                className="form-control input-sm"
                type="text"
                label={p.tu('freights.service_type')}
                name={`${freight}.serviceType`}
                disabled={true}
                component={TextField}
                required='required'/>
            </Col>
            <Col md="4">
              <Field
                className="form-control input-sm"
                type="text"
                label={p.tu('freights.tracking_number')}
                name={`${freight}.trackingNumber`}
                component={TextField}
                required='required'/>
            </Col>
          </Row>
        )
      })}
    </div>
  )
}
class ShipmentDetailFreightForm extends Component {
  constructor(props){
    super(props);
    this._submitForm = this._submitForm.bind(this);
    this._closeUpdateFreightModal = this._closeUpdateFreightModal.bind(this);
  }
  _closeUpdateFreightModal(shipmentId){
    this.props.closeUpdateFreightModal(shipmentId);
  }
  _submitForm(_form){
    const { shipmentId, shipmentDetail } = this.props;
    const form = Object.assign({}, _form, {
      vendorId : shipmentDetail[shipmentId].vendorId
    })
    this.props.updateShipmentFreight({form, shipmentId, orderNumber : shipmentDetail[shipmentId].orderNumber});
  }
  render(){
    const { p, pError, shipmentDetail, shipmentDetailFreight, shipmentId, handleSubmit } = this.props;

    if(shipmentDetailFreight.isFetching && !shipmentDetailFreight.isSuccessful) {
      return (
        <ReactLoader message={p.tc('freights.loading')}/>
      )
    }else if(!shipmentDetailFreight.isFetching && shipmentDetailFreight.isSuccessful){
      return (
        <div>
          <Row className="m-t-20">
            <Col md={{size : 5, offset : 1}} className="p-t-10">
              <img src={Logo} alt="logo"/>
            </Col>
            <Col md={{size: 2, offset : 3}}>
                  <ButtonStrap outline onClick={this._closeUpdateFreightModal.bind(this, shipmentId)}><i className="fa fa-remove"></i> {p.tc('close')} </ButtonStrap>
            </Col>
          </Row>
          <Row>
            <Col tag='h4' className="text-center">
              {p.tc('freights.success')}
            </Col>
          </Row>
      </div>
      )
    }
    return (
      <form onSubmit={handleSubmit(this._submitForm.bind(this))}>
        <Row className="m-t-20">
          <Col md={{size : 5, offset : 1}} className="p-t-10">
            <img src={Logo} alt="logo"/>
          </Col>
          <Col md={{size: 2, offset : 3}}>
            <ButtonStrap outline onClick={this._closeUpdateFreightModal.bind(this, shipmentId)}><i className="fa fa-remove"></i> {p.tc('close')} </ButtonStrap>
          </Col>
        </Row>
        <Row>
          <Col md="12" tag="h4" className="m-t-20 text-center">
            {p.tc('freights.title')}
          </Col>
        </Row>
        <Row>
          <Col md="12" tag="h6" className="m-t-20 text-center">
            {p.tc('freights.order_number')}{shipmentDetail[shipmentId].orderNumber}
          </Col>
        </Row>
        <FieldArray name="freights" p={p} component={renderFreights}/>
        <Row>
          <Col md="7">
            <small className="text-danger">{shipmentDetailFreight.message && pError.tc(shipmentDetailFreight.message)}</small>
          </Col>
          <Col md="5">
            <ButtonStrap className="btn btn-block btn-primary" type="submit">{p.tc('freights.action.edit')}</ButtonStrap>
          </Col>
        </Row>
      </form>
    )
  }
}
const mapStateToProps = (state,ownProps) => {
  const { shipment : { shipmentDetail }, page : { modal : { shipmentDetailFreight } } } = state;
  const p = getP(state, { polyglotScope : 'shipment.detail.form'} );
  const pError = getP(state, { polyglotScope : 'error'});
  const { shipmentId } = shipmentDetailFreight;
  const { freights } = shipmentDetail[shipmentId];
  const _initialValues = {
    freights : freights
  }
  return {
    pError,
    p,
    shipmentDetail,
    shipmentDetailFreight,
    shipmentId,
    initialValues : _initialValues
  }
}
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    closeUpdateFreightModal,
    updateShipmentFreight,
  }, dispatch)
}
ShipmentDetailFreightForm = reduxForm({
  form : 'ShipmentDetailFreightForm',
  enableReinitialize : true,
  validate : validationFreight
})(ShipmentDetailFreightForm)

ShipmentDetailFreightForm = connect(mapStateToProps,mapDispatchToProps)(ShipmentDetailFreightForm)

export default ShipmentDetailFreightForm;
