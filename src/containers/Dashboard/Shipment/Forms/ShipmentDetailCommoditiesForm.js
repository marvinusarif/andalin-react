import React, { Component } from 'react';
import Logo from '../../../../assets/img/logo.png';
import { Row, Col, Button as ButtonStrap } from 'reactstrap';
import { reduxForm, Field } from 'redux-form';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { SelectFieldReact, TextArea, TextField } from '../../../../components/Bootstrap/Forms/';
import { closeUpdateCommoditiesModal } from '../../../../actions/page';
import { updateShipmentCommodities} from '../../../../actions/shipment';
import { ReactLoader } from '../../../../components/Dashboard/Loader/';
import { getP } from 'redux-polyglot';
import { commoditiesClassOptions, commoditiesTypeOptions } from '../../Book/Forms';
import { validationCommodities } from './Validation';

class ShipmentDetailCommoditiesForm extends Component {
  constructor(props){
    super(props);
    this._submitForm = this._submitForm.bind(this);
    this._closeUpdateCommoditiesModal = this._closeUpdateCommoditiesModal.bind(this);
  }
  _closeUpdateCommoditiesModal(shipmentId){
    this.props.closeUpdateCommoditiesModal(shipmentId);
  }
  _submitForm(form){
    const { shipmentId, shipmentDetail } = this.props;
    this.props.updateShipmentCommodities({form, shipmentId, orderNumber : shipmentDetail[shipmentId].orderNumber});
  }
  render(){
    const { p, pBook, pError, shipmentDetail, shipmentDetailCommodities, shipmentId, handleSubmit } = this.props;
    const convertedCommoditiesClassOptions = commoditiesClassOptions.reduce( (types,type) => {
            const { value, label } = type;
            return types.concat({ value, label : pBook.tu(label)});
          }, []);
    const convertedCommoditiesTypeOptions = commoditiesTypeOptions.reduce( (types,type) => {
            const { value, label } = type;
                  return types.concat({ value, label : pBook.tc(label)});
          }, []);
    if(shipmentDetailCommodities.isFetching && !shipmentDetailCommodities.isSuccessful) {
      return (
        <ReactLoader message={p.tc('order_stat.loading')}/>
      )
    }else if(!shipmentDetailCommodities.isFetching && shipmentDetailCommodities.isSuccessful){
      return (
        <div>
          <Row className="m-t-20">
            <Col md={{size : 5, offset : 1}} className="p-t-10">
              <img src={Logo} alt="logo"/>
            </Col>
            <Col md={{size: 2, offset : 3}}>
                  <ButtonStrap outline onClick={this._closeUpdateCommoditiesModal.bind(this, shipmentId)}><i className="fa fa-remove"></i> {p.tc('close')} </ButtonStrap>
            </Col>
          </Row>
          <Row>
            <Col tag='h4' className="text-center">
              {p.tc('commodities.success')}
            </Col>
          </Row>
      </div>
      )
    }
    return (
      <form onSubmit={handleSubmit(this._submitForm.bind(this))}>
        <Row className="m-t-20">
          <Col md={{size : 5, offset : 1}} className="p-t-10">
            <img src={Logo} alt="logo"/>
          </Col>
          <Col md={{size: 2, offset : 3}}>
                <ButtonStrap outline onClick={this._closeUpdateCommoditiesModal.bind(this, shipmentId)}><i className="fa fa-remove"></i> {p.tc('close')} </ButtonStrap>
          </Col>
        </Row>
        <Row>
          <Col md="12" tag="h4" className="m-t-20 text-center">
            {p.tc('commodities.title')}
          </Col>
        </Row>
        <Row>
          <Col md="12" tag="h6" className="m-t-20 text-center">
            {p.tc('commodities.order_number')}{shipmentDetail[shipmentId].orderNumber}
          </Col>
        </Row>
        <Row>
          <Col md="4">
            <Field
              label={pBook.tu('product_class.label')}
              placeholder={pBook.tc('product_class.placeholder')}
              name='commoditiesClass'
              clearable={false}
              simpleValue={true}
              items={convertedCommoditiesClassOptions}
              component={SelectFieldReact}
              required='required'/>
          </Col>
          <Col md="5">
            <Field
              label={pBook.tu('product_type.label')}
              name='commoditiesType'
              items={convertedCommoditiesTypeOptions}
              placeholder={pBook.tc('product_type.placeholder')}
              clearable={false}
              simpleValue={true}
              component={SelectFieldReact}
              required='required'/>
          </Col>
          <Col md="3">
            <Field
              className="form-control input-sm"
              label={pBook.tu('hscode.label')}
              name='commoditiesHSCode'
              component={TextField}
              placeholder={pBook.tc('hscode.placeholder')}
              required='required'/>
          </Col>
        </Row>
        <Row>
          <Col md="12">
            <Field className="form-control input-sm"
              label={`${pBook.tc('product_desc.label')}`}
              placeholder={pBook.tc('product_desc.placeholder')}
              name="commoditiesProduct"
              component={TextArea}/>
          </Col>
        </Row>
        <Row>
          <Col md="7">
            <small className="text-danger">{shipmentDetailCommodities.message && pError.tc(shipmentDetailCommodities.message)}</small>
          </Col>
          <Col md="5">
            <ButtonStrap className="btn btn-block btn-primary" type="submit">{p.tc('commodities.action.edit')}</ButtonStrap>
          </Col>
        </Row>
      </form>
    )
  }
}
const mapStateToProps = (state,ownProps) => {
  const { shipment : { shipmentDetail }, page : { modal : { shipmentDetailCommodities } } } = state;
  const p = getP(state, { polyglotScope : 'shipment.detail.form'} );
  const pBook = getP(state, {polyglotScope : 'book.shipment_detail.commodities'});
  const pError = getP(state, { polyglotScope : 'error'});
  const { shipmentId } = shipmentDetailCommodities;
  const { commoditiesHSCode, commoditiesClass, commoditiesType, commoditiesProduct } = shipmentDetail[shipmentId];
  const _initialValues = {
    commoditiesHSCode,
    commoditiesClass,
    commoditiesType,
    commoditiesProduct
  }
  return {
    pError,
    pBook,
    p,
    shipmentDetail,
    shipmentDetailCommodities,
    shipmentId,
    initialValues : _initialValues
  }
}
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    closeUpdateCommoditiesModal,
    updateShipmentCommodities,
  }, dispatch)
}
ShipmentDetailCommoditiesForm = reduxForm({
  form : 'ShipmentDetailCommoditiesForm',
  enableReinitialize : true,
  validate : validationCommodities
})(ShipmentDetailCommoditiesForm)

ShipmentDetailCommoditiesForm = connect(mapStateToProps,mapDispatchToProps)(ShipmentDetailCommoditiesForm)

export default ShipmentDetailCommoditiesForm;
