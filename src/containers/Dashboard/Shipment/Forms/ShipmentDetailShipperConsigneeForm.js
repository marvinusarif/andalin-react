import React, { Component } from 'react';
import Logo from '../../../../assets/img/logo.png';
import { Row, Col, Button as ButtonStrap } from 'reactstrap';
import { reduxForm, Field } from 'redux-form';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { TextField, TextArea } from '../../../../components/Bootstrap/Forms/';
import { closeUpdateShipperConsigneeModal } from '../../../../actions/page';
import { updateShipmentShipperConsignee } from '../../../../actions/shipment';
import { ReactLoader } from '../../../../components/Dashboard/Loader/';
import { getP } from 'redux-polyglot';
import { validateShipperConsignee } from './Validation.js';

class ShipmentDetailShipperConsigneeForm extends Component {
  constructor(props){
    super(props);
    this._submitForm = this._submitForm.bind(this);
    this._closeUpdateShipperConsigneeModal = this._closeUpdateShipperConsigneeModal.bind(this);
  }
  _closeUpdateShipperConsigneeModal(shipmentId){
    this.props.closeUpdateShipperConsigneeModal(shipmentId);
  }
  _submitForm(form){
    const { shipmentId, shipmentDetail } = this.props;
    this.props.updateShipmentShipperConsignee({form, shipmentId, orderNumber : shipmentDetail[shipmentId].orderNumber});
  }
  render(){
    const { p, pError, shipmentDetail, shipmentDetailShipperConsignee, shipmentId, handleSubmit } = this.props;
    if(shipmentDetailShipperConsignee.isFetching && !shipmentDetailShipperConsignee.isSuccessful) {
      return (
        <ReactLoader message={p.tc('consignee_shipper.loading')}/>
      )
    }else if(!shipmentDetailShipperConsignee.isFetching && shipmentDetailShipperConsignee.isSuccessful){
      return (
        <div>
          <Row className="m-t-20">
            <Col md={{size : 5, offset : 1}} className="p-t-10">
              <img src={Logo} alt="logo"/>
            </Col>
            <Col md={{size: 2, offset : 3}}>
              <ButtonStrap outline onClick={this._closeUpdateShipperConsigneeModal.bind(this, shipmentId)}><i className="fa fa-remove"></i> {p.tc('close')} </ButtonStrap>
            </Col>
          </Row>
          <Row>
            <Col tag='h4' className="text-center">
              {p.tc('consignee_shipper.success')}
            </Col>
          </Row>
      </div>
      )
    }
    return (
      <form onSubmit={handleSubmit(this._submitForm.bind(this))}>
        <Row className="m-t-20">
          <Col md={{size : 5, offset : 1}} className="p-t-10">
            <img src={Logo} alt="logo"/>
          </Col>
          <Col md={{size: 2, offset : 3}}>
                <ButtonStrap outline onClick={this._closeUpdateShipperConsigneeModal.bind(this, shipmentId)}><i className="fa fa-remove"></i> {p.tc('close')} </ButtonStrap>
          </Col>
        </Row>
        <Row>
          <Col md="12" tag="h4" className="m-t-20 text-center">
            {p.tc('consignee_shipper.title')}
          </Col>
        </Row>
        <Row>
          <Col md="12" tag="h6" className="m-t-20 text-center">
            {p.tc('consignee_shipper.order_number')}{shipmentDetail[shipmentId].orderNumber}
          </Col>
        </Row>
        <Row>
          <Col md="6" className="text-success">
            <Row>
              <Col tag="h6" className="text-success">
                {p.tc('shipper.title')}
              </Col>
            </Row>
            <Row>
              <Col>
                <Field
                  className="form-control input-sm"
                  label={p.tu('shipper.company.label')}
                  name='shipperCompany'
                  component={TextField}
                  placeholder={p.tc('shipper.company.placeholder')}
                  required='required'/>
              </Col>
            </Row>
            <Row>
              <Col>
                <Field
                  className="form-control input-sm"
                  label={p.tu('shipper.name.label')}
                  name='shipperPICName'
                  component={TextField}
                  placeholder={p.tc('shipper.name.placeholder')}
                  required='required'/>
              </Col>
            </Row>
            <Row>
              <Col>
                <Field
                  className="form-control input-sm"
                  label={`${p.tu('shipper.address.label')} (${shipmentDetail[shipmentId] && shipmentDetail[shipmentId].originCityCode.toUpperCase()}, ${shipmentDetail[shipmentId] && shipmentDetail[shipmentId].originCountryCode.toUpperCase()})`}
                  name='shipperAddress'
                  component={TextArea}
                  placeholder={p.tc('shipper.address.placeholder')}
                  required='required'/>
              </Col>
            </Row>
            <Row>
              <Col>
                <Field
                  className="form-control input-sm"
                  label={p.tu('shipper.email.label')}
                  name='shipperEmail'
                  component={TextField}
                  placeholder={p.tc('shipper.email.placeholder')}
                  required='required'/>
              </Col>
            </Row>
            <Row>
              <Col>
                <Field
                  className="form-control input-sm"
                  label={p.tu('shipper.phone.label')}
                  name='shipperPhone'
                  component={TextField}
                  placeholder={p.tc('shipper.phone.placeholder')}
                  required='required'/>
              </Col>
            </Row>
            <Row>
              <Col>
                <Field
                  className="form-control input-sm"
                  label={p.tu('shipper.fax.label')}
                  name='shipperFax'
                  component={TextField}
                  placeholder={p.tc('shipper.fax.placeholder')}/>
              </Col>
            </Row>
          </Col>
          <Col md="6" className="text-primary">
            <Row>
              <Col tag="h6" className="text-primary">
              {p.tc('consignee.title')}
              </Col>
            </Row>
            <Row>
              <Col>
                <Field
                  className="form-control input-sm"
                  label={p.tu('consignee.company.label')}
                  name='consigneeCompany'
                  component={TextField}
                  placeholder={p.tc('consignee.company.placeholder')}
                  required='required'/>
              </Col>
            </Row>
            <Row>
              <Col>
                <Field
                  className="form-control input-sm"
                  label={p.tu('consignee.name.label')}
                  name='consigneePICName'
                  component={TextField}
                  placeholder={p.tc('consignee.name.placeholder')}
                  required='required'/>
              </Col>
            </Row>
            <Row>
              <Col>
                <Field
                  className="form-control input-sm"
                  label={`${p.tu('consignee.address.label')} (${shipmentDetail[shipmentId] && shipmentDetail[shipmentId].destinationCityCode.toUpperCase()}, ${shipmentDetail[shipmentId] && shipmentDetail[shipmentId].destinationCountryCode.toUpperCase()})`}
                  name='consigneeAddress'
                  component={TextArea}
                  placeholder={p.tc('consignee.address.placeholder')}
                  required='required'/>
              </Col>
            </Row>
            <Row>
              <Col>
                <Field
                  className="form-control input-sm"
                  label={p.tu('consignee.email.label')}
                  name='consigneeEmail'
                  component={TextField}
                  placeholder={p.tc('consignee.email.placeholder')}
                  required='required'/>
              </Col>
            </Row>
            <Row>
              <Col>
                <Field
                  className="form-control input-sm"
                  label={p.tu('consignee.phone.label')}
                  name='consigneePhone'
                  component={TextField}
                  placeholder={p.tc('consignee.phone.placeholder')}
                  required='required'/>
              </Col>
            </Row>
            <Row>
              <Col>
                <Field
                  className="form-control input-sm"
                  label={p.tu('consignee.fax.label')}
                  name='consigneeFax'
                  component={TextField}
                  placeholder={p.tc('consignee.fax.placeholder')}/>
              </Col>
            </Row>
          </Col>
        </Row>
        <Row>
          <Col className="text-success">
              <Field
                className="form-control input-sm"
                label={p.tu('shipper.note.label')}
                name='shipperNotes'
                component={TextArea}
                placeholder={p.tc('shipper.note.placeholder')}/>
          </Col>
        </Row>
        <Row>
          <Col md="7">
            <small className="text-danger">{shipmentDetailShipperConsignee.message && pError.tc(shipmentDetailShipperConsignee.message)}</small>
          </Col>
          <Col md="5">
            <ButtonStrap className="btn btn-block btn-primary" type="submit">{p.tc('consignee_shipper.action.edit')}</ButtonStrap>
          </Col>
        </Row>
      </form>
    )
  }
}
const mapStateToProps = (state,ownProps) => {
  const { shipment : { shipmentDetail }, page : { modal : { shipmentDetailShipperConsignee } } } = state;
  const p = getP(state, { polyglotScope : 'shipment.detail.form'} );
  const pError = getP(state, { polyglotScope : 'error'});
  const { shipmentId } = shipmentDetailShipperConsignee;
  const { shipperCompany, shipperName, shipperAddress, shipperPhone, shipperFax, shipperEmail, shipperNotes,
          consigneeCompany, consigneeName, consigneeAddress, consigneePhone, consigneeFax, consigneeEmail} = shipmentDetail[shipmentId];
  const _initialValues = {
    shipperCompany,
    shipperPICName : shipperName,
    shipperAddress,
    shipperPhone,
    shipperFax,
    shipperEmail,
    consigneeCompany,
    consigneePICName : consigneeName,
    consigneeAddress,
    consigneePhone,
    consigneeFax,
    consigneeEmail,
    shipperNotes
  }
  return {
    pError,
    p,
    shipmentDetail,
    shipmentDetailShipperConsignee,
    shipmentId,
    initialValues : _initialValues
  }
}
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    closeUpdateShipperConsigneeModal,
    updateShipmentShipperConsignee
  }, dispatch)
}
ShipmentDetailShipperConsigneeForm = reduxForm({
  form : 'ShipmentDetailShipperConsigneeForm',
  enableReinitialize : true,
  validate : validateShipperConsignee
})(ShipmentDetailShipperConsigneeForm)

ShipmentDetailShipperConsigneeForm = connect(mapStateToProps,mapDispatchToProps)(ShipmentDetailShipperConsigneeForm)

export default ShipmentDetailShipperConsigneeForm;
