import React, { Component } from 'react';
import { Row, Col, Button as ButtonStrap } from 'reactstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { reduxForm, Field } from 'redux-form';
import { getP } from 'redux-polyglot';
import Logo from '../../../../assets/img/logo.png';
import { SelectFieldReact, FileInputField, Button, TextField } from '../../../../components/Bootstrap/Forms';
import { closePostUpdateShipmentDocs } from '../../../../actions/page';
import { postUpdateShipmentDocs } from '../../../../actions/shipment';
import { shipmentDocumentsCategoryOptions } from './';
import { validationFile } from './Validation';

class ShipmentDocumentsUploadForm extends Component {
  constructor(props){
    super(props);
    this.state = {
      fileCategory : null
    }
    this._submitForm = this._submitForm.bind(this);
    this._closePostUpdateShipmentDocs = this._closePostUpdateShipmentDocs.bind(this);
  }
  _closePostUpdateShipmentDocs(shipmentId){
    this.props.closePostUpdateShipmentDocs(shipmentId);
  }
  _submitForm(form){
    const { shipment, shipmentDocumentUploadFile : { shipmentId }} = this.props;
    const { orderNumber } = shipment[shipmentId];
    this.setState({
      fileCategory : form.documentCategory
    });
    let formData = new FormData();
        formData.append('shipmentId', shipmentId)
        formData.append('orderNumber', orderNumber)
        formData.append('documentCategory', form.documentCategory)
        formData.append('documentOrigin', 'WEBAPP')
        formData.append('documentDesc', form.documentDesc)
        formData.append('fileDocument', form.fileDocument)
    this.props.postUpdateShipmentDocs({form : formData, shipmentId});
  }
  render(){
    const { p,err, handleSubmit, submitting, pristine, shipment, shipmentDocumentUploadFile : { shipmentId, isFetching, percentCompleted, isSuccessful, message } } = this.props;
    const displayShipmentDocumentsCategoryOptions = shipmentDocumentsCategoryOptions.map(opt => ({
      label : p.tu(`${opt.label}`),
      value : opt.value
    }));
    if(isFetching && !isSuccessful) {
      return (
        <Row className="m-t-20">
          <Col md={{size:10, offset:1}} className="text-center">
            {p.tc('loading')}
            <br/>
            {percentCompleted}%
          </Col>
        </Row>
      )
    }
    if(!isFetching && isSuccessful){
      return (
      <div>
        <Row className="m-t-20">
          <Col md={{size : 4, offset : 1}} className="p-t-10">
            <img src={Logo} alt="logo"/>
          </Col>
          <Col md={{size: 2, offset : 3}}>
            <ButtonStrap outline onClick={this._closePostUpdateShipmentDocs.bind(this,shipmentId)}><i className="fa fa-remove"></i> {p.tc('close')} </ButtonStrap>
          </Col>
        </Row>
        <Row>
          <Col md={{size : 10, offset: 1}} tag="h6" className="text-center">
              {p.tc('success', { fileCategory : this.state.fileCategory })}
          </Col>
        </Row>
      </div>
      )
    }
    return (
      <form onSubmit={handleSubmit(this._submitForm)}>
      <Row className="m-t-20">
        <Col md={{size : 5, offset : 1}} className="p-t-10">
          <img src={Logo} alt="logo"/>
        </Col>
        <Col md={{size: 2, offset : 3}}>
          <ButtonStrap outline onClick={this._closePostUpdateShipmentDocs.bind(this,shipmentId)}><i className="fa fa-remove"></i> {p.tc('close')} </ButtonStrap>
        </Col>
      </Row>
      <Row>
        <Col md="12" tag="h4" className="m-t-20 text-center">
          {p.tc('title')}
        </Col>
      </Row>
      <Row>
        <Col md="12" tag="h6" className="m-t-20 text-center">
          {p.tc('order_number')}{shipment[shipmentId].orderNumber}
        </Col>
      </Row>
      <Row className='m-t-10'>
        <Col md={{size:10, offset : 1}}>
          <Field
            label={p.tu('category.label')}
            name='documentCategory'
            items={displayShipmentDocumentsCategoryOptions}
            placeholder={p.tc('category.placeholder')}
            clearable={false}
            simpleValue={true}
            component={SelectFieldReact}
            required='required'/>
        </Col>
      </Row>
      <Row>
        <Col md={{size:10, offset : 1}}>
          <Field
            className="form-control input-sm"
            label={p.tu('description.label')}
            name='documentDesc'
            placeholder={p.tc('description.placeholder')}
            component={TextField}
            required='required'/>
        </Col>
      </Row>
      <Row>
        <Col md={{size:10, offset : 1}}>
          <Field
            label={p.tu('file_input.label')}
            className="form-control input-sm"
            name='fileDocument'
            component={FileInputField}
            required='required'/>
        </Col>
      </Row>
      { message && (
        <Row>
          <Col md={{size:10, offset : 1}} className="text-center">
            <small className="text-danger text-center">{err.tc(`${message}`)}</small>
          </Col>
        </Row>
      )}
      <Row>
        <Col md={{ size : 6, offset : 3}} className="text-center">
          <Button
              style={{margin : '20px 0px'}}
              className="btn btn-primary btn-block"
              type="submit"
              disabled={submitting || pristine || isFetching }
              value={isFetching ? p.tc('action.uploading') :  p.tc('action.upload')}/>
        </Col>
      </Row>
    </form>
    )
  }
}

ShipmentDocumentsUploadForm = reduxForm({
  form : 'ShipmentsDocumentsForm',
  enableReinitialize : true,
  validate : validationFile
})(ShipmentDocumentsUploadForm)

const mapStateToProps = (state,ownProps) => {
  const { page : { modal : { shipmentDocumentUploadFile }}, shipment : { shipment } } = state;
  const err = getP(state, {polyglotScope : 'error'});
  const p = getP(state, {polyglotScope : 'shipment.detail.document.form' });
  return {
    shipment,
    shipmentDocumentUploadFile,
    p,
    err
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    closePostUpdateShipmentDocs,
    postUpdateShipmentDocs
  }, dispatch)
}
export default connect(mapStateToProps,mapDispatchToProps)(ShipmentDocumentsUploadForm)
