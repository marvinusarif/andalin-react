export const orderByOptions = [{
  label : 'order_by.options.created_at',
  value : 'createdAt'
},{
  label : 'order_by.options.shipment_date',
  value : 'shipmentDate'
},{
  label : 'order_by.options.carrier_name',
  value : 'carrierName'
},{
  label : 'order_by.options.vendor_name',
  value : 'vendorName'
},{
  label : 'order_by.options.order_stat',
  value : 'orderStat'
}]

export const orderByDirectionOptions = [{
  label : 'order_by_direction.options.asc',
  value : 'asc'
},{
  label : 'order_by_direction.options.desc',
  value : 'desc'
}]

export const orderStatAdminOptions = [{
  label : 'order_stat.options.book',
  value : 'BOOK'
},{
  label : 'order_stat.options.confirm',
  value : 'CONFIRM'
},{
  label : 'order_stat.options.scheduled',
  value : 'SCHEDULED'
},{
  label : 'order_stat.options.dispatch',
  value : 'DISPATCH'
},{
  label : 'order_stat.options.discharge',
  value : 'DISCHARGE'
},{
  label : 'order_stat.options.waiting_payment',
  value : 'PAYMENT'
},{
  label : 'order_stat.options.complete',
  value : 'COMPLETE'
}]


export const orderStatVendorOptions = [{
  label : 'order_stat.options.book',
  value : 'BOOK'
},{
  label : 'order_stat.options.confirm',
  value : 'CONFIRM'
},{
  label : 'order_stat.options.scheduled',
  value : 'SCHEDULED'
}]

export const requestStatOptions = [{
  label : 'requests.approved.options.U',
  value : 'U'
},{
  label : 'requests.approved.options.Y',
  value : 'Y'
},{
  label : 'requests.approved.options.N',
  value : 'N'
}]

export const priceComponentTypeOptions = [{
  label : 'prices.component_type.options.freight',
  value : 'FREIGHT'
},{
  label : 'prices.component_type.options.custom',
  value : 'CUSTOM'
},{
  label : 'prices.component_type.options.document',
  value : 'DOCUMENT'
},{
  label : 'prices.component_type.options.trucking',
  value : 'TRUCKING'
},{
  label : 'prices.component_type.options.insurance',
  value : 'INSURANCE'
}]

export const priceUOMOptions = [{
  label : 'prices.price_uom.options.w',
  value : 'W'
},{
  label : 'prices.price_uom.options.v',
  value : 'V'
},{
  label : 'prices.price_uom.options.vw',
  value : 'VW'
},{
  label : 'prices.price_uom.options.container',
  value : 'CONT'
},{
  label : 'prices.price_uom.options.all',
  value : 'ALL'
}]

export const shipmentDocumentsCategoryOptions =[{
  label : 'category.options.bl_draft',
  value : 'BL_DRAFT'
},{
  label : 'category.options.bl',
  value : 'BL'
},{
  label : 'category.options.api',
  value : 'API'
},{
  label : 'category.options.certificate',
  value : 'CERTIFICATE'
},{
  label : 'category.options.pl',
  value : 'PL'
},{
  label : 'category.options.awb',
  value : 'AWB'
},{
  label : 'category.options.photo',
  value : 'PHOTO'
},{
  label : 'category.options.others',
  value : 'OTHERS'
}]

export const paymentMethodOptions = [
  {
    label : 'order_stat.payment_method.options.transfer',
    value : 'TRANSFER'
  },{
    label : 'order_stat.payment_method.options.giro',
    value : 'GIRO'
  },{
    label : 'order_stat.payment_method.options.wire_transfer',
    value : 'WIRETRANSFER'
  }
]
