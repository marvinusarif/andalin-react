import Moment from 'moment';
import React, { Component } from 'react';
import Logo from '../../../../assets/img/logo.png';
import { Row, Col, Button as ButtonStrap } from 'reactstrap';
import { reduxForm, Field, FieldArray, formValueSelector } from 'redux-form';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { SelectFieldReact, TextField, TextArea, Button, NumberField, DateField } from '../../../../components/Bootstrap/Forms/';
import { closeUpdateOrderStatModal, fetchShipmentBankAcc } from '../../../../actions/page';
import { updateShipmentOrderStat } from '../../../../actions/shipment';
import { fetchCountriesOrigin, fetchCitiesOrigin, fetchCountriesDestination, fetchCitiesDestination } from '../../../../actions/search';
import { ReactLoader } from '../../../../components/Dashboard/Loader/';
import { getP } from 'redux-polyglot';
import { orderStatAdminOptions, orderStatVendorOptions, paymentMethodOptions } from './';
import { currencyOptions } from '../../Search/Forms/';
import { validationOrderStat } from './Validation';

const renderSchedules = (props) => {
  const { fields, p, countries, cities, formValue, onOriginChange, onDestinationChange } = props;
  return (
    <div>
      { fields.map( (schedule,index) => {
        const { destinationCountry, originCountry } = formValue.schedules[index];
        const destinationCitiesDisplay = destinationCountry ?
                                          cities.destination[destinationCountry.value] ? cities.destination[destinationCountry.value] : null
                                         : null ;
        const originCitiesDisplay = originCountry ?
                                      cities.origin[originCountry.value] ? cities.origin[originCountry.value] : null
                                    : null;
        return (
        <Row className="m-t-20" key={index}>
          <Col>
            { index === 0 && (
              <div>
              <Row>
                <Col md="12">
                  <Field
                    className="form-control"
                    type="text"
                    label={p.tu('order_stat.port_open_time.label')}
                    placeholder ={p.tc('order_stat.port_open_time.placeholder')}
                    name={`${schedule}.portOpenTime`}
                    dateFormat={'DD/MM/YY HH:mm'}
                    minDate={Moment()}
                    showTimeSelect={true}
                    timeFormat={'HH:mm'}
                    component={DateField}
                    required='required'/>
                </Col>
              </Row>
              <Row>
                <Col md="12">
                  <Field
                    className="form-control"
                    type="text"
                    label={p.tu('order_stat.port_cutoff_time.label')}
                    placeholder ={p.tc('order_stat.port_cutoff_time.placeholder')}
                    name={`${schedule}.portCutoffTime`}
                    dateFormat={'DD/MM/YY HH:mm'}
                    minDate={Moment()}
                    showTimeSelect={true}
                    timeFormat={'HH:mm'}
                    component={DateField}
                    required='required'/>
                </Col>
              </Row>
              <Row>
                <Col md="12">
                  <Field
                    className="form-control"
                    type="text"
                    label={p.tu('order_stat.inland_cutoff_time.label')}
                    placeholder ={p.tc('order_stat.inland_cutoff_time.placeholder')}
                    dateFormat={'DD/MM/YY HH:mm'}
                    minDate={Moment()}
                    showTimeSelect={true}
                    timeFormat={'HH:mm'}
                    name={`${schedule}.inlandCutoffTime`}
                    component={DateField}
                    required='required'/>
                </Col>
              </Row>
            </div>
            )}
            <Row>
              <Col md="6">
                <Field
                  className="form-control input-sm"
                  type="text"
                  label={p.tu('order_stat.date_departure.label', { cityCode : formValue.schedules[index].originCity ? formValue.schedules[index].originCity : '', countryCode : formValue.schedules[index].originCountry ? formValue.schedules[index].originCountry.value : ''})}
                  placeholder ={p.tc('order_stat.date_departure.placeholder')}
                  minDate={Moment()}
                  name={`${schedule}.dateDeparture`}
                  component={DateField}
                  required='required'/>
              </Col>
              <Col md="6">
                <Field
                  className="form-control input-sm"
                  type="text"
                  label={p.tu('order_stat.date_arrival.label', { cityCode : formValue.schedules[index].destinationCity ? formValue.schedules[index].destinationCity : '', countryCode : formValue.schedules[index].destinationCountry ? formValue.schedules[index].destinationCountry.value : '' })}
                  placeholder ={p.tc('order_stat.date_arrival.placeholder')}
                  minDate={Moment()}
                  name={`${schedule}.dateArrival`}
                  component={DateField}
                  required='required'/>
              </Col>
            </Row>
            <Row>
              <Col md="12">
                <Field
                  className="form-control input-sm"
                  type="text"
                  label={p.tu('order_stat.vessel_name.label')}
                  placeholder ={p.tc('order_stat.vessel_name.placeholder')}
                  name={`${schedule}.vesselName`}
                  component={TextField}
                  required='required'/>
              </Col>
            </Row>
            <Row>
              <Col md="4">
                <Field
                  label={p.tc('order_stat.origin_country.label')}
                  placeholder={p.tc(`${countries.origin.length > 0 ? 'order_stat.origin_country.placeholder' :'order_stat.origin_country.loading'}`)}
                  name={`${schedule}.originCountry`}
                  onChange={(e) => {onOriginChange(e.value)}}
                  clearable={false}
                  items={countries.origin}
                  component={SelectFieldReact}
                  required='required'/>
              </Col>
              <Col md="4">
                <Field
                  label={p.tc('order_stat.origin_city.label')}
                  placeholder={p.tc(`${ originCitiesDisplay ? 'order_stat.origin_city.placeholder' : 'order_stat.origin_city.loading'}`)}
                  name={`${schedule}.originCity`}
                  clearable={false}
                  simpleValue={true}
                  items={originCitiesDisplay}
                  component={SelectFieldReact}
                  required='required'/>
              </Col>
              <Col md="4">
                <Field
                  className="form-control input-sm"
                  type="text"
                  label={p.tu('order_stat.pol.label')}
                  placeholder ={p.tc('order_stat.pol.placeholder')}
                  name={`${schedule}.pol`}
                  component={TextField}
                  required='required'/>
              </Col>
            </Row>
            <Row>
              <Col md="4">
                <Field
                  label={p.tc('order_stat.destination_country.label')}
                  placeholder={p.tc(`${countries.destination.length > 0 ? 'order_stat.destination_country.placeholder' : 'order_stat.destination_country.loading'}`)}
                  name={`${schedule}.destinationCountry`}
                  onChange={(e) => {onDestinationChange(e.value)}}
                  clearable={false}
                  items={countries.destination}
                  component={SelectFieldReact}
                  required='required'/>
              </Col>
              <Col md="4">
                   <Field
                     label={p.tc('order_stat.destination_city.label') }
                     placeholder={p.tc(`${ destinationCitiesDisplay ? 'order_stat.destination_city.placeholder' : 'order_stat.destination_city.loading'}`)}
                     name={`${schedule}.destinationCity`}
                     clearable={false}
                     simpleValue={true}
                     items={destinationCitiesDisplay}
                     component={SelectFieldReact}
                     required='required'/>
              </Col>
              <Col md="4">
                <Field
                  className="form-control input-sm"
                  type="text"
                  label={p.tu('order_stat.pod.label')}
                  placeholder ={p.tc('order_stat.pod.placeholder')}
                  name={`${schedule}.pod`}
                  component={TextField}
                  required='required'/>
              </Col>
            </Row>
            <Row>
              <Col md="12">
                { fields.length > 1 ? (
                  <Button
                    className="btn btn-danger btn-xs btn-block"
                    value={`remove schedule ${index+1}`}
                    icon="remove"
                    onClick={() => fields.remove(index)}/>
                ) : (
                  <Button
                    className="btn btn-info btn-xs btn-block"
                    value={`remove schedule ${index+1}`}
                    icon="remove"/>
                )}
              </Col>
            </Row>
          </Col>
        </Row>
        )
      })}
      <Row className="m-t-20">
        <Col md="8">
          <Button
            className="btn btn-primary btn-sm"
            onClick={() => fields.push({})}
            value={p.tc('order_stat.action.add_schedule')}
            icon="plus"/>
        </Col>
        <Col md="4">
          <p className="text text-primary">{}</p>
        </Col>
      </Row>
    </div>
  )
}
const renderPayments = (props) => {
  const { fields, p, bankAcc} = props;
  const displayPaymentMethodOptions = paymentMethodOptions.reduce( (types,type) => {
          const { value, label } = type;
          return types.concat({ value, label : p.tu(label)});
        }, []);
  return (
    <div>
    { fields.map( (payment, index) => {
      return (
        <Row key={index}>
          <Col>
            <Row>
              <Col md="4">
                <Field
                  className="form-control input-sm"
                  type="text"
                  label={p.tu('order_stat.shipper_bank_name.label')}
                  placeholder ={p.tc('order_stat.shipper_bank_name.placeholder')}
                  name={`${payment}.shipperBankName`}
                  component={TextField}
                  required='required'/>
              </Col>
              <Col md="4">
                <Field
                  className="form-control input-sm"
                  type="text"
                  label={p.tu('order_stat.shipper_bank_account_name.label')}
                  placeholder ={p.tc('order_stat.shipper_bank_account_name.placeholder')}
                  name={`${payment}.shipperBankAccName`}
                  component={TextField}
                  required='required'/>
              </Col>
              <Col md="4">
                <Field
                  className="form-control input-sm"
                  type="text"
                  label={p.tu('order_stat.shipper_bank_account_number.label')}
                  placeholder ={p.tc('order_stat.shipper_bank_account_number.placeholder')}
                  name={`${payment}.shipperBankAccNumber`}
                  component={TextField}
                  required='required'/>
              </Col>
            </Row>
            <Row>
              <Col md="4">
                <Field
                  type="text"
                  label={p.tu('order_stat.payment_method.label')}
                  placeholder ={p.tc('order_stat.payment_method.placeholder')}
                  items={displayPaymentMethodOptions}
                  clearable={false}
                  simpleValue={true}
                  name={`${payment}.paymentMethod`}
                  component={SelectFieldReact}
                  required='required'/>
              </Col>
              <Col md="8">
                <Field
                  type="text"
                  label={p.tu('order_stat.payment_to_bank.label')}
                  placeholder ={p.tc('order_stat.payment_to_bank.placeholder')}
                  items={bankAcc}
                  clearable={false}
                  simpleValue={true}
                  name={`${payment}.paymentToBank`}
                  component={SelectFieldReact}
                  required='required'/>
              </Col>
            </Row>
            <Row>
              <Col md="6">
                <Field
                  className="form-control input-sm"
                  type="text"
                  label={p.tu('order_stat.payment_reference_number.label')}
                  placeholder ={p.tc('order_stat.payment_reference_number.placeholder')}
                  name={`${payment}.paymentReferenceNumber`}
                  component={TextField}
                  required='required'/>
              </Col>
              <Col md="6">
                <Field
                  className="form-control"
                  type="text"
                  label={p.tu('order_stat.payment_date.label')}
                  placeholder ={p.tc('order_stat.payment_date.placeholder')}
                  name={`${payment}.paymentDate`}
                  maxDate={Moment()}
                  component={DateField}
                  required='required'/>
              </Col>
            </Row>
            <Row>
              <Col md="3" >
                <Field
                  type="text"
                  label={p.tu('order_stat.payment_currency_code.label')}
                  placeholder ={p.tc('order_stat.payment_currency_code.placeholder')}
                  items={currencyOptions}
                  simpleValue={true}
                  clearable={false}
                  name={`${payment}.paymentCurrencyCode`}
                  component={SelectFieldReact}
                  required='required'/>
              </Col>
              <Col md="6">
                <Field
                  className="form-control input-sm"
                  type="text"
                  label={p.tu('order_stat.payment_total_paid.label')}
                  placeholder ={p.tc('order_stat.payment_total_paid.placeholder')}
                  name={`${payment}.paymentTotalPaid`}
                  component={NumberField}
                  required='required'/>
              </Col>
              <Col md="2">
                { fields.length > 1 ? (
                  <Button
                    className="btn btn-danger btn-xs m-t-40 m-l-40"
                    value={`remove`}
                    icon="remove"
                    onClick={() => fields.remove(index)}/>
                ) : (
                  <Button
                    className="btn btn-info btn-xs m-t-40 m-l-40"
                    value={`remove`}
                    icon="remove"/>
                )}
              </Col>
            </Row>
          </Col>
        </Row>)
    }) }
    <Row className="m-t-20">
      <Col md="8">
        <Button
          className="btn btn-primary btn-sm"
          onClick={() => fields.push({})}
          value={p.tc('order_stat.action.add_payment')}
          icon="plus"/>
      </Col>
      <Col md="4">
        <p className="text text-primary">{}</p>
      </Col>
    </Row>
  </div>)
}
class ShipmentDetailOrderStatForm extends Component {
  constructor(props){
    super(props);
    this._submitForm = this._submitForm.bind(this);
    this._closeUpdateOrderStatModal = this._closeUpdateOrderStatModal.bind(this);
    this._handleFetchOriginCities = this._handleFetchOriginCities.bind(this);
    this._handleFetchDestinationCities = this._handleFetchDestinationCities.bind(this);
  }
  _handleFetchOriginCities(originCountry){
    if(!this.props.cities.origin[originCountry]){
        this.props.fetchCitiesOrigin(originCountry);
      }
  }
  _handleFetchDestinationCities(destinationCountry){
    if(!this.props.cities.destination[destinationCountry]){
        this.props.fetchCitiesDestination(destinationCountry);
      }
  }
  _closeUpdateOrderStatModal(shipmentId){
    this.props.closeUpdateOrderStatModal(shipmentId);
  }
  _submitForm(form){
    const { shipmentId, shipmentDetail } = this.props;
    this.props.updateShipmentOrderStat({form, shipmentId, orderNumber : shipmentDetail[shipmentId].orderNumber});
  }
  componentDidMount(){
    this.props.fetchShipmentBankAcc();
    this.props.fetchCountriesOrigin();
    this.props.fetchCountriesDestination();
  }
  render(){
    const { p, pSearch, pError, company, shipmentDetail, shipmentDetailOrderStat, shipmentId, handleSubmit, formValueSelect, cities, countries } = this.props;
    const displayOrderStatOptions = company.profile.type === 'VENDOR' ? orderStatVendorOptions.map(opt => ({
                                      label : p.tc(`${opt.label}`),
                                      value : opt.value
                                    }))
                                    : orderStatAdminOptions.map(opt => ({
                                      label : p.tc(`${opt.label}`),
                                      value : opt.value
                                    }));

    if(shipmentDetailOrderStat.isFetching && !shipmentDetailOrderStat.isSuccessful) {
      return (
        <ReactLoader message={p.tc('order_stat.loading')}/>
      )
    }else if(!shipmentDetailOrderStat.isFetching && shipmentDetailOrderStat.isSuccessful){
      return (
        <div>
          <Row className="m-t-20">
            <Col md={{size : 5, offset : 1}} className="p-t-10">
              <img src={Logo} alt="logo"/>
            </Col>
            <Col md={{size: 2, offset : 3}}>
              <ButtonStrap outline onClick={this._closeUpdateOrderStatModal.bind(this, shipmentId)}><i className="fa fa-remove"></i> {p.tc('close')} </ButtonStrap>
            </Col>
          </Row>
          <Row>
            <Col tag='h4' className="text-center">
              {p.tc('order_stat.success')}
            </Col>
          </Row>
      </div>
      )
    }
    return (
      <form onSubmit={handleSubmit(this._submitForm.bind(this))}>
        <Row className="m-t-20">
          <Col md={{size : 5, offset : 1}} className="p-t-10">
            <img src={Logo} alt="logo"/>
          </Col>
          <Col md={{size: 2, offset : 3}}>
                <ButtonStrap outline onClick={this._closeUpdateOrderStatModal.bind(this, shipmentId)}><i className="fa fa-remove"></i> {p.tc('close')} </ButtonStrap>
          </Col>
        </Row>
        <Row>
          <Col md="12" tag="h4" className="m-t-20 text-center">
            {p.tc('order_stat.title')}
          </Col>
        </Row>
        <Row>
          <Col md="12" tag="h6" className="m-t-20 text-center">
            {p.tc('order_stat.order_number')}{shipmentDetail[shipmentId].orderNumber}
          </Col>
        </Row>
        <Row>
          <Col md="12">
            <Field
              label={p.tu('order_stat.label')}
              name='orderStat'
              items={displayOrderStatOptions}
              placeholder={p.tc('order_stat.placeholder')}
              clearable={false}
              simpleValue={true}
              component={SelectFieldReact}
              required='required'/>
          </Col>
        </Row>
        { formValueSelect.orderStat  === 'SCHEDULED' && (
          <FieldArray name="schedules" p={p}
            component={renderSchedules}
            formValue={formValueSelect}
            cities={cities}
            countries={countries}
            onOriginChange={this._handleFetchOriginCities.bind(this)}
            onDestinationChange={this._handleFetchDestinationCities.bind(this)}/>
        )}
        { formValueSelect.orderStat  === 'PAYMENT' && (
          <div>
          <Row>
            <Col md="6">
              <Field
                className="form-control"
                type="text"
                label={p.tu('order_stat.due_date.label')}
                placeholder ={p.tc('order_stat.due_date.placeholder')}
                name={`invoiceDueDate`}
                minDate={Moment()}
                maxDate={Moment().add(3,"months")}
                component={DateField}
                required='required'/>
            </Col>
            <Col md="6">
              <Field
                className="form-control"
                label={`${p.tu('order_stat.currency_rate.label')} from ${shipmentDetail[shipmentId].bookPriceCurrencyCode} to IDR`}
                placeholder ={p.tc('order_stat.currency_rate.placeholder')}
                name={`invoiceCurrencyRate`}
                component={NumberField}
                required='required'/>
            </Col>
          </Row>
          <Row>
            <Col md="6">
              <Field
                className="form-control input-sm"
                type="text"
                label={p.tu('order_stat.ppn.label')}
                placeholder ={p.tc('order_stat.ppn.placeholder')}
                name={`invoicePPN`}
                component={TextField}
                required='required'/>
            </Col>
          </Row>
          <Row>
            <Col md="12">
              <Field
                className="form-control input-sm"
                label={p.tu('order_stat.note.label')}
                placeholder={p.tc('order_stat.note.placeholder')}
                name={`invoiceNote`}
                component={TextArea}
                required='required'/>
            </Col>
          </Row>
          </div>
        )}
        { formValueSelect.orderStat === 'COMPLETE' && (
          <FieldArray name="payments" p={p} pSearch={pSearch} component={renderPayments} bankAcc={ shipmentDetailOrderStat.bankAcc }/>
        )}
        <Row>
          <Col md="7">
            <small className="text-danger">{shipmentDetailOrderStat.message && pError.tc(shipmentDetailOrderStat.message)}</small>
          </Col>
          <Col md="5">
            <ButtonStrap className="btn btn-block btn-primary" type="submit">{p.tc('order_stat.action.edit')}</ButtonStrap>
          </Col>
        </Row>
      </form>
    )
  }
}
const selector = formValueSelector("shipmentDetailOrderStatForm");

const mapStateToProps = (state,ownProps) => {
  const { company, search : { formValue : { countries, cities } }, shipment : { shipmentDetail }, page : { modal : { shipmentDetailOrderStat } } } = state;
  const p = getP(state, { polyglotScope : 'shipment.detail.form'} );
  const pSearch = getP(state, { polyglotScope : 'search.form'});
  const pError = getP(state, { polyglotScope : 'error'});
  const { shipmentId, bankAcc } = shipmentDetailOrderStat;
  const { orderStat } = shipmentDetail[shipmentId];
  let _initialValues;
  if (orderStat === 'SCHEDULED'){
    _initialValues = {
      orderStat,
      schedules : [{
        dateDeparture : Moment().format('DD/MM/YY'),
        dateArrival : Moment().add(3, 'days').format('DD/MM/YY'),
        vesselName : '',
        pol : 'IDTPP',
        pod : 'SGSCT'
      }]
    }
  } else if ( orderStat === 'PAYMENT'){
    _initialValues = {
      orderStat,
      invoiceDueDate : Moment().add(14,'days').format('DD/MM/YY'),
      invoiceCurrencyRate : 0,
      invoicePPN : 0,
      invoiceNote : ''
    }
  } else if ( orderStat === 'COMPLETE'){
    _initialValues = {
      orderStat,
      payments : [{
        paymentCurrencyCode : 'IDR',
        paymentDate : Moment().format('DD/MM/YY'),
        paymentToBank : bankAcc.length > 0 ? bankAcc[0].value : null
      }]
    }
  } else {
    _initialValues = {
      orderStat
    }
  }
  return {
    company,
    countries,
    cities,
    pError,
    p,
    pSearch,
    shipmentDetail,
    shipmentDetailOrderStat,
    shipmentId,
    formValueSelect : selector(state, 'orderStat', 'schedules'),
    initialValues : _initialValues
  }
}
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    closeUpdateOrderStatModal,
    updateShipmentOrderStat,
    fetchShipmentBankAcc,
    fetchCountriesOrigin,
    fetchCountriesDestination,
    fetchCitiesOrigin,
    fetchCitiesDestination,
  }, dispatch)
}
ShipmentDetailOrderStatForm = reduxForm({
  form : 'shipmentDetailOrderStatForm',
  enableReinitialize : true,
  validate : validationOrderStat
})(ShipmentDetailOrderStatForm)

ShipmentDetailOrderStatForm = connect(mapStateToProps,mapDispatchToProps)(ShipmentDetailOrderStatForm)

export default ShipmentDetailOrderStatForm;
