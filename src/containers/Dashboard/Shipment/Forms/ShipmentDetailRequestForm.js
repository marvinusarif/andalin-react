import React, { Component } from 'react';
import Logo from '../../../../assets/img/logo.png';
import { Row, Col, Button as ButtonStrap } from 'reactstrap';
import { reduxForm, Field, FieldArray, formValueSelector } from 'redux-form';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { TextField, HiddenField, SelectField, Button } from '../../../../components/Bootstrap/Forms/';
import { closeUpdateOrderRequestModal } from '../../../../actions/page';
import { updateShipmentOrderRequest } from '../../../../actions/shipment';
import { ReactLoader } from '../../../../components/Dashboard/Loader/';
import { getP } from 'redux-polyglot';
import { validationRequest } from './Validation';
import { requestOptions } from '../../Book/Forms/';
import { requestStatOptions } from './';

const renderRequests = (props) => {
  const {fields, requestsDetail, p, pBook, requestsSelect, company } = props;
  const oldRequestslength = requestsDetail.length;
  const convertedRequestOptions = requestOptions.reduce( (types,type) => {
          const { value, label } = type;
          return types.concat({ value, label : pBook.tu(label)});
        }, []);
  const convertedRequestStatOptions = requestStatOptions.reduce( (types,type) => {
          const { value, label } = type;
          return types.concat({ value, label : p.tu(label)});
        }, []);
  return (
    <div>
      { fields.map( (request,index) => {
        return ([
          <Row key={index}>
            <Col md="4">
              <Field
                name={`${request}.requestId`}
                component={HiddenField}/>
              <Field
                className="form-control input-sm"
                type="text"
                label={p.tu('requests.type')}
                name={`${request}.type`}
                items={convertedRequestOptions}
                component={SelectField}
                disabled={index < oldRequestslength ? true : false}
                required='required'/>
            </Col>
            <Col md="2">
              <Field
                className="form-control input-sm"
                type="text"
                label={p.tu('requests.value')}
                name={`${request}.value`}
                disabled={index < oldRequestslength ? true : false}
                component={TextField}
                required='required'/>
            </Col>
            <Col md="2" className="m-t-40">
              {requestsDetail[index] ? requestsDetail[index].unit : pBook.tc(`type.options.${requestsSelect[index].type}.unit`) }
            </Col>
            <Col md="3">
              { index < oldRequestslength && (
                <Field
                  className="form-control input-sm"
                  type="text"
                  label={p.tu('requests.approved.label')}
                  name={`${request}.stat`}
                  items={convertedRequestStatOptions}
                  disabled={requestsDetail[index].createdByComptp === company.profile.type ? true : false}
                  component={SelectField}
                  required='required'/>
              ) }
            </Col>
            <Col md="1" style={{ padding : '38px 0px'}}>
              { index >= oldRequestslength && (
              <Button
                className="btn btn-danger btn-xs"
                icon="remove"
                onClick={() => fields.remove(index)}/>
                ) }
            </Col>
          </Row>,
          <Row key={`note_${index}`}>
            <Col md="12">
              <Field
                className="form-control input-sm"
                type="text"
                label={p.tu('requests.note')}
                name={`${request}.note`}
                disabled={index < oldRequestslength ? true : false}
                component={TextField}
                required='required'/>
            </Col>
          </Row>
        ])
      })}
      { (company.profile.type && company.profile.type === 'USER') && (
        <Row>
          <Col md="8">
            <Button
              className="btn btn-primary btn-sm"
              onClick={() => fields.push({requestId : 0, type : 'free_time', value : '', note : ''})}
              value={p.tc('requests.action.add')}
              icon="plus"/>
          </Col>
        </Row>
      )}
    </div>
  )
}

class ShipmentDetailRequestForm extends Component {
  constructor(props){
    super(props);
    this._submitForm = this._submitForm.bind(this);
    this._closeUpdateOrderRequestModal = this._closeUpdateOrderRequestModal.bind(this);
  }
  _closeUpdateOrderRequestModal(shipmentId){
    this.props.closeUpdateOrderRequestModal(shipmentId);
  }
  _submitForm(_form){
    const { shipmentId, shipmentDetail } = this.props;
    const form = Object.assign({}, _form, {
      vendorId : shipmentDetail[shipmentId].vendorId
    })
    this.props.updateShipmentOrderRequest({form, shipmentId, orderNumber : shipmentDetail[shipmentId].orderNumber});
  }
  render(){
    const { p, pError, pBook, company, shipmentDetail, shipmentDetailRequest, shipmentId, handleSubmit, formValueSelect } = this.props;
    if(shipmentDetailRequest.isFetching && !shipmentDetailRequest.isSuccessful) {
      return (
        <ReactLoader message={p.tc('requests.loading')}/>
      )
    }else if(!shipmentDetailRequest.isFetching && shipmentDetailRequest.isSuccessful){
      return (
        <div>
          <Row className="m-t-20">
            <Col md={{size : 5, offset : 1}} className="p-t-10">
              <img src={Logo} alt="logo"/>
            </Col>
            <Col md={{size: 2, offset : 3}}>
                  <ButtonStrap outline onClick={this._closeUpdateOrderRequestModal.bind(this, shipmentId)}><i className="fa fa-remove"></i> {p.tc('close')} </ButtonStrap>
            </Col>
          </Row>
          <Row>
            <Col tag='h4' className="text-center">
              {p.tc('requests.success')}
            </Col>
          </Row>
      </div>
      )
    }
    return (
      <form onSubmit={handleSubmit(this._submitForm.bind(this))}>
        <Row className="m-t-20">
          <Col md={{size : 5, offset : 1}} className="p-t-10">
            <img src={Logo} alt="logo"/>
          </Col>
          <Col md={{size: 2, offset : 3}}>
            <ButtonStrap outline onClick={this._closeUpdateOrderRequestModal.bind(this, shipmentId)}><i className="fa fa-remove"></i> {p.tc('close')} </ButtonStrap>
          </Col>
        </Row>
        <Row>
          <Col md="12" tag="h4" className="m-t-20 text-center">
            {p.tc('requests.title')}
          </Col>
        </Row>
        <Row>
          <Col md="12" tag="h6" className="m-t-20 text-center">
            {p.tc('requests.order_number')}{shipmentDetail[shipmentId].orderNumber}
          </Col>
        </Row>
        <FieldArray name="requests" p={p} pBook={pBook} company={company} component={renderRequests} requestsSelect={formValueSelect ? formValueSelect : []} requestsDetail={shipmentDetail[shipmentId].requests}/>
        <Row>
          <Col md="7">
            <small className="text-danger">{shipmentDetailRequest.message && pError.tc(shipmentDetailRequest.message)}</small>
          </Col>
          <Col md="5">
            <ButtonStrap className="btn btn-block btn-primary" type="submit">{p.tc('requests.action.edit')}</ButtonStrap>
          </Col>
        </Row>
      </form>
    )
  }
}
const selector = formValueSelector('ShipmentDetailRequestForm');

const mapStateToProps = (state,ownProps) => {
  const { shipment : { shipmentDetail }, page : { modal : { shipmentDetailRequest } }, company } = state;
  const p = getP(state, { polyglotScope : 'shipment.detail.form'} );
  const pError = getP(state, { polyglotScope : 'error'});
  const pBook = getP(state, { polyglotScope : 'book.additional_request' });
  const { shipmentId } = shipmentDetailRequest;
  const { requests } = shipmentDetail[shipmentId];
  const _initialValues = {
    requests
  }
  return {
    pError,
    p,
    pBook,
    company,
    shipmentDetail,
    shipmentDetailRequest,
    shipmentId,
    formValueSelect: selector(state, 'requests'),
    initialValues : _initialValues
  }
}
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    closeUpdateOrderRequestModal,
    updateShipmentOrderRequest,
  }, dispatch)
}
ShipmentDetailRequestForm = reduxForm({
  form : 'ShipmentDetailRequestForm',
  enableReinitialize : true,
  validate : validationRequest
})(ShipmentDetailRequestForm)

ShipmentDetailRequestForm = connect(mapStateToProps,mapDispatchToProps)(ShipmentDetailRequestForm)

export default ShipmentDetailRequestForm;
