import React, { Component } from 'react';
import Logo from '../../../../assets/img/logo.png';
import { Row, Col, Button as ButtonStrap } from 'reactstrap';
import { reduxForm, Field, FieldArray } from 'redux-form';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { TextField, NumberField, SelectFieldReact, Button, HiddenField } from '../../../../components/Bootstrap/Forms/';
import { closeUpdateOrderPriceModal } from '../../../../actions/page';
import { updateShipmentOrderPrice } from '../../../../actions/shipment';
import { ReactLoader } from '../../../../components/Dashboard/Loader/';
import { getP } from 'redux-polyglot';
import { validationPrices } from './Validation';
import { priceComponentTypeOptions, priceUOMOptions } from './';

const renderPrices = (props) => {

  const {fields, p, bookCurrency } = props;
  const convertedPriceComponentTypeOptions = priceComponentTypeOptions.reduce( (types,type) => {
        const { value, label } = type;
        return types.concat({ value, label : p.tu(label)});
      }, []);
  const convertedPriceUOMOptions = priceUOMOptions.reduce( (types,type) => {
        const { value, label } = type;
        return types.concat({ value, label : p.tu(label)});
      }, []);
  return (
    <div>
      { fields.map( (price,index) => {
        return ([
          <Row key={index}>
            <Col md="4">
              <Field
                name={`${price}.bookPriceCurrencyCode`}
                component={HiddenField}/>
              <Field
                  label={p.tu('prices.component_type.label')}
                  name={`${price}.componentType`}
                  items={convertedPriceComponentTypeOptions}
                  placeholder={p.tc('prices.component_type.placeholder')}
                  clearable={false}
                  simpleValue={true}
                  component={SelectFieldReact}
                  required='required'/>
            </Col>
            <Col md="2">
              <Field
                className="form-control input-sm"
                label={p.tu('prices.qty.label')}
                name={`${price}.qty`}
                component={TextField}
                placeholder={p.tc('prices.qty.placeholder')}
                required='required'/>
            </Col>
            <Col md="4">
              <Field
                  label={p.tu('prices.price_uom.label')}
                  name={`${price}.priceUOM`}
                  items={convertedPriceUOMOptions}
                  placeholder={p.tc('prices.price_uom.placeholder')}
                  clearable={false}
                  simpleValue={true}
                  component={SelectFieldReact}
                  required='required'/>
            </Col>
            <Col md="2" style={{ padding : '38px 0px'}}>
              <Button
                className="btn btn-danger btn-xs"
                icon="remove"
                onClick={() => fields.remove(index)}/>
            </Col>
          </Row>,
          <Row>
            <Col md="6">
              <Field
                className="form-control input-sm"
                label={p.tu('prices.description.label')}
                name={`${price}.priceDesc`}
                component={TextField}
                placeholder={p.tc('prices.description.placeholder')}
                required='required'/>
            </Col>
            <Col md="6">
              <Field
                className="form-control input-sm"
                label={p.tu('prices.price_sum.label', { currency : bookCurrency })}
                name={`${price}.priceSum`}
                component={NumberField}
                placeholder={p.tc('prices.price_sum.placeholder')}
                required='required'/>
            </Col>
          </Row>
        ])
      })}
      <Row>
        <Col md="8">
          <Button
            className="btn btn-primary btn-sm"
            onClick={() => fields.push({bookPriceCurrencyCode : bookCurrency, componentType : 'FREIGHT', qty: 0, priceUOM : 'ALL' })}
            value={p.tc('prices.action.add')}
            icon="plus"/>
        </Col>
      </Row>
    </div>
  )
}

class ShipmentDetailPriceForm extends Component {
  constructor(props){
    super(props);
    this._submitForm = this._submitForm.bind(this);
    this._closeUpdateOrderPriceModal = this._closeUpdateOrderPriceModal.bind(this);
  }
  _closeUpdateOrderPriceModal(shipmentId){
    this.props.closeUpdateOrderPriceModal(shipmentId);
  }
  _submitForm(form){
    const { shipmentId, shipmentPrice } = this.props;
    this.props.updateShipmentOrderPrice({form, shipmentId, orderNumber : shipmentPrice[shipmentId][0].orderNumber});
  }
  render(){
    const { p, pError, company, shipmentPrice, shipmentDetailPrice, shipmentId, handleSubmit } = this.props;
    if(shipmentDetailPrice.isFetching && !shipmentDetailPrice.isSuccessful) {
      return (
        <ReactLoader message={p.tc('prices.loading')}/>
      )
    }else if(!shipmentDetailPrice.isFetching && shipmentDetailPrice.isSuccessful){
      return (
        <div>
          <Row className="m-t-20">
            <Col md={{size : 5, offset : 1}} className="p-t-10">
              <img src={Logo} alt="logo"/>
            </Col>
            <Col md={{size: 2, offset : 3}}>
                  <ButtonStrap outline onClick={this._closeUpdateOrderPriceModal.bind(this, shipmentId)}><i className="fa fa-remove"></i> {p.tc('close')} </ButtonStrap>
            </Col>
          </Row>
          <Row>
            <Col tag='h4' className="text-center">
              {p.tc('prices.success')}
            </Col>
          </Row>
      </div>
      )
    }
    return (
      <form onSubmit={handleSubmit(this._submitForm.bind(this))}>
        <Row className="m-t-20">
          <Col md={{size : 5, offset : 1}} className="p-t-10">
            <img src={Logo} alt="logo"/>
          </Col>
          <Col md={{size: 2, offset : 3}}>
            <ButtonStrap outline onClick={this._closeUpdateOrderPriceModal.bind(this, shipmentId)}><i className="fa fa-remove"></i> {p.tc('close')} </ButtonStrap>
          </Col>
        </Row>
        <Row>
          <Col md="12" tag="h4" className="m-t-20 text-center">
            {p.tc('prices.title')}
          </Col>
        </Row>
        <Row>
          <Col md="12" tag="h6" className="m-t-20 text-center">
            {p.tc('prices.order_number')}{shipmentPrice[shipmentId][0].orderNumber}
          </Col>
        </Row>
        <FieldArray name="prices" p={p} bookCurrency={shipmentPrice[shipmentId][0].bookPriceCurrencyCode} company={company} component={renderPrices}/>
        <Row>
          <Col md="7">
            <small className="text-danger">{shipmentDetailPrice.message && pError.tc(shipmentDetailPrice.message)}</small>
          </Col>
          <Col md="5">
            <ButtonStrap className="btn btn-block btn-primary" type="submit">{p.tc('prices.action.edit')}</ButtonStrap>
          </Col>
        </Row>
      </form>
    )
  }
}

const mapStateToProps = (state,ownProps) => {
  const { shipment : { shipmentPrice }, page : { modal : { shipmentDetailPrice } }, company } = state;
  const p = getP(state, { polyglotScope : 'shipment.detail.form'} );
  const pError = getP(state, { polyglotScope : 'error'});
  const { shipmentId } = shipmentDetailPrice;
  const { prices } = shipmentPrice[shipmentId];
  const _initialValues = {
    prices
  }
  return {
    prices,
    pError,
    p,
    company,
    shipmentPrice,
    shipmentDetailPrice,
    shipmentId,
    initialValues : _initialValues
  }
}
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    closeUpdateOrderPriceModal,
    updateShipmentOrderPrice,
  }, dispatch)
}
ShipmentDetailPriceForm = reduxForm({
  form : 'ShipmentDetailPriceForm',
  enableReinitialize : true,
  validate : validationPrices
})(ShipmentDetailPriceForm)

ShipmentDetailPriceForm = connect(mapStateToProps,mapDispatchToProps)(ShipmentDetailPriceForm)

export default ShipmentDetailPriceForm;
