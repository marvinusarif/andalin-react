import React, { Component } from 'react';
import Logo from '../../../../assets/img/logo.png';
import { Row, Col, Button as ButtonStrap } from 'reactstrap';
import { reduxForm, Field } from 'redux-form';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { TextField } from '../../../../components/Bootstrap/Forms/';
import { closeUpdateCarrierModal } from '../../../../actions/page';
import { updateShipmentCarrier } from '../../../../actions/shipment';
import { ReactLoader } from '../../../../components/Dashboard/Loader/';
import { getP } from 'redux-polyglot';
import { validationCarrier } from './Validation';

class ShipmentDetailCarrierForm extends Component {
  constructor(props){
    super(props);
    this._submitForm = this._submitForm.bind(this);
    this._closeUpdateCarrierModal = this._closeUpdateCarrierModal.bind(this);
  }
  _closeUpdateCarrierModal(shipmentId){
    this.props.closeUpdateCarrierModal(shipmentId);
  }
  _submitForm(form){
    const { shipmentId, shipmentDetail } = this.props;
    this.props.updateShipmentCarrier({form, shipmentId, orderNumber : shipmentDetail[shipmentId].orderNumber});
  }
  render(){
    const { p, pError, shipmentDetail, shipmentDetailCarrier, shipmentId, handleSubmit } = this.props;

    if(shipmentDetailCarrier.isFetching && !shipmentDetailCarrier.isSuccessful) {
      return (
        <ReactLoader message={p.tc('carrier.loading')}/>
      )
    }else if(!shipmentDetailCarrier.isFetching && shipmentDetailCarrier.isSuccessful){
      return (
        <div>
          <Row className="m-t-20">
            <Col md={{size : 5, offset : 1}} className="p-t-10">
              <img src={Logo} alt="logo"/>
            </Col>
            <Col md={{size: 2, offset : 3}}>
                  <ButtonStrap outline onClick={this._closeUpdateCarrierModal.bind(this, shipmentId)}><i className="fa fa-remove"></i> {p.tc('close')} </ButtonStrap>
            </Col>
          </Row>
          <Row>
            <Col tag='h4' className="text-center">
              {p.tc('carrier.success')}
            </Col>
          </Row>
      </div>
      )
    }
    return (
      <form onSubmit={handleSubmit(this._submitForm.bind(this))}>
        <Row className="m-t-20">
          <Col md={{size : 5, offset : 1}} className="p-t-10">
            <img src={Logo} alt="logo"/>
          </Col>
          <Col md={{size: 2, offset : 3}}>
            <ButtonStrap outline onClick={this._closeUpdateCarrierModal.bind(this, shipmentId)}><i className="fa fa-remove"></i> {p.tc('close')} </ButtonStrap>
          </Col>
        </Row>
        <Row>
          <Col md="12" tag="h4" className="m-t-20 text-center">
            {p.tc('carrier.title')}
          </Col>
        </Row>
        <Row>
          <Col md="12" tag="h6" className="m-t-20 text-center">
            {p.tc('carrier.order_number')}{shipmentDetail[shipmentId].orderNumber}
          </Col>
        </Row>
        <Row>
          <Col md="4">
            <Field
              className="form-control input-sm"
              label={p.tu('carrier.name.label')}
              name='carrierName'
              component={TextField}
              placeholder={p.tc('carrier.name.placeholder')}
              required='required'/>
          </Col>
          <Col md="4">
            <Field
              className="form-control input-sm"
              label={p.tu('carrier.bl_awb.label')}
              name='carrierBlAwb'
              component={TextField}
              placeholder={p.tc('carrier.bl_awb.placeholder')}
              required='required'/>
          </Col>
          <Col md="4">
            <Field
              className="form-control input-sm"
              label={p.tu('carrier.contract.label')}
              name='carrierContract'
              component={TextField}
              placeholder={p.tc('carrier.contract.placeholder')}
              required='required'/>
          </Col>
        </Row>
        <Row>
          <Col md="7">
            <small className="text-danger">{shipmentDetailCarrier.message && pError.tc(shipmentDetailCarrier.message)}</small>
          </Col>
          <Col md="5">
            <ButtonStrap className="btn btn-block btn-primary" type="submit">{p.tc('carrier.action.edit')}</ButtonStrap>
          </Col>
        </Row>
      </form>
    )
  }
}
const mapStateToProps = (state,ownProps) => {
  const { shipment : { shipmentDetail }, page : { modal : { shipmentDetailCarrier } } } = state;
  const p = getP(state, { polyglotScope : 'shipment.detail.form'} );
  const pError = getP(state, { polyglotScope : 'error'});
  const { shipmentId } = shipmentDetailCarrier;
  const { carrierBlAwb, carrierName, carrierContract } = shipmentDetail[shipmentId];
  const _initialValues = {
    carrierBlAwb,
    carrierName,
    carrierContract
  }
  return {
    pError,
    p,
    shipmentDetail,
    shipmentDetailCarrier,
    shipmentId,
    initialValues : _initialValues
  }
}
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    closeUpdateCarrierModal,
    updateShipmentCarrier,
  }, dispatch)
}
ShipmentDetailCarrierForm = reduxForm({
  form : 'ShipmentDetailCarrierForm',
  enableReinitialize : true,
  validate : validationCarrier
})(ShipmentDetailCarrierForm)

ShipmentDetailCarrierForm = connect(mapStateToProps,mapDispatchToProps)(ShipmentDetailCarrierForm)

export default ShipmentDetailCarrierForm;
