export const validateShipperConsignee = (values,{ p }) => {
  const errors = {}
  if(!values.shipperCompany) {
    errors.shipperCompany = p.tc('validation.required');
  }
  if(!values.shipperPICName) {
    errors.shipperPICName = p.tc('validation.required');
  }
  if(!values.shipperAddress){
    errors.shipperAddress = p.tc('validation.required');
  }
  if(!values.shipperPhone){
    errors.shipperPhone = p.tc('validation.required');
  }
  if(!values.shipperEmail){
    errors.shipperEmail = p.tc('validation.required');
  }else if(!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.shipperEmail)){
    errors.shipperEmail = p.tc('validation.email_invalid');
  }
  if(!values.shipperFax){
    errors.shipperFax = p.tc('validation.required');
  }
  if(!values.consigneeCompany) {
    errors.consigneeCompany = p.tc('validation.required');
  }
  if(!values.consigneePICName) {
    errors.consigneePICName = p.tc('validation.required');
  }
  if(!values.consigneeAddress){
    errors.consigneeAddress = p.tc('validation.required');
  }
  if(!values.consigneePhone){
    errors.consigneePhone = p.tc('validation.required');
  }
  if(!values.consigneeEmail){
    errors.consigneeEmail = p.tc('validation.required');
  }else if(!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.consigneeEmail)){
    errors.consigneeEmail = p.tc('validation.email_invalid');
  }
  if(!values.consigneeFax){
    errors.consigneeFax = p.tc('validation.required');
  }
  return errors;
}

export const validationCommodities = (values, {p}) => {
  const errors = {}
  if(!values.commoditiesHSCode){
    errors.commoditiesHSCode = p.tc('validation.required');
  }
  if(!values.commoditiesProduct){
    errors.commoditiesProduct = p.tc('validation.required');
  }
  return errors;
}

export const validationCarrier = (values, {p}) => {
  const errors = {}
  if(!values.carrierBlAwb){
    errors.carrierBlAwb = p.tc('validation.required');
  }
  if(!values.carrierName || values.carrierName === '-'){
    errors.carrierName = p.tc('validation.required');
  }
  if(!values.carrierContract){
    errors.carrierContract = p.tc('validation.required');
  }
  return errors;
}

export const validationFreight = (values, {p}) => {
  const errors = {}
  if(!values.freights.length){
    errors.freights = { _error : 'At least one load must be entered'}
  }else{
    const freightArrayError = [];
    values.freights.forEach( (freight,index) => {
      const freightError = { trackingNumber : null }
      if(!freight.trackingNumber){
        freightError.trackingNumber = p.tc('validation.required');
      }
      freightArrayError[index] = freightError;
      return freightError;
    })
    if(freightArrayError.length) {
      errors.freights = freightArrayError;
    }
  }
  return errors;
}

export const validationRequest = (values, {p}) => {
  const errors = {}
  if(values.requests.length < 1){
    errors.requests = { _error : 'At least one request must be entered'}
  }else{
    const requestsArrayError = [];
    values.requests.forEach( (request,index) => {
      const requestError = { value : null }
      if(!request.value){
        requestError.value = p.tc('validation.required');
      }
      requestsArrayError[index] = requestError;
      return requestError;
    })
    if(requestsArrayError.length) {
      errors.requests = requestsArrayError;
    }
  }
  return errors;
}

export const validationPrices = (values, {p}) => {
  const errors = {}
  if(!values.prices){
    errors.prices = { _error : 'At least one price must be entered'}
  }else{
    const pricesArrayError = [];
    values.prices.forEach( (price,index) => {
      const priceError = { qty : null , priceDesc : null, priceSum : null }
      if(!price.qty){
        priceError.qty = p.tc('validation.required');
      }
      if(!price.priceDesc){
        priceError.priceDesc = p.tc('validation.required');
      }
      if(!price.priceSum){
        priceError.priceSum = p.tc('validation.required');
      }
      pricesArrayError[index] = priceError;
      return priceError;
    })
    if(pricesArrayError.length) {
      errors.prices = pricesArrayError;
    }
  }
  return errors;
}

export const validationFile = (values,{ p }) => {
  const errors = {};
  if(!values.documentDesc){
    errors.documentDesc = p.tc('validation.required');
  }
  if(!values.documentCategory){
    errors.documentCategory = p.tc('validation.required');
  }
  return errors;
}

export const validationOrderStat = (values, { p, formValueSelect}) => {
  const errors = {};
  if(formValueSelect.orderStat === 'SCHEDULED'){
    if(!values.schedules){
      errors.schedules = { _error : 'At least one schedule must be entered'}
    }else{
      const arrayScheduleErrors = [];
      values.schedules.forEach( (schedule,index) => {
        const scheduleError = {
          portCutoffTime : null,
          inlandCutoffTime : null,
          dateDeparture : null,
          dateArrival : null,
          vesselName : null,
          pol :null ,
          pod :null,
          originCity : null,
          originCountry : null,
          destinationCity : null,
          destinationCountry : null
        }
        if(index === 0){
          if(!schedule.portCutoffTime){
            scheduleError.portCutoffTime = p.tc('validation.required');
          }
          if(!schedule.inlandCutoffTime){
            scheduleError.inlandCutoffTime = p.tc('validation.required');
          }
        }
        if(!schedule.dateDeparture){
          scheduleError.dateDeparture = p.tc('validation.required');
        }
        if(!schedule.dateArrival){
          scheduleError.dateArrival = p.tc('validation.required');
        }
        if(!schedule.vesselName){
          scheduleError.vesselName = p.tc('validation.required');
        }
        if(!schedule.pod){
          scheduleError.pod = p.tc('validation.required');
        }
        if(!schedule.pol){
          scheduleError.pol = p.tc('validation.required');
        }
        if(!schedule.originCity){
          scheduleError.originCity = p.tc('validation.required');
        }
        if(!schedule.originCountry){
          scheduleError.originCountry = p.tc('validation.required');
        }
        if(!schedule.destinationCity){
          scheduleError.destinationCity = p.tc('validation.required');
        }
        if(!schedule.destinationCountry){
          scheduleError.destinationCountry = p.tc('validation.required');
        }
        arrayScheduleErrors[index] = scheduleError;
        return scheduleError
      })
      if(arrayScheduleErrors.length) {
        errors.schedules = arrayScheduleErrors;
      }
    }
  } else if ( formValueSelect.orderStat === 'PAYMENT') {
    if(!values.invoiceDueDate){
      errors.invoiceDueDate = p.tc('validation.required');
    }
    if(!values.invoiceCurrencyRate){
      errors.invoiceCurrencyRate = p.tc('validation.required');
    }
    if(!values.invoicePPN){
      errors.invoicePPN = p.tc('validation.required');
    }
    if(!values.invoiceNote){
      errors.invoiceNote = p.tc('validation.required');
    }
  } else if ( formValueSelect.orderStat === 'COMPLETE'){
    if(!values.payments){
      errors.payments = { _error : 'At least one payment should be inserted' }
    } else {
      const arrayPaymentErrors = [];
      values.payments.forEach( (payment,index) => {
        const paymentError = {
          shipperBankName : null,
          shipperBankAccName : null,
          shipperBankAccNumber : null,
          paymentToBank : null,
          paymentReferenceNumber : null,
          paymentDate : null,
          paymentCurrencyCode : null,
          paymentTotalPaid : null
        }
        if(!payment.shipperBankName){
          paymentError.shipperBankName =  p.tc('validation.required');
        }
        if(!payment.shipperBankAccName){
          paymentError.shipperBankAccName =  p.tc('validation.required');
        }
        if(!payment.shipperBankAccNumber){
          paymentError.shipperBankAccNumber =  p.tc('validation.required');
        }
        if(!payment.paymentToBank){
          paymentError.paymentToBank =  p.tc('validation.required');
        }
        if(!payment.paymentReferenceNumber){
          paymentError.paymentReferenceNumber=  p.tc('validation.required');
        }
        if(!payment.paymentDate){
          paymentError.paymentDate =  p.tc('validation.required');
        }
        if(!payment.paymentCurrencyCode){
          paymentError.paymentCurrencyCode =  p.tc('validation.required');
        }
        if(!payment.paymentTotalPaid){
          paymentError.paymentTotalPaid =  p.tc('validation.required');
        }
        arrayPaymentErrors[index] = paymentError;
        return paymentError;
      });
      if(arrayPaymentErrors.length > 0){
        errors.payments = arrayPaymentErrors;
      }
    }
  } else {
    if(!values.orderStat) {
      errors.orderStat = p.tc('validation.required');
    }
  }
  return errors;
}
