import _ from 'lodash';
import classnames from 'classnames';
import moment from 'moment';
import React, { Component } from 'react';
import { Container, Nav, TabContent, TabPane, NavItem, NavLink, Row, Col, Card, CardBlock, Button as ButtonStrap, Badge } from 'reactstrap';
import NumberFormat from 'react-number-format';
import { bindActionCreators } from 'redux';
import { reduxForm, Field, formValueSelector } from 'redux-form';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { getP } from 'redux-polyglot';
import { ReactLoader } from '../../../components/Dashboard/Loader/';
import { TextField, SelectField } from '../../../components/Bootstrap/Forms/';
import { fetchCompany } from '../../../actions/company';
import { fetchShipments, fetchShipmentDetail, fetchShipmentPrice, fetchShipmentCarrier, fetchShipmentDocument, fetchShipmentHistory, downloadShipmentDoc, downloadShipmentInvoice } from '../../../actions/shipment';
import { fetchShipmentMessagesHead } from '../../../actions/message';
import { openPostUpdateShipmentDocs, openUpdateShipperConsigneeModal, openUpdateOrderStatModal, openUpdateOrderRequestModal, openUpdateCommoditiesModal, openUpdateFreightModal, openUpdateCarrierModal, openUpdateOrderPriceModal } from '../../../actions/page';
import { orderByOptions, orderByDirectionOptions, orderStatAdminOptions } from './Forms/';
import Message from '../Message/';
import MessagesForm from '../Message/Forms/MessagesForm';


const getPriceUOM = (p, priceUOM) => {
  if(priceUOM === 'V'){
    return p.tc('detail.price.display.uom_options.volume');
  }else if(priceUOM === 'W'){
    return p.tc('detail.price.display.uom_options.weight');
  }else if(priceUOM === 'CONT'){
    return p.tc('detail.price.display.uom_options.container');
  }else if(priceUOM === 'ALL'){
    return p.tc('detail.price.display.uom_options.shipment');
  }else{
    return p.tc('detail.price.display.uom_options.volumetric');
  }
}

class Shipment extends Component {
  constructor(props){
    super(props);
    this._toggle = this._toggle.bind(this);
    this.state = {
      activeTab : {}
    }
    this._handleUpdateShipperConsigneeModal = this._handleUpdateShipperConsigneeModal.bind(this);
    this._handleUpdateOrderStatModal = this._handleUpdateOrderStatModal.bind(this);
    this._handleUpdateOrderPriceModal = this._handleUpdateOrderPriceModal.bind(this);
    this._handleUpdateCarrierModal = this._handleUpdateCarrierModal.bind(this);
    this._handleUpdateCommoditiesModal = this._handleUpdateCommoditiesModal.bind(this);
    this._handleUpdateFreightModal = this._handleUpdateFreightModal.bind(this);
    this._handleUpdateOrderRequestModal = this._handleUpdateOrderRequestModal.bind(this);
    this._handlePostUpdateShipmentDocs = this._handlePostUpdateShipmentDocs.bind(this);
    this._handleDownloadShipmentDoc = this._handleDownloadShipmentDoc.bind(this);
    this._handleDownloadShipmentInvoice = this._handleDownloadShipmentInvoice.bind(this);
    this._getBackground = this._getBackground.bind(this);
  }
  _handleUpdateShipperConsigneeModal(shipmentId){
    this.props.openUpdateShipperConsigneeModal(shipmentId);
  }
  _handleUpdateOrderStatModal(shipmentId){
    this.props.openUpdateOrderStatModal(shipmentId);
  }
  _handleUpdateCommoditiesModal(shipmentId){
    this.props.openUpdateCommoditiesModal(shipmentId);
  }
  _handleUpdateCarrierModal(shipmentId){
    this.props.openUpdateCarrierModal(shipmentId);
  }
  _handleUpdateOrderPriceModal(shipmentId){
    this.props.openUpdateOrderPriceModal(shipmentId);
  }
  _handleUpdateFreightModal(shipmentId){
    this.props.openUpdateFreightModal(shipmentId);
  }
  _handleUpdateOrderRequestModal(shipmentId){
    this.props.openUpdateOrderRequestModal(shipmentId);
  }
  _handlePostUpdateShipmentDocs(shipmentId){
    this.props.openPostUpdateShipmentDocs(shipmentId);
  }
  _handleDownloadShipmentDoc({shipmentId, orderNumber, fileCategory, fileId, extFileName, saveAsFileName}){
    this.props.downloadShipmentDoc({shipmentId, orderNumber, fileCategory, fileId, extFileName, saveAsFileName})
  }
  _handleDownloadShipmentInvoice({shipmentId, orderNumber, orderStat}){
    let finalOrProforma
    if(orderStat === 'PAYMENT'){
      finalOrProforma = 'FINAL'
    }else if ( orderStat === 'COMPLETE' ){
      finalOrProforma = 'PAID_FINAL'
    }else {
      finalOrProforma = 'PROFORMA'
    }
    this.props.downloadShipmentInvoice({shipmentId, orderNumber, saveAsFileName : `${orderNumber.split('/').join('-')}_${finalOrProforma}_INV`, extFileName : '.pdf'});
  }
  _getBackground(orderStat){
    if(orderStat === 'BOOK'){
      return '#f2f2f2'
    }else if(orderStat === 'CONFIRM'){
      return '#e5e5ff'
    }else if(orderStat === 'DISPATCH'){
      return '#ffffcc'
    }else if(orderStat === 'DISCHARGE'){
      return '#e5f2e5'
    }else if(orderStat === 'PAYMENT'){
      return '#e5d5df'
    }else{
      return '#e5f2e5'
    }
  }
  _toggle({shipmentId, orderNumber}, tab){
    if (this.state.activeTab[shipmentId] !== tab) {
     this.setState({
       activeTab: Object.assign({}, this.state.activeTab, {
         [shipmentId] : tab
       })
     });
     if(tab === 'detail') {
       this.props.fetchShipmentDetail({shipmentId, orderNumber});
     }else if (tab === 'price'){
       this.props.fetchShipmentDetail({shipmentId, orderNumber});
       this.props.fetchShipmentPrice({shipmentId, orderNumber});
     }else if (tab === 'document'){
       this.props.fetchShipmentDocument({shipmentId, orderNumber});
     }else if (tab === 'carrier'){
       this.props.fetchShipmentCarrier({shipmentId, orderNumber});
     }else if (tab === 'history'){
       this.props.fetchShipmentHistory({shipmentId, orderNumber});
     }
   }else{
     this.setState({
       activeTab: Object.assign({}, this.state.activeTab, {
         [shipmentId] : ''
       })
     });
   }
  }
  componentWillMount(){
    !this.props.user.session.isValid && this.props.history.push('/search');
    this.props.fetchCompany()
  }
  componentDidMount(){
     this.props.fetchShipments();
  }
  render(){
    const { shipment, p, pProductType, formValueSelect, company, message} = this.props;
    const { activeTab } = this.state;
    let filteredShipment;
    const convertedOrderByOptions = orderByOptions.reduce( (types,type) => {
      const { value, label } = type;
      return types.concat([{ value, label : p.tu(`form.${label}`)}]);
    }, []);
    const convertedorderStatAdminOptions = orderStatAdminOptions.reduce( (types,type) => {
      const { value, label } = type;
      return types.concat([{ value, label : p.tu(`detail.form.${label}`)}]);
    }, [{
      label : p.tu('detail.form.order_stat.options.all'),
      value : 'ALL'
    }]);
    const convertedOrderByDirectionOptions = orderByDirectionOptions.reduce( (types,type) => {
      const { value, label } = type;
      return types.concat([{ value, label : p.tu(`form.${label}`)}]);
    }, [])
    if(shipment.isFetching){
      return (
        <ReactLoader message={p.tc('loading')}/>
      )
    }
    if(formValueSelect.orderNumber){
      filteredShipment = _.filter(shipment.shipment, (shipment) => {
          return shipment.orderNumber.includes(formValueSelect.orderNumber.toUpperCase());
      })
    } else {
      filteredShipment = shipment.shipment
    }
    if(formValueSelect.orderStatFilter !== 'ALL'){
      filteredShipment = _.filter(filteredShipment, (shipment) => {
        return shipment.orderStat === formValueSelect.orderStatFilter;
      })
    }
    const shipmentDisplay = _.orderBy(filteredShipment, [formValueSelect.orderBy], [formValueSelect.orderByDirection]);

    return (
      <Container>
        <Card>
          <CardBlock>
            <form>
              <Row>
                <Col md="4">
                  <Field
                    className="form-control input-sm"
                    label={p.tu('form.order_number.label')}
                    placeholder ={p.tc('form.order_number.placeholder')}
                    name={`orderNumber`}
                    component={TextField}
                  />
                </Col>
                <Field
                  label={p.tu('detail.form.order_stat.label')}
                  className="form-control input-sm"
                  name={`orderStatFilter`}
                  items={convertedorderStatAdminOptions}
                  component={SelectField}
                  required='required'/>
                <Col md="2">
                  <Field
                    label={p.tu('form.order_by.label')}
                    className="form-control input-sm"
                    name={`orderBy`}
                    items={convertedOrderByOptions}
                    component={SelectField}
                    required='required'/>
                </Col>
                <Col md="1">
                  <Field
                    label={p.tu('form.order_by_direction.label')}
                    className="form-control input-sm"
                    name={`orderByDirection`}
                    items={convertedOrderByDirectionOptions}
                    component={SelectField}
                    required='required'/>
                </Col>
              </Row>
              <Row>
                <Col md="12">
                  {p.t('form.filtered_results', { smart_count : Object.keys(filteredShipment).length })}
                  {` `}
                  {p.t('form.total_results', { smart_count : Object.keys(shipment.shipment).length})}
                </Col>
              </Row>
            </form>
          </CardBlock>
        </Card>
        <Card>
          <CardBlock>
            {_.map(shipmentDisplay, (shipmentHead) => {
              const { shipmentId, orderNumber, freightMode, freightType,
                      vendorName, originCountryCode, originCityCode, destinationCityCode,
                      destinationCountryCode, transittimeMax, transittimeMin, createdAt, orderStat, isNew } = shipmentHead;
              return(
                <Card key={shipmentId}>
                  <CardBlock className="text-center" style={{background : `${this._getBackground(orderStat)}`}}>
                    <Row>
                      <Col md="1">
                        <small>{moment(createdAt).format('DD MMM \'YY')}</small>
                      </Col>
                      <Col md="1">
                        <small>{freightMode === 'OCEAN' ? <span className="fa fa-ship"></span> : <span className="fa fa-plane"></span>}
                          {freightType.toUpperCase()}
                        </small>
                      </Col>
                      <Col md="3">
                        <small>{orderNumber}</small>
                        {isNew && (
                          <Badge color="danger">New</Badge>
                        )}
                      </Col>
                      <Col md="1">
                        <small>{originCityCode}, {originCountryCode}</small>
                      </Col>
                      <Col md="1">
                        <small>{destinationCityCode}, {destinationCountryCode}</small>
                      </Col>
                      <Col md="2">
                        <small>{`${transittimeMin} - ${transittimeMax} days`}</small>
                      </Col>
                      <Col md="3">
                        <small>{vendorName}</small>
                      </Col>
                    </Row>
                      <Nav tabs className="nav nav-tabs nav-tabs-simple m-t-10">
                          <NavItem>
                            <NavLink
                              className={classnames({ active: this.state.activeTab[shipmentId] === 'detail' })}
                              onClick={this._toggle.bind(this,{shipmentId, orderNumber}, 'detail')}>
                              <small className="text-success">{p.tc('tab.shipment')}</small>
                            </NavLink>
                          </NavItem>
                          <NavItem>
                            <NavLink
                              className={classnames({ active: this.state.activeTab[shipmentId] === 'price' })}
                              onClick={this._toggle.bind(this,{shipmentId, orderNumber}, 'price')}>
                              <small className="text-success">{p.tc('tab.price')}</small>
                            </NavLink>
                          </NavItem>
                          {/*
                          <NavItem>
                            <NavLink
                              className={classnames({ active: this.state.activeTab[shipmentId] === 'carrier' })}
                              onClick={this._toggle.bind(this,{shipmentId, orderNumber}, 'carrier')}>
                              <small className="text-success">{p.tc('tab.carrier')}</small>
                            </NavLink>
                          </NavItem>
                            */}
                          <NavItem>
                            <NavLink
                              className={classnames({ active: this.state.activeTab[shipmentId] === 'document' })}
                              onClick={this._toggle.bind(this,{shipmentId, orderNumber}, 'document')}>
                              <small className="text-success">{p.tc('tab.documents')}</small> { ` `}
                              {message.notifications.hasOwnProperty(shipmentId) && message.notifications[shipmentId].documentTypeLength > 0 && (
                                <Badge color='danger'>{message.notifications[shipmentId].documentTypeLength}</Badge>
                              )}
                            </NavLink>
                          </NavItem>
                          <NavItem>
                            <NavLink
                              className={classnames({ active: this.state.activeTab[shipmentId] === 'message' })}
                              onClick={this._toggle.bind(this,{shipmentId, orderNumber}, 'message')}>
                              <small className="text-success">{p.tc('tab.messages')}</small> { ` `}
                                {message.notifications.hasOwnProperty(shipmentId) && message.notifications[shipmentId].messageTypeLength > 0 && (
                                  <Badge color='danger'>{message.notifications[shipmentId].messageTypeLength}</Badge>
                                )}
                            </NavLink>
                          </NavItem>
                          <NavItem>
                            <NavLink
                              className={classnames({ active: this.state.activeTab[shipmentId] === 'history' })}
                              onClick={this._toggle.bind(this,{shipmentId, orderNumber}, 'history')}>
                              <small className="text-success">{p.tc('tab.history')}</small>
                            </NavLink>
                          </NavItem>
                        </Nav>
                        <TabContent activeTab={this.state.activeTab[shipmentId]}>
                          <TabPane tabId="detail">
                            <Row className="row column-seperation">
                              { shipment.isFetchingDetail[shipmentId] && !shipment.shipmentDetail.hasOwnProperty(shipmentId) ? (
                                <Col md="12">
                                  <ReactLoader message='loading detail'/>
                                </Col>
                                ) : (
                                  shipment.shipmentDetail.hasOwnProperty(shipmentId) && (
                                    <Col md="12" className="text-left">
                                      <Row>
                                        <Col md="10" tag="h5" style={{background : '#c8d9ed'}}>
                                          {p.tc('detail.title')}
                                        </Col>
                                        <Col md="2" tag="h5" style={{background : '#c8d9ed'}}>
                                          {  ( (company.profile.type && company.profile.type === 'ANDALIN'  && shipment.shipment[shipmentId].orderStat !== 'COMPLETE' ) || (company.profile.type && company.profile.type !== 'USER') && ( company.profile.type === 'VENDOR' && (shipment.shipment[shipmentId].orderStat === 'BOOK' || shipment.shipment[shipmentId].orderStat === 'CONFIRM' || shipment.shipment[shipmentId].orderStat === 'SCHEDULED'))) && (
                                            <ButtonStrap size="xs" className="btn btn-primary btn-block m-l-20" onClick={this._handleUpdateOrderStatModal.bind(this, shipmentId)}>{p.tc('detail.edit_order')}</ButtonStrap>
                                          )}
                                        </Col>
                                      </Row>
                                      <Row>
                                        <Col md="2">
                                          <small style={{fontWeight : '600'}}>{p.tc('detail.order_number')}</small>
                                        </Col>
                                        <Col md="4">
                                          <small>{ shipment.shipmentDetail[shipmentId].orderNumber}</small>
                                        </Col>
                                        <Col md="2" className="text-bold">
                                          <small style={{fontWeight : '600'}}>{p.tc('detail.freight.mode')}</small>
                                        </Col>
                                        <Col md="4">
                                          <small>{shipment.shipmentDetail[shipmentId].freightMode}</small>
                                        </Col>
                                      </Row>
                                      <Row>
                                        <Col md="2">
                                          <small style={{fontWeight : '600'}}>{p.tc('detail.order_stat')}</small>
                                        </Col>
                                        <Col md="4">
                                          <small><strong className="text-primary">{ shipment.shipmentDetail[shipmentId].orderStat}</strong></small>
                                        </Col>
                                        <Col md="2" className="text-bold">
                                          <small style={{fontWeight : '600'}}>{p.tc('detail.freight.type')}</small>
                                        </Col>
                                        <Col md="4">
                                          <small>{shipment.shipmentDetail[shipmentId].freightType}</small>
                                        </Col>
                                      </Row>
                                      <Row>
                                        <Col md="2">
                                          <small style={{fontWeight : '600'}}>{p.tc('detail.shipment_date')}</small>
                                        </Col>
                                        <Col md="4">
                                          <small>{ moment(shipment.shipmentDetail[shipmentId].shipmentDate).format('DD/MM/YYYY')}</small>
                                        </Col>
                                        <Col md="2">
                                          <small style={{fontWeight : '600'}}>{p.tc('detail.custom')}</small>
                                        </Col>
                                        <Col md="4">
                                          <small>{ shipment.shipmentDetail[shipmentId].customStat ? p.tc('detail.stat.yes') : p.tc('detail.stat.no') }</small>
                                        </Col>
                                      </Row>
                                      <Row>
                                        <Col md="2">
                                          <small style={{fontWeight : '600'}}>{p.tc('detail.book_price.total')}</small>
                                        </Col>
                                        <Col md="4">
                                          <small><NumberFormat value={ shipment.shipmentDetail[shipmentId].bookPriceTotal.toFixed(2)} displayType={'text'} thousandSeparator={true} prefix={`${ shipment.shipmentDetail[shipmentId].bookPriceCurrencyCode} `} /></small>
                                        </Col>
                                        <Col md="2">
                                          <small style={{fontWeight : '600'}}>{p.tc('detail.trucking')}</small>
                                        </Col>
                                        <Col md="4">
                                          <small>{ shipment.shipmentDetail[shipmentId].truckingStat ? p.tc('detail.stat.yes') : p.tc('detail.stat.no') }</small>
                                        </Col>
                                      </Row>
                                      <Row>
                                        <Col md="2">
                                          <small style={{fontWeight : '600'}}>{p.tc('detail.book_price.currency_code')}</small>
                                        </Col>
                                        <Col md="1">
                                          <small>{shipment.shipmentDetail[shipmentId].bookPriceCurrencyCode}</small>
                                        </Col>
                                        <Col md="2">
                                          <small style={{fontWeight : '600'}}>{p.tc('detail.book_price.currency_rate')} {`(${shipment.shipmentDetail[shipmentId].bookPriceCurrencyCode}/${shipment.shipmentDetail[shipmentId].priceCurrencyCode})`}</small>
                                        </Col>
                                        <Col md="1">
                                          <small><NumberFormat value={parseFloat(shipment.shipmentDetail[shipmentId].bookPriceCurrencyRate,2)} displayType='text' thousandSeparator={true}/></small>
                                        </Col>
                                        <Col md="2">
                                          <small style={{fontWeight : '600'}}>{p.tc('detail.insurance.title')}</small>
                                        </Col>
                                        <Col md="4">
                                          <small>{ shipment.shipmentDetail[shipmentId].insuranceStat ? p.tc('detail.stat.yes') : p.tc('detail.stat.no') }</small>
                                        </Col>
                                      </Row>
                                      <br/>
                                      { parseInt(shipment.shipmentDetail[shipmentId].insuranceStat,0) > 0 && ([
                                        <Row key={`insurance_goods_value${shipmentId}`}>
                                          <Col md="2">
                                            <small style={{fontWeight : '600'}}>{p.tc('detail.insurance.goods_value')}</small>
                                          </Col>
                                          <Col md="4">
                                            <small>
                                              <NumberFormat value={parseInt(shipment.shipmentDetail[shipmentId].insurances[0].goodsValue,0)} displayType='text' prefix={`${shipment.shipmentDetail[shipmentId].insurances[0].goodsValueCurrency} `} thousandSeparator={true} />
                                            </small>
                                          </Col>
                                          <Col md="2">
                                            <small style={{fontWeight : '600'}}>{p.tc('detail.insurance.pcg')}</small>
                                          </Col>
                                          <Col md="4">
                                            <small>{shipment.shipmentDetail[shipmentId].insurances[0].pcg*100}%</small>
                                          </Col>
                                        </Row>,
                                        <Row key={`insurance_goods_value_currency${shipmentId}`}>
                                          <Col md="2">
                                            <small style={{fontWeight : '600'}}>{p.tc('detail.insurance.price_sum')}</small>
                                          </Col>
                                          <Col md="4">
                                            <small>
                                            <NumberFormat value={parseFloat(shipment.shipmentDetail[shipmentId].insurances[0].priceSum,2)} displayType='text' prefix={`${shipment.shipmentDetail[shipmentId].insurances[0].priceCurrencyCode} `} thousandSeparator={true} />
                                            </small>
                                          </Col>
                                          <Col md="2">
                                            <small style={{fontWeight : '600'}}>{p.tc('detail.insurance.price_currency_rate')}  {`(${shipment.shipmentDetail[shipmentId].insurances[0].priceCurrencyCode}/${shipment.shipmentDetail[shipmentId].insurances[0].goodsValueCurrency})`}</small>
                                          </Col>
                                          <Col md="4">
                                            <small>
                                              <NumberFormat value={parseFloat(shipment.shipmentDetail[shipmentId].insurances[0].priceCurrencyRate,2)} displayType='text' thousandSeparator={true}/>
                                            </small>
                                          </Col>
                                        </Row>,
                                        <br key={`brInsurance${shipmentId}`}/>
                                      ])}

                                      <Row>
                                        <Col md="2">
                                          <small style={{fontWeight : '600'}}>{p.tc('detail.origin.country')}</small>
                                        </Col>
                                        <Col md="4">
                                          <small>{ shipment.shipmentDetail[shipmentId].originCountry } {`(${shipment.shipmentDetail[shipmentId].originCountryCode})`}</small>
                                        </Col>
                                        <Col md="2" className="text-bold">
                                          <small style={{fontWeight : '600'}}>{p.tc('detail.destination.country')}</small>
                                        </Col>
                                        <Col md="4">
                                          <small>{shipment.shipmentDetail[shipmentId].destinationCountry } {`(${shipment.shipmentDetail[shipmentId].destinationCountryCode})`} </small>
                                        </Col>
                                      </Row>
                                      <Row>
                                        <Col md="2">
                                          <small style={{fontWeight : '600'}}>{p.tc('detail.origin.city')}</small>
                                        </Col>
                                        <Col md="4">
                                          <small>{ shipment.shipmentDetail[shipmentId].originCity } {`(${shipment.shipmentDetail[shipmentId].originCityCode})`}</small>
                                        </Col>
                                        <Col md="2" className="text-bold">
                                          <small style={{fontWeight : '600'}}>{p.tc('detail.destination.city')}</small>
                                        </Col>
                                        <Col md="4">
                                          <small>{shipment.shipmentDetail[shipmentId].destinationCity } {`(${shipment.shipmentDetail[shipmentId].destinationCityCode})`}</small>
                                        </Col>
                                      </Row>
                                      <Row>
                                        <Col md="2">
                                          <small style={{fontWeight : '600'}}>{p.tc('detail.origin.pol')}</small>
                                        </Col>
                                        <Col md="4">
                                          <small>{ shipment.shipmentDetail[shipmentId].pol } </small>
                                        </Col>
                                        <Col md="2" className="text-bold">
                                          <small style={{fontWeight : '600'}}>{p.tc('detail.destination.pod')}</small>
                                        </Col>
                                        <Col md="4">
                                          <small>{shipment.shipmentDetail[shipmentId].pod }</small>
                                        </Col>
                                      </Row>
                                      <br/>
                                      <Row>
                                        <Col md="10" tag="h5" style={{background : '#c8d9ed'}}>
                                          {p.tc('detail.carrier.title')}
                                        </Col>
                                        <Col md="2" tag="h5" style={{background : '#c8d9ed'}}>
                                          { (company.profile.type && company.profile.type !== 'USER' && shipment.shipment[shipmentId].orderStat !== 'COMPLETE') && (
                                            <ButtonStrap size="xs" className="btn btn-primary btn-block m-l-20" onClick={this._handleUpdateCarrierModal.bind(this, shipmentId)}>{p.tc('detail.carrier.edit_carrier')}</ButtonStrap>
                                          )}
                                        </Col>
                                      </Row>
                                      <Row>
                                        <Col md="2">
                                          <small style={{fontWeight : '600'}}>{p.tc('detail.carrier.name')}</small>
                                        </Col>
                                        <Col md="4">
                                          <small>{ shipment.shipmentDetail[shipmentId].carrierName}</small>
                                        </Col>
                                        <Col md="2" className="text-bold">
                                          <small style={{fontWeight : '600'}}>{p.tc('detail.vendor.name')}</small>
                                        </Col>
                                        <Col md="4">
                                          <small>{shipment.shipmentDetail[shipmentId].vendorName}</small>
                                        </Col>
                                      </Row>
                                      <Row>
                                        <Col md="2">
                                          <small style={{fontWeight : '600'}}>{p.tc('detail.carrier.do_awb')}</small>
                                        </Col>
                                        <Col md="4">
                                          <small>{ shipment.shipmentDetail[shipmentId].carrierBlAwb}</small>
                                        </Col>
                                        <Col md="2">
                                          <small style={{fontWeight : '600'}}>{p.tc('detail.carrier.contract')}</small>
                                        </Col>
                                        <Col md="4">
                                          <small>{ shipment.shipmentDetail[shipmentId].carrierContract}</small>
                                        </Col>
                                      </Row>
                                      <br/>
                                      <Row>
                                        <Col md="10" tag="h5" style={{background : '#c8d9ed'}}>
                                          {p.tc('detail.schedules.title')}
                                        </Col>
                                        <Col md="2" tag="h5" style={{background : '#c8d9ed'}}>

                                        </Col>
                                      </Row>
                                      <Row>
                                        <Col md="2"><small style={{fontWeight : '600'}}>{p.tc('detail.schedules.port_open_time')}</small></Col>
                                        <Col md="2"><small style={{fontWeight : '600'}}>{p.tc('detail.schedules.port_cutoff_time')}</small>
                                          <br/><small style={{fontWeight : '600'}}>{p.tc('detail.schedules.inland_cutoff_time')}</small></Col>
                                        <Col md="1"><small style={{fontWeight : '600'}}>{p.tc('detail.schedules.vessel_name')}</small></Col>
                                        <Col md="2"><small style={{fontWeight : '600'}}>{p.tc('detail.schedules.origin')}</small></Col>
                                        <Col md="2"><small style={{fontWeight : '600'}}>{p.tc('detail.schedules.destination')}</small></Col>
                                        <Col md="1"><small style={{fontWeight : '600'}}>{p.tc('detail.schedules.scheduled')}</small></Col>
                                        <Col md="2"><small style={{fontWeight : '600'}}>{p.tc('detail.schedules.created_at')}</small></Col>
                                      </Row>
                                        { _.map(shipment.shipmentDetail[shipmentId].schedules, (schedule,index) => {
                                          return(
                                            <Row key={index} className={`${schedule.isValid ? 'text-primary' : 'text-muted'}`}>
                                              <Col md="2"><small>{schedule.portOpenTime ? moment(schedule.portOpenTime).format('DD/MM/YY HH:mm') : ''}</small></Col>
                                              <Col md="2"><small>{schedule.portCutoffTime ? `${moment(schedule.portCutoffTime).format('DD/MM/YY HH:mm')}(P)` : ''}</small><br/>
                                                          <small>{schedule.inlandCutoffTime ? `${moment(schedule.inlandCutoffTime).format('DD/MM/YY HH:mm')}(I)` : ''}</small></Col>
                                              <Col md="1"><small>{schedule.vesselName}</small></Col>
                                              <Col md="2"><small>{schedule.originCityCode},{schedule.originCountryCode}({schedule.pol})<br/>{moment(schedule.dateDeparture).format('DD/MM/YY')}</small></Col>
                                              <Col md="2"><small>{schedule.destinationCityCode},{schedule.destinationCountryCode}({schedule.pod})<br/>{moment(schedule.dateArrival).format('DD/MM/YY')}</small></Col>
                                              <Col md="1"><small>{schedule.isValid ? <span className="fa fa-check-square-o"></span> : <span className="fa fa-close"></span> }</small></Col>
                                              <Col md="2"><small>{moment(schedule.createdAt).format('DD/MM/YY')}</small></Col>
                                            </Row>
                                          )
                                        })}
                                      <br/>
                                      <Row>
                                        <Col md="10" tag="h5" style={{background : '#c8d9ed'}}>
                                          {p.tc('detail.commodities.title')}
                                        </Col>
                                        <Col md="2" tag="h5" style={{background : '#c8d9ed'}}>
                                          { ( shipment.shipmentDetail[shipmentId].orderStat === 'BOOK' || shipment.shipmentDetail[shipmentId].orderStat === 'CONFIRM' || shipment.shipmentDetail[shipmentId].orderStat === 'SCHEDULED') && (
                                            <ButtonStrap size="xs" className="btn btn-primary btn-block m-l-20" onClick={this._handleUpdateCommoditiesModal.bind(this, shipmentId)}>{p.tc('detail.commodities.edit_commodities')}</ButtonStrap>
                                          )}
                                        </Col>
                                      </Row>
                                      <Row>
                                        <Col md="2">
                                          <small style={{fontWeight : '600'}}>{p.tc('detail.commodities.class.title')}</small>
                                        </Col>
                                        <Col md="4">
                                          <small>{ p.tc(`detail.commodities.class.options.${shipment.shipmentDetail[shipmentId].commoditiesClass}`)}</small>
                                        </Col>
                                      </Row>
                                      <Row>
                                        <Col md="2">
                                          <small style={{fontWeight : '600'}}>{p.tc('detail.commodities.type')}</small>
                                        </Col>
                                        <Col md="4">
                                          <small>{ pProductType.tc(`product_type.options.${shipment.shipmentDetail[shipmentId].commoditiesType}`)}</small>
                                        </Col>
                                      </Row>
                                      <Row>
                                        <Col md="2">
                                          <small style={{fontWeight : '600'}}>{p.tc('detail.commodities.hscode')}</small>
                                        </Col>
                                        <Col md="4">
                                          <small>{ shipment.shipmentDetail[shipmentId].commoditiesHSCode}</small>
                                        </Col>
                                      </Row>
                                      <Row>
                                        <Col md="2">
                                          <small style={{fontWeight : '600'}}>{p.tc('detail.commodities.product')}</small>
                                        </Col>
                                        <Col md="10">
                                          <small>{ shipment.shipmentDetail[shipmentId].commoditiesProduct}</small>
                                        </Col>
                                      </Row>
                                      <br/>
                                      <Row>
                                        <Col md="10" tag="h5" style={{background : '#c8d9ed'}}>
                                          {p.tc('detail.shipper.title')}{p.tc('detail.consignee.title')}
                                        </Col>
                                        <Col md="2" tag="h5" style={{background : '#c8d9ed'}}>
                                          { ( shipment.shipmentDetail[shipmentId].orderStat === 'BOOK' || shipment.shipmentDetail[shipmentId].orderStat === 'CONFIRM' || shipment.shipmentDetail[shipmentId].orderStat === 'SCHEDULED') && (
                                            <ButtonStrap size="xs" className="btn btn-primary btn-block m-l-20" onClick={this._handleUpdateShipperConsigneeModal.bind(this, shipmentId)}>{p.tc('detail.shipper.edit')}/{p.tc('detail.consignee.edit')}</ButtonStrap>
                                          )}
                                        </Col>
                                      </Row>
                                      <Row>
                                        <Col md="2">
                                          <small style={{fontWeight : '600'}}>{p.tc('detail.shipper.company')}</small>
                                        </Col>
                                        <Col md="4">
                                          <small>{ shipment.shipmentDetail[shipmentId].shipperCompany}</small>
                                        </Col>
                                        <Col md="2">
                                          <small style={{fontWeight : '600'}}>{p.tc('detail.consignee.company')}</small>
                                        </Col>
                                        <Col md="4">
                                          <small>{ shipment.shipmentDetail[shipmentId].consigneeCompany}</small>
                                        </Col>
                                      </Row>
                                      <Row>
                                        <Col md="2">
                                          <small style={{fontWeight : '600'}}>{p.tc('detail.shipper.name')}</small>
                                        </Col>
                                        <Col md="4">
                                          <small>{ shipment.shipmentDetail[shipmentId].shipperName}</small>
                                        </Col>
                                        <Col md="2">
                                          <small style={{fontWeight : '600'}}>{p.tc('detail.consignee.name')}</small>
                                        </Col>
                                        <Col md="4">
                                          <small>{ shipment.shipmentDetail[shipmentId].consigneeName}</small>
                                        </Col>
                                      </Row>
                                      <Row>
                                        <Col md="2">
                                          <small style={{fontWeight : '600'}}>{p.tc('detail.shipper.address')}</small>
                                        </Col>
                                        <Col md="4">
                                          <small>{ shipment.shipmentDetail[shipmentId].shipperAddress}</small>
                                        </Col>
                                        <Col md="2">
                                          <small style={{fontWeight : '600'}}>{p.tc('detail.consignee.address')}</small>
                                        </Col>
                                        <Col md="4">
                                          <small>{ shipment.shipmentDetail[shipmentId].consigneeAddress}</small>
                                        </Col>
                                      </Row>
                                      <Row>
                                        <Col md="2">
                                          <small style={{fontWeight : '600'}}>{p.tc('detail.shipper.email')}</small>
                                        </Col>
                                        <Col md="4">
                                          <small>{ shipment.shipmentDetail[shipmentId].shipperEmail}</small>
                                        </Col>
                                        <Col md="2">
                                          <small style={{fontWeight : '600'}}>{p.tc('detail.consignee.email')}</small>
                                        </Col>
                                        <Col md="4">
                                          <small>{ shipment.shipmentDetail[shipmentId].consigneeEmail}</small>
                                        </Col>
                                      </Row>
                                      <Row>
                                        <Col md="2">
                                          <small style={{fontWeight : '600'}}>{p.tc('detail.shipper.phone')}</small>
                                        </Col>
                                        <Col md="4">
                                          <small>{ shipment.shipmentDetail[shipmentId].shipperPhone}</small>
                                        </Col>
                                        <Col md="2">
                                          <small style={{fontWeight : '600'}}>{p.tc('detail.consignee.phone')}</small>
                                        </Col>
                                        <Col md="4">
                                          <small>{ shipment.shipmentDetail[shipmentId].consigneePhone}</small>
                                        </Col>
                                      </Row>
                                      <Row>
                                        <Col md="2">
                                          <small style={{fontWeight : '600'}}>{p.tc('detail.shipper.fax')}</small>
                                        </Col>
                                        <Col md="4">
                                          <small>{ shipment.shipmentDetail[shipmentId].shipperFax}</small>
                                        </Col>
                                        <Col md="2">
                                          <small style={{fontWeight : '600'}}>{p.tc('detail.consignee.fax')}</small>
                                        </Col>
                                        <Col md="4">
                                          <small>{ shipment.shipmentDetail[shipmentId].consigneeFax}</small>
                                        </Col>
                                      </Row>
                                      <Row>
                                        <Col md="2">
                                          <small style={{fontWeight : '600'}}>{p.tc('detail.shipper.notes')}</small>
                                        </Col>
                                        <Col md="10">
                                          <small>{ shipment.shipmentDetail[shipmentId].shipperNotes}</small>
                                        </Col>
                                      </Row>
                                      <br/>
                                      <Row>
                                        <Col md="10" tag="h5" style={{background : '#c8d9ed'}}>
                                          {p.tc('detail.requests.title')}
                                        </Col>
                                        <Col md="2" tag="h5" style={{background : '#c8d9ed'}}>
                                          { ( shipment.shipmentDetail[shipmentId].orderStat === 'BOOK' || shipment.shipmentDetail[shipmentId].orderStat === 'CONFIRM' || shipment.shipmentDetail[shipmentId].orderStat === 'SCHEDULED') && (
                                              <ButtonStrap size="xs" className="btn btn-primary btn-block m-l-20" onClick={this._handleUpdateOrderRequestModal.bind(this, shipmentId)}>{p.tc('detail.requests.edit')}</ButtonStrap>
                                          )}
                                        </Col>
                                      </Row>
                                      { shipment.shipmentDetail[shipmentId].requests.length > 0 && ([
                                        <Row className="text-left" key={`requestHeader${shipmentId}`}>
                                          <Col md="2">
                                            <small style={{fontWeight : '600'}}>{p.tc('detail.requests.header.created_at')}</small>
                                          </Col>
                                          <Col md="2">
                                            <small style={{fontWeight : '600'}}>{p.tc('detail.requests.header.created_by')}</small>
                                          </Col>
                                          <Col md="2">
                                            <small style={{fontWeight : '600'}}>{p.tc('detail.requests.header.type')}</small>
                                          </Col>
                                          <Col md="1">
                                            <small style={{fontWeight : '600'}}>{p.tc('detail.requests.header.value')}</small>
                                          </Col>
                                          <Col md="2">
                                            <small style={{fontWeight : '600'}}>{p.tc('detail.requests.header.unit')}</small>
                                          </Col>
                                          <Col md="2">
                                            <small style={{fontWeight : '600'}}>{p.tc('detail.requests.header.note')}</small>
                                          </Col>
                                          <Col md="1">
                                            <small style={{fontWeight : '600'}}>{p.tc('detail.requests.header.stat')}</small>
                                          </Col>
                                        </Row>,
                                        _.map( shipment.shipmentDetail[shipmentId].requests, (request, requestIndex) => {
                                          return (
                                            <Row key={`requestDisplay${shipmentId}_${requestIndex}`} className="text-left">
                                              <Col md="2">
                                                <small>{moment(request.createdAt).format('DD/MM/YYYY')}</small>
                                              </Col>
                                              <Col md="2">
                                                <small>{request.createdByComptp}</small>
                                              </Col>
                                              <Col md="2">
                                                <small>{p.tc(`detail.requests.display.type.${request.type}`)}</small>
                                              </Col>
                                              <Col md="1">
                                                <small>{request.value}</small>
                                              </Col>
                                              <Col md="2">
                                                <small>{request.unit}</small>
                                              </Col>
                                              <Col md="2">
                                                <small>{request.note}</small>
                                              </Col>
                                              <Col md="1">
                                                <small>{p.tc(`detail.requests.display.stat.${request.stat}`)}</small>
                                              </Col>
                                            </Row>
                                          )})
                                      ])}
                                      <br/>
                                      <Row>
                                        <Col md="10" tag="h5" style={{background : '#c8d9ed'}}>
                                          {p.tc('detail.freights.title')}
                                        </Col>
                                        <Col md="2" tag="h5" style={{background : '#c8d9ed'}}>
                                          { (company.profile.type && company.profile.type !== 'USER' && shipment.shipment[shipmentId].orderStat !== 'COMPLETE' ) && (
                                            <ButtonStrap size="xs" className="btn btn-primary btn-block m-l-20" onClick={this._handleUpdateFreightModal.bind(this, shipmentId)}>Edit Freight</ButtonStrap>
                                          )}
                                        </Col>
                                      </Row>
                                      <Row className="text-left">
                                        <Col md="1">
                                          <small style={{fontWeight : '600'}}>{p.tc('detail.freights.header.index')}</small>
                                        </Col>
                                        <Col md="2">
                                          <small style={{fontWeight : '600'}}>{p.tc('detail.freights.header.type')}</small>
                                        </Col>
                                        <Col md="3">
                                          <small style={{fontWeight : '600'}}>{p.tc('detail.freights.header.tracking_number')}</small>
                                        </Col>
                                        <Col md="2">
                                          <small style={{fontWeight : '600'}}>{p.tc('detail.freights.header.qty')}</small>
                                        </Col>
                                        <Col md="2">
                                          <small style={{fontWeight : '600'}}>{p.tc('detail.freights.header.volume')}</small>
                                        </Col>
                                        <Col md="2">
                                          <small style={{fontWeight : '600'}}>{p.tc('detail.freights.header.weight')}</small>
                                        </Col>
                                      </Row>
                                      { _.map( shipment.shipmentDetail[shipmentId].freights, (freight, freightIndex) => {
                                        return (
                                          <Row key={freightIndex} className="text-left">
                                            <Col md="1">
                                              <small>{freightIndex+1}</small>
                                            </Col>
                                            <Col md="2">
                                              <small>{p.tc(`detail.freights.display.service_type.${freight.serviceType}`)}</small>
                                            </Col>
                                            <Col md="3">
                                              <small>{freight.trackingNumber}</small>
                                            </Col>
                                            <Col md="2">
                                              <small>
                                              { freight.serviceType === 'AIRCARGO' || freight.serviceType === 'SEALCL' || freight.serviceType === 'AIRCOURIER' ? (
                                                  `${freight.qty} ${ freight.additionalInfo }`
                                              ) : (
                                                p.tc('detail.freights.display.unit', { smart_count : parseInt(freight.qty,0)} )
                                              )}</small>
                                            </Col>
                                            <Col md="2">
                                              <small>
                                                { freight.serviceType === 'AIRCARGO' || freight.serviceType === 'SEALCL' || freight.serviceType === 'AIRCOURIER' ? (
                                                    `${freight.volume} CBM`
                                                ) : (
                                                    '-' )}</small>
                                            </Col>
                                            <Col md="2">
                                              <small>
                                              { freight.serviceType === 'AIRCARGO' || freight.serviceType === 'SEALCL' || freight.serviceType === 'AIRCOURIER' ? (
                                                  `${freight.weight} Kgs`
                                              ) : (
                                                  p.tc(`detail.freights.display.additional_info.${freight.additionalInfo}`)
                                              )}
                                              </small>
                                            </Col>
                                          </Row>
                                        )
                                      })}
                                      <br/>
                                      <Row>
                                        <Col md="12" className="text-center">
                                          <ButtonStrap onClick={this._toggle.bind(this,{shipmentId, orderNumber}, '')} size="xs" className="btn btn-block btn-danger"> {p.tc('detail.close')} </ButtonStrap>
                                        </Col>
                                      </Row>
                                    </Col>
                                  )
                              )}
                            </Row>
                          </TabPane>
                          <TabPane tabId="price">
                              <Row className="row column-seperation">
                                { shipment.isFetchingPrice[shipmentId] && !shipment.shipmentPrice.hasOwnProperty(shipmentId) ? (
                                  <Col md="12">
                                    <ReactLoader message='loading price detail'/>
                                  </Col>
                                  ) : (
                                  <Col md="12">
                                    <Row>
                                      <Col md="8" tag="h5" className="text-left" style={{background : '#c8d9ed'}}>
                                        {p.tc('detail.price.title')}
                                      </Col>
                                      <Col md="2" tag="h5" style={{background: '#c8d9ed'}} className="text-right">
                                        { (company.profile.type && company.profile.type === 'ANDALIN' && shipment.shipment[shipmentId].orderStat !== 'COMPLETE' ) && (
                                          <ButtonStrap size="xs"
                                            className="btn btn-primary btn-block"
                                            onClick={this._handleUpdateOrderPriceModal.bind(this, shipmentId)}>{p.tc('detail.price.edit')}</ButtonStrap>
                                        )}
                                      </Col>
                                      <Col md="2" tag="h5" style={{background: '#c8d9ed'}} className="text-right">
                                        <ButtonStrap
                                          className="btn btn-primary m-l-20 btn-block"
                                          size="xs"
                                          onClick={this._handleDownloadShipmentInvoice.bind(this,{shipmentId : shipmentId, orderNumber : shipment.shipment[shipmentId].orderNumber, orderStat : shipment.shipment[shipmentId].orderStat})}>
                                          <span className="fa fa-save"> </span>
                                          <span className="m-l-10">{p.tc('detail.price.download_invoice')}</span>
                                        </ButtonStrap>
                                      </Col>
                                    </Row>
                                    <Row className="text-left">
                                      { /*
                                        <Col md="2">
                                          <small style={{fontWeight : '600'}}>{p.tc('detail.price.header.code_fee')}</small>
                                        </Col>
                                        <Col md="1">
                                          <small style={{fontWeight : '600'}}>{p.tc('detail.price.header.service_type')}</small>
                                        </Col>
                                      */}

                                      <Col md="4">
                                          <small style={{fontWeight : '600'}}>{p.tc('detail.price.header.price_description')}</small>
                                      </Col>
                                      <Col md="2">
                                        <small style={{fontWeight : '600'}}>{p.tc('detail.price.header.qty')}</small>
                                      </Col>
                                      {
                                        /*
                                        <Col md="1">
                                          <small style={{fontWeight : '600'}}>{p.tc('detail.price.header.volume')}</small>
                                        </Col>
                                        <Col md="1">
                                          <small style={{fontWeight : '600'}}>{p.tc('detail.price.header.weight')}</small>
                                        </Col>
                                        */
                                      }

                                      <Col md="2">
                                        <small style={{fontWeight : '600'}}>{p.tc('detail.price.header.price_uom')}</small>
                                      </Col>
                                      <Col md="4">
                                        <small style={{fontWeight : '600'}}>{p.tc('detail.price.header.price_sum')}</small>
                                      </Col>
                                    </Row>
                                    {_.map(shipment.shipmentPrice[shipmentId], (_shipment, _shipmentIndex) => {
                                      const { componentType, componentCityCode, serviceType, priceDesc, bookPriceSum, bookPriceCurrencyCode, volume, weight, qty, priceUOM } = _shipment;
                                      return(
                                        <Row key={_shipmentIndex} className="text-left">
                                          { /*
                                            <Col md="2">
                                              <small style={{fontSize : '10px'}}>{componentType} {componentCityCode ? `: ${componentCityCode}` : ''}</small>
                                            </Col>
                                            <Col md="1">
                                              <small style={{fontSize : '10px'}}>{serviceType}</small>
                                            </Col>
                                          */}
                                          <Col md="4">
                                            <small style={{fontSize : '10px'}}>{priceDesc}</small>
                                          </Col>
                                          <Col md="2">
                                            <small style={{fontSize : '10px'}}>{ qty }</small>
                                          </Col>
                                          { /*
                                            <Col md="1">
                                              <small style={{fontSize : '10px'}}>{ volume }</small>
                                            </Col>
                                            <Col md="1">
                                              <small style={{fontSize : '10px'}}>{ weight }</small>
                                            </Col>
                                            */}
                                          <Col md="2">
                                            <small style={{fontSize : '10px'}}>{ getPriceUOM(p, priceUOM) }</small>
                                          </Col>
                                          <Col md="4">
                                            <small style={{fontSize : '10px'}}><NumberFormat value={bookPriceSum.toFixed(2)} prefix={`${bookPriceCurrencyCode} `} displayType={`text`} thousandSeparator={true}/></small>
                                          </Col>
                                        </Row>
                                      )
                                    })}
                                  </Col>
                                  )}
                              </Row>
                                  { shipment.shipmentDetail.hasOwnProperty(shipmentId) && (
                                      shipment.shipmentDetail[shipmentId].hasOwnProperty('insurances') && (
                                        shipment.shipmentDetail[shipmentId].insurances.length > 0 && ([
                                          <Row className="text-left" key={`insuranceTitle_${shipmentId}`}>
                                            <Col md="10" tag="h5" style={{background : '#c8d9ed'}}>
                                              {p.tc('detail.price.header.insurance.title')}
                                            </Col>
                                            <Col md="2" tag="h5" style={{background : '#c8d9ed'}}>
                                            </Col>
                                          </Row>,
                                          <Row className="text-left" key={`insuranceHead_${shipmentId}`}>
                                            <Col md="2">
                                              <small style={{fontWeight : '600'}}>{p.tc('detail.price.header.insurance.commodities_type')}</small>
                                            </Col>
                                            <Col md="3">
                                              <small style={{fontWeight : '600'}}>{p.tc('detail.price.header.insurance.commodities_product')}</small>
                                            </Col>
                                            <Col md="2">
                                              <small style={{fontWeight : '600'}}>{p.tc('detail.price.header.insurance.goods_value')}</small>
                                            </Col>
                                            <Col md="2">
                                              <small style={{fontWeight : '600'}}>{p.tc('detail.price.header.insurance.currency_rate')}</small>
                                            </Col>
                                            <Col md="1">
                                              <small style={{fontWeight : '600'}}>{p.tc('detail.price.header.insurance.pcg')}</small>
                                            </Col>
                                            <Col md="2">
                                              <small style={{fontWeight : '600'}}>{p.tc('detail.price.header.insurance.price_sum')}</small>
                                            </Col>
                                          </Row>,
                                          <Row className="text-left" key={`insuranceDetail_${shipmentId}`}>
                                            <Col md="2">
                                              <small style={{fontSize : '10px'}}>{ pProductType.tc(`product_type.options.${shipment.shipmentDetail[shipmentId].commoditiesType}`)}</small>
                                            </Col>
                                            <Col md="3">
                                              <small style={{fontSize : '10px'}}>{shipment.shipmentDetail[shipmentId].commoditiesProduct}</small>
                                            </Col>
                                            <Col md="2">
                                              <small style={{fontSize : '10px'}}><NumberFormat value={shipment.shipmentDetail[shipmentId].insurances[0].goodsValue} displayType="text" thousandSeparator={true} prefix={`${shipment.shipmentDetail[shipmentId].insurances[0].goodsValueCurrency} `}/></small>
                                            </Col>
                                            <Col md="2">
                                              <small style={{fontSize : '10px'}}><NumberFormat value={shipment.shipmentDetail[shipmentId].insurances[0].priceCurrencyRate} displayType="text" thousandSeparator={true} prefix={`${shipment.shipmentDetail[shipmentId].insurances[0].priceCurrencyCode}/${shipment.shipmentDetail[shipmentId].insurances[0].goodsValueCurrency} `}/></small>
                                            </Col>
                                            <Col md="1">
                                              <small style={{fontSize : '10px'}}>{shipment.shipmentDetail[shipmentId].insurances[0].pcg * 100}%</small>
                                            </Col>
                                            <Col md="2">
                                              <small style={{fontSize : '10px'}}><NumberFormat value={shipment.shipmentDetail[shipmentId].insurances[0].priceSum} displayType="text" thousandSeparator={true} prefix={`${shipment.shipmentDetail[shipmentId].insurances[0].priceCurrencyCode} `}/></small>
                                            </Col>
                                          </Row>
                                        ])
                                    )
                                  )}
                                  <br/>
                                  <Row>
                                    <Col md="12" className="text-center">
                                      <ButtonStrap onClick={this._toggle.bind(this,{shipmentId, orderNumber}, '')} size="xs" className="btn btn-block btn-danger"> {p.tc('detail.close')} </ButtonStrap>
                                    </Col>
                                  </Row>
                          </TabPane>
                          <TabPane tabId="document">
                            { shipment.isFetchingDocument[shipmentId] && !shipment.shipmentDocument.hasOwnProperty(shipmentId) ? (
                              <Col md="12">
                                <ReactLoader message='loading documents'/>
                              </Col>
                            ) : ([
                              <Row key={`document_title_${shipmentId}`}>
                                <Col md="10" tag="h5" className="text-left" style={{background : '#c8d9ed'}}>
                                  {p.tc('detail.document.title')}
                                </Col>
                                <Col md="2" tag="h5" style={{background : '#c8d9ed'}}>
                                  <ButtonStrap size="xs" className="btn btn-primary btn-block m-l-20" onClick={this._handlePostUpdateShipmentDocs.bind(this, shipmentId)}>{p.tc('detail.document.edit')}</ButtonStrap>
                                </Col>
                              </Row>,
                              <Row key={`document_head_${shipmentId}`} className="text-left">
                                <Col md="2">
                                  <small style={{fontWeight : '600'}}>{p.tc('detail.document.header.created_at')}</small>
                                </Col>
                                <Col md="2">
                                  <small style={{fontWeight : '600'}}>{p.tc('detail.document.header.file_category')}</small>
                                </Col>
                                <Col md="1">

                                </Col>
                                <Col md="3">
                                  <small style={{fontWeight : '600'}}>{p.tc('detail.document.header.file_description')}</small>
                                </Col>
                                <Col md="2">
                                    <small style={{fontWeight : '600'}}>{p.tc('detail.document.header.file_origin')}</small>
                                </Col>
                                <Col md="2">
                                  <small style={{fontWeight : '600'}}>{p.tc('detail.document.header.created_by_username')}</small>
                                </Col>
                              </Row>,
                              _.map(shipment.shipmentDocument[shipmentId], (_document, index) => {
                                return (
                                  <Row key={`document_display_${shipmentId}_${index}`} className="m-t-5 text-left">
                                    <Col md="2">
                                      <small style={{fontSize : '10px'}}>{moment(_document.createdAt).format('DD/MM/YYYY hh:mm')}</small>
                                    </Col>
                                    <Col md="2">
                                      <small style={{fontSize : '10px'}}>
                                        {_document.fileCategory}
                                      </small>
                                    </Col>
                                    <Col md="1">
                                      <ButtonStrap
                                        className="btn btn-primary"
                                        size="xs"
                                        onClick={this._handleDownloadShipmentDoc.bind(this,{shipmentId :_document.shipmentId,orderNumber : _document.orderNumber,fileId : _document.fileId,fileCategory : _document.fileCategory, saveAsFileName : `${_document.orderNumber.split('/').join('-')}_${_document.fileCategory}`, extFileName : _document.fileExt })}>
                                        <span className="fa fa-save"></span>
                                      </ButtonStrap>
                                    </Col>
                                    <Col md="3">
                                      <small style={{fontSize : '10px'}}>{_document.fileDesc}</small>
                                    </Col>
                                    <Col md="2">
                                      <small style={{fontSize : '10px'}}>{_document.fileOrigin}</small>
                                    </Col>
                                    <Col md="2">
                                      <small style={{fontSize : '10px'}}>{_document.createdByUsername}</small>
                                    </Col>
                                  </Row>
                                )
                              }),
                              <br key={`document_br_${shipmentId}`}/>,
                              <Row key={`document_close_${shipmentId}`}>
                                <Col md="12" className="text-center">
                                  <ButtonStrap onClick={this._toggle.bind(this,{shipmentId, orderNumber}, '')} size="xs" className="btn btn-block btn-danger"> {p.tc('detail.close')} </ButtonStrap>
                                </Col>
                              </Row>
                            ])}
                          </TabPane>
                          <TabPane tabId="message">
                              <Row className="row column-seperation">
                                <Col md="12" tag="h5" className="text-left" style={{background : '#c8d9ed'}}>
                                  {p.tc('detail.message.title')} {orderNumber}
                                </Col>
                                <Col md="12">
                                { (activeTab && activeTab[shipmentId] === 'message') && (
                                  <Message shipmentId={shipmentId} orderNumber={orderNumber} vendorName={vendorName}/>
                                )}
                                </Col>
                                { message.subscribedChannels.includes(orderNumber) && (
                                  <Col md="12" className="text-center">
                                    <MessagesForm form={`MessagesForm_${shipmentId}`} shipmentId={shipmentId} orderNumber={orderNumber}/>
                                  </Col>
                                )}
                                <Col md="12" className="text-center m-t-10">
                                  <ButtonStrap onClick={this._toggle.bind(this,{shipmentId, orderNumber}, '')} size="xs" className="btn btn-block btn-danger"> {p.tc('detail.close')} </ButtonStrap>
                                </Col>
                              </Row>
                          </TabPane>
                          <TabPane tabId="history">
                            { shipment.isFetchingHistory[shipmentId] && !shipment.shipmentHistory.hasOwnProperty(shipmentId) ? (
                              <Col md="12">
                                <ReactLoader message='loading shipment history'/>
                              </Col>
                            ) : ([
                              <Row key={`history_title${shipmentId}`}>
                                <Col md="10" tag="h5" className="text-left" style={{background : '#c8d9ed'}}>
                                  {p.tc('detail.history.title')}
                                </Col>
                                <Col md="2" tag="h5" style={{background : '#c8d9ed'}}>

                                </Col>
                              </Row>,
                              <Row key={`history_head${shipmentId}`} className="text-left">
                                <Col md="2">
                                  <small style={{fontWeight : '600'}}>{p.tc('detail.history.header.created_at')}</small>
                                </Col>
                                <Col md="2">
                                  <small style={{fontWeight : '600'}}>{p.tc('detail.history.header.created_by_comptp')}</small>
                                </Col>
                                <Col md="2">
                                  <small style={{fontWeight : '600'}}>{p.tc('detail.history.header.created_by_username')}</small>
                                </Col>
                                <Col md="6">
                                  <small style={{fontWeight : '600'}}>{p.tc('detail.history.header.description')}</small>
                                </Col>
                              </Row>,
                              _.map(shipment.shipmentHistory[shipmentId], (history, index) => {
                                return (
                                  <Row key={`history_display_${shipmentId}_${index}`} className="text-left">
                                    <Col md="2">
                                      <small style={{fontSize : '10px'}}>{moment(history.createdAt).format('DD/MM/YYYY hh:mm')}</small>
                                    </Col>
                                    <Col md="2">
                                      <small style={{fontSize : '10px'}}>{history.createdByComptp}</small>
                                    </Col>
                                    <Col md="2">
                                      <small style={{fontSize : '10px'}}>{history.createdByUsername}</small>
                                    </Col>
                                    <Col md="6">
                                      <small style={{fontSize : '10px'}}>{history.description}</small>
                                    </Col>
                                  </Row>
                                )
                              }),
                              <br key={`history_br_${shipmentId}`}/>,
                              <Row key={`history_close_${shipmentId}`}>
                                <Col md="12" className="text-center">
                                  <ButtonStrap onClick={this._toggle.bind(this,{shipmentId, orderNumber}, '')} size="xs" className="btn btn-block btn-danger"> {p.tc('detail.close')} </ButtonStrap>
                                </Col>
                              </Row>
                            ])}
                          </TabPane>
                        </TabContent>
                  </CardBlock>
                </Card>
              )
            })}
          </CardBlock>
        </Card>
      </Container>
    )
  }
}
const selector = formValueSelector("ShipmentSearchForm");

Shipment = reduxForm({
  form : 'ShipmentSearchForm',
  enableReinitialize : true
})(Shipment)

const mapStateToProps = (state,prevState) => {
  const { shipment, company, message, user } = state;
  const p = getP(state, { polyglotScope : 'shipment'});
  const pProductType = getP(state, { polyglotScope : 'book.shipment_detail.commodities'})
  return {
    formValueSelect : selector(state, 'orderNumber', 'orderStatFilter', 'orderBy', 'orderByDirection'),
    initialValues : {
      orderStatFilter : 'ALL',
      orderBy : 'createdAt',
      orderByDirection : 'desc'
    },
    user,
    company,
    p,
    shipment,
    pProductType,
    message
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    fetchCompany,
    fetchShipments,
    fetchShipmentDetail,
    fetchShipmentPrice,
    fetchShipmentCarrier,
    fetchShipmentDocument,
    fetchShipmentHistory,
    openUpdateShipperConsigneeModal,
    openUpdateCommoditiesModal,
    openUpdateOrderStatModal,
    openUpdateFreightModal,
    openUpdateCarrierModal,
    openUpdateOrderPriceModal,
    openUpdateOrderRequestModal,
    openPostUpdateShipmentDocs,
    downloadShipmentDoc,
    downloadShipmentInvoice,
    fetchShipmentMessagesHead
  }, dispatch)
}

Shipment = connect(mapStateToProps, mapDispatchToProps)(Shipment)

export default withRouter(Shipment)
