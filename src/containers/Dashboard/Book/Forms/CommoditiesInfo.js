import React from 'react';
import { Row, Col } from 'reactstrap';
import translate from 'redux-polyglot/translate';
import { Field } from 'redux-form';
import { TextField, TextArea, SelectFieldReact } from '../../../../components/Bootstrap/Forms/'
import { commoditiesClassOptions, commoditiesTypeOptions } from './';

const CommoditiesInfo = (props) => {
  const { p } = props;
  const convertedCommoditiesClassOptions = commoditiesClassOptions.reduce( (types,type) => {
          const { value, label } = type;
          return types.concat({ value, label : p.tu(label)});
        }, []);
  const convertedCommoditiesTypeOptions = commoditiesTypeOptions.reduce( (types,type) => {
          const { value, label } = type;
                return types.concat({ value, label : p.tc(label)});
        }, []);
  return (
      <Col>
        <Row>
          <Col md="8">
            <h6>{p.tc('title')}</h6>
          </Col>
        </Row>
        <Row>
          <Col md="3">
            <Field
              label={p.tu('product_class.label')}
              placeholder={p.tc('product_class.placeholder')}
              name='commoditiesClass'
              clearable={false}
              simpleValue={true}
              items={convertedCommoditiesClassOptions}
              component={SelectFieldReact}
              disabled={true}
              required='required'/>
          </Col>
          <Col md="6">
            <Field
              label={p.tu('product_type.label')}
              placeholder={p.tc('product_type.placeholder')}
              name='commoditiesType'
              clearable={false}
              simpleValue={true}
              items={convertedCommoditiesTypeOptions}
              component={SelectFieldReact}
              required='required'/>
          </Col>
          <Col md="3">
            <Field
              className="form-control input-sm"
              label={p.tu('hscode.label')}
              name='commoditiesHSCode'
              component={TextField}
              placeholder={p.tc('hscode.placeholder')}
              required='required'/>
          </Col>
        </Row>
        <Row>
          <Col md="12">
            <Field className="form-control input-sm"
              label={`${p.tc('product_desc.label')}`}
              placeholder={p.tc('product_desc.placeholder')}
              name="commoditiesProduct"
              component={TextArea}/>
          </Col>
        </Row>
      </Col>
  )
}

export default translate({polyglotScope : 'book.shipment_detail.commodities'}) (CommoditiesInfo);
