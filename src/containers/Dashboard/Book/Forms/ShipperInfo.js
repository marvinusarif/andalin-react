import React from 'react';
import { Row, Col } from 'reactstrap';
import translate from 'redux-polyglot/translate';
import { Field } from 'redux-form';
import { TextField, TextArea } from '../../../../components/Bootstrap/Forms/'

const ShipperInfo = (props) => {
  const { p, query } = props;
  return (
    <Col md="6" className="text-primary">
      <Row>
        <Col>
          <h6 className="text-primary">{p.tc('title')}</h6>
        </Col>
      </Row>
      <Row>
        <Col>
          <Field
            className="form-control input-sm"
            label={p.tu('company.label')}
            name='shipperCompany'
            component={TextField}
            placeholder={p.tc('company.placeholder')}
            required='required'/>
        </Col>
      </Row>
      <Row>
        <Col>
          <Field
            className="form-control input-sm"
            label={p.tu('name.label')}
            name='shipperPICName'
            component={TextField}
            placeholder={p.tc('name.placeholder')}
            required='required'/>
        </Col>
      </Row>
      <Row>
        <Col>
          <Field
            className="form-control input-sm"
            label={`${p.tu('address.label')} (${query.form.originCity && query.form.originCity.toUpperCase()}, ${query.form.originCountry && query.form.originCountry.toUpperCase()})`}
            name='shipperAddress'
            component={TextArea}
            placeholder={p.tc('address.placeholder')}
            required='required'/>
        </Col>
      </Row>
      <Row>
        <Col>
          <Field
            className="form-control input-sm"
            label={p.tu('email.label')}
            name='shipperEmail'
            component={TextField}
            placeholder={p.tc('email.placeholder')}
            required='required'/>
        </Col>
      </Row>
      <Row>
        <Col>
          <Field
            className="form-control input-sm"
            label={p.tu('phone.label')}
            name='shipperPhone'
            component={TextField}
            placeholder={p.tc('phone.placeholder')}
            required='required'/>
        </Col>
      </Row>
      <Row>
        <Col>
          <Field
            className="form-control input-sm"
            label={p.tu('fax.label')}
            name='shipperFax'
            component={TextField}
            placeholder={p.tc('fax.placeholder')}/>
        </Col>
      </Row>
    </Col>
  )
}

export default translate({polyglotScope : 'book.shipment_detail.shipper'}) (ShipperInfo);
