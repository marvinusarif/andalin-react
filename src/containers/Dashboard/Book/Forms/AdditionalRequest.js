import React from 'react';
import { requestOptions } from './';
import { Row, Col } from 'reactstrap';
import { Field, FieldArray } from 'redux-form';
import translate from 'redux-polyglot/translate';
import { TextField, Button, SelectField } from '../../../../components/Bootstrap/Forms/'

const renderRequests = (props) => {
  const { fields, p, requests } = props;
  const convertedRequestOptions = requestOptions.reduce( (types,type) => {
          const { value, label } = type;
          return types.concat({ value, label : p.tu(label)});
        }, []);

  return ( <div>
            <Row>
              <Col md="12">
              { fields.map( (req,index) => {
              return(
                  <Row key={index}>
                    <Col md="5">
                      <Field
                        label={p.tu('type.title')}
                        className="form-control input-sm"
                        name={`${req}.type`}
                        items={convertedRequestOptions}
                        component={SelectField}
                        required='required'/>
                      <small key={index}><i>
                        { requests &&
                            p.t(`type.options.${requests[index].type}.description`) !== '' ?
                            ([<span key={index} className="bold">{p.tc('type.note')} </span>, ` : `,
                            p.tc(`type.options.${requests[index].type}.description`)]) : ''
                        }</i>
                      </small>
                    </Col>
                    <Col md="2">
                      <Field
                        className="form-control input-sm"
                        label={p.tu('type.value')}
                        name={`${req}.value`}
                        placeholder={requests && p.t(`type.options.${requests[index].type}.placeholder`)}
                        component={TextField}
                        required='required'/>
                    </Col>
                    <Col md="1" style={{ padding : '38px 0px'}}>
                      { requests && p.t(`type.options.${requests[index].type}.unit`) }
                    </Col>
                    <Col md="3">
                      <Field
                        className="form-control input-sm"
                        label={p.tu('type.note')}
                        name={`${req}.note`}
                        placeholder={p.tc('type.note')}
                        component={TextField}
                        required='required'/>
                    </Col>
                    <Col md="1" style={{ padding : '38px 0px'}}>
                      <Button
                        className="btn btn-danger btn-xs"
                        icon="remove"
                        onClick={() => fields.remove(index)}/>
                    </Col>
                  </Row>
                )}) }
              </Col>
            </Row>
            <Row>
              <Col md="8">
                <Button
                  className="btn btn-primary btn-xs"
                  icon="plus"
                  value={p.tc('action.add')}
                  onClick={() => fields.push({type : 'free_time'})}/>
              </Col>
            </Row>
          </div>)
}

const AdditionalRequestInfo = (props) => {
  const { p, requests } = props;
  return (
    <Col md="12">
      <Row>
        <Col className='form-group text-primary'>
          <label>{p.tu('title')} ({p.tc('optional')})</label>
        </Col>
      </Row>
      <Row>
        <Col className='form-group text-primary'>
          <FieldArray name="requests" p={p} component={renderRequests} requests={requests}/>
        </Col>
      </Row>
    </Col>
  )
}

export default translate({ polyglotScope : 'book.additional_request' })(AdditionalRequestInfo)
