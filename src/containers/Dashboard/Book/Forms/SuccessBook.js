import React from 'react';
import { Link } from 'react-router-dom';
import { Container, Card, CardBlock, Row, Col } from 'reactstrap';

export const SuccessBook = (props) => {
  const { p } = props;
  return (
    <Container>
      <Card>
        <CardBlock className="text-center">
          <Row>
            <Col md="12" className="text-center" tag='h3'>
              {p.tc('booking_successful.title')}
            </Col>
            <Col md="12" className="text-center" tag='h5'>
              {p.tc('booking_successful.description')}
            </Col>
          </Row>
          <Row>
            <Col md="12">
              {p.tc('booking_contact.title')}
            </Col>
          </Row>
          <Row>
            <Col md="12">
              {p.tc('booking_contact.description')}
            </Col>
          </Row>
          <br/>
          <Row>
            <Col md={{size : 2, offset : 2}} className="text-left">
              Bobby
            </Col>
            <Col md='3'>
              {p.tc('booking_contact.email.bobby')}
            </Col>
            <Col md='3'>
              {p.tc('booking_contact.whatsapp.bobby')}
            </Col>
            <Col md="2">

            </Col>
          </Row>
          <br/>
          <Link to="/shipments" className="btn btn-success">{p.tc('booking_contact.action.go_to_shipment')}</Link>
        </CardBlock>
      </Card>
    </Container>
  )
}
