export const validate = (values,{ p }) => {
  const errors = {}
  if(!values.commoditiesHSCode){
    errors.commoditiesHSCode = p.tc('validation.required');
  }
  if(!values.commoditiesProduct){
    errors.commoditiesProduct = p.tc('validation.required');
  }
  if(!values.shipperCompany) {
    errors.shipperCompany = p.tc('validation.required');
  }
  if(!values.shipperPICName) {
    errors.shipperPICName = p.tc('validation.required');
  }
  if(!values.shipperAddress){
    errors.shipperAddress = p.tc('validation.required');
  }
  if(!values.shipperPhone){
    errors.shipperPhone = p.tc('validation.required');
  }
  if(!values.shipperEmail){
    errors.shipperEmail = p.tc('validation.required');
  }else if(!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.shipperEmail)){
    errors.shipperEmail = p.tc('validation.email_invalid');
  }
  if(!values.shipperFax){
    errors.shipperFax = p.tc('validation.required');
  }
  if(!values.consigneeCompany) {
    errors.consigneeCompany = p.tc('validation.required');
  }
  if(!values.consigneePICName) {
    errors.consigneePICName = p.tc('validation.required');
  }
  if(!values.consigneeAddress){
    errors.consigneeAddress = p.tc('validation.required');
  }
  if(!values.consigneePhone){
    errors.consigneePhone = p.tc('validation.required');
  }
  if(!values.consigneeEmail){
    errors.consigneeEmail = p.tc('validation.required');
  }else if(!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.consigneeEmail)){
    errors.consigneeEmail = p.tc('validation.email_invalid');
  }
  if(!values.consigneeFax){
    errors.consigneeFax = p.tc('validation.required');
  }
  if(values.requests){
    const requestsErrors = []
    values.requests.forEach((request,requestIndex) => {
      const requestError = {};
      if(!request.value){
        requestError.value = p.tc('validation.required');
      }
      requestsErrors[requestIndex] = requestError;
    });
    if(requestsErrors.length) {
      errors.requests = requestsErrors;
    }
  }
  return errors;
}
