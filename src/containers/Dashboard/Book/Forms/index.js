export const requestOptions = [{
  label : 'type.options.free_time.title',
  value : 'free_time'
},{
  label : 'type.options.min_temperature.title',
  value : 'min_temperature'
},{
  label : 'type.options.max_temperature.title',
  value : 'max_temperature'
},{
  label : 'type.options.shipping_line.title',
  value : 'shipping_line'
},{
  label : 'type.options.vessel.title',
  value : 'vessel'
}]

export const commoditiesClassOptions = [{
  label : 'product_class.options.GENCO',
  value : 'GENCO'
},{
  label : 'product_class.options.DG',
  value : 'DG'
}]

export const commoditiesTypeOptions = [{
  label : 'product_type.options.FOODBEVERAGES',
  value : 'FOODBEVERAGES'
},{
  label : 'product_type.options.CLOTHES',
  value : 'CLOTHES'
},{
  label : 'product_type.options.FURNITURE',
  value : 'FURNITURE'
},{
  label : 'product_type.options.OTHERS',
  value : 'OTHERS'
}]
