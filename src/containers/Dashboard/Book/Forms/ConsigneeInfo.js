import React from 'react';
import { Row, Col } from 'reactstrap';
import translate from 'redux-polyglot/translate';
import { Field } from 'redux-form';
import { TextField, TextArea } from '../../../../components/Bootstrap/Forms/'

const ConsigneeInfo = (props) => {
  const { p, query } = props;
  return (
    <Col md="6" className="text-success">
      <Row>
        <Col>
          <h6 className="text-success">{p.tc('title')}</h6>
        </Col>
      </Row>
      <Row>
        <Col>
          <Field
            className="form-control input-sm"
            label={p.tu('company.label')}
            name='consigneeCompany'
            component={TextField}
            placeholder={p.tc('company.placeholder')}
            required='required'/>
        </Col>
      </Row>
      <Row>
        <Col>
          <Field
            className="form-control input-sm"
            label={p.tu('name.label')}
            name='consigneePICName'
            component={TextField}
            placeholder={p.tc('name.placeholder')}
            required='required'/>
        </Col>
      </Row>
      <Row>
        <Col>
          <Field
            className="form-control input-sm"
            label={`${p.tu('address.label')} (${query.form.destinationCity && query.form.destinationCity.toUpperCase()}, ${query.form.destinationCountry && query.form.destinationCountry.toUpperCase()})`}
            name='consigneeAddress'
            component={TextArea}
            placeholder={p.tc('address.placeholder')}
            required='required'/>
        </Col>
      </Row>
      <Row>
        <Col>
          <Field
            className="form-control input-sm"
            label={p.tu('email.label')}
            name='consigneeEmail'
            component={TextField}
            placeholder={p.tc('email.placeholder')}
            required='required'/>
        </Col>
      </Row>
      <Row>
        <Col>
          <Field
            className="form-control input-sm"
            label={p.tu('phone.label')}
            name='consigneePhone'
            component={TextField}
            placeholder={p.tc('phone.placeholder')}
            required='required'/>
        </Col>
      </Row>
      <Row>
        <Col>
          <Field
            className="form-control input-sm"
            label={p.tu('fax.label')}
            name='consigneeFax'
            component={TextField}
            placeholder={p.tc('fax.placeholder')}/>
        </Col>
      </Row>
    </Col>
  )
}

export default translate({polyglotScope : 'book.shipment_detail.consignee'}) (ConsigneeInfo);
