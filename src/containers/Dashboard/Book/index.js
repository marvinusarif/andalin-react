import _ from 'lodash';
import React, {Component} from 'react';
import {
  Container,
  Card,
  CardHeader,
  CardBlock,
  Table,
  Row,
  Col, Button as ButtonStrap
} from 'reactstrap';
import moment from 'moment';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { reduxForm, formValueSelector, Field } from 'redux-form';
import { withRouter } from 'react-router-dom';
import { getP } from 'redux-polyglot';
import { TextArea, ButtonV, SelectField } from '../../../components/Bootstrap/Forms/';
import { ReactLoader } from '../../../components/Dashboard/Loader/';
import {validate } from './Forms/Validation';
import { SuccessBook } from './Forms/SuccessBook';
import CommoditiesInfo from './Forms/CommoditiesInfo';
import ShipperInfo from './Forms/ShipperInfo';
import ConsigneeInfo from './Forms/ConsigneeInfo';
import AdditionalRequestInfo from './Forms/AdditionalRequest';
import { PriceTableOriginDestinationCharges, PriceTableFreightCharges, PriceTableSummary, PriceConversion} from '../Result/Forms/PriceTable';
import { fetchCompany } from '../../../actions/company';
import { postBookOrder } from '../../../actions/book';
import { fetchCurrencyRate, switchCurrency } from '../../../actions/currency';
import { currencyOptions} from '../Search/Forms/index';

class BookForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showPriceDetail : false
    }
    this._renderServiceType = this._renderServiceType.bind(this);
    this._submitForms = this._submitForms.bind(this);
    this._handleChangeCurrency = this._handleChangeCurrency.bind(this);
  }
  componentWillMount(){
    const { quote, isValidating } = this.props;
    if(Object.keys(quote).length < 1 && !isValidating){
      this.props.history.push('/result')
    }else{
      this.props.fetchCompany();
      this.props.fetchCurrencyRate();
    }
  }
  _handleChangeCurrency(e){
    const { target : {value}} = e;
    this.props.switchCurrency(value)
  }
  _togglePriceDetail(){
    this.setState({
      showPriceDetail : !this.state.showPriceDetail
    })
  }
  _renderServiceType({form}) {
    const {originArea, destinationArea} = form;
    const originServiceType = originArea === 'fob'? 'Port': 'Door';
    const destinationServiceType = destinationArea === 'fob' ? 'Port' : 'Door';
    return `${originServiceType} to ${destinationServiceType}`;
  }
  _submitForms(form) {
    const {quote, query } = this.props;
    const submitValues = {
      query,
      quote,
      form }
    if (Object.keys(quote).length > 0) {
      //console.log(quote);
      this.props.postBookOrder(submitValues);
    } else {
      this.props.history.push('/search')
    }
  }
  render() {
    const { quoteValidity, bookValidity, isValidating, isValidatingSuccessful, isBooking, isBookingSuccessful, query, quote, p, pR, formValueSelect, handleSubmit, submitting, currency } = this.props;
    const { showPriceDetail } = this.state;

    if(!quoteValidity && !isValidating && !isValidatingSuccessful && !isBooking && !isBookingSuccessful) {
        return (
          <div>
            <ReactLoader message={p.tc('invalid_validation')}/>
          </div>
        )
    } else if (isValidating && !isValidatingSuccessful && !isBooking && !isBookingSuccessful) {
        return (
          <div>
            <ReactLoader message={p.tc('loading_validation')}/>
          </div>
        )
    } else if (!bookValidity && quoteValidity && isBooking && !isBookingSuccessful ){
        return (
          <div>
            <ReactLoader message={p.tc('loading_booking')}/>
          </div>
        )
    } else if (bookValidity && !quoteValidity && !isBooking && isBookingSuccessful){
        return (
          <div>
            <SuccessBook p={p}/>
          </div>
        )
    } else if ( quoteValidity && !isValidating && isValidatingSuccessful ){
        const orderRoute = (quote.orderDirect)
          ? `EXPRESS`
          : `${quote.orderPol} > ${quote.orderPod}`;
        let totalShipment;
        let displayFCLShipment;
        if (query.shipmentLoad === 'lcl' && Object.keys(query.form).length > 0) {
          const {loadsLCL} = query.form;
          totalShipment = {
            qty: loadsLCL.reduce((totalQty, shipment) => {
              return totalQty += shipment.qty
                ? parseInt(shipment.qty, 10)
                : 0;
            }, 0),
            volume: loadsLCL.reduce((totalVolume, shipment) => {
              let volume = parseInt(shipment.size.length_, 10) * parseInt(shipment.size.width, 10) * parseInt(shipment.size.height, 10) / 1000000 * parseInt(shipment.qty, 10)
              volume = shipment.size.unit === 'in'
                ? volume / 35.3147
                : volume
              return totalVolume += !isNaN(volume)
                ? parseInt(Math.ceil(volume), 10)
                : 0;
            }, 0),
            weight: loadsLCL.reduce((totalWeight, shipment) => {
              let weight = parseInt(shipment.weight, 10) * parseInt(shipment.qty, 10)
              weight = shipment.weightUnit === 'lbs'
                ? weight / 2.20462
                : weight
              return totalWeight += !isNaN(weight)
                ? parseInt(Math.ceil(weight), 10)
                : 0;
            }, 0)
          }
        } else if (query.shipmentLoad === 'fcl' && Object.keys(query.form).length > 0) {
          const {loadsFCL} = query.form;
          totalShipment = {
            qty: loadsFCL.reduce((totalQty, shipment) => {
              return totalQty += shipment.qty
                ? parseInt(shipment.qty, 10)
                : 0;
            }, 0),
            summary: loadsFCL.reduce((totalVolume, shipment) => {
              const {qty, size, type, weight} = shipment;
              totalVolume[size] = !totalVolume.hasOwnProperty(size)
                ? {}
                : totalVolume[size];
              totalVolume[size][type] =!totalVolume[size].hasOwnProperty(type)
                ? {}
                : totalVolume[size][type];
              totalVolume[size][type][weight] = totalVolume[size][type][weight]
                ? (totalVolume[size][type][weight] + qty)
                : qty;
              return totalVolume
            }, {})
          }
          displayFCLShipment = _.map(totalShipment.summary, (size, sizeKey) =>
                                  (_.map(size, (type, typeKey) =>
                                    (_.map(type, (qty, weightKey) => ([
                                      p.tu(`load.fcl.volume.${sizeKey}`, {
                                        smart_count: qty,
                                        type: p.tu(`load.fcl.type.${typeKey}`),
                                        weight: p.tu(`load.fcl.weight.${weightKey}`)
                                      }),<br/>
                                    ]))))));
        }
        return (
          <Container>
            <Card className="card-transparent">
              <form onSubmit={handleSubmit(this._submitForms.bind(this))}>
                <Row>
                  <Col md="12" >
                    <Card style={{ background : '#edf2d0' }} block>
                      <CardHeader>
                        <h4>{p.tc('title')}</h4>
                      </CardHeader>
                      <CardBlock>
                        <Table responsive>
                          {Object.keys(quote).length > 0 && (
                            <tbody>
                              <tr>
                                <td colSpan="6" className="bg-transparent">
                                  <span className={`fa fa-${quote.orderFreight === "OCEAN"
                                    ? 'ship'
                                    : 'plane'}`}></span>
                                  {`${quote.orderOriginCity.toUpperCase()}`}
                                  {` > `}
                                  <strong>
                                    {`${orderRoute.toUpperCase()}`}
                                  </strong>
                                  {` > `}
                                  {`${quote.orderDestinationCity.toUpperCase()}`}
                                  <br/>
                                  <small className="bold">
                                    {`${p.tu('providers')}`}
                                    :
                                  </small>
                                  <small>{` ${quote.vendorName}`}
                                  </small>
                                  <small className="bold">
                                    {` ${p.tu('valid_until')}`}
                                    :
                                  </small>
                                  <small>{` ${moment(quote.orderValidUntil).format('DD-MMM-YYYY')}`}
                                  </small>
                                </td>
                              </tr>
                              <tr>
                                <td className="bg-transparent">
                                  <small className="bold">
                                    {`${p.tu('services')}`}
                                    :
                                  </small>
                                  <br/>
                                  <small>{`${this._renderServiceType(query)}`}</small>
                                </td>
                                <td className="bg-transparent">
                                  <small className="bold">
                                    {`${p.tu('load.title')}`}
                                    :
                                  </small>
                                  <br/>
                                  <small>{query.shipmentLoad === 'lcl'
                                      ? (`${p.tu('load.lcl.qty', {smart_count: totalShipment.qty})}`): (`${p.tu('load.fcl.qty', {smart_count: totalShipment.qty})}`)}
                                  </small>
                                </td>
                                <td className="bg-transparent">
                                  <small className="bold">
                                    {`${p.tu('total_shipment')}`}
                                    :
                                  </small>
                                  <br/>
                                  <small>
                                    {query.shipmentLoad === 'lcl'
                                      ? (`${p.tu('load.lcl.volume', {
                                        smart_count: totalShipment.volume})}, ${p.tu('load.lcl.weight', {smart_count: totalShipment.weight})}`): (displayFCLShipment)}
                                  </small>
                                </td>
                                <td className="bg-transparent">
                                  <small className="bold">
                                    {`${p.tu('date')}`}
                                    :
                                  </small>
                                  <br/>
                                  <small>{`${query.form.datePickup}`}</small>
                                </td>
                                <td className="bg-transparent">
                                  <small className="bold">
                                    {`${p.tu('delivery_estimate')}`}
                                    :
                                  </small>
                                  <br/>
                                  <small>{`${quote.orderTransittimeMin} - ${quote.orderTransittimeMax} ${p.t('days')}`}</small>
                                </td>
                                <td className="bg-transparent"></td>
                              </tr>
                              <tr>
                                <td className="bg-transparent">
                                  <small className="bold">
                                    {`${p.tu('optional_services.title')}`}
                                    :
                                  </small>
                                  <br/>
                                  <small>{query.form.insurance || query.form.custom
                                      ? (`${p.tc('optional_services.yes')}`)
                                      : (`${p.tc('optional_services.no')}`)}</small>
                                </td>
                                <td className="bg-transparent">
                                  {query.form.insurance && (
                                    <small className="bold">
                                      {p.tu('optional_services.insurance.title')}
                                    </small>
                                  )}
                                </td>
                                <td className="bg-transparent">
                                  {query.form.custom && (
                                    <small className="bold">
                                      {p.tu('optional_services.custom.title')}
                                     </small>
                                   )}
                                </td>
                                <td colSpan="3" className="bg-transparent"></td>
                              </tr>
                              <tr>
                                <td colSpan="6" className="bg-transparent">
                                  <ButtonStrap size="xs" color="primary" onClick={this._togglePriceDetail.bind(this)}>{p.tu('action.show_price_detail')}</ButtonStrap>
                                </td>
                              </tr>
                              {  showPriceDetail && ([
                                  <tr key={`origin`}>
                                    <td colSpan="6" className="bg-transparent">
                                      {quote.orderOriginCountryCode},{quote.orderOriginCityCode} {`${ quote.orderPol ? `> ${quote.orderPol}` : ''}`}
                                    </td>
                                  </tr>,
                                  <tr key={`originCharges`}>
                                    <td colSpan="6" className="bg-transparent">
                                      <PriceTableOriginDestinationCharges p={pR} currency={currency} quote={quote} cityCode={quote.orderOriginCityCode} originOrDestination={'origin'}/>,
                                    </td>
                                  </tr>,
                                  <tr key={`freight`}>
                                    <td colSpan="6" className="bg-transparent">
                                      {!quote.orderPol && !quote.orderPod ? (
                                        `${quote.orderOriginCityCode}, ${quote.orderOriginCountryCode} > ${quote.orderDestinationCityCode}, ${quote.orderDestinationCountryCode}`
                                      ) : (
                                        `${quote.orderOriginCityCode}, ${quote.orderPol} > Port : ${quote.orderPod}, ${quote.orderOriginCityCode}`
                                      )}
                                    </td>
                                  </tr>,
                                  <tr key={`freightCharges`}>
                                    <td colSpan="6" className="bg-transparent">
                                      <PriceTableFreightCharges p={pR}  currency={currency} quote={quote}/>
                                    </td>
                                  </tr>,
                                  <tr key={`destination`}>
                                    <td colSpan="6" className="bg-transparent">
                                      {`${ quote.orderPol ? `${quote.orderPol} >` : ''}`} {quote.orderDestinationCountryCode}, {quote.orderDestinationCityCode}
                                    </td>
                                  </tr>,
                                  <tr key={`destinationCharges`}>
                                    <td colSpan="6" className="bg-transparent">
                                      <PriceTableOriginDestinationCharges p={pR} quote={quote}  currency={currency} cityCode={quote.orderDestinationCityCode} originOrDestination={'destination'}/>,
                                    </td>
                                  </tr>,
                                  <tr key={`summary`}>
                                    <td colSpan="6" className="bg-transparent">
                                      {pR.tu('display.detail.summary')}
                                    </td>
                                  </tr>,
                                  <tr key={`summaryCharges`}>
                                    <td colSpan="6" className="bg-transparent">
                                      <PriceTableSummary p={pR} currency={currency}  quote={quote}/>
                                    </td>
                                  </tr>
                                ])}
                            </tbody>
                          )}
                        </Table>
                      </CardBlock>
                    </Card>
                  </Col>
                </Row>
                <Row>
                  <Col md="9">
                    <Card style={{background :'#dddddd'}} block>
                      <CardHeader>
                        <h4>{p.tc('shipment_detail.title')}</h4>
                      </CardHeader>
                      <CardBlock>
                        <Row>
                          <CommoditiesInfo query={query}/>
                        </Row>
                        <Row>
                          <ShipperInfo query={query}/>
                          <ConsigneeInfo query={query}/>
                        </Row>
                        <Row>
                          <Col className="text-primary">
                            <Field className="form-control input-sm"
                              label={`${p.tc('shipment_detail.shipper.note.label')} (${p.tc('shipment_detail.shipper.note.optional')})`}
                              placeholder={p.tc('shipment_detail.shipper.note.placeholder')}
                              name="shipperNote"
                              component={TextArea}/>
                          </Col>
                        </Row>
                        <Row>
                          <AdditionalRequestInfo requests={formValueSelect.requests}/>
                        </Row>
                      </CardBlock>
                    </Card>
                  </Col>
                  <Col md="3">
                    <Card style={{background : '#dce8c9'}} block>
                      <CardHeader tag="h4">
                        {p.tc('payment.title')}
                      </CardHeader>
                      <CardBlock>
                        <Row>
                          <Col md="12">
                            <h5 className="bold text-center">
                              <PriceConversion price={quote.orderPriceTotal} currency={currency} currencyBase={quote.orderPriceCurrencyCode}/>
                            </h5>
                          </Col>
                        </Row>
                        <Row style={{padding: '10px 0px'}}>
                          <Col md="12">
                            <Field
                              label={p.tu('currency')}
                              className="form-control input-sm"
                              name='orderCurrency'
                              onChange={this._handleChangeCurrency.bind(this)}
                              items={currencyOptions}
                              component={SelectField}
                              required='required'/>
                          </Col>
                        </Row>
                        <Row style={{padding: '10px 0px'}}>
                          <Col>
                            <ButtonV disabled={submitting}
                              type="submit"
                              value={`${p.tu('payment.action.pay')} in ${currency.select}`}
                              className="btn btn-success btn-sm btn-block" icon="lock"/>
                          </Col>
                        </Row>
                        <Row>
                          <Col md="12" className="text-center">
                            { Object.keys(currency.rates).length > 0 && (
                              <small style={{fontSize:'9px'}}>*rates {'USD'} 1 =  {currency.select} {currency.rates['USD'][currency.select].rate}   </small>
                            )}
                          </Col>
                        </Row>
                        <Row>
                          <Col md="12">
                            <h6 className='bold'>{p.tc('payment.information.title')}</h6>
                          </Col>
                        </Row>
                        <Row>
                          <Col md="12">
                            <small>{p.tc('payment.information.description')}</small>
                          </Col>
                        </Row>

                      </CardBlock>
                    </Card>
                  </Col>
                </Row>
              </form>
            </Card>
            <Card className="card-transparent">
              <CardBlock>
                <Row>
                  <Col md="12">
                    <h6 className="bold">{p.tc('terms_of_booking.title')}</h6>
                    <small>
                      {p.tc('terms_of_booking.description')}
                    </small>
                  </Col>
                </Row>
                <Row>
                  <Col md="12">
                    <h6 className="bold">{p.tc('cancellation_policy.title')}</h6>
                    <small>
                      {p.tc('cancellation_policy.description')}
                    </small>
                  </Col>
                </Row>
              </CardBlock>
            </Card>
          </Container>
        )
      }
  }
}
const selector = formValueSelector("BookForm");

const mapStateToProps = (state, ownProps) => {
  const { book: { query, quote, isBooking, isBookingSuccessful, isValidating, isValidatingSuccessful, quoteValidity, bookValidity }, company, user, currency } = state;
  const p = getP(state, {polyglotScope: 'book'});
  const pR = getP( state, {polyglotScope : 'result'});
  const _initialValues = {
    orderCurrency : currency.select,
    commoditiesClass : query.form.dangerousGoods ? 'DG' : 'GENCO',
    commoditiesType : 'FOODBEVERAGES',
    shipperCompany : company.profile.name,
    shipperPICName: user.profile.fullname,
    shipperAddress: company.profile.address,
    shipperPhone: company.profile.phone,
    shipperFax : company.profile.fax,
    shipperEmail : company.profile.email ? company.profile.email : user.profile.email
  }
  return {
    isBooking,
    isBookingSuccessful,
    isValidating,
    isValidatingSuccessful,
    quoteValidity,
    bookValidity,
    query,
    quote,
    currency,
    p,
    pR,
    formValueSelect: selector(state, 'shipperName', 'shipperAddress', 'shipperPhone', 'consigneeName', 'consigneeAddress', 'consigneePhone', 'requests', 'commoditiesClass', 'commoditiesType'),
    initialValues: _initialValues
  }
}
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    fetchCompany,
    fetchCurrencyRate,
    switchCurrency,
    postBookOrder
  }, dispatch)
}

BookForm = reduxForm({form: 'BookForm', enableReinitialize: true, keepDirtyOnReinitialize : true, validate})(BookForm)

BookForm = connect(mapStateToProps, mapDispatchToProps)(BookForm)

export default withRouter(BookForm)
