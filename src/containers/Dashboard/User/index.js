import React, { Component } from 'react';
import { Row, Col } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';
import { withRouter } from 'react-router-dom';
import { getP } from 'redux-polyglot';
import { SelectField, TextField, Button } from '../../../components/Bootstrap/Forms/';
import { fetchUser, updateUser } from '../../../actions/user';
import { sexOptions } from './Forms/';
import { validate } from './Forms/Validation';
import UploadUserPhotoForm from './Forms/UploadUserPhoto';

class UserForm extends Component {
  constructor(props){
    super(props);
    this._submitForm = this._submitForm.bind(this);
  }
  componentWillMount(){
    const { user } = this.props;
    if(user.session.isValid) {
      this.props.fetchUser();
    }
  }
  _submitForm(form){
    this.props.updateUser(form);
  }
  render(){
    const { p, user, handleSubmit, submitting, pristine, reset, } = this.props;
    const { isFetching } = user;
    const displaySexOptions = sexOptions.map(opt => ({
      label : p.tc(`sex.${opt.label}`),
      value : opt.value
    }));
    return(
      <div>
        <Row>
          <Col md="3">
            <UploadUserPhotoForm/>
          </Col>
          <Col md="9">
            <form onSubmit={handleSubmit(this._submitForm.bind(this))}>
            <Row>
              <Col md="10">
                <Field
                  label={p.tu('fullname.label')}
                  className="form-control input-sm"
                  name='userFullname'
                  placeholder={p.tc('fullname.placeholder')}
                  component={TextField}
                  required='required'/>
              </Col>
              <Col md="2">
                <Field
                  label={p.tu('sex.label')}
                  className="form-control input-sm"
                  name='userSex'
                  items={displaySexOptions}
                  component={SelectField}
                  required='required'/>
              </Col>
            </Row>
            <Row>
              <Col md="4">
                <Field
                  label={p.tu('email.label')}
                  className="form-control input-sm"
                  name='userEmail'
                  placeholder={p.tc('email.placeholder')}
                  component={TextField}
                  required='required'
                  disabled='disabled'/>
              </Col>
              <Col md="4">
                <Field
                  label={p.tu('phone.label')}
                  className="form-control input-sm"
                  name='userPhone'
                  placeholder={p.tc('phone.placeholder')}
                  component={TextField}
                  required='required'/>
              </Col>
              <Col md="4">
                <Field
                  label={p.tu('job.label')}
                  className="form-control input-sm"
                  name='userJob'
                  placeholder={p.tc('job.placeholder')}
                  component={TextField}
                  required='required'/>
              </Col>
            </Row>
            <Row>
              <Col md="6">
                <Field
                  label={p.tu('password_change.label')}
                  type='password'
                  className="form-control input-sm"
                  name='userPassword'
                  placeholder={p.tc('password_change.placeholder')}
                  component={TextField}
                  required='required'/>
              </Col>
              <Col md="6">
                <Field
                  label={p.tu('password_confirm.label')}
                  type='password'
                  className="form-control input-sm"
                  name='userPasswordConfirm'
                  placeholder={p.tc('password_confirm.placeholder')}
                  component={TextField}
                  required='required'/>
              </Col>
              </Row>
              <Row>
                <Col md="6"></Col>
                <Col md="3" className="text-center">
                  <Button
                    style={{margin : '20px 0px'}}
                    className="btn btn-danger btn-block"
                    type="button"
                    onClick={reset}
                    value={p.tc('action.reset')}/>
                </Col>
                <Col md="3" className="text-center">
                  <Button
                    style={{margin : '20px 0px'}}
                    className="btn btn-primary btn-block"
                    type="submit"
                    disabled={submitting || pristine }
                    value={isFetching ? p.tc('action.updating') :  p.tc('action.update_button')}/>
                </Col>
              </Row>
              </form>
            </Col>
        </Row>
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  const { user, company } = state;
  const p = getP(state, { polyglotScope : 'register'});
  const _initialValues = {
    userPassword : '',
    userPasswordConfirm : '',
    userEmail : user.profile.email,
    userFullname : user.profile.fullname,
    userSex : user.profile.gender,
    userJob : user.profile.job,
    userPhone : user.profile.phone,
  }
  return {
    user,
    company,
    p,
    initialValues : _initialValues
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    fetchUser,
    updateUser
  }, dispatch)
}

UserForm = reduxForm({
  form : 'UserForm',
  enableReinitialize : true,
  validate
})(UserForm)

UserForm = connect(mapStateToProps,mapDispatchToProps)(UserForm)

export default withRouter(UserForm)
