
export const validate = (values, props) => {
  const { p } = props;
  const errors = {}
  if(!values.userFullname){
    errors.userFullname = p.tc('validation.required');
  }
  if(!values.userPhone){
    errors.userPhone = p.tc('validation.required');
  }
  if(!values.userJob){
    errors.userJob = p.tc('validation.required');
  }
  if(values.userPassword){
    if(values.userPassword !== values.userPasswordConfirm){
      errors.userPasswordConfirm = p.tc('validation.password_confirm_invalid')
    }
  }
  return errors;
}
