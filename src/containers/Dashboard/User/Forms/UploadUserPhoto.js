import { URL_FILE_AZURE_USER } from '../../../../actions/';
import React, { Component } from 'react';
import { Row, Col } from 'reactstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { reduxForm, Field } from 'redux-form';
import { getP } from 'redux-polyglot';
import { FileInputField, Button } from '../../../../components/Bootstrap/Forms';
import { updateUserPhoto } from '../../../../actions/user';

class UploadUserPhotoForm extends Component {
  constructor(props){
    super(props);
    this._submitForm = this._submitForm.bind(this);
  }
  _submitForm(form){
    let formData = new FormData();
        formData.append('fileImage', form.fileImage)
    this.props.updateUserPhoto(formData);
  }
  render(){
    const { p,err, user, handleSubmit, submitting, pristine, uploadPhoto : { isFetching, message } } = this.props;

    return (
      <form onSubmit={handleSubmit(this._submitForm)}>
      <Row className='m-t-10'>
        { (!isFetching) ? (
          <Col md={{size:6, offset : 3}} className="text-center">
            { user.profile.photo ? (
              <div className="ratio img-responsive img-circle" style={{backgroundImage : `url(${URL_FILE_AZURE_USER}${user.profile.photo})`}}></div>
            ) : (
              <div className="ratio img-responsive img-circle" style={{backgroundImage : `url(img/no_photo.jpg)`}}></div>
            )}

          </Col>
        ) : (
          <Col md={{size:10, offset:1}} className="text-center">
            {p.tc('form.loading')}
          </Col>
        ) }
      </Row>
      <Row className='m-t-10'>
        <Col md={{size:10, offset : 1}}>
          <Field
            label={p.tu('form.file_input.label')}
            className="form-control input-sm"
            name='fileImage'
            component={FileInputField}
            required='required'/>
        </Col>
      </Row>
      { message && (
        <Row>
          <Col md={{size:10, offset : 1}} className="text-center">
            <small className="text-danger text-center">{err.tc(`${message}`)}</small>
          </Col>
        </Row>
      )}
      <Row>
        <Col md={{ size : 10, offset : 1}} className="text-center">
          <Button
              className="btn btn-primary btn-block"
              type="submit"
              disabled={submitting || pristine || isFetching }
              value={isFetching ? p.tc('action.uploading') :  p.tc('action.upload')}/>
        </Col>
      </Row>
    </form>
    )
  }
}

UploadUserPhotoForm = reduxForm({
  form : 'uploadUserPhotoForm',
  enableReinitialize : true
})(UploadUserPhotoForm)

const mapStateToProps = (state,ownProps) => {
  const { user, page : { uploadPhoto } } = state;
  const err = getP(state, {polyglotScope : 'error'});
  const p = getP(state, {polyglotScope : 'settings.user' });
  return {
    user,
    uploadPhoto,
    p,
    err
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    updateUserPhoto
  }, dispatch)
}
export default connect(mapStateToProps,mapDispatchToProps)(UploadUserPhotoForm)
