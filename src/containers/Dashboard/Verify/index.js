import React, { Component } from 'react';
import { Row, Col, Button as ButtonStrap } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { getP } from 'redux-polyglot';
import { openLoginModal, verifyUser } from '../../../actions/page';
import * as queryString from 'query-string';

class Verify extends Component {
  constructor(props){
    super(props);
    this._handleLogin = this._handleLogin.bind(this);
  }
  componentWillMount(){
    if(queryString.parse(this.props.location.search)){
      const verifcode = queryString.parse(this.props.location.search).code;
      if(verifcode){
        this.props.verifyUser(verifcode);
      }
    }
  }
  _handleLogin(){
    this.props.openLoginModal();
  }
  render(){
    const { p, err, verify : { isFetching, isSuccessful, message } } = this.props;
    if(isFetching && !isSuccessful){
      return (
        <div> {p.tc('loading')} </div>
      )
    }else if( !isFetching && !isSuccessful) {
      return (
        <div> {err.tc(message)} </div>
      )
    }
    return(
      <div>
      <Row>
        <Col md='12' tag='h3'>
          {p.tc('title')}
        </Col>
      </Row>
      <Row>
        <Col md='12' tag='h6'>
          {p.tc('description')}
        </Col>
      </Row>
      <Row>
        <Col md="12">
          <ButtonStrap color="primary" onClick={this._handleLogin.bind(this)}> { p.tc('login') }</ButtonStrap>
        </Col>
      </Row></div>)
  }
}

const mapStateToProps = (state, ownProps) => {
  const { page : { verify } } = state;
  const err = getP( state, { polyglotScope : 'error'});
  const p = getP( state, { polyglotScope : 'verify'});
  return {
    verify,
    p,
    err
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    verifyUser,
    openLoginModal
  }, dispatch)
}

Verify = connect(mapStateToProps,mapDispatchToProps)(Verify)

export default withRouter(Verify)
