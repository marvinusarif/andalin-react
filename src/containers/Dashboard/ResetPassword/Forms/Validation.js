export const validate = (values,{ p }) => {
  const errors = {}

  if(!values.userPassword){
    errors.userPassword = p.tc('validation.required');
    errors.userPasswordConfirm = p.tc('validation.required');
  }
  if(values.userPassword !== values.userPasswordConfirm){
    errors.userPasswordConfirm = p.tc('validation.password_confirm_invalid');
  }

  return errors;
}
