import React, { Component } from 'react';
import { Row, Col, Button as ButtonStrap} from 'reactstrap';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { reduxForm, Field } from 'redux-form';
import { getP } from 'redux-polyglot';
import { TextField, Button } from '../../../components/Bootstrap/Forms';
import { openLoginModal } from '../../../actions/page';
import { resetPassword } from '../../../actions/user';
import { validate } from './Forms/Validation';
import * as queryString from 'query-string';

class ResetPasswordForm extends Component {
  constructor(props){
    super(props);
    this._submitForm = this._submitForm.bind(this);
    this._handleLogin = this._handleLogin.bind(this);
  }
  componentWillMount(){
    if(!queryString.parse(this.props.location.search).code){
      this.props.history.push('/');
    }
  }
  _handleLogin(){
    this.props.openLoginModal();
  }
  _submitForm(form){
    if(queryString.parse(this.props.location.search)){
      const resetcode = queryString.parse(this.props.location.search).code;
      if(resetcode){
        const submittedForm = {
          resetcode,
          ...form
        }
        this.props.resetPassword(submittedForm);
      }
    }
  }
  render(){
    const { p, handleSubmit, submitting, pristine, forgotPassword : { isFetching, isSuccessful } } = this.props;
    if( isSuccessful ) {
      return (
      <div>
        <Row>
          <Col md="12">
            {p.tc('success')}
          </Col>
        </Row>
        <Row>
          <Col md="12">
            <ButtonStrap color="primary" onClick={this._handleLogin}> { p.tc('action.login') }</ButtonStrap>
          </Col>
        </Row>
      </div>
      )
    }
    return (
      <form onSubmit={handleSubmit(this._submitForm.bind(this))}>
        <Row>
          <Col md={{offset : 1, size : 11}} tag="h3">
            {p.tc('title')}
          </Col>
        </Row>
        <Row>
          <Col md={{offset : 1, size : 11}} tag="h6">
            {p.tc('description')}
          </Col>
        </Row>
        <Row>
          <Col md={{offset : 1, size : 3}}>
            <Field
              label={p.tu('password_new.label')}
              type='password'
              className="form-control input-sm"
              name='userPassword'
              placeholder={p.tc('password_new.placeholder')}
              component={TextField}
              required='required'/>
          </Col>
          <Col md="3">
            <Field
              label={p.tu('password_new_confirm.label')}
              type='password'
              className="form-control input-sm"
              name='userPasswordConfirm'
              placeholder={p.tc('password_new_confirm.placeholder')}
              component={TextField}
              required='required'/>
          </Col>
        </Row>
        <Row>
          <Col md={{size : 3, offset : 4}}>
            <Button
              className="btn btn-success btn-xl btn-block"
              type="submit"
              disabled={submitting || pristine}
              value={ isFetching ? p.tc('action.updating') : p.tc('action.change_password')}/>
          </Col>
        </Row>
      </form>
    );
  }
}

const mapStateToProps = (state,ownProps) => {
  const { page : { modal : { forgotPassword }}} = state;
  const p = getP(state, {polyglotScope : 'resetPassword'})
  return {
    p,
    forgotPassword
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    resetPassword,
    openLoginModal
  }, dispatch)
}

ResetPasswordForm = reduxForm({
  form : 'ResetPasswordForm',
  enableReinitialize : true,
  validate
})(ResetPasswordForm)

ResetPasswordForm = connect(mapStateToProps,mapDispatchToProps)(ResetPasswordForm)

export default withRouter(ResetPasswordForm)
