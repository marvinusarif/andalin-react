import React, { Component } from 'react';
import { Container, Card, CardBlock, Table, Row, Button as ButtonStrap} from 'reactstrap';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { getP } from 'redux-polyglot';
import NumberFormat from 'react-number-format';
import { postQuery } from '../../../actions/search';
import { postBookValidateQuote } from '../../../actions/book';
import { fetchQuotes, deleteQuote } from '../../../actions/quote';
import moment from 'moment';

class Quote extends Component {
  constructor(props) {
    super(props);
    this._handleQuoteToSearch = this._handleQuoteToSearch.bind(this);
    this._handleQuoteToBook = this._handleQuoteToBook.bind(this);
    this._handleDeleteQuote = this._handleDeleteQuote.bind(this);
    this._loadMoreQuotes = this._loadMoreQuotes.bind(this);
  }
  componentWillMount(){
    !this.props.user.session.isValid && this.props.history.push('/');
  }
  componentDidMount(){
    this.props.fetchQuotes();
  }
  _handleQuoteToSearch(selectedQuote){
    const { query : { shipmentLoad, form} } = selectedQuote;
    this.props.postQuery({shipmentLoad, form});
    this.props.history.push('/result');
  }
  _handleDeleteQuote(selectedQuote){
    const { quote : { quoteId }, createdAt } = selectedQuote;
    this.props.deleteQuote(quoteId, createdAt)
  }
  _handleQuoteToBook(selectedQuote){
    const { quote, query } = selectedQuote;
    this.props.postBookValidateQuote({ quote, query});
    this.props.history.push('/book');
  }
  _loadMoreQuotes(){
    const { quote : { pageState } } = this.props;
    this.props.fetchQuotes(pageState);
  }
  render(){
    const { quote, p, quote : { isFetching } } = this.props;
    const { quotes } = quote;
    return (
    <Container>
      <Row>
        <Card>
          <CardBlock>
            <Table hover>
              <thead >
                <tr>
                  <th>
                    {p.tu('header.freight_mode')}
                  </th>
                  <th style={{textAlign: 'center'}}>
                    {p.tu('header.shipment_load')}
                  </th>
                  <th style={{textAlign: 'center'}}>
                    {p.tu('header.origin')}-{p.tu('header.destination')}<br/>
                    ({p.tu('header.delivery_estimate')})
                  </th>
                  <th style={{textAlign: 'center'}}>
                    {p.tu('header.providers')}
                  </th>
                  <th style={{textAlign: 'center'}}>
                    {p.tu('header.price_total')}
                  </th>
                  <th style={{textAlign: 'center'}}>
                    {p.tu('header.valid_until')}
                  </th>
                  <th style={{textAlign: 'center'}}>
                    {p.tu('header.creator')}
                  </th>
                  <th style={{textAlign: 'center'}}>
                    {p.t('header.action')}
                  </th>
                </tr>
              </thead>
              <tbody className="text-center">
                { (quotes.length < 1 && !isFetching) && (
                  <tr>
                    <td colSpan="9">
                      {p.tc('content.empty')}
                    </td>
                  </tr>
                )}
                { quotes.map( (quoteObj,index) => {
                    const { quote, query : { shipmentLoad }, createdBy } = quoteObj;
                  return (<tr key={index} className="text-center">
                    <td style={{fontSize : '11px'}}>
                      {quote.orderFreight !== 'OCEAN' ? <span className="fa fa-plane"></span> : <span className="fa fa-ship"></span> }
                      {` `}{p.tc(`content.freight_mode.${quote.orderFreight.toLowerCase()}`)}
                    </td>
                    <td style={{fontSize : '11px'}}>
                      {shipmentLoad.toUpperCase()}
                    </td>
                    <td style={{fontSize : '11px'}}>
                      {quote.orderOriginCity}, {quote.orderOriginCountryCode} -
                      {quote.orderDestinationCity}, {quote.orderDestinationCountryCode} <br/>
                      ({p.tc('content.delivery_estimate.title', { max : quote.orderTransittimeMax, min : quote.orderTransittimeMin})})
                    </td>
                    <td style={{fontSize : '11px'}}>
                      {quote.vendorName}
                    </td>
                    <td style={{fontSize : '11px'}}>
                      <NumberFormat value={quote.orderPriceTotal.toFixed(2)} displayType={'text'} thousandSeparator={true} prefix={`${quote.orderPriceCurrencyCode} `} />
                    </td>
                    <td style={{fontSize : '11px'}}>
                      {moment(quote.orderValidUntil).format('DD MMM \'YY')}
                    </td>
                    <td style={{fontSize : '11px'}}>
                      {createdBy}
                    </td>
                    <td >
                      <ButtonStrap onClick={this._handleQuoteToBook.bind(this, quoteObj)} size='xs' className="btn-success btn-block" outline>{p.tu('content.action.book')}</ButtonStrap>
                      <ButtonStrap onClick={this._handleQuoteToSearch.bind(this, quoteObj)} size='xs' className="btn-primary" outline><span className="fa fa-search"></span>{p.tu('content.action.search')}</ButtonStrap>
                      <ButtonStrap onClick={this._handleDeleteQuote.bind(this, quoteObj)} size='xs' className="btn-danger" outline><span className="fa fa-trash"></span>{p.tu('content.action.delete')}</ButtonStrap>
                    </td>
                  </tr>)
                })}
                { isFetching && (
                  <tr>
                    <td colSpan="8" className="text-center">
                      {p.tc('loading')}
                    </td>
                  </tr>
                )}
                    { quote.pageState && (
                      <tr>
                        <td colSpan="8" className="text-center">
                      <ButtonStrap outline color="primary" onClick={this._loadMoreQuotes.bind(this)}>{p.tc('content.action.load_more')}</ButtonStrap>
                        </td>
                      </tr>
                    )}
              </tbody>
            </Table>
          </CardBlock>
        </Card>
      </Row>
    </Container>)
  }
}
const mapStateToProps = (state,ownProps) => {
  const { quote, user } = state;
  const p = getP(state, { polyglotScope : 'quote'});
  return {
    p,
    user,
    quote
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    fetchQuotes,
    postQuery,
    postBookValidateQuote,
    deleteQuote
  }, dispatch)
}

Quote = connect(mapStateToProps, mapDispatchToProps)(Quote)

export default withRouter(Quote)
