import React,{Component} from 'react';
import {Link, withRouter} from 'react-router-dom';

const menuItems = {
  search : 'New Search',
  quote : 'Quotes',
  shipments : 'Shipments',
  companysettings : 'Company Settings'
}

class Breadcrumb extends Component {

  render(){
    const { pathname } = this.props.location;
    const paths = pathname.split("/").filter(path => path !== '');
    let pathsTemp = ''
    const items = paths.map( (item,index) => {
      const displayItem = menuItems[item] ? menuItems[item] : item;
      pathsTemp += '/'+item;
      return (
        <li key={index} className={`breadcrumb-item ${index===paths.length-1 ? 'active' : ''}`}>
          { (index < paths.length-1) ? <Link to={`${pathsTemp}`}>{displayItem}</Link> : displayItem }
        </li>)
      })
    return(
         <ul className="breadcrumb p-l-0">
          {items}
        </ul>
    )
  }
}

export default withRouter(Breadcrumb);
