import _ from 'lodash';
import React, { Component } from 'react';
import { Container, Row, Col, CardBlock, Card } from 'reactstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getP } from 'redux-polyglot';
import Iframe from 'react-iframe';
import { Widgets } from '../../../components/Bootstrap/Widgets';
import { fetchShipments } from '../../../actions/shipment';
import { withRouter } from 'react-router-dom';

class Main extends Component {
  componentWillMount(){
    if(!this.props.user.session.isValid) {
      this.props.history.push('/search');
    }
  }
  componentDidMount(){
    this.props.fetchShipments();
  }
  render(){
    const { p, shipment } = this.props;
    const totalShipment = _.reduce(shipment, (totalShipment, _shipment) => {
      const { orderStat } = _shipment;
      totalShipment[orderStat] = totalShipment.hasOwnProperty(orderStat) ?  totalShipment[orderStat] + 1 : 1;
      return totalShipment
    }, {})
    return (
      <Container>
        <Row>
          <Card>
            <CardBlock>
              <Row>
                <Col md="3">
                  <Widgets content={p.tu('total_shipment.bookings')} title={p.tc('shipment', { smart_count : totalShipment.BOOK ? totalShipment.BOOK : 0})}/>
                </Col>
                <Col md="3">
                  <Widgets content={p.tu('total_shipment.progress')} title={p.tc('shipment', { smart_count : isNaN(totalShipment.CONFIRM + totalShipment.DISPATCH + totalShipment.SCHEDULED + totalShipment.DISCHARGE) ? 0 : totalShipment.CONFIRM + totalShipment.DISPATCH + totalShipment.SCHEDULED + totalShipment.DISCHARGE })}/>
                </Col>
                <Col md="3">
                  <Widgets content={p.tu('total_shipment.payment')} title={p.tc('shipment', { smart_count : totalShipment.PAYMENT ? totalShipment.PAYMENT : 0 })}/>
                </Col>
                <Col md="3">
                  <Widgets content={p.tu('total_shipment.complete')} title={p.tc('shipment',{ smart_count : totalShipment.COMPLETE ? totalShipment.COMPLETE : 0 })}/>
                </Col>
              </Row>
              <Row>
                <Col md="12">
                  <Iframe url="https://sirius.searates.com/tracking?container=&sealine="
                  width="100%"
                  height="450px"
                  display="initial"
                  position="relative"
                  allowFullScreen/>
                </Col>
              </Row>
            </CardBlock>
          </Card>
        </Row>
      </Container>
    )
  }
}

const mapStateToProps = (state,ownProps) => {
  const { shipment : { shipment }, user } = state;
  const p = getP(state, { polyglotScope : 'main' })
  return {
    p,
    user,
    shipment
  }
}
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    fetchShipments
  }, dispatch)
}
Main = connect(mapStateToProps, mapDispatchToProps)(Main)

export default withRouter(Main)
