import React, { Component } from 'react';
import { Row, Col, Button as ButtonStrap } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';
import { withRouter } from 'react-router-dom';
import { getP } from 'redux-polyglot';
import { TextField, Button } from '../../components/Bootstrap/Forms/';
import { closeInvitationModal } from '../../actions/page';
import { postCompanyUsersInviteMail } from '../../actions/company';
import { validate } from './Forms/Validation';
import emailAsyncValidate from './Forms/emailAsyncValidation';
import Logo from '../../assets/img/logo.png';


class InviteForm extends Component {
  constructor(props){
    super(props);
    this._submitForm = this._submitForm.bind(this);
    this._closeInvitation = this._closeInvitation.bind(this);
    this.state = {
      sentToEmail : null
    }
  }
  _closeInvitation(){
    this.props.closeInvitationModal();
  }
  _submitForm(form){
    const submitValue = {
      ...form,
      companyName : this.props.company.profile.name
    }
    this.props.postCompanyUsersInviteMail(submitValue);
    this.setState({
      sentToEmail : form.userEmail
    })
  }
  render(){
    const { handleSubmit, submitting, pristine, p , invite : { isFetching, isSuccessful }} = this.props;
    if(isFetching && !isSuccessful) {
      return (
        <Row className="m-t-20">
          <Col md={{size:10, offset:1}} className="text-center">
            {p.tc('loading')}
          </Col>
        </Row>
      )
    }
    if(!isFetching && isSuccessful){
      return (
      <div>
        <Row className="m-t-20">
          <Col md={{size : 4, offset : 1}} className="p-t-10">
            <img src={Logo} alt="logo"/>
          </Col>
          <Col md={{size: 2, offset : 3}}>
            <ButtonStrap outline onClick={this._closeInvitation}><i className="fa fa-remove"></i> {p.tc('close')} </ButtonStrap>
          </Col>
        </Row>
        <Row>
          <Col md={{size : 10, offset: 1}} tag="h6" className="text-center">
              {p.tc('success', { email : this.state.sentToEmail })}
          </Col>
        </Row>
      </div>
      )
    }
    return (
      <form onSubmit={handleSubmit(this._submitForm.bind(this))}>
        <Row className="m-t-20">
          <Col md={{size : 5, offset : 1}} className="p-t-10">
            <img src={Logo} alt="logo"/>
          </Col>
          <Col md={{size: 2, offset : 3}}>
            <ButtonStrap outline onClick={this._closeInvitation}><i className="fa fa-remove"></i> {p.tc('close')} </ButtonStrap>
          </Col>
        </Row>
        <Row>
          <Col md={{size:10, offset : 1}} tag="h3">
            {p.tc('title')}
          </Col>
        </Row>
        <Row>
          <Col md={{size:10, offset : 1}} tag="h6">
            {p.tc('description')}
          </Col>
        </Row>
        <Row>
          <Col md={{size:10, offset : 1}}>
            <Field
              label={p.tu('email.label')}
              className="form-control input-sm"
              name='userEmail'
              placeholder={p.tc('email.placeholder')}
              component={TextField}
              required='required'/>
          </Col>
        </Row>
        <Row>
          <Col md={{ size : 6, offset : 3}} className="text-center">
            <Button
                style={{margin : '20px 0px'}}
                className="btn btn-primary btn-block"
                type="submit"
                disabled={submitting || pristine }
                value={isFetching ? p.tc('action.inviting') :  p.tc('action.invite')}/>
          </Col>
        </Row>
      </form>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  const { page : { modal : { invite }}, company } = state;
  const p = getP(state, { polyglotScope : 'invite'});
  return {
    p,
    invite,
    company
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    closeInvitationModal,
    postCompanyUsersInviteMail
  }, dispatch)
}

InviteForm = reduxForm({
  form : 'inviteForm',
  enableReinitialize : true,
  validate,
  asyncValidate : emailAsyncValidate,
  asyncBlurFields: ['userEmail']
})(InviteForm)

InviteForm = connect(mapStateToProps,mapDispatchToProps)(InviteForm)

export default withRouter(InviteForm)
