import { URL_REST, URL_USER, URL_USER_EMAIL_VALIDITY} from '../../../actions/';
import { ajax } from 'rxjs/observable/dom/ajax';

const emailAsyncValidate = (values) => {
  const { userEmail } = values;
  return ajax.post(`${URL_REST}${URL_USER}${URL_USER_EMAIL_VALIDITY}`,
            { email : userEmail },
            { 'Content-Type': 'application/json' })
            .toPromise()
            .then( ({response}) => {
              const responseAsError = !response.validity ? { userEmail : 'User has been registered' } : {};
              throw responseAsError;
            })
            .catch( errFromThrow => {
              //report err
              return errFromThrow;
            })
}

export default emailAsyncValidate;
