import React, {Component} from 'react';
import { Row, Col, Button as ButtonStrap } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { reduxForm, formValueSelector, Field } from 'redux-form';
import { withRouter, Link } from 'react-router-dom';
import { Button , TextField } from '../../components/Bootstrap/Forms/';
import { getP } from 'redux-polyglot';
import { postUserLogin } from '../../actions/user';
import { closeLoginModal, openRegisterModal, openResetPasswordModal } from '../../actions/page';
import { validate } from './Forms/Validation';
import Logo from '../../assets/img/logo.png';

class LoginForm extends Component {
  constructor(props) {
    super(props);
    this._handleUserLogin = this._handleUserLogin.bind(this);
    this._handleUserRegister = this._handleUserRegister.bind(this);
    this._closeLoginForm = this._closeLoginForm.bind(this);
    this._handleForgotPassword = this._handleForgotPassword.bind(this);
  }
  _handleUserLogin(form){
    this.props.postUserLogin(form);
    this.props.history.push('/');
  }
  _handleForgotPassword(){
    this.props.closeLoginModal();
    this.props.openResetPasswordModal();
  }
  _handleUserRegister(){
    this.props.closeLoginModal();
    this.props.openRegisterModal();
  }
  _closeLoginForm(){
    this.props.closeLoginModal();
  }
  render() {
    const { submitting, pristine, handleSubmit, p, err, user, page } = this.props;
    return (
      <div>
            <form onSubmit={handleSubmit(this._handleUserLogin)}>
              <Row className="m-t-20">
                <Col md={{size : 6, offset : 1}} className="p-t-10">
                  <img src={Logo} alt="logo"/>
                </Col>
                <Col md={{size: 2, offset : 3}}>
                  <ButtonStrap outline onClick={this._closeLoginForm}><i className="fa fa-remove"></i> {p.tc('close')} </ButtonStrap>
                </Col>
              </Row>
              <Row>
                <Col md={{size:10, offset : 1}} tag="h3">
                  {p.tc('title')}
                </Col>
              </Row>
            { user.isFetching ? (
              <Row style={{ width : '100%', margin : '50px 0px'}}>
                <Col className="text-center">
                  {p.tc('loading')}
                </Col>
              </Row>
            ) : ([
              <Row key='username'>
                <Col md={{size : 10, offset : 1}}>
                  <Field
                    label={p.tu('username.label')}
                    className="form-control input-sm"
                    name='username'
                    placeholder={p.tc('username.placeholder')}
                    component={TextField}
                    required='required'/>
                </Col>
              </Row>,
              <Row key='password'>
                <Col md={{size : 10, offset : 1}}>
                  <Field
                    label={p.tu('password.label')}
                    type='password'
                    className="form-control input-sm"
                    name='password'
                    placeholder={p.tc('password.placeholder')}
                    component={TextField}
                    required='required'/>
                </Col>
              </Row>,
              <Row key="message">
                <Col md={{size : 10, offset : 1}}>
                  { page.modal.login.error && (
                    <small className="text-danger"> {err.tc(`${page.modal.login.error}`)}</small>
                  ) }
                </Col>
              </Row>,
              <Row key='submit' className="p-t-10">
                <Col md={{size : 5, offset : 1}}>
                  <Link style={{position: 'absolute', bottom : '0'}} to="#" onClick={this._handleForgotPassword}>{p.tc('forgot_password')}</Link>
                </Col>
                <Col md={{size : 4, offset : 1}}>
                  <Button
                    className="btn btn-success btn-sm btn-block"
                    type="submit"
                    disabled={submitting || pristine}
                    value={p.tc('login')}/>
                </Col>
              </Row>,
              <Row key='or'>
                <Col md="5">
                  <hr/>
                </Col>
                <Col md="2" className="text-center m-t-25 m-b-10">
                  {p.tu('or')}
                </Col>
                <Col md="5">
                  <hr/>
                </Col>
              </Row>,
              <Row key='signup'>
                <Col md={{size : 6, offset : 3 }}>
                <Button
                  className="btn btn-primary btn-sm btn-block"
                  type="button"
                  onClick={this._handleUserRegister}
                  value={p.tc('signup')}/>
                </Col>
              </Row>
            ]) }
          </form>
      </div>
    )
  }
}

const _initialValues = {
  username: '',
  password: ''
}
const selector = formValueSelector("LoginForm");

const mapStateToProps = (state, ownProps) => {
  const {user, page} = state;
  const formValues = _initialValues;
  const p = getP(state, {polyglotScope: 'login'});
  const err = getP(state, {polyglotScope : 'error'})
  return {
    formValueSelect: selector(state, 'username', 'password'),
    initialValues: formValues,
    user,
    page,
    p,
    err
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    postUserLogin,
    closeLoginModal,
    openRegisterModal,
    openResetPasswordModal
  }, dispatch)
}

LoginForm = reduxForm({
  form: 'LoginForm',
  enableReinitialize: true,
  validate,
})(LoginForm)

LoginForm = connect(mapStateToProps, mapDispatchToProps)(LoginForm)

export default withRouter(LoginForm)
