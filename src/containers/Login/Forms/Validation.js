export const validate = (values, { p }) => {
  const errors = {}
  if(!values.username){
    errors.username = p.tc('validation.required')
  }
  if(!values.password){
    errors.password = p.tc('validation.required')
  }
  return errors;
}
