import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import Dashboard from './Dashboard';
import SideBar from './Dashboard/SideBar';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import Notifications from 'react-notification-system-redux';
import { ModalStrap } from '../components/Bootstrap/modal';
import LoginForm from './Login';
import Logout from './Logout';
import RegisterForm from './Register';
import ContactForm from './Contact';
import ForgotPasswordForm from './ForgotPassword';
import InviteForm from './Invite';
import CompanyDocumentsForm from './Dashboard/Company/Forms/CompanyDocumentsForm';
import ShipmentDetailShipperConsigneeForm from './Dashboard/Shipment/Forms/ShipmentDetailShipperConsigneeForm';
import ShipmentDetailOrderStatForm from './Dashboard/Shipment/Forms/ShipmentDetailOrderStatForm';
import ShipmentDetailCommoditiesForm from './Dashboard/Shipment/Forms/ShipmentDetailCommoditiesForm';
import ShipmentDetailCarrierForm from './Dashboard/Shipment/Forms/ShipmentDetailCarrierForm';
import ShipmentDetailFreightForm from './Dashboard/Shipment/Forms/ShipmentDetailFreightForm';
import ShipmentDetailRequestForm from './Dashboard/Shipment/Forms/ShipmentDetailRequestForm';
import ShipmentDetailPriceForm from './Dashboard/Shipment/Forms/ShipmentDetailPriceForm';
import ShipmentDocumentsUploadForm from './Dashboard/Shipment/Forms/ShipmentDocumentsUploadForm';
import { fetchUnreadNotifications } from '../actions/message'
import { socketJoinRoom } from '../actions/websocket'

class App extends Component {
  componentDidMount(){
    const { user, subscribedChannels } = this.props
    if(user.session.isValid && subscribedChannels.length < 1){
      this.props.socketJoinRoom(user.session.token)
      this.props.fetchUnreadNotifications();
    }
  }
  componentDidUpdate(){
    const { user, subscribedChannels} = this.props
    if(user.session.isValid && subscribedChannels.length < 1 ){
      this.props.socketJoinRoom(user.session.token)
      this.props.fetchUnreadNotifications();
    }
  }
  render() {
    const { page, notifications } = this.props;
    return (
      <div className="App">
          <SideBar/>
          <Dashboard/>
          <ModalStrap
            isOpen={page.modal.login.isOpen}
            content={<LoginForm/>}
            size="md"
          />
          <ModalStrap
            isOpen={page.modal.logout.isOpen}
            content={<Logout/>}
            size="md"
          />
          <ModalStrap
            isOpen={page.modal.register.isOpen}
            content={<RegisterForm/>}
            size="lg"
          />
          <ModalStrap
            isOpen={page.modal.contact.isOpen}
            content={<ContactForm/>}
            size="lg"
          />
          <ModalStrap
            isOpen={page.modal.forgotPassword.isOpen}
            content={<ForgotPasswordForm/>}
            size="md"/>
          <ModalStrap
            isOpen={page.modal.invite.isOpen}
            content={<InviteForm/>}
            size="md"/>
          <ModalStrap
            isOpen={page.modal.uploadFile.isOpen}
            content={<CompanyDocumentsForm/>}
            size="md"/>
          <ModalStrap
            isOpen={page.modal.shipmentDetailShipperConsignee.isOpen}
            content={<ShipmentDetailShipperConsigneeForm/>}
            size="lg"/>
          <ModalStrap
            isOpen={page.modal.shipmentDetailOrderStat.isOpen}
            content={<ShipmentDetailOrderStatForm/>}
            size="lg"/>
          <ModalStrap
            isOpen={page.modal.shipmentDetailCommodities.isOpen}
            content={<ShipmentDetailCommoditiesForm/>}
            size="lg"/>
          <ModalStrap
            isOpen={page.modal.shipmentDetailCarrier.isOpen}
            content={<ShipmentDetailCarrierForm/>}
            size="lg"/>
          <ModalStrap
            isOpen={page.modal.shipmentDetailFreight.isOpen}
            content={<ShipmentDetailFreightForm/>}
            size="lg"/>
          <ModalStrap
            isOpen={page.modal.shipmentDetailRequest.isOpen}
            content={<ShipmentDetailRequestForm/>}
            size="lg"/>
          <ModalStrap
            isOpen={page.modal.shipmentDetailPrice.isOpen}
            content={<ShipmentDetailPriceForm/>}
            size="lg"/>
          <ModalStrap
            isOpen={page.modal.shipmentDocumentUploadFile.isOpen}
            content={<ShipmentDocumentsUploadForm/>}
            size="lg"/>
         <Notifications notifications={notifications}/>
      </div>
    );
  }
}

const mapStateToProps = (state,prevState) => {
  const { message : { subscribedChannels }, page, notifications, user } = state;
  return {
    subscribedChannels,
    user,
    page,
    notifications
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    fetchUnreadNotifications,
    socketJoinRoom
  }, dispatch)
}
export default withRouter(connect(mapStateToProps,mapDispatchToProps)(App));
