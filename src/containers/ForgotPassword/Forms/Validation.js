export const validate = (values,props) => {
  const errors = {};
  const { p } = props;
  if(!values.userEmail){
    errors.userEmail = p.tc('validation.email_invalid')
  }else if(!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.userEmail)) {
    errors.userEmail = p.tc('validation.email_invalid');
  }
  return errors;
}
