import React, { Component } from 'react';
import { Row, Col, Button as ButtonStrap } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';
import { withRouter } from 'react-router-dom';
import { getP } from 'redux-polyglot';
import { TextField, Button } from '../../components/Bootstrap/Forms/';
import { resetPasswordMail, closeResetPasswordModal } from '../../actions/page';
import { validate } from './Forms/Validation';
import Logo from '../../assets/img/logo.png';

class ForgotPasswordForm extends Component {
  constructor(props){
    super(props);
    this._submitForm = this._submitForm.bind(this);
    this._closeResetPasswordModal = this._closeResetPasswordModal.bind(this);
  }
  _closeResetPasswordModal(){
    this.props.closeResetPasswordModal();
  }
  _submitForm(form){
    this.props.resetPasswordMail(form);
  }
  render(){
    const { p, pt, handleSubmit, submitting, pristine, forgotPassword : { isFetching, isSuccessful } } = this.props;
    if(isFetching && !isSuccessful){
      return ( <Row>
                <Col md="12" tag='h6' className="text-center">
                  {pt.tc('loading')}
                </Col>
              </Row>)
    }else if(!isFetching && isSuccessful){
      return (<div>
                <Row className="m-t-20">
                  <Col md={{size : 6, offset : 1}} className="p-t-10">
                      <img src={Logo} alt="logo"/>
                  </Col>
                  <Col md={{size: 2, offset : 2}}>
                    <ButtonStrap outline onClick={this._closeResetPasswordModal}><i className="fa fa-remove"></i> {p.tc('close')} </ButtonStrap>
                  </Col>
                </Row>
                <Row>
                  <Col md="12" tag='h6' className="text-center">
                    {pt.tc('success')}
                  </Col>
                </Row>
              </div>)
    }
    return(
      <form onSubmit={handleSubmit(this._submitForm.bind(this))}>
        <Row className="m-t-20">
          <Col md={{size : 6, offset : 1}} className="p-t-10">
            <img src={Logo} alt="logo"/>
          </Col>
          <Col md={{size: 2, offset : 3}}>
            <ButtonStrap outline onClick={this._closeResetPasswordModal}><i className="fa fa-remove"></i> {p.tc('close')} </ButtonStrap>
          </Col>
        </Row>
        <Row>
          <Col md={{size:10, offset : 1}} tag="h3">
            {pt.tc('title')}
          </Col>
        </Row>
        <Row>
          <Col md={{size:10, offset : 1}} tag='h6'>
            {pt.tc('description')}
          </Col>
        </Row>
        <Row>
          <Col md={{size:10, offset : 1}}>
            <Field
              label={p.tu('email.label')}
              className="form-control input-sm"
              name='userEmail'
              placeholder={p.tc('email.placeholder')}
              component={TextField}
              required='required'/>
          </Col>
          </Row>
          <Row>
            <Col md={{ size : 8, offset : 2}} className="text-center">
              <Button
                  style={{margin : '20px 0px'}}
                  className="btn btn-primary btn-block"
                  type="submit"
                  disabled={submitting || pristine }
                  value={isFetching ? pt.tc('action.resetting') :  pt.tc('action.reset_password')}/>
            </Col>
          </Row>
        </form>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  const { page : { modal : { forgotPassword } } } = state;
  const pt = getP(state, { polyglotScope : 'forgotPassword'});
  const p = getP(state, { polyglotScope : 'register'});
  return {
    forgotPassword,
    p,
    pt
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    resetPasswordMail,
    closeResetPasswordModal
  }, dispatch)
}

ForgotPasswordForm = reduxForm({
  form : 'forgotPasswordForm',
  enableReinitialize : true,
  validate
})(ForgotPasswordForm)

ForgotPasswordForm = connect(mapStateToProps,mapDispatchToProps)(ForgotPasswordForm)

export default withRouter(ForgotPasswordForm)
