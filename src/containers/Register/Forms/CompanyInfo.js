import React, { Component } from 'react';
import { Row, Col } from 'reactstrap';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';
import { withRouter } from 'react-router-dom';
import { Button, TextField, SelectFieldReact } from '../../../components/Bootstrap/Forms/';
import { getP } from 'redux-polyglot';
import { industryOptions } from './';
import { validate } from './Validation';
import { fetchAllCountries } from '../../../actions/page';

class CompanyInfo extends Component {
  componentWillMount(){
    if(this.props.formValue.countries.length < 1){
      this.props.fetchAllCountries();
    }
  }
  render(){
    const { handleSubmit, p, previousPage, submitting, pristine, formValue } = this.props;
    const displayIndustryOptions = industryOptions.map(opt => ({
      label : p.tc(`company_industry.${opt.label}`),
      value : opt.value
    }));
    return (
      <form onSubmit={handleSubmit}>
    <Row>
      <Col md="12">
        <Field
          label={p.tu('company_name.label')}
          className="form-control input-sm"
          name='companyName'
          placeholder={p.tc('company_name.placeholder')}
          component={TextField}
          required='required'/>
      </Col>
    </Row>
    <Row>
      <Col md="12">
        <Field
          label={p.tu('company_address.label')}
          className="form-control input-sm"
          name='companyAddress'
          placeholder={p.tc('company_address.placeholder')}
          component={TextField}
          required='required'/>
      </Col>
    </Row>
    <Row>
      <Col md="6">
        <Field
          label={p.tu('company_country.label')}
          placeholder={p.tc('company_country.placeholder')}
          name='companyCountry'
          clearable={false}
          simpleValue={true}
          items={formValue.countries}
          component={SelectFieldReact}
          required='required'/>
      </Col>
      <Col md="6">
        <Field
          label={p.tu('company_phone.label')}
          className="form-control input-sm"
          name='companyPhone'
          placeholder={p.tc('company_phone.placeholder')}
          component={TextField}
          required='required'/>
      </Col>
      </Row>
      <Row>
        <Col md="12">
          <Field
            label={p.tu('company_industry.label')}
            name='companyIndustry'
            placeholder={p.tc('company_industry.placeholder')}
            clearable={false}
            simpleValue={true}
            items={displayIndustryOptions}
            component={SelectFieldReact}
            required='required'/>
        </Col>
      </Row>
      <Row style={{margin : '25px 0px'}}>
        <Col md={{size : 4, offset : 1}}>
          <Button
            className="btn btn-success btn-xl btn-block"
            type="button"
            onClick={previousPage}
            value={p.tc('action.before')}/>
        </Col>
        <Col md={{size : 4, offset : 2}}>
          <Button
            className="btn btn-success btn-xl btn-block"
            type="submit"
            disabled={submitting ||  pristine}
            value={p.tc('action.next')}/>
        </Col>
      </Row>
  </form>
    )
  }
}

const _initialValues = {
  companyCountry : 'IDN',
  companyIndustry : 'agribusiness'
}
const mapStateToProps = (state,ownProps) => {
  const { user, page : { modal : { register : { formValue }}} } = state;
  const p = getP(state, {polyglotScope : 'register'})
  return {
    user,
    p,
    formValue,
    initialValues : _initialValues
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    fetchAllCountries
  }, dispatch)
}

CompanyInfo = reduxForm({
  form : 'RegisterForm',
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: true,
  validate
})(CompanyInfo)

CompanyInfo = connect(mapStateToProps,mapDispatchToProps)(CompanyInfo)

export default withRouter(CompanyInfo)
