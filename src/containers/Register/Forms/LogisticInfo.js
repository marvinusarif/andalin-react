import React, { Component } from 'react';
import { Row, Col } from 'reactstrap';
import { connect } from 'react-redux';
import { reduxForm, Field, formValueSelector } from 'redux-form';
import { withRouter } from 'react-router-dom';
import { Button, TextArea, CheckBox, SelectField, SelectFieldReact } from '../../../components/Bootstrap/Forms/';
import { getP } from 'redux-polyglot';
import { annualShipmentFCLOptions, annualShipmentLCLOptions } from './';
import { validate } from './Validation';

const TermsAgreement = () => {
  return (
    <Row>
      <Col md="12">
        I have read the <a className="m-l-10 m-r-10" target="_blank" href="https://www.andalin.com/termcondition">Terms&Conditions</a> and <a className="m-l-10" target="_blank" href="https://www.andalin.com/privacypolicy">Privacy Policy</a>
      </Col>
    </Row>
  )
}
class LogisticInfo extends Component {
  render(){
    const { handleSubmit, pristine, p, previousPage, submitting, formValue, formValueSelect } = this.props;
    console.log(formValueSelect);
    return (
    <form onSubmit={handleSubmit}>
    <Row>
      <Col md="12">
        <Field
          label={p.tu('route_origin_country.label')}
          name='companyRouteOriginCountry'
          multi={true}
          simpleValue={true}
          items={formValue.countries}
          component={SelectFieldReact}
          required='required'/>
      </Col>
    </Row>
    <Row>
      <Col md="12">
        <Field
          label={p.tu('route_destination_country.label')}
          name='companyRouteDestinationCountry'
          multi={true}
          simpleValue={true}
          items={formValue.countries}
          component={SelectFieldReact}
          required='required'/>
      </Col>
    </Row>
    <Row>
      <Col md="12">
        <div className='form-group form-inline'>
          <label>{p.tc('freight.title')}</label>
        </div>
      </Col>
    </Row>
    <Row>
      <Col className="form-inline">
          <Col md="4">
            <Field name="oceanFreight"
              label={<span style={{fontWeight:'400'}}><i className="fa fa-ship"></i> {p.tc('freight.options.ocean')}</span>}
              component={CheckBox}/>
          </Col>
          <Col md="4">
            <Field name="airFreight"
              label={<span style={{fontWeight:'400'}}><i className="fa fa-plane"></i> {p.tc('freight.options.air')}</span>}
              component={CheckBox}/>
          </Col>
          <Col md="4">
            <Field name="landFreight"
              label={<span style={{fontWeight :'400'}}><i className="fa fa-truck"></i> {p.tc('freight.options.land')}</span>}
              component={CheckBox}/>
          </Col>
      </Col>
    </Row>
    <Row style={{ margin : '20px 0px 0px 0px'}}>
      <Col md="12">
        <Field
          label={p.tu('annual_shipmentLCL.label')}
          className="form-control input-sm"
          name='companyAnnualLCLShipment'
          items={annualShipmentLCLOptions}
          component={SelectField}
          required='required'/>
      </Col>
    </Row>
    <Row style={{ margin : '20px 0px 0px 0px'}}>
      <Col md="12">
        <Field
          label={p.tu('annual_shipmentFCL.label')}
          className="form-control input-sm"
          name='companyAnnualFCLShipment'
          items={annualShipmentFCLOptions}
          component={SelectField}
          required='required'/>
      </Col>
    </Row>
    <Row>
      <Col md="12">
        <Field
          label={p.tu('product.label')}
          className="form-control input-sm"
          name='companyProduct'
          placeholder={p.tc('product.placeholder')}
          component={TextArea}
          required='required'/>
      </Col>
    </Row>
    <Row>
      <Col md="12">
        <Field name="agreement"
          label={<TermsAgreement/>}
          component={CheckBox}/>
      </Col>
    </Row>
      <Row style={{margin : '25px 0px'}}>
        <Col md={{size : 4, offset : 1}}>
          <Button
            className="btn btn-success btn-xl btn-block"
            type="button"
            onClick={previousPage}
            value={p.tc('action.before')}/>
        </Col>
        <Col md={{size : 4, offset : 2}}>
          <Button
            className="btn btn-success btn-xl btn-block"
            type="submit"
            disabled={submitting || pristine || !formValueSelect}
            value={p.tc('action.register')}/>
        </Col>
      </Row>
  </form>
    )
  }
}

const _initialValues = {
  companyAnnualFCLShipment : '1-25',
  companyAnnualLCLShipment : '-100'
}
const selector = formValueSelector("RegisterForm");

const mapStateToProps = (state,ownProps) => {
  const { user, page : { modal : { register : { formValue }}}}  = state;
  const p = getP(state, {polyglotScope : 'register'})
  return {
    user,
    p,
    formValue,
    formValueSelect : selector(state, 'agreement'),
    initialValues : _initialValues
  }
}

LogisticInfo = reduxForm({
  form : 'RegisterForm',
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: true,
  validate
})(LogisticInfo)

LogisticInfo = connect(mapStateToProps,null)(LogisticInfo)

export default withRouter(LogisticInfo)
