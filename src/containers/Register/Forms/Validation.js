export const validate = (values, { p }) => {
  const errors = {};
  if(!values.userUsername){
    errors.userUsername = p.tc('validation.required');
  }
  if(!values.userPassword){
    errors.userPassword = p.tc('validation.required');
    errors.userPasswordConfirm = p.tc('validation.required');
  }
  if(values.userPassword !== values.userPasswordConfirm){
    errors.userPasswordConfirm = p.tc('validation.password_confirm_invalid');
  }
  if(!values.userEmail){
    errors.userEmail = p.tc('validation.required');
  }else if(!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.userEmail)){
    errors.userEmail = p.tc('validation.email_invalid');
  }
  if(!values.userFullname){
    errors.userFullname = p.tc('validation.required');
  }
  if(!values.userPhone){
    errors.userPhone = p.tc('validation.required');
  }
  if(!values.userJob){
    errors.userJob = p.tc('validation.required');
  }
  if(!values.companyName){
    errors.companyName = p.tc('validation.required');
  }
  if(!values.companyAddress){
    errors.companyAddress = p.tc('validation.required');
  }
  if(!values.companyPhone){
    errors.companyPhone = p.tc('validation.required');
  }
  if(!values.airFreight && !values.oceanFreight && !values.landFreight){
    errors.airFreight = ' ';
    errors.oceanFreight = ' ';
    errors.landFreight = ' ';
  }
  if(!values.companyProduct){
    errors.companyProduct = p.tc('validation.required');
  }
  return errors;
}
