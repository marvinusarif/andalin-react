
export const knoweximkuOptions = [{
  label : 'options.facebook',
  value : 'Facebook'
},{
  label : 'options.google',
  value : 'Google'
},{
  label : 'options.telemarketing',
  value : 'Telemarketing'
},{
  label : 'options.other',
  value : 'Other'
}]

export const sexOptions = [{
  label : 'options.female',
  value : 'F'
},{
  label : 'options.male',
  value : 'M'
}]

export const industryOptions = [{
  label : 'options.aerospace',
  value : 'aerospace'
},{
  label : 'options.agribusiness',
  value : 'agribusiness'
},{
  label : 'options.agricultural_services',
  value : 'agricultural_services'
},{
  label : 'options.auto_dealers',
  value : 'auto_dealers'
},{
  label : 'options.automotive',
  value : 'automotive'
},{
  label : 'options.banks_commercial',
  value : 'banks_commercial'
},
{
  label : 'options.accounting',
  value : 'accounting'
},
{
  label : 'options.airlines',
  value : 'airlines'
},
{
  label : 'options.medicine',
  value : 'medicine'
},
{
  label : 'options.animation',
  value : 'animation'
},
{
  label : 'options.fashion',
  value : 'fashion'
},
{
  label : 'options.architecture',
  value : 'architecture'
},
{
  label : 'options.arts',
  value : 'arts'
},
{
  label : 'options.automotive',
  value : 'automotive'
},
{
  label : 'options.banking',
  value : 'banking'
},
{
  label : 'options.biotechnology',
  value : 'biotechnology'
},
{
  label : 'options.broadcast_media',
  value : 'broadcast_media'
},
{
  label : 'options.aviation',
  value : 'aviation'
},
{
  label : 'options.building_materials',
  value : 'building_materials'
},
{
  label : 'options.business_supplies',
  value : 'business_supplies'
},
{
  label : 'options.equipment',
  value : 'equipment'
},
{
  label : 'options.capital_market',
  value : 'capital_market'
},
{
  label : 'options.private_equity',
  value : 'private_equity'
},
{
  label : 'options.broadcast',
  value : 'broadcast'
},
{
  label : 'options.chemicals',
  value : 'chemicals'
},
{
  label : 'options.social_organization',
  value : 'social_organization'
},
{
  label : 'options.civil_engineering',
  value : 'civil_engineering'
},
{
  label : 'options.real_estate',
  value : 'real_estate'
},
{
  label : 'options.developer_games',
  value : 'developer_games'
},
{
  label : 'options.computer_hardware',
  value : 'computer_hardware'
},
{
  label : 'options.computer_networking',
  value : 'computer_networking'
},
{
  label : 'options.developer_software',
  value : 'developer_software'
},
{
  label : 'options.construction',
  value : 'construction'
},
{
  label : 'options.consumer_electronics',
  value : 'consumer_electronics'
},
{
  label : 'options.consumer_goods',
  value : 'consumer_goods'
},
{
  label : 'options.consumer_services',
  value : 'consumer_services'
},
{
  label : 'options.cosmetics',
  value : 'cosmetics'
},
{
  label : 'options.dairy',
  value : 'dairy'
},
{
  label : 'options.defense',
  value : 'defense'
},
{
  label : 'options.space',
  value : 'space'
},
{
  label : 'options.design',
  value : 'design'
},
{
  label : 'options.e_learning',
  value : 'e_learning'
},
{
  label : 'options.education_management',
  value : 'education_management'
},
{
  label : 'options.electronic_manufacturing',
  value : 'electronic_manufacturing'
},
{
  label : 'options.entertainment',
  value : 'entertainment'
},
{
  label : 'options.movie_production',
  value : 'movie_production'
},
{
  label : 'options.environmental_services',
  value : 'environmental_services'
},
{
  label : 'options.events_services',
  value : 'events_services'
},
{
  label : 'options.executive_office',
  value : 'executive_office'
},
{
  label : 'options.facilities_services',
  value : 'facilities_services'
},
{
  label : 'options.farming',
  value : 'farming'
},
{
  label : 'options.financial',
  value : 'financial'
},
{
  label : 'options.fishery',
  value : 'fishery'
},
{
  label : 'options.food_production',
  value : 'food_production'
},
{
  label : 'options.beverages',
  value : 'beverages'
},
{
  label : 'options.government_administration',
  value : 'government_administration'
},
{
  label : 'options.fundraising',
  value : 'fundraising'
},
{
  label : 'options.furniture',
  value : 'furniture'
},
{
  label : 'options.government_relations',
  value : 'government_relations'
},
{
  label : 'options.graphic_design',
  value : 'graphic_design'
},
{
  label : 'options.web_design',
  value : 'web_design'
},
{
  label : 'options.fitness',
  value : 'fitness'
},
{
  label : 'options.hospital',
  value : 'hospital'
},
{
  label : 'options.human_resource',
  value : 'human_resource'
},
{
  label : 'options.information_technology',
  value : 'information_technology'
},
{
  label : 'options.insurance',
  value : 'insurance'
},
{
  label : 'options.investment_banking',
  value : 'investment_banking'
},
{
  label : 'options.law_firms',
  value : 'law_firms'
},
{
  label : 'options.legal_services',
  value : 'legal_services'
},
{
  label : 'options.logistics',
  value : 'logistics'
},
{
  label : 'options.jewelry',
  value : 'jewelry'
},
{
  label : 'options.maritime',
  value : 'maritime'
},
{
  label : 'options.advertising',
  value : 'advertising'
},
{
  label : 'options.mediaproduction',
  value : 'mediaproduction'
},
{
  label : 'options.mining',
  value : 'mining'
},
{
  label : 'options.institutions',
  value : 'institutions'
},
{
  label : 'options.nanotechnology',
  value : 'nanotechnology'
},
{
  label : 'options.oil',
  value : 'oil'
},
{
  label : 'options.pharmaceuticals',
  value : 'pharmaceuticals'
},
{
  label : 'options.plastics',
  value : 'plastics'
},
{
  label : 'options.political_organization',
  value : 'political_organization'
},
{
  label : 'options.professional_training',
  value : 'professional_training'
},
{
  label : 'options.public_relations',
  value : 'public_relations'
},
{
  label : 'options.public_safety',
  value : 'public_safety'
},
{
  label : 'options.real_estate',
  value : 'real_estate'
},
{
  label : 'options.religious_institutions',
  value : 'religious_institutions'
},
{
  label : 'options.renewables',
  value : 'renewables'
},
{
  label : 'options.research_industry',
  value : 'research_industry'
},
{
  label : 'options.restaurants',
  value : 'restaurants'
},
{
  label : 'options.supermarkets',
  value : 'supermarkets'
},
{
  label : 'options.telecommunications',
  value : 'telecommunications'
},
{
  label : 'options.tobacco',
  value : 'tobacco'
},
{
  label : 'options.warehousing',
  value : 'warehousing'
},
{
  label : 'options.wholesale',
  value : 'wholesale'
},
{
  label : 'options.wireless',
  value : 'wireless'
},
{
  label : 'options.other',
  value : 'other'
}]


export const annualShipmentFCLOptions = [{
  label : '1-25 CONTAINER',
  value : '1-25'
},{
  label : '25-150 CONTAINER',
  value : '25-150'
},{
  label : '150-1000 CONTAINER',
  value : '150-1000'
},{
  label : '1000+ CONTAINER',
  value : '1000+'
}]

export const annualShipmentLCLOptions = [{
  label : '< 100 Kgs',
  value : '-100'
},{
  label : '100 - 300 Kgs',
  value : '100-300'
},{
  label : '300 - 900 Kgs',
  value : '300-900'
},{
  label : '> 900 Kgs',
  value : '900+'
}]
