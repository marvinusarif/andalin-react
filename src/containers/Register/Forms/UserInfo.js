import React, { Component } from 'react';
import { Row, Col } from 'reactstrap';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';
import { withRouter } from 'react-router-dom';
import { Button, TextField, SelectField } from '../../../components/Bootstrap/Forms/';
import { getP } from 'redux-polyglot';
import { sexOptions, knoweximkuOptions } from './';
import { validate } from './Validation';
import emailAsyncValidate from './emailAsyncValidation';

class UserInfo extends Component {
  render(){
    const { handleSubmit, p, submitting, pristine } = this.props;
    const displayKnoweximkuOptions = knoweximkuOptions.map(opt => ({
      label : p.tc(`knoweximku.${opt.label}`),
      value : opt.value
    }));
    const displaySexOptions = sexOptions.map(opt => ({
      label : p.tc(`sex.${opt.label}`),
      value : opt.value
    }));
    return (
      <form onSubmit={handleSubmit}>
        <Row>
          <Col md="10">
            <Field
              label={p.tu('fullname.label')}
              className="form-control input-sm"
              name='userFullname'
              placeholder={p.tc('fullname.placeholder')}
              component={TextField}
              required='required'/>
          </Col>
          <Col md="2">
            <Field
              label={p.tu('sex.label')}
              className="form-control input-sm"
              name='userSex'
              items={displaySexOptions}
              component={SelectField}
              required='required'/>
          </Col>
        </Row>
    <Row>
      <Col md="6">
        <Field
          label={p.tu('password.label')}
          type='password'
          className="form-control input-sm"
          name='userPassword'
          placeholder={p.tc('password.placeholder')}
          component={TextField}
          required='required'/>
      </Col>
      <Col md="6">
        <Field
          label={p.tu('password_confirm.label')}
          type='password'
          className="form-control input-sm"
          name='userPasswordConfirm'
          placeholder={p.tc('password_confirm.placeholder')}
          component={TextField}
          required='required'/>
      </Col>
      </Row>
      <Row>
        <Col md="6">
          <Field
            label={p.tu('email.label')}
            className="form-control input-sm"
            name='userEmail'
            placeholder={p.tc('email.placeholder')}
            component={TextField}
            required='required'/>
        </Col>
        <Col md="6">
          <Field
            label={p.tu('phone.label')}
            className="form-control input-sm"
            name='userPhone'
            placeholder={p.tc('phone.placeholder')}
            component={TextField}
            required='required'/>
        </Col>
      </Row>
      <Row>
        <Col md="6">
          <Field
            label={p.tu('job.label')}
            className="form-control input-sm"
            name='userJob'
            placeholder={p.tc('job.placeholder')}
            component={TextField}
            required='required'/>
        </Col>
        <Col md="6">

        </Col>
      </Row>
      <Row>
        <Col md="12">
          <Field
            label={p.tu('knoweximku.label')}
            className="form-control input-sm"
            name='userKnoweximku'
            items={displayKnoweximkuOptions}
            component={SelectField}
            required='required'/>
        </Col>
      </Row>
      <Row style={{margin : '25px 0px'}}>
        <Col md={{size : 4, offset : 1}}>
          <Button
            className="btn btn-success btn-xl btn-block"
            type="button"
            disabled
            value={p.tc('action.before')}/>
        </Col>
        <Col md={{size : 4, offset : 2}}>
          <Button
            className="btn btn-success btn-xl btn-block"
            type="submit"
            disabled={submitting || pristine}
            value={p.tc('action.next')}/>
        </Col>
      </Row>
  </form>
    )
  }
}
const _initialValues = {
  userPassword : '',
  userPasswordConfirm : '',
  userEmail : '',
  userFullname : '',
  userSex : 'M',
  userJob : '',
  userPhone : '',
  userKnoweximku : 'facebook'
}

const mapStateToProps = (state,ownProps) => {
  const { user } = state;
  const p = getP(state, {polyglotScope : 'register'})
  return {
    user,
    p,
    initialValues : _initialValues
  }
}

UserInfo = reduxForm({
  form : 'RegisterForm',
  destroyOnUnmount: false,
  forceUnregisterOnUnmount: true,
  validate,
  asyncValidate : emailAsyncValidate,
  asyncBlurFields: ['userEmail']
})(UserInfo)

UserInfo = connect(mapStateToProps,null)(UserInfo)

export default withRouter(UserInfo)
