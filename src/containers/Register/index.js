import React, { Component } from 'react';
import classnames from 'classnames';
import { Button as ButtonStrap, Nav, NavItem, NavLink, Row, Col, TabPane, TabContent} from 'reactstrap';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { getP } from 'redux-polyglot';
import { Button } from '../../components/Bootstrap/Forms';
import { openContactModal, closeRegisterModal, postVerificationEmail } from '../../actions/page';
import { postUserRegister } from '../../actions/user';
import UserInfo from './Forms/UserInfo';
import CompanyInfo from './Forms/CompanyInfo';
import LogisticInfo from './Forms/LogisticInfo';
import Logo from '../../assets/img/logo.png';

class RegisterForm extends Component {
  constructor(props){
    super(props);
    this.nextPage = this.nextPage.bind(this);
    this.previousPage = this.previousPage.bind(this);
    this._submitUserRegister = this._submitUserRegister.bind(this);
    this._handleUserRegister = this._handleUserRegister.bind(this);
    this._handleResendEmail = this._handleResendEmail.bind(this);
    this._handleNeedHelp = this._handleNeedHelp.bind(this);
    this._closeRegisterForm = this._closeRegisterForm.bind(this);
    this.state = {
      page: 1
    }
  }
  nextPage() {
    this.setState({page: this.state.page + 1})
  }
  previousPage() {
    this.setState({page: this.state.page - 1})
  }
  _submitUserRegister(_form){
    if(_form){
      const form = Object.assign({}, _form, {
        companyReference : process.env.REACT_APP_PRODUCTION_COMPANY_REFERENCE
      })
      this.props.postUserRegister(form);
      this.setState({page: this.state.page + 1});
    }
  }
  _handleUserRegister(){
    const { form } = this.props.register;
    this.props.postUserRegister(form);
  }
  _handleResendEmail(){
    const { register : { user } } = this.props;
    this.props.postVerificationEmail(user);
  }
  _handleNeedHelp(){
    this.props.closeRegisterModal();
    this.props.openContactModal();
  }
  _closeRegisterForm(){
    this.props.closeRegisterModal();
  }
  render(){
    const { p, register } = this.props;
    if(this.state.page === 4) {
      if( register.isFetching && !register.isSuccessful) {
        //registration on process
        return (
          <div>
            <Row>
              <Col md="12" tag="h5" className="text-center">
                  {p.tc('loading')}
              </Col>
            </Row>
          </div>
        )
      }else if(!register.isFetching && !register.isSuccessful){
        //registration is unsuccessful
        return(
          <div>
            <Row>
              <Col md={{size : 6, offset : 1}} className="p-t-10">
                <img src={Logo} alt="logo"/>
              </Col>
              <Col md={{size : 2, offset : 3}}>
                <ButtonStrap outline onClick={this._closeRegisterForm}><i className="fa fa-remove"></i> {p.tc('close')} </ButtonStrap>
              </Col>
            </Row>
            <Row>
              <Col md="12" tag='h5' className="text-center">
                {p.tc('status.failed')}
              </Col>
            </Row>
            <Row>
              <Col md={{size: 8, offset: 2 }} className="m-t-10">
                <Button
                  className="btn btn-success btn-sm btn-block"
                  type="button"
                  onClick={this._handleUserRegister}
                  value={p.tc('action.register_again')}/>
              </Col>
            </Row>
          </div>
        )
      }else {
        //registration successfull
        return (
          <div>
            <Row className="p-t-10">
              <Col md={{size : 6, offset : 1}}>
                <img src={Logo} alt="logo"/>
              </Col>
              <Col md={{size : 2, offset : 3}}>
                <ButtonStrap outline onClick={this._closeRegisterForm}><i className="fa fa-remove"></i> {p.tc('close')} </ButtonStrap>
              </Col>
            </Row>
            <Row>
              <Col md="12" tag="h5" className="text-center">
                {p.tc('status.success', { email : register.user.userEmail})}
              </Col>
            </Row>
            <Row>
              <Col md="12" tag="h6" className="text-center">
                { register.mail.emailSent > 0 ? (
                  p.tc('status.attempt', { smart_count : register.mail.emailSent })
                ) : (
                  p.tc('status.no_attempt')
                )}
              </Col>
            </Row>
            <Row>
              <Col md={{size: 8, offset: 2 }} className="m-t-10">
              { register.mail.emailSent > 0 ? (
                <Button
                  className="btn btn-success btn-sm btn-block"
                  type="button"
                  onClick={this._handleResendEmail}
                  disabled={register.mail.timer > 0 ? true : false}
                  value={
                    register.mail.timer > 0 ?
                    (p.tc('action.resend_email', { seconds : p.t('action.seconds', { smart_count : register.mail.timer })}))
                    :
                    (p.tc('action.send_now'))}/>
              ) : (
                <Button
                  className="btn btn-success btn-sm btn-block"
                  type="button"
                  onClick={this._handleNeedHelp}
                  value={p.tc('action.need_help')}/>
              )}
              </Col>
            </Row>
          </div>
        )
      }
    }
    return(
      <div>
        <Row className="m-t-20">
          <Col md={{size : 6, offset : 1}} className="p-t-10">
            <img src={Logo} alt="logo"/>
          </Col>
          <Col md={{size : 2, offset : 3}}>
            <ButtonStrap outline onClick={this._closeRegisterForm}><i className="fa fa-remove"></i> {p.tc('close')} </ButtonStrap>
          </Col>
        </Row>
        <Row>
          <Col md={{size: 10, offset : 1}} tag="h3">
            {p.tc('title')}
          </Col>
        </Row>
        <Row>
          <Col md={{size: 10, offset : 1}}>
            <Nav tabs className='nav nav-tabs nav-tabs-simple'>
              <NavItem>
                <NavLink
                  className={classnames({ active : this.state.page === 1})}>
                    <i className="fa fa-user"></i>
                    {` `}{p.t('tabs.user_info')}
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  className={classnames({ active : this.state.page === 2})}>
                    <i className="fa fa-industry"></i>
                    {` `}{p.t('tabs.company_info')}
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  className={classnames({ active : this.state.page === 3})}>
                    <i className="fa fa-truck"></i>
                    {` `}{p.t('tabs.logistic_info')}
                </NavLink>
              </NavItem>
            </Nav>
            <TabContent activeTab={`${this.state.page}`}>
              <TabPane tabId='1'>
                <Row className="row column-seperation">
                  <Col sm="12">
                    {this.state.page === 1 && (
                      <UserInfo
                        previousPage={this.previousPage}
                        onSubmit={this.nextPage}/>
                    )}
                  </Col>
                </Row>
              </TabPane>
              <TabPane tabId='2'>
                <Row className="row column-seperation">
                  <Col sm="12">
                    {this.state.page === 2 && (
                    <CompanyInfo
                      previousPage={this.previousPage}
                      onSubmit={this.nextPage}/>
                    )}
                  </Col>
                </Row>
              </TabPane>
              <TabPane tabId='3'>
                <Row className="row column-seperation">
                  <Col sm="12">
                    {this.state.page === 3 && (
                    <LogisticInfo
                      previousPage={this.previousPage}
                      onSubmit={this._submitUserRegister}/>
                    )}
                  </Col>
                </Row>
              </TabPane>
            </TabContent>
          </Col>
        </Row>
      </div>)
  }
}


const mapStateToProps = (state,ownProps) => {
  const { user, page : { modal : { register }}} = state;
  const p = getP(state, {polyglotScope : 'register'});
  return {
    user,
    p,
    register
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    postVerificationEmail,
    postUserRegister,
    openContactModal,
    closeRegisterModal
  }, dispatch)
}

RegisterForm = connect(mapStateToProps,mapDispatchToProps)(RegisterForm)

export default withRouter(RegisterForm)
