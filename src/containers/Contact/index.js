import React, {Component} from 'react';
import { Row, Col, Button as ButtonStrap} from 'reactstrap';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';
import { withRouter } from 'react-router-dom';
import { Button , TextField, TextArea, SelectFieldReact } from '../../components/Bootstrap/Forms/';
import { getP } from 'redux-polyglot';
import { closeContactModal, postContact } from '../../actions/page';
import { fetchUser } from '../../actions/user';
import { subjectOptions } from './Forms/';
import { validate } from './Forms/Validation';
import Logo from '../../assets/img/logo.png';

class ContactForm extends Component {
  constructor(props) {
    super(props);
    this._handleSubmitForm = this._handleSubmitForm.bind(this);
    this._closeContactForm = this._closeContactForm.bind(this);
    this.state = {
      editable : true
    }
  }
  componentDidMount(){
    if(this.props.user.session.isValid){
      this.setState({ editable : false });
    }
  }
  _handleSubmitForm(form){
    const { user : { profile } } = this.props;
    const { editable } = this.state;
    const { fullname, email, phone, subject, subjectDescription, message } = form;
    const submitValue = {
      fullname : editable ? fullname : profile.fullname,
      email : editable ? email : profile.email,
      phone : editable ? phone : profile.phone,
      subject,
      subjectDescription,
      message,
      sender : profile.codeusertype ? profile.codeusertype : 'NEWUSER'
    }
    this.props.postContact(submitValue);
  }
  _closeContactForm(){
    this.props.closeContactModal();
  }
  render() {
    const { submitting, pristine, handleSubmit, p, contact } = this.props;
    const { editable } = this.state;
    const displaySubjectOptions = subjectOptions.map(opt => ({
      label : p.tc(`subject.options.${opt.label}`),
      value : opt.value
    }));
    return (
      <div>
            <form onSubmit={handleSubmit(this._handleSubmitForm)}>
            <Row className="m-t-20">
              <Col md={{size : 6, offset : 1}} className="p-t-10">
                <img src={Logo} alt="logo"/>
              </Col>
              <Col md={{size : 2, offset : 3}} className="p-l-25">
                <ButtonStrap outline onClick={this._closeContactForm}><i className="fa fa-remove"></i> {p.tc('close')} </ButtonStrap>
              </Col>
            </Row>
            { contact.isSuccessful ? (
            <Row>
              <Col md={{size:10, offset : 1}} tag="h3" className="text-center">
                  {p.tc('feedback.title')}
              </Col>
            </Row>
          ) : (
            <Row>
              <Col md={{size:10, offset : 1}} tag="h3">
                  {p.tc('title')}
              </Col>
            </Row>
          ) }
            { contact.isSuccessful ? (
              <Row>
                <Col className="text-center" tag="h5">
                  {p.tc('feedback.on_success')}
                </Col>
              </Row>
            ) : (
              contact.isFetching ? (
                <Row style={{ width : '100%', height : '300px'}}>
                  <Col md="12" className="text-center">
                    {p.tc('loading')}
                  </Col>
                </Row>
              ) : ([
                <Row key='fullname'>
                  <Col md={{size : 10, offset : 1}}>
                    { editable && (
                      <Field
                        label={p.tu('fullname.label')}
                        className="form-control input-sm"
                        name='fullname'
                        placeholder={p.tc('fullname.placeholder')}
                        component={TextField}
                        required='required'/>
                    )}

                  </Col>
                </Row>,
                <Row key='email'>
                  <Col md={{size : 5, offset : 1}}>
                    { editable && (
                      <Field
                        label={p.tu('email.label')}
                        className="form-control input-sm"
                        name='email'
                        placeholder={p.tc('email.placeholder')}
                        component={TextField}
                        required='required'/>
                    )}
                  </Col>
                  <Col md={{size : 5, offset : 0}}>
                    { editable && (
                      <Field
                        label={p.tu('phone.label')}
                        className="form-control input-sm"
                        name='phone'
                        placeholder={p.tc('phone.placeholder')}
                        component={TextField}
                        required='required'/>
                    )}
                  </Col>
                </Row>,
                <Row key='subject'>
                  <Col md={{size : 10, offset : 1}}>
                    <Field
                      label={p.tu('subject.label')}
                      name='subject'
                      items={displaySubjectOptions}
                      placeholder={p.tc('subject.placeholder')}
                      clearable={false}
                      simpleValue={true}
                      component={SelectFieldReact}
                      required='required'/>
                  </Col>
                </Row>,
                <Row key='subjectDescription'>
                  <Col md={{size : 10, offset : 1}}>
                    <Field
                      label={p.tu('subject_description.label')}
                      className="form-control input-sm"
                      name='subjectDescription'
                      placeholder={p.tc('subject_description.placeholder')}
                      component={TextField}
                      required='required'/>
                  </Col>
                </Row>,
                <Row key='message'>
                  <Col md={{size : 10, offset : 1}}>
                    <Field
                      label={p.tu('message.label')}
                      className="form-control input-sm"
                      name='message'
                      placeholder={p.tc('message.placeholder')}
                      component={TextArea}
                      required='required'/>
                  </Col>
                </Row>,
                <Row key='submit' className="p-t-10">
                  <Col md={{size : 4, offset : 7}}>
                    <Button
                      className="btn btn-success btn-sm btn-block"
                      type="submit"
                      disabled={submitting || pristine}
                      value={p.tc('get_help')}/>
                  </Col>
                </Row>])
            )}
          </form>
      </div>)
  }
}

const mapStateToProps = (state, ownProps) => {
  const { user, page : { modal : { contact } } } = state;
  const p = getP(state, {polyglotScope: 'contact'})
  return {
    p,
    contact,
    user
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    closeContactModal,
    fetchUser,
    postContact
  }, dispatch)
}

ContactForm = reduxForm({
  form: 'ContactForm',
  enableReinitialize: true,
  validate
})(ContactForm)

ContactForm = connect(mapStateToProps, mapDispatchToProps)(ContactForm)

export default withRouter(ContactForm)
