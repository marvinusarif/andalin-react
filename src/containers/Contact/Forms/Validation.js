export const validate = (values, { p }) => {
  const errors = {}
  if(!values.fullname){
    errors.fullname = p.tc('validation.required');
  }
  if(!values.email) {
    errors.email = p.tc('validation.required');
  }else if(!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)){
    errors.email = p.tc('validation.email_invalid');
  }
  if(!values.phone) {
    errors.phone = p.tc('validation.required');
  }
  if(!values.subjectDescription){
    errors.subjectDescription =  p.tc('validation.required');
  }
  if(!values.message){
    errors.message =  p.tc('validation.required');
  }
  return errors;
}
