
import { _enLang } from './enLang.js';
import { _idnLang } from './idnLang.js';

export const enLang = _enLang;
export const idnLang = _idnLang;
