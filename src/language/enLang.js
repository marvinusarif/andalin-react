export const _enLang = {
  message : {
    subscribers : {
      title : '%{smart_count} users join this room |||| %{smart_count} user join this room',
      empty : 'Nobody joins the room yet',
      description : 'You may subscribe to start conversation with %{talkTo}'
    },
    action : {
      subscribe : 'subscribe'
    }
  },
  notifications: {
    user: {
      update: {
        title: 'Success',
        message: 'User Profile has been updated'
      }
    },
    company: {
      users: {
        update: {
          title: 'Success',
          message: 'Company Users Privilege has been updated'
        }
      },
      profile: {
        update: {
          title: 'Success',
          message: 'Company Profile has been updated'
        }
      }
    },
    quote: {
      receive: {
        title: 'Success',
        message: 'This Quote has been saved'
      }
    }
  },
  error: {
    title: 'Error',
    message: {
      not_found: 'page not found',
      unauthorized: 'unauthorized',
      invalid_login: 'invalid username/ password',
      account_unverified: 'your account has not been verified',
      suspended: 'your account has been suspended',
      server_error: 'internal server error',
      bad_request: 'bad request',
      no_content: 'no content',
      verified_twice: 'your account has been verified',
      request_too_large: 'file size maximum is 3 MB',
      bad_file_type: 'file extension is not allowed',
      connection_lost: 'Internet connection is in trouble',
      payment_is_lesser_than_total_invoice : 'Payment is lesser than total invoice'
    }
  },
  verify: {
    title: 'Account Verification Success',
    loading: 'We are currently activating your account. Please Wait...',
    description: 'your account has been successfully confirmed, please log into your account',
    login: 'sign in'
  },
  invitation: {
    title: 'Invitation to collaborate',
    loading: 'loading...',
    success: 'You have updated your profile and password. Please log into your account.',
    description: 'Please fulfill form below to start collaborating with your teammates',
    action: {
      submit: 'update my profile and password',
      login: 'sign in'
    }
  },
  invite: {
    title: 'Invite New User',
    loading: 'loading...',
    close: 'close',
    description: 'enter email address of your teammates member to collaborate together',
    success: 'email has been sent to %{email}',
    email: {
      label: 'email address',
      placeholder: 'enter user email address'
    },
    action: {
      invite: 'invite',
      inviting: 'sending email...'
    },
    validation: {
      required: 'required',
      email_invalid: 'invalid email address'
    }
  },
  forgotPassword: {
    title: 'Reset Password',
    loading: 'loading...',
    success: 'we have sent an email to your inbox. Thank you.',
    description: 'Enter your registered email to reset your password',
    action: {
      reset_password: 'send new password to my email',
      resetting: 'sending...'
    }
  },
  resetPassword: {
    title: 'Password Reset',
    description: 'reset your new password by fulfilling form below',
    success: 'You have successfully changed your password. Please log into your account',
    password_new: {
      label: 'new password',
      placeholder: 'enter your new password'
    },
    password_new_confirm: {
      label: 'new password confirmation',
      placeholder: 'enter new password confirmation'
    },
    action: {
      change_password: 'change your password',
      updating: 'updating...',
      login: 'sign in'
    },
    validation: {
      required: 'required',
      password_confirm_invalid: 'password does not match'
    }
  },
  logout: {
    title: 'user logout',
    description: 'are you sure?',
    action: {
      cancel: 'cancel',
      logout: 'logout'
    }
  },
  contact: {
    title: 'Having trouble? We are ready to help.',
    loading: 'loading...',
    close: 'close',
    fullname: {
      label: 'full name',
      placeholder: 'enter your name'
    },
    email: {
      label: 'email',
      placeholder: 'enter your email address'
    },
    phone: {
      label: 'phone',
      placeholder: 'enter your phone number'
    },
    subject: {
      label: 'subject',
      placeholder: 'select subject',
      options: {
        register: 'having trouble while registering account',
        account: 'cannot log into my account',
        password: 'password recovery does not work',
        invite: 'cannot invite new user',
        no_quote: 'cannot find quote from any routes',
        other: 'others'
      }
    },
    subject_description: {
      label: 'subject description',
      placeholder: 'enter subject description'
    },
    message: {
      label: 'message',
      placeholder: 'enter your message'
    },
    validation: {
      required: 'Required',
      email_invalid: 'email is not valid'
    },
    feedback: {
      title: 'thank you for contacting us.',
      on_success: 'we will be back to you shortly.'
    },
    get_help: 'Request Assistance'
  },
  register: {
    title: 'register to andalin',
    loading: 'Your registration is being processed. Please wait...',
    close: 'close',
    tabs: {
      user_info: 'user',
      company_info: 'company',
      logistic_info: 'logistic'
    },
    fullname: {
      label: 'full name',
      placeholder: 'enter your full name'
    },
    password: {
      label: 'password',
      placeholder: 'enter password'
    },
    password_change: {
      label: 'password',
      placeholder: 'enter password to change your password'
    },
    password_confirm: {
      label: 'password confirmation',
      placeholder: 'enter password confirmation'
    },
    photo: {
      label: 'Upload your photo',
      placeholder: 'upload your photo'
    },
    sex: {
      label: 'sex',
      options: {
        male: 'male',
        female: 'female'
      }
    },
    phone: {
      label: 'phone',
      placeholder: 'enter your phone number'
    },
    job: {
      label: 'job',
      placeholder: 'enter your work position'
    },
    email: {
      label: 'email',
      placeholder: 'enter your email'
    },
    knoweximku: {
      label: 'how do you know Andalin?',
      options: {
        facebook: 'facebook',
        google: 'google',
        telemarketing: 'telemarketing',
        other: 'others'
      }
    },
    company_name: {
      label: 'company name',
      placeholder: 'enter your company name'
    },
    company_address: {
      label: 'company address',
      placeholder: 'enter your company address'
    },
    company_fax: {
      label: 'company fax',
      placeholder: 'enter your company fax'
    },
    company_phone: {
      label: 'company phone',
      placeholder: 'enter your company phone'
    },
    company_email: {
      label: 'company email',
      placeholder: 'enter your company email'
    },
    company_description: {
      label: 'company description',
      placeholder: 'enter your company description'
    },
    company_country: {
      label: 'country',
      placeholder: 'select country'
    },
    company_industry: {
      label: 'what industry are you in?',
      placeholder: 'select industry',
      options: {
        aerospace: 'Aerospace',
        agribusiness: 'Agribusiness',
        agricultural_services: 'Agricultural Services',
        auto_dealers: 'Auto Dealers',
        automotive: 'Automotive',
        banks_commercial: 'Banks Commercial',
        accounting: 'Accounting',
        airlines: 'Airlines',
        medicine: 'Medicine',
        animation: 'Animation',
        fashion: 'Fashion',
        architecture: 'Architecture',
        arts: 'Arts& Design',
        banking: 'Banking',
        biotechnology: 'Biotechnology',
        broadcast_media: 'Broadcast Media',
        aviation: 'Aviation',
        building_materials: 'Building Materials',
        business_supplies: 'Business Supplies',
        equipment: 'Equipment',
        capital_market: 'Capital Market',
        private_equity: 'Private Equity',
        broadcast: 'Broadcast',
        chemicals: 'Chemicals',
        social_organization: 'Social Organization',
        civil_engineering: 'Civil Engineering',
        real_estate: 'Real Estate',
        developer_games: 'Developer Games',
        computer_hardware: 'Computer Hardware',
        computer_networking: 'Computer Networking',
        developer_software: 'Developer Software',
        construction: 'Construction',
        consumer_electronics: 'Consumer Electronics',
        consumer_goods: 'Consumer Goods',
        consumer_services: 'Consumer Services',
        cosmetics: 'Cosmetics',
        dairy: 'Dairy',
        defense: 'Defense',
        space: 'Space',
        design: 'Design',
        e_learning: 'E-Learning',
        education_management: 'Education Management',
        electronic_manufacturing: 'Electronic Manufacturing',
        entertainment: 'Entertainment',
        movie_production: 'Movie Production',
        environmental_services: 'Environmental Services',
        events_services: 'Events Services',
        executive_office: 'Executive Office',
        facilities_services: 'Facilities Services',
        farming: 'Farming',
        financial: 'Financial',
        fishery: 'Fishery',
        food_production: 'Food Production',
        beverages: 'Beverages',
        government_administration: 'Government Administration',
        fundraising: 'Fundraising',
        furniture: 'Furniture',
        government_relations: 'Government Relations',
        graphic_design: 'Graphic Design',
        web_design: 'Web Design',
        fitness: 'Fitness',
        hospital: 'Hospital',
        human_resource: 'Human Resource',
        information_technology: 'Information Technology',
        insurance: 'Insurance',
        investment_banking: 'Investment Banking',
        law_firms: 'Law Firms',
        legal_services: 'Legal Services',
        logistics: 'Logistics',
        jewelry: 'Jewelry',
        maritime: 'Maritime',
        advertising: 'Advertising',
        mediaproduction: 'Media Production',
        mining: 'Mining',
        institutions: 'Institutions',
        nanotechnology: 'Nanotechnology',
        oil: 'oil',
        pharmaceuticals: 'Pharmaceuticals',
        plastics: 'Plastics',
        political_organization: 'Political Organization',
        professional_training: 'Professional Training',
        public_relations: 'Public Relations',
        public_safety: 'Public Safety',
        religious_institutions: 'Religious Institutions',
        renewables: 'Renewables',
        research_industry: 'Research Industry',
        restaurants: 'Restaurants',
        supermarkets: 'Supermarkets',
        telecommunications: 'Telecommunications',
        tobacco: 'Tobacco',
        warehousing: 'Warehousing',
        wholesale: 'Wholesale',
        wireless: 'Wireless',
        other: 'Other'
      }
    },
    route_origin_country: {
      label: 'What countries do you ship from?'
    },
    route_destination_country: {
      label: 'What countries do you ship to?'
    },
    freight: {
      title: 'How do you usually ship ?',
      options: {
        ocean: 'ocean freight',
        air: 'air freight',
        land: 'land freight'
      }
    },
    annual_shipmentLCL: {
      label: 'What is your annual low container load shipment?'
    },
    annual_shipmentFCL: {
      label: 'What is your annual full container load shipment?'
    },
    product: {
      label: 'what product do you ship?',
      placeholder: 'Describe your product here'
    },
    status: {
      failed: 'Your registration has been failed. Please check your internet connection',
      success: 'Thank you for registering in our system. We just send the verification code to %{email}. Please kindly check your inbox and confirm your account.',
      attempt: 'You may resend %{smart_count} email to your inbox',
      no_attempt: 'Having trouble in receiving emails? Please contact our support.'
    },
    action: {
      next: 'next',
      before: 'back',
      register: 'register',
      seconds: '%{smart_count} second |||| %{smart_count} seconds',
      resend_email: 'resend email in %{seconds}',
      send_now: 'resend email now',
      register_again: 'try again',
      need_help: 'click here to get assistance',
      update_button: 'update',
      reset: 'reset',
      updating: 'updating...'
    },
    validation: {
      email_invalid: 'email is not valid',
      required: 'Required',
      number: 'must be a number',
      password_confirm_invalid: 'password does not match'
    }
  },
  main : {
    total_shipment : {
      bookings : 'book',
      progress : 'in progress',
      payment : 'waiting for payment',
      complete : 'completed',
    },
    shipment : '%{smart_count} shipment |||| %{smart_count} shipments'
  },
  login: {
    title: 'enter to andalin',
    loading: 'please wait',
    close: 'close',
    username: {
      label: 'email address',
      placeholder: 'enter your email address'
    },
    password: {
      label: 'password',
      placeholder: '*****'
    },
    login: 'sign in',
    or: 'or',
    signup: 'create new account',
    forgot_password: 'forgot your password?',
    validation: {
      required: 'Required'
    }
  },
  sidebar: {
    menu: {
      home : 'home',
      new_search: 'new search',
      quotes: 'quotes',
      shipments: 'shipments',
      messages: 'messages',
      settings: 'settings'
    }
  },
  header: {
    search: {
      description: 'type anything to ',
      search: 'search'
    },
    userinfo: {
      hello: 'Hello, %{fullname}',
      user_options: {
        logged_in: 'settings',
        logged_out: 'login',
        settings: 'settings',
        logout: 'logout',
        help: 'help',
        signup: 'sign up',
        login: 'login'
      }
    }
  },
  search: {
    tabs: {
      boxes: 'boxes/crates/pallets',
      containers: 'containers'
    },
    form: {
      lcl: {
        title: 'low container load',
        type: 'lcl type',
        qty: 'qty',
        length_: 'length',
        width: 'width',
        height: 'height',
        size: 'size',
        weight: 'weight',
        unit: 'unit',
        cm: 'cm', in: 'in',
        cbm: 'cbm',
        cft: 'cft',
        kg: 'kg',
        lbs: 'lbs',
        typeOptions: {
          boxes: 'boxes/crates',
          pallets: 'pallets'
        },
        add_load: 'add more boxes/crates/pallets',
        total_shipment: {
          title: 'total shipment : ',
          qty: '%{smart_count} unit |||| %{smart_count} units',
          volume: '%{smart_count} cbm',
          weight: '%{smart_count} kg |||| %{smart_count} kgs'
        }
      },
      fcl: {
        title: 'full container load',
        qty: 'qty',
        size: 'container size',
        type: 'type',
        weight: 'weight',
        sizeOptions: {
          ft20: '20FT Container',
          ft40: '40FT Container',
          hc40: '40FT Highcube'
        },
        typeOptions: {
          dry: 'dry',
          reefer: 'reefer',
          ventilate: 'ventilate'
        },
        weightOptions: {
          standard: 'standard',
          overweight: 'overweight'
        },
        add_load: 'add more containers',
        total_shipment: 'total shipment : %{smart_count} unit |||| total shipment : %{smart_count} units'
      },
      areaOptions: {
        factory: {
          label: 'Factory/ Warehouse',
          description: 'Industrial Setting'
        },
        business: {
          label: 'Business/Commercial Area',
          description: 'Commercial Address'
        },
        residential: {
          label: 'Residential Area',
          description: 'Home Address'
        },
        fob: {
          label: 'Port/Airport (FOB)',
          description: 'Pickup at Port'
        },
        include_trucking_custom: '*include trucking& custom clearance',
        exclude_trucking_custom: '*exclude trucking& custom clearance'
      },
      pickup: {
        title: 'pickup goods :',
        from: 'from',
        origin_country: {
          label: 'country of origin',
          placeholder: 'Select origin country',
          loading: 'loading origin countries...'
        },
        origin_city: {
          label: 'city of origin',
          placeholder: 'Select origin city',
          loading: 'loading cities...'
        },
        date: 'goods readiness date'
      },
      deliver: {
        title: 'deliver goods :',
        to: 'to',
        destination_country: {
          label: 'country of destination',
          placeholder: 'Select destination country',
          loading: 'loading destination countries...'
        },
        destination_city: {
          label: 'city of destination',
          placeholder: 'Select destination city',
          loading: 'loading cities...'
        }
      },
      optional_services: {
        title: 'optional services',
        dangerous_goods: 'dangerous goods',
        insurance: 'insurance',
        currency: 'currency',
        goods_value: {
          label: 'goods value',
          placeholder: 'enter your goods value'
        }
      },
      custom_clearance: {
        title: 'custom clearance services',
        custom_brokerage: 'custom brokerage',
        commodities: '# of commodities'
      },
      action: {
        search_button: 'search & book'
      }
    },
    validation: {
      required: 'required',
      number: 'must be a number'
    }
  },
  result: {
    loading: 'we currently plan the best offer for you... please wait',
    empty_result: 'Sorry, we do not have any match quote for this route/ freight types, Please try again with different routes/ freight type',
    action: {
      back_to_search: 'search for different routes/ freight type'
    },
    sortBy: {
      title: 'Sort Results By :',
      best_deal: 'Best Deal',
      fastest: 'Fastest',
      cheapest: 'Cheapest'
    },
    filter: {
      reset: 'reset filter',
      result_shown: '%{smart_count} result shown |||| %{smart_count} results shown',
      price: {
        title: 'price Range',
        currency: 'show price in : '
      },
      providers: {
        title: 'provider Names'
      },
      shipping_lines: {
        title: 'Carrier Names'
      },
      ratings: {
        title: 'provider Ratings'
      },
      modes: {
        title: 'freight Modes',
        air: 'air',
        ocean: 'ocean',
        expressdoc : 'express Document',
        expressparcel : 'express Parcel',
        express: 'express'
      }
    },
    form: {
      service: 'services : ',
      route: {
        title: 'shipment : ',
        origin: '%{origin_city}, %{origin_country}',
        destination: '%{destination_city}, %{destination_country}'
      },
      load: {
        title: 'load : ',
        lcl: {
          qty: '%{smart_count} unit |||| %{smart_count} units',
          volume: '%{smart_count} CBM',
          weight: '%{smart_count} KG |||| %{smart_count} KGS'
        },
        fcl: {
          qty: '%{smart_count} unit |||| %{smart_count} units',
          volume: {
            ft20: '%{smart_count} x 20\' %{type} %{weight}',
            ft40: '%{smart_count} x 40\' %{type} %{weight}',
            hc40: '%{smart_count} x 40\'HC %{type} %{weight}'
          },
          type: {
            dry: 'dry',
            reefer: 'reefer',
            ventilate: 'ventilate'
          },
          weight: {
            standard: 'standard',
            overweight: 'overweight'
          }
        }
      },
      date: {
        title: 'pickup date :'
      },
      optional_services: {
        title: 'optional service : ',
        insurance: 'insurance',
        custom: 'custom clearance',
        commodities: 'commodities'
      },
      action: {
        back_to_search: 'Edit Search'
      }
    },
    display: {
      header: {
        service_providers: 'service providers',
        services: 'services',
        price: 'price'
      },
      content: {
        reviews: 'reviews',
        delivery_estimate: 'estimate delivery days :',
        valid_until: 'valid until',
        days: 'days',
        price: '%{currency} %{smart_count}',
        shipping_line: 'shipping line',
        unknown: '-',
        action: {
          book: 'book',
          save: 'save',
          price_detail: 'show price detail'
        }
      },
      detail: {
        fee_empty: 'no trucking or local charges in %{place}',
        fee_code: 'code fee',
        fee_desc: 'description',
        fee_units: 'qty',
        fee_uom: 'units',
        fee_price: 'unit price',
        fee_pricesum: 'amount',
        subtotal: 'subtotal',
        minimum_charge: 'min',
        insurance: "insurance",
        summary: 'price summary',
        notes: 'notes',
        notes_description: '%{notes}',
        notes_empty: 'No Pricing Note',
        subamount: {
          trucking_origin: 'origin trucking charges',
          custom_origin: 'origin custom service charges',
          freight: 'freight charges',
          document: 'document charges',
          insurance: 'insurance charges',
          trucking_destination: 'destination trucking charges',
          custom_destination: 'destination custom service charges'
        },
        uom_options: {
          volume: 'm3',
          weight: 'kg',
          shipment: 'shipment',
          container: 'cnt',
          volumetric: 'volmtr'
        }
      }
    }
  },
  quote: {
    loading: 'loading...',
    header: {
      freight_mode: 'freight mode',
      shipment_load: 'loads',
      origin: 'origin',
      destination: 'destination',
      providers: 'providers',
      delivery_estimate: 'est. delivery days',
      price_total: 'total price',
      valid_until: 'valid until',
      creator: 'created by',
      action: ''
    },
    content: {
      empty: 'no quotes had been saved',
      delivery_estimate: {
        title: '%{min} - %{max} days'
      },
      price: {
        title: '%{currency} %{smart_count}'
      },
      freight_mode: {
        air: 'air',
        ocean: 'ocean',
        express: 'express'
      },
      action: {
        book: 'book',
        search: '',
        delete: '',
        load_more: 'load more...'
      }
    }
  },
  book: {
    title: 'your booking summary',
    loading_validation: 'we are currently validating this quote. Please Wait...',
    invalid_validation: 'this quote is not valid',
    loading_booking: 'we are currently validating your booking. Please Wait...',
    booking_successful: {
      title: 'congratulation!',
      description: 'Your booking is successful'
    },
    booking_contact: {
      title: 'if you need another support for this shipment, ',
      description: 'you may contact to our customer supports',
      email: {
        bobby: 'bobby.pratama@andalin.com'
      },
      whatsapp: {
        bobby: '(62) 856 9201 4841'
      },
      action: {
        go_to_shipment: 'or go to your shipment list to manage your shipment bookings'
      }
    },
    providers: 'providers',
    valid_until: 'valid until',
    services: 'service',
    currency: 'place order in ',
    load: {
      title: 'loads',
      lcl: {
        qty: '%{smart_count} unit |||| %{smart_count} units',
        volume: '%{smart_count} cbm',
        weight: '%{smart_count} kg |||| %{smart_count} kgs'
      },
      fcl: {
        qty: '%{smart_count} unit |||| %{smart_count} units',
        volume: {
          ft20: '%{smart_count} x 20\' %{type} %{weight}',
          ft40: '%{smart_count} x 40\' %{type} %{weight}',
          hc40: '%{smart_count} x 40\'HC %{type} %{weight}'
        },
        type: {
          dry: 'dry',
          reefer: 'reefer'
        },
        weight: {
          standard: 'standard',
          overweight: 'overweight'
        }
      }
    },
    optional_services: {
      title: 'optional services',
      yes: 'yes',
      no: 'none',
      insurance: {
        title: 'insurance',
        goods_value: {
          title: 'goods value',
          value: '%{smart_count} %{currency}'
        }
      },
      custom: {
        title: 'custom clearance'
      }
    },
    action: {
      show_price_detail: 'show price detail'
    },
    total_shipment: 'total shipment',
    date: 'pickup date',
    delivery_estimate: 'est. delivery days',
    days: 'days',
    payment: {
      title: 'total cost',
      total: '%{currency} %{smart_count}',
      information: {
        title: 'order information',
        description: 'The price quoted is based on the item description you provided. If the actual dimensions differ when picked up, you will be charged accordingly.'
      },
      action: {
        pay: 'place order'
      }
    },
    shipment_detail: {
      title: 'Enter your shipment detail',
      commodities: {
        title: 'Commodity details',
        hscode: {
          label: 'product HS code',
          placeholder: 'product HS Code'
        },
        product_desc: {
          label: 'product description',
          placeholder: 'enter your product description'
        },
        product_class: {
          label: 'product class',
          placeholder: 'enter product class',
          options: {
            GENCO: 'GENERAL CARGO',
            DG: 'DANGEROUS GOODS'
          }
        },
        product_type: {
          label: 'product type',
          placeholder: 'enter product type',
          options: {
            CLOTHES: 'clothes',
            FURNITURE: 'furniture',
            FOODBEVERAGES: 'food& beverages',
            OTHERS: 'others'
          }
        }
      },
      shipper: {
        title: 'shipper details',
        company: {
          label: 'company shipper name',
          placeholder: 'insert company shipper name'
        },
        name: {
          label: 'shipper PIC name',
          placeholder: "insert shipper PIC name"
        },
        address: {
          label: 'shipper address',
          placeholder: "insert shipper address"
        },
        phone: {
          label: 'shipper phone',
          placeholder: "insert shipper phone"
        },
        fax: {
          label: 'shipper fax',
          placeholder: "insert shipper fax number"
        },
        email: {
          label: 'shipper email',
          placeholder: 'insert shipper email address'
        },
        note: {
          optional: 'optional',
          label: 'shipper note',
          placeholder: 'leave notes for our provider'
        }
      },
      consignee: {
        title: 'consignee details',
        company: {
          label: 'company consignee name',
          placeholder: 'insert company consignee name'
        },
        name: {
          label: 'consignee PIC name',
          placeholder: "insert consignee PIC name"
        },
        address: {
          label: 'consignee address',
          placeholder: "insert consignee address"
        },
        phone: {
          label: 'consignee phone',
          placeholder: "insert consignee phone number"
        },
        fax: {
          label: 'consignee fax',
          placeholder: "insert consignee fax number"
        },
        email: {
          label: 'consignee email',
          placeholder: 'insert consignee email address'
        }
      }
    },
    additional_request: {
      title: 'shipper additional requests',
      request: 'request',
      optional: 'optional',
      action: {
        add: 'request'
      },
      type: {
        title: 'request for',
        value: 'value',
        note: 'note',
        options: {
          free_time: {
            title: 'maximum free time',
            unit: 'days',
            placeholder: 'days',
            description: 'max. free time will be confirmed after booking'
          },
          min_temperature: {
            title: 'minimum temperature',
            unit: '℃elcius',
            placeholder: 'degree',
            description: 'applicable for reefer only'
          },
          max_temperature: {
            title: 'maximum temperature',
            unit: '℃elcius',
            placeholder: 'degree',
            description: 'applicable for reefer only'
          },
          shipping_line : {
            title: 'carrier/shipping line',
            unit: 'name',
            placeholder: 'preference',
            description: 'carrier/ liners preference'
          },
          vessel : {
            title: 'vessel',
            unit: 'name',
            placeholder: 'preference',
            description: 'vessel type/ specification preference'
          }
        }
      }
    },
    terms_of_booking: {
      title: 'terms of booking',
      description: 'Items’ description on your order must exactly be matching with the physical items and your documents e.g. commercial invoice, packing list, etc. If the actual weight is more than what you’ve declared on your order, Andalin will charge you for the difference, and vice versa. Andalin do NOT accept illegal items e.g. illegal drugs, narcotics, weapons, etc. You accept all legal consequences for whatever items you are to export/import and hold Andalin harmless against any legal claim whatsoever. The price may change from time to time due to wrong HS-Code, random check by Custom Officer, incomplete licenses or any other reasons that may only arise at a later day.'
    },
    cancellation_policy: {
      title: 'cancellation policy',
      description: ' An order cannot be cancelled by a User when it is confirmed for processing by Freight Forwarder. If for some reasons, such order is cancelled by a User after getting a confirmation from Freight Forwarder for further process, a 25% penalty of freight cost may apply. A compensation will then be paid to such Freight Forwarder based on this penalty. A penalty will not apply if it’s caused by a Force Majeur.'
    },
    validation: {
      email_invalid: 'email is not valid',
      required: 'Required'
    }
  },
  settings: {
    tabs: {
      user: 'my account',
      company: 'company settings',
      company_documents: 'company documents',
      company_users: 'company users'
    },
    docs: {
      type: {
        title: 'document type',
        label: 'document type',
        placeholder: 'What document is this?',
        options: {
          ktp: 'ktp',
          npwp: 'npwp',
          siup: 'siup',
          tdp: 'tdp',
          passport: 'passport',
          license: 'license',
          nikexport: 'nik export',
          nikimport: 'nik import'
        }
      },
      uploaded: {
        title: 'uploaded?',
        options: {
          exists: 'Yes',
          not_exists: 'No'
        }
      },
      action: {
        new_upload: 'upload document',
        upload: 'upload',
        uploading: 'uploading',
        download: 'download'
      },
      form: {
        title: 'Upload New Document',
        description: 'By uploading new document you will remove the latest particular document',
        loading: 'we are currently storing your file into our private storage. Please Wait...',
        close: 'close',
        success: 'your %{fileType} has been uploaded',
        file_input: {
          label: 'upload file'
        },
        validation: {
          required: 'required'
        }
      }
    },
    user: {
      photo: 'photo',
      form: {
        loading: 'uploading...',
        file_input: {
          label: 'upload photo'
        }
      },
      action: {
        upload: 'upload avatar',
        uploading: 'uploading your avatar'
      }
    },
    users: {
      username: 'username',
      gender: 'gender',
      fullname: 'fullname',
      statverif: 'is active?',
      codeusertype: 'type',
      action: {
        'updating': 'updating...',
        'update_users': 'update',
        'invite': 'invite new users'
      },
      options: {
        'USRA': 'Company Admin',
        'USRS': 'Officer',
        'VDRA': 'Forwarder Admin',
        'VDRS': 'Officer',
        'SADM': 'system admin'
      }
    }
  },
  shipment: {
    loading: 'Fetch all shipments data. Please Wait..',
    form: {
      filtered_results : 'show only %{smart_count} record |||| show only %{smart_count} records ',
      total_results : ' of %{smart_count} total shipments |||| of %{smart_count} total shipments ',
      order_number: {
        label: 'search by order number',
        placeholder: 'search shipment by order number'
      },
      order_by_direction: {
        label: 'ASC/DESC',
        options: {
          asc: 'ASC',
          desc: 'DESC'
        }
      },
      order_by: {
        label: 'sort by',
        options: { //object key based on the json field
          created_at: 'Booking Date',
          shipment_date: 'shipment Date',
          carrier_name: 'carrier',
          vendor_name: 'vendor name',
          order_stat: 'order status'
        }
      }
    },
    tab : {
      shipment : 'Shipment Detail',
      price : 'Price Detail',
      carrier : 'Carrier Schedule',
      documents : 'Documents',
      messages : 'Messages',
      history : 'History'
    },
    detail: {
      title : 'Order Information',
      edit_order : 'Edit Order Status',
      order_number: 'Order Number',
      close : 'Close This Tab',
      order_stat: 'Order Status',
      message : {
        title : 'Messages Channel of #',
        label : 'Post Message to #',
        placeholder : 'Enter your message here',
        file_description : 'description',
        action : {
          post : 'post message',
          posting : 'posting...',
          download : 'download %{fileName}'
        },
        validation : {
          required : 'required'
        }
      },
      shipper: {
        title : 'Shipper&',
        edit : 'Edit Shipper',
        name: 'Shipper Name',
        company: 'Shipper Company',
        address: 'Shipper Address',
        phone: 'Shipper Phone',
        email: 'Shipper Email',
        fax: 'Shipper Fax',
        notes: 'Shipper Notes'
      },
      consignee: {
        title: 'Consignee Information',
        edit : 'Consignee',
        name: 'Consignee Name',
        company: 'Consignee Company',
        address: 'Consignee Address',
        phone: 'Consignee Phone',
        email: 'Consignee Email',
        fax: 'Consignee Fax'
      },
      commodities: {
        title : 'Commodities Information',
        edit_commodities : 'Edit Commodities',
        class: {
          title : 'commodities Class',
          options : {
            GENCO: 'General Cargo',
            DG: 'Dangerous Goods'
          }
        },
        type: 'Commodities Type',
        hscode: 'HS Code',
        product: 'Product Description'
      },
      custom: 'custom service',
      insurance: {
        title: 'insurance',
        goods_value: 'Goods Value',
        goods_value_currency: 'Currency',
        pcg: '% Insurance Premium',
        price_currency_rate: 'Currency Rate',
        price_sum: 'Total Insurance'
      },
      trucking: 'trucking',
      stat: {
        yes: 'Yes',
        no: 'No'
      },
      freight: {
        mode: 'Freight Mode',
        type: 'Freight Type'
      },
      vendor: {
        name: 'Vendor Name'
      },
      shipment_date: 'Readiness Date',
      book_price: {
        total: 'Total Price',
        currency_code: 'Currency',
        currency_rate: 'Currency Rate'
      },
      carrier: {
        title : 'Carrier Information',
        edit_carrier : 'Edit Carrier Information',
        name: 'Carrier Name',
        do_awb: 'BL/AWB',
        contract: 'Service Contract'
      },
      origin: {
        country: 'Country of Origin',
        city: 'City of Origin',
        title: 'Origin',
        pol: 'Port of Loading'
      },
      destination: {
        country: 'Country of Destination',
        city: 'City of Destination',
        title: 'Destination',
        pod: 'Port of Discharge'
      },
      transittime: 'Est. Delivery Days',
      requests: {
        title: 'Additional Requests',
        edit : 'Edit Requests',
        header: {
          type: 'Type',
          value: 'Value',
          stat: 'Approval',
          unit: 'Unit',
          note : 'Note',
          created_at: 'date',
          created_by: 'created by'
        },
        display: {
          stat: {
            U: 'unconfirmed',
            Y: 'approved',
            N: 'rejected'
          },
          type: {
            free_time: 'Free Time',
            min_temperature: 'Min Temperature',
            max_temperature: 'Max Temperature',
            shipping_line : 'Carrier/Shipping Line',
            vessel : 'Vessel'
          }
        }
      },
      form: {
        close : 'close',
        validation : {
          required : 'required',
          email_invalid : 'invalid email'
        },
        consignee_shipper : {
          title : 'Edit shipper& consignee profile',
          order_number : 'order number #',
          loading : 'loading...',
          success : 'Shipper and Consignee Information has been updated!',
          action : {
            edit : 'edit consignee& shipper'
          }
        },
        order_stat : {
          title : 'Edit Order Status',
          order_number : 'order number #',
          loading : 'loading',
          success : 'Order Status has been updated',
          label : 'order status',
          placeholder : 'order status',
          options : {
            book : 'book',
            confirm : 'confirmed',
            scheduled : 'scheduled',
            dispatch : 'dispatch',
            discharge : 'discharge',
            waiting_payment : 'waiting for payment',
            complete : 'complete',
            all : 'all'
          },
          vessel_name : {
            label : 'Vessel Name',
            placeholder : 'Enter Vessel Name'
          },
          date_arrival : {
            label : 'Arrival Date (%{cityCode},%{countryCode})',
            placeholder : 'Enter arrival date'
          },
          date_departure : {
            label : 'Departure Date (%{cityCode},%{countryCode})',
            placeholder : 'Enter departure date'
          },
          port_open_time : {
            label : 'Port Open Time',
            placeholder : 'Port Open Time'
          },
          port_cutoff_time : {
            label : 'Port Cutoff Time',
            placeholder : 'Port Cut Off Time'
          },
          inland_cutoff_time : {
            label : 'Inland Cutoff Time',
            placeholder : 'Inland CutOff Time'
          },
          pol : {
            label : 'Port of Loading',
            placeholder : 'Port of Loading'
          },
          pod : {
            label : 'Port of Discharge',
            placeholder : 'Port of Discharge'
          },
          origin_city : {
            label : 'Origin City',
            placeholder : 'Origin City',
            loading : 'loading'
          },
          origin_country : {
            label : 'Origin Country',
            placeholder : 'Origin Country',
            loading : 'loading'
          },
          destination_city : {
            label : 'Destination City',
            placeholder : 'Destination City',
            loading : 'loading'
          },
          destination_country : {
            label : 'Destination Country',
            placeholder : 'Destination Country',
            loading : 'loading'
          },
          due_date : {
            label : 'Invoice Due Date',
            placeholder : 'DD/MM/YY'
          },
          payment_method : {
            label : 'Payment Method',
            placeholder : 'enter payment method',
            options : {
              transfer : 'transfer',
              giro : 'GIRO transfer',
              wire_transfer : 'wire transfer'
            }
          },
          payment_to_bank : {
            label : 'Paid To',
            placeholder : 'paid to'
          },
          currency_from_code : {
            label : 'Invoice base currency code',
            placeholder : 'enter base currency from'
          },
          currency_rate : {
            label : 'currency rate',
            placeholder : 'enter currency rate'
          },
          ppn : {
            label : `% Tax`,
            placeholder : 'enter %ppn'
          },
          note : {
            label : 'invoice note',
            placeholder : 'enter invoice note'
          },
          shipper_bank_name : {
            label : 'Bank Name',
            placeholder : 'enter shipper bank name'
          },
          shipper_bank_account_name : {
            label : 'Bank Acc Name',
            placeholder : 'enter shipper bank account name'
          },
          shipper_bank_account_number : {
            label : 'Bank Acc Number',
            placeholder : 'enter shipper bank account number'
          },
          payment_reference_number : {
            label : 'Payment Reference Number',
            placeholder : 'Enter reference number'
          },
          payment_date : {
            label : 'Payment Date',
            placeholder : 'DD/MM/YY'
          },
          payment_total_paid : {
            label : 'Total Paid',
            placeholder : 'enter payment total'
          },
          payment_currency_code : {
            label : 'currency',
            placeholder : 'currency'
          },
          action : {
            edit : 'edit order status',
            add_payment : 'add payment record',
            add_schedule : 'add schedule'
          }
        },
        commodities : {
          title : 'Edit Commodities Status',
          order_number : 'order number #',
          loading : 'loading...',
          success : 'Commodities has been updated',
          action : {
            edit : 'edit commodities'
          }
        },
        order_number: {
          title: 'order number',
          placeholder: 'order number'
        },
        order_status: {
          options: {
            BOOK: 'book',
            CONFIRM: 'confirm',
            DISPATCH: 'dispatch',
            DISCHARGE: 'discharge',
            WAITING_PAYMENT : 'waiting for payment',
            COMPLETE: 'complete'
          }
        },
        carrier: {
          title : 'Edit Carrier',
          loading : 'loading...',
          order_number : 'order number #',
          success : 'Carrier has been updated',
          action : {
            edit : 'edit carrier'
          },
          name: {
            label: 'Carrier Name',
            placeholder: 'Carrier Name'
          },
          bl_awb: {
            label: 'BL/AWB',
            placeholder: 'Enter your BL/AWB'
          },
          contract: {
            label: 'Service Contract',
            placeholder: 'Enter your BL/AWB'
          }
        },
        freights : {
          title : 'Edit Freights',
          order_number : 'order number #',
          loading : 'loading...',
          success : 'Freights have been updated',
          action : {
            edit : 'edit freights'
          },
          qty : 'qty',
          volume : 'volume',
          weight : 'weight',
          tracking_number : 'tracking number',
          service_type : 'type'
        },
        requests : {
          title : 'Edit Requests',
          order_number : 'order number #',
          loading : 'loading...',
          success : 'Requests have been updated',
          action : {
            edit : 'edit requests',
            add : 'add request'
          },
          value : 'value',
          note : 'note',
          type : 'type',
          approved : {
            label : 'approved?',
            options : {
              U : 'unconfirmed',
              Y : 'yes',
              N : 'rejected'
            }
          }
        },
        prices : {
          title : 'Edit Price',
          loading : 'loading',
          order_number : 'order number #',
          success : 'Price components has been added',
          action : {
            add : 'add price component',
            edit : 'edit prices'
          },
          component_type : {
            label : 'price type',
            placeholder : 'price type',
            options : {
              freight : 'freight',
              custom : 'custom',
              document : 'document',
              trucking : 'trucking',
              insurance : 'insurance'
            }
          },
          description : {
            label : 'description',
            placeholder : 'description'
          },
          price_uom :{
            label : 'UOM',
            placeholder : 'uom',
            options : {
              w : 'weight',
              v : 'volume',
              vw : 'volumetric',
              container : 'cont',
              all : 'shipment'
            }
          },
          qty : {
            label : 'qty',
            placeholder : 'qty'
          },
          price_sum : {
            label : 'total price in %{currency}',
            placeholder : 'total price'
          }
        },
        shipper: {
          title: 'shipper details',
          company: {
            label: 'company shipper name',
            placeholder: 'insert company shipper name'
          },
          name: {
            label: 'shipper PIC name',
            placeholder: "insert shipper PIC name"
          },
          address: {
            label: 'shipper address',
            placeholder: "insert shipper address"
          },
          phone: {
            label: 'shipper phone',
            placeholder: "insert shipper phone"
          },
          fax: {
            label: 'shipper fax',
            placeholder: "insert shipper fax number"
          },
          email: {
            label: 'shipper email',
            placeholder: 'insert shipper email address'
          },
          note: {
            optional: 'optional',
            label: 'shipper note',
            placeholder: 'leave notes for our provider'
          }
        },
        consignee: {
          title: 'consignee details',
          company: {
            label: 'company consignee name',
            placeholder: 'insert company consignee name'
          },
          name: {
            label: 'consignee PIC name',
            placeholder: "insert consignee PIC name"
          },
          address: {
            label: 'consignee address',
            placeholder: "insert consignee address"
          },
          phone: {
            label: 'consignee phone',
            placeholder: "insert consignee phone number"
          },
          fax: {
            label: 'consignee fax',
            placeholder: "insert consignee fax number"
          },
          email: {
            label: 'consignee email',
            placeholder: 'insert consignee email address'
          }
        }
      },
      document : {
        title : 'Uploaded Document',
        edit : 'Upload New Document',
        header : {
          created_at : 'date',
          file_category : 'category',
          file_description : 'description',
          file_origin : 'uploaded from',
          created_by_username : 'uploaded by'
        },
        form : {
          title  : 'Upload New Document',
          order_number : 'order number #',
          loading : 'loading...',
          close : 'close',
          success : '%{fileCategory} has been uploaded',
          category : {
            label : 'file category',
            placeholder : 'what file is this?',
            options : {
              bl : 'BL - BILL OF LADING',
              pl : 'PL - PACKING LIST',
              bl_draft : 'BL DRAFT - BILL OF LADING DRAFT',
              photo : 'GOODS PHOTO',
              awb : 'AWB - AIRWAY BILL',
              api : 'API - IMPORT LICENSE (API)',
              certificate : 'CERTIFICATE',
              others : 'OTHERS'
            }
          },
          description : {
            label : 'description',
            placeholder : 'enter description'
          },
          file_input : {
            label : 'upload document'
          },
          action : {
            uploading : 'uploading...',
            upload : 'upload new document'
          },
          validation : {
            required : 'required'
          }
        }
      },
      history : {
        title : 'Shipment History',
        header : {
          created_at : 'date',
          description : 'description',
          created_by_comptp : 'created by',
          created_by_username : 'username'
        }
      },
      price: {
        title: 'Price Detail',
        edit : 'Edit Price Detail',
        download_invoice : 'Download Invoice',
        header: {
          code_fee: 'code fee',
          service_type: 'type',
          qty: 'Qty',
          price_description : 'description',
          weight : 'weight',
          volume : 'volume',
          price_uom: 'unit',
          price_sum: 'total price',
          insurance : {
            title : 'Insurance Detail',
            goods_value : 'goods value',
            currency_rate : 'currency rate',
            pcg : '%premium',
            price_sum : 'total premium',
            commodities_type : 'product type',
            commodities_product : 'product description'
          }
        },
        display: {
          uom_options: {
            volume: 'm3',
            weight: 'kg',
            shipment: 'shipment',
            container: 'container',
            volumetric: 'volmtr'
          }
        }
      },
      schedules : {
        title : 'Carrier Schedule',
        port_open_time : 'open time(P)',
        port_cutoff_time : 'cutoff time(P)',
        inland_cutoff_time : 'cutoff time(I)',
        vessel_name : 'vessel',
        origin : 'origin/departure date',
        destination : 'destination/arrival date',
        scheduled : 'Active?',
        created_at : 'created at'
      },
      freights: {
        title: 'freights Summary',
        header: {
          index: '#',
          type: 'Type',
          tracking_number: 'Tracking Number',
          qty: 'Quantity',
          weight: 'Weight',
          volume: 'Volume'
        },
        display: {
          service_type: {
            SEAFCL20FT: '20\' DRY',
            SEAFCL40FT: '40\' DRY',
            SEAFCL40FTHC: '40\'HC DRY',
            SEAFCL20FTREF: '20\' REEFER',
            SEAFCL40FTREF: '40\' REEFER',
            SEAFCL20FTVENT: '20\' VENTILATE',
            SEAFCL40FTVENT: '40\' VENTILATE',
            SEALCL: 'OCEAN LCL',
            AIRCARGO: 'AIR CARGO',
            AIRCOURIER: 'AIR COURIER'
          },
          additional_info: {
            standard: 'standard',
            box: 'box',
            pallets: 'pallets',
            overweight: 'overweight'
          },
          unit: '%{smart_count} unit |||| %{smart_count} units'
        }
      }
    }
  }
}
