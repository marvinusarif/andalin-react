export const _idnLang = {
  message : {
    subscribers : {
      title : '%{smart_count} pengguna bergabung di kanal ini',
      empty : 'Kanal Kosong',
      description : 'Ikuti kanal ini untuk berbicara dengan %{talkTo}'
    },
    action : {
      subscribe : 'Ikuti Kanal'
    }
  },
  notifications : {
    user : {
      update : {
        title : 'Sukses',
        message : 'Profil pengguna sudah diperbaharui'
      }
    },
    company : {
      users : {
        update : {
          title : 'Sukses',
          message : 'Status pengguna sudah diperbaharui'
        }
      },
      profile : {
        update : {
          title : 'Sukses',
          message : 'Profil perusahaan sudah diperbaharui'
        }
      }
    },
    quote : {
      receive : {
        title : 'Sukses',
        message : 'Penawaran telah tersimpan'
      }
    }
  },
  error : {
    title : 'Terjadi Kesalahan!',
    message : {
      not_found : 'Halaman tidak ditemukan',
      unauthorized : 'Belum diotorisasi',
      invalid_login : 'Username atau Kata Sandi salah',
      account_unverified : 'Akun anda belum diverifikasi',
      suspended : 'Akun anda sedang ditangguhkan oleh Andalin',
      server_error : 'Internal Server Bermasalah',
      bad_request : 'Bad Request',
      no_content : 'Tidak ada Konten',
      verified_twice : 'Akun anda telah diverifikasi',
      request_too_large : 'Ukuran file Maksimum adalah 3MB',
      bad_file_type : 'Jenis file tidak sesuai',
      connection_lost : 'Koneksi internet terputus',
      payment_is_lesser_than_total_invoice : 'Pembayaran kurang dari Total Penagihan'
    }
  },
  verify : {
    title : 'Akun Verifikasi Sukses',
    loading : 'Kita sedang mengaktivasi akun anda. Tolong tunggu...',
    description : 'Akun anda telah sukses dikonfirmasi, silahkan lakukan Login',
    login : 'Login'
  },
  invitation :{
    title : 'Undangan untuk berkolaborasi',
    loading : 'Silahkan tunggu sebentar...',
    success : 'Anda sudah memperbaharui profil dan password anda. Silahkan masuk ke dalam akun anda.',
    description : 'Silahkan isi formulir dibawah untuk mulai kolaborasi dengan tim anda.',
    action : {
      submit : 'Perbaharui Password dan Profil Saya',
      login : 'Masuk'
    }
  },
  invite : {
    title : 'Undang Pengguna Baru',
    loading : 'Silahkan Tunggu...',
    close : 'Tutup',
    description : 'masukkan alamat email teman anda untuk mulai kolaborasi',
    success : 'email telah terkirim ke %{email}',
    email : {
      label : 'alamat email',
      placeholder : 'masukkan alamat email'
    },
    action : {
      invite : 'undang',
      inviting : 'email sedang dikirim...'
    },
    validation : {
      required : 'diperlukan',
      email_invalid : 'alamat email tidak valid'
    }
  },
  forgotPassword : {
    title : 'Reset Password',
    loading : 'silahkan tunggu...',
    success : 'Kami telah mengirimkan email ke akun anda. Mohon Cek Email Anda.',
    description : 'Masukkan email yang sudah teregistrasi untuk mereset password anda',
    action : {
      reset_password : 'kirim password ke email saya',
      resetting : 'sedang mengirim...'
    }
  },
  resetPassword : {
    title : 'Password Reset',
    description : 'reset password anda dengan mengisi formulir di bawah ini',
    success : 'Anda sudah berhasil merubah password anda. Silahkan masuk ke akun anda.',
    password_new : {
      label : 'password baru',
      placeholder : 'masukkan password baru anda'
    },
    password_new_confirm : {
      label : 'konfirmasi password baru',
      placeholder : 'masukkan konfirmasi password baru'
    },
    action : {
      change_password : 'rubah password',
      updating : 'sedang merubah...',
      login : 'masuk'
    },
    validation : {
      required : 'diperlukan',
      password_confirm_invalid : 'password tidak sama'
    }
  },
  logout : {
    title : 'Pengakhiran Sesi',
    description : 'Anda yakin ingin keluar?',
    action : {
      cancel : 'batalkan',
      logout : 'keluar'
    }
  },
  contact : {
    title : 'Ada Kendala ? Kita siap untuk menolong.',
    loading : 'Tunggu Sebentar...',
    close : 'tutup',
    fullname : {
      label : 'Nama Lengkap',
      placeholder : 'Masukkan Nama Anda'
    },
    email : {
      label : 'Email',
      placeholder : 'Masukkan alamat email anda'
    },
    phone : {
      label : 'Telepon',
      placeholder : 'Masukkan Telepon anda'
    },
    subject : {
      label : 'subject',
      placeholder : 'select subject',
      options : {
        register : 'tidak dapat meregistrasi pengguna',
        account : 'tidak dapat masuk ke akun saya',
        password : 'perubahan password tidak berhasil',
        invite : 'tidak dapat mengundang pengguna lain',
        no_quote : 'tidak mendapat penawaran dari rute yang dicari',
        other : 'lainnya',
      }
    },
    subject_description : {
      label : 'deskripsi subyek',
      placeholder : 'masukkan deskripsi subyek'
    },
    message : {
      label : 'pesan',
      placeholder : 'tinggalkan pesan anda'
    },
    validation : {
      required : 'diperlukan',
      email_invalid : 'email tidak valid'
    },
    feedback : {
      title : 'terima kasih sudah menghubungi kami',
      on_success : 'kami akan menghubungi anda segera.'
    },
    get_help : 'Butuh Bantuan'
  },
  register : {
    title : 'Pendaftaran Baru',
    loading : 'Pendaftara anda sedang diproses. Mohon tunggu sebentar...',
    close : 'tutup',
    tabs : {
      user_info : 'Pengguna',
      company_info : 'Perusahaan',
      logistic_info : 'Logistik'
    },
    fullname : {
      label : 'nama lengkap',
      placeholder : 'masukkan nama lengkap anda'
    },
    password : {
      label : 'password',
      placeholder : 'masukkan password anda'
    },
    password_change : {
      label : 'password',
      placeholder : 'masukkan password untuk merubah password anda'
    },
    password_confirm : {
      label : 'konfirmasi password',
      placeholder : 'masukkan konfirmasi password anda'
    },
    photo : {
      label : 'Unggah Foto Anda',
      placeholder : 'unggah Foto Anda'
    },
    sex : {
      label : 'J/K',
      options : {
        male : 'Pria',
        female : 'Wanita'
      }
    },
    phone : {
      label : 'nomor telepon',
      placeholder : 'masukkan nomor telepon anda'
    },
    job : {
      label : 'Pekerjaan',
      placeholder : 'masukkan pekerjaan anda'
    },
    email : {
      label : 'email',
      placeholder : 'masukkan email anda'
    },
    knoweximku : {
      label : 'Darimana anda tahu tentang Andalin?',
      options : {
        facebook : 'facebook',
        google : 'google',
        telemarketing : 'telemarketing',
        other : 'lainnya'
      }
    },
    company_name : {
      label : 'nama perusahaan',
      placeholder : 'masukkan nama perusahaan anda'
    },
    company_address : {
      label : 'alamat perusahaan',
      placeholder : 'masukkan alamat perusahaan anda'
    },
    company_fax : {
      label : 'Nomor Fax Perusahaan',
      placeholder : 'masukkan nomor fax perusahaan anda'
    },
    company_phone : {
      label : 'Nomor Telepon Perusahaan',
      placeholder : 'masukkan nomor telp perusahaan anda'
    },
    company_email : {
      label : 'Email Perusahaan',
      placeholder : 'masukkan email perusahaan anda'
    },
    company_description : {
      label : 'Deskripsi Perusahaan',
      placeholder : 'masukkan deskripsi perusahaan anda'
    },
    company_country :{
      label : 'Negara',
      placeholder : 'Pilih Negara'
    },
    company_industry : {
      label : 'Bergerak di sektor apakah perusahaan anda?',
      placeholder : 'Pilih sektor',
      options : {
        aerospace : 'Dirgantara',
        agribusiness : 'Bisnis Pertanian',
        agricultural_services : 'Layanan Pertanian',
        auto_dealers : 'Dealer Mobil',
        automotive : 'Otomotif',
        banks_commercial : 'Bank Komersial',
        accounting : 'Akunting',
        airlines : 'Maskapai Penerbangan',
        animation : 'Animasi',
        arts : 'Seni',
        banking : 'Perbankan',
        biotechnology : 'Bioteknologi',
        broadcast_media : 'Media Broadcast',
        aviation : 'Penerbangan',
        medicine : 'Farmasi& Obat- obatan',
        fashion : 'Konveksi& tekstil',
        architecture : 'arsitek',

        building_materials : 'Material Bangunan',
        business_supplies : 'Supply Bisnis',
        equipment : 'Peralatan',
        capital_market : 'Pasar Modal',
        private_equity : 'Ekuitas Swasta',
        broadcast : 'Penyiaran',
        chemicals : 'Bahan Kimia',
        social_organization : 'Organisasi Sosial',
        civil_engineering : 'Teknik Sipil',
        real_estate : 'Perumahan',
        developer_games : 'Pengembang Game',
        computer_hardware : 'Perangkat Keras Komputer',
        computer_networking : 'Jaringan Komputer',
        developer_software : 'Pengembang Perangkat Lunak',
        construction : 'Konstruksi',
        consumer_electronics : 'Elektronik',
        consumer_goods : 'Makanan',
        consumer_services : 'Layanan Konsumen',
        cosmetics : 'Kosmetik',
        dairy : 'Susu',
        defense : 'Pertahanan',
        space : 'Ruang Angkasa',
        design : 'Desain',
        e_learning : 'Edukasi',
        education_management : 'Manajemen Edukasi',
        electronic_manufacturing : 'Manufaktur Elektronik',
        entertainment : 'Hiburan',
        movie_production : 'Produksi Film',
        environmental_services : 'Layanan Lingkungan',
        events_services : 'Layanan Acara',
        executive_office : 'Kantor Executive',
        facilities_services : 'Fasilitas Layanan',
        farming : 'Pertanian',
        financial : 'Keuangan',
        fishery : 'Perikanan',
        food_production : 'Produksi Makanan',
        beverages : 'Minuman',
        government_administration : 'Administrasi Pemerintah',
        fundraising : 'Penggalangan Dana',
        furniture : 'Mebel',
        government_relations : 'Hubungan Pemerintah',
        graphic_design : 'Desain Grafis',
        web_design : 'Desain Web',
        fitness : 'Kebugaran',
        hospital : 'Rumah Sakit',
        human_resource : 'Pengembangan SDM',
        information_technology : 'IT',
        supermarkets : 'supermarkets',
        insurance : 'Asuransi',
        investment_banking : 'Perbankan dan Investasi',
        law_firms : 'Konsultan Hukum',
        legal_services : 'Layanan Hukum',
        logistics : 'Logistik',
        jewelry : 'Perhiasan',
        maritime : 'Maritim',
        advertising : 'Periklanan',
        mediaproduction : 'Media Produksi',
        mining : 'Tambang',
        institutions : 'Institusi',
        nanotechnology : 'Teknologi Nano',
        oil : 'Minyak',
        pharmaceuticals : 'Industri Farmasi',
        plastics : 'Plastik',
        political_organization : 'Organisasi Politik',
        professional_training : 'Pelatihan Profesional',
        public_relations : 'Relasi Publik',
        public_safety : 'Jasa Keamanan Publik',
        religious_institutions : 'Institusi Agama',
        renewables : 'Energi Terbarukan',
        research_industry : 'Industri Riset',
        restaurants : 'Restoran dan Catering',
        telecommunications : 'Telekomunikasi',
        tobacco : 'Rokok',
        warehousing : 'Pergudangan',
        wholesale : 'Perdagangan Grosir',
        wireless : 'Nirkabel',
        other : 'Lainnya'
      }
    },
    route_origin_country : {
      label : 'Negara asal pengiriman?'
    },
    route_destination_country : {
      label : 'Negara tujuan pengiriman?'
    },
    freight : {
      title : 'Pengiriman Anda?',
      options : {
        ocean : 'ocean freight',
        air : 'air freight',
        land : 'land freight'
      }
    },
    annual_shipmentLCL : {
      label : 'Pengiriman tahunan anda?'
    },
    annual_shipmentFCL : {
      label : 'Pengiriman tahunan anda?'
    },
    product : {
      label : 'Deskripsi Produk yang biasa anda kirimkan',
      placeholder : 'Masukkan deskripsi produk'
    },
    status : {
      failed : 'Registrasi anda gagal.',
      success : 'Terima kasih telah mendaftar di sistem kami. Kami baru saja mengirimkan email ke %{email}. Mohon cek kotak masuk email anda.',
      attempt : 'Anda dapat mengirimkan %{smart_count} email ke kotak masuk email anda',
      no_attempt : 'Bermasalah dalam menerima email? hubungi support kami'
    },
    action : {
      next : 'lanjut',
      before : 'kembali',
      register : 'daftar',
      seconds : '%{smart_count} detik |||| %{smart_count} detik',
      resend_email : 'kirim email dalam waktu %{seconds}',
      send_now : 'kirim email sekarang',
      register_again : 'coba kembali',
      need_help : 'klik disini untuk bantuan',
      update_button : 'Perbaharui',
      reset : 'reset',
      updating : 'sedang memperbaharui...'
    },
    validation : {
      email_invalid : 'email tidak valid',
      required : 'Diperlukan',
      number : 'harus angka',
      password_confirm_invalid : 'password tidak sama'
    }
  },
  main : {
    total_shipment : {
      bookings : 'Dipesan',
      progress : 'Dalam proses',
      payment : 'Menunggu Pembayaran',
      complete : 'Selesai',
    },
    shipment : '%{smart_count} pengiriman |||| %{smart_count} pengiriman'
  },
  login : {
    title : 'Masuk ke Andalin',
    loading : 'silahkan tunggu',
    close : 'tutup',
    username : {
      label : 'alamat email',
      placeholder : 'masukkan email anda'
    },
    password : {
      label : 'password',
      placeholder : '*****'
    },
    login : 'masuk',
    or : 'atau',
    signup : 'daftar akun baru',
    forgot_password : 'lupa password?',
    validation : {
      required : 'diperlukan'
    }
  },
  sidebar: {
    menu: {
      home : 'beranda',
      new_search: 'pencarian baru',
      quotes: 'penawaran',
      shipments: 'pengiriman',
      messages : 'pesan',
      settings: 'pengaturan'
    }
  },
  header: {
    search: {
      description: 'ketik apapun untuk mencari',
      search: 'cari'
    },
    userinfo : {
      hello : 'Halo, %{fullname}',
      user_options : {
        logged_in : 'pengaturan',
        logged_out : 'masuk',
        settings : 'pengaturan',
        logout : 'keluar',
        help : 'bantuan',
        signup : 'daftar',
        login : 'masuk'
      }
    }
  },
  search: {
    tabs: {
      boxes: 'kotak/peti kayu/palet',
      containers: 'kontainer'
    },
    form: {
      lcl: {
        title: 'low container load',
        type: 'tipe',
        qty: 'jumlah',
        length_: 'panjang',
        width: 'lebar',
        height: 'tinggi',
        size: 'ukuran',
        weight: 'berat',
        unit: 'unit',
        cm: 'cm', in: 'in',
        cbm: 'cbm',
        cft: 'cft',
        kg: 'kg',
        lbs: 'lbs',
        typeOptions: {
          boxes: 'kotak/peti kayu',
          pallets: 'palet'
        },
        add_load: 'tambahkan kotak/peti kayu/palet',
        total_shipment: {
          title: 'total pengiriman : ',
          qty: '%{smart_count} unit |||| %{smart_count} unit',
          volume: '%{smart_count} cbm',
          weight: '%{smart_count} kg |||| %{smart_count} kg'
        }
      },
      fcl: {
        title: 'full container load',
        qty: 'jumlah',
        size: 'ukuran kontainer',
        type: 'tipe',
        weight: 'berat',
        sizeOptions: {
          ft20: '20FT Kontainer',
          ft40: '40FT Kontainer',
          hc40: '40FT Highcube'
        },
        typeOptions: {
          dry: 'dry',
          reefer: 'reefer',
          ventilate : 'ventilate'
        },
        weightOptions: {
          standard: 'standar',
          overweight: 'muatan lebih'
        },
        add_load: 'tambahkan kontainer',
        total_shipment: 'total pengiriman : %{smart_count} unit |||| total pengiriman : %{smart_count} unit'
      },
      areaOptions: {
        factory: {
          label: 'Industri/ Pergudangan',
          description: 'Pengantaran ke Daerah Industri/ Pergudangan'
        },
        business: {
          label: 'Perkantoran',
          description: 'Pengantaran ke Daerah Perkantoran'
        },
        residential: {
          label: 'Perumahan',
          description: 'Pengantaran ke Daerah Perumahan'
        },
        fob: {
          label: 'Pelabuhan/ Bandara (FOB)',
          description: 'Pengantaran ke Pelabuhan/ Bandara'
        },
        include_trucking_custom : '*menggunakan jasa truk& kustom',
        exclude_trucking_custom : '*tidak menggunakan jasa truk& kustom'
      },
      pickup: {
        title: 'Ambil Barang',
        from: 'dari',
        origin_country: {
          label : 'negara asal',
          placeholder : 'pilih negara asal',
          loading : 'mencari daftar negara asal...'
        },
        origin_city: {
          label : 'kota asal',
          placeholder : 'pilih kota asal',
          loading : 'mencari daftar kota asal...'
        },
        date: 'tanggal kesiapan barang'
      },
      deliver: {
        title: 'Kirim Barang',
        to: 'ke',
        destination_country: {
          label : 'negara tujuan',
          placeholder : 'pilih negara tujuan',
          loading : 'mencari daftar negara tujuan...'
        },
        destination_city: {
          label : 'kota tujuan',
          placeholder : 'pilih kota tujuan',
          loading : 'mencari daftar kota tujuan...'
        }
      },
      optional_services: {
        title: 'Layanan Pilihan',
        dangerous_goods: 'barang berbahaya',
        insurance: 'asuransi',
        currency: 'mata uang',
        goods_value: {
          label : 'nilai barang',
          placeholder : 'masukkan nilai barang anda sesuai mata uang'
        }
      },
      custom_clearance: {
        title: 'Layanan kustom',
        custom_brokerage: 'Pengurusan Kustom',
        commodities: '# komoditas'
      },
      action: {
        search_button: 'cari & pesan'
      }
    },
    validation : {
      required : 'diperlukan',
      number : 'harus angka'
    }
  },
  result: {
    loading: 'kami sedang mencarikan penawaran terbaik untuk anda. Silahkan tunggu sebentar',
    empty_result : 'Mohon Maaf, kami belum memiliki penawaran yang cocok untuk rute dan tipe pengiriman ini. Mohon coba kembali',
    action : {
      back_to_search : 'cari untuk rute/ tipe pengiriman lainnya'
    },
    sortBy: {
      title: 'Urutkan dari yang',
      best_deal: 'Terbaik',
      fastest: 'Tercepat',
      cheapest: 'Termurah'
    },
    filter: {
      reset: 'Reset Penyaringan Hasil',
      result_shown: '%{smart_count} hasil ditampilkan |||| %{smart_count} hasil ditampilkan',
      price: {
        title: 'Harga',
        currency : 'tampilkan harga dalam kurs : '
      },
      providers: {
        title: 'Penyedia Jasa'
      },
      shipping_lines : {
        title : 'Pelayaran/ Penerbangan'
      },
      ratings: {
        title: 'Rating Penyedia Jasa'
      },
      modes: {
        title: 'Jalur Pengiriman',
        air: 'udara',
        ocean: 'laut',
        expressdoc : 'ekspres Dokumen',
        expressparcel : 'ekspres Parcel',
        express: 'ekspres'
      }
    },
    form: {
      service: 'jasa : ',
      route: {
        title: 'pengiriman : ',
        origin: '%{origin_city}, %{origin_country}',
        destination: '%{destination_city}, %{destination_country}'
      },
      load: {
        title: 'jumlah pengiriman : ',
        lcl: {
          qty: '%{smart_count} unit |||| %{smart_count} unit',
          volume: '%{smart_count} CBM',
          weight: '%{smart_count} KG |||| %{smart_count} KG'
        },
        fcl: {
          qty: '%{smart_count} unit |||| %{smart_count} unit',
          volume: {
            ft20: '%{smart_count} x 20\' %{type} %{weight}',
            ft40: '%{smart_count} x 40\' %{type} %{weight}',
            hc40: '%{smart_count} x 40\'HC %{type} %{weight}'
          },
          type: {
            dry: 'dry',
            reefer: 'reefer',
            ventilate : 'ventilate'
          },
          weight: {
            standard: 'standar',
            overweight: 'muatan lebih'
          }
        }
      },
      date: {
        title: 'tanggal kesiapan barang :'
      },
      optional_services: {
        title: 'layanan pilihan : ',
        insurance: 'asuransi',
        custom: 'kustom',
        commodities: 'komoditas'
      },
      action: {
        back_to_search: 'Ubah Pencarian'
      }
    },
    display: {
      header: {
        service_providers: 'penyedia jasa',
        services: 'Detail Jasa',
        price: 'harga'
      },
      content: {
        reviews: 'review',
        delivery_estimate: 'Perkiraan durasi pengiriman:',
        valid_until: 'berlaku hingga',
        days: 'hari',
        price : '%{currency} %{smart_count}',
        shipping_line : 'pelayaran',
        unknown : '-',
        action: {
          book: 'pesan',
          save: 'simpan',
          price_detail: 'lihat rincian harga'
        }
      },
      detail : {
        fee_empty : 'tidak ada biaya kustom/ angkutan di %{place}',
        fee_code : 'kode biaya',
        fee_desc : 'deskripsi',
        fee_units : 'jml',
        fee_uom : 'unit',
        fee_price : 'harga satuan',
        fee_pricesum : 'jumlah',
        subtotal : 'subtotal',
        minimum_charge : 'min',
        insurance : "asuransi",
        summary : 'ringkasan biaya',
        notes : 'catatan',
        notes_description : '%{notes}',
        notes_empty : 'catatan harga kosong',
        subamount : {
          trucking_origin : 'biaya angkutan negara asal',
          custom_origin : 'biaya kustom negara asal',
          freight : 'biaya pengiriman',
          document : 'biaya dokumen',
          insurance : 'biaya asuransi',
          trucking_destination : 'biaya angkutan negara tujuan',
          custom_destination : 'biaya kustom negara tujuan'
        },
        uom_options : {
          volume : 'm3',
          weight : 'kg',
          shipment : 'pengiriman',
          container : 'cnt',
          volumetric : 'volmtr'
        }
      }
    }
  },
  quote : {
    loading : 'sedang mengambil. Silahkan tunggu sebentar...',
    header : {
      freight_mode : 'Jalur Kirim',
      shipment_load : 'Jml Kiriman',
      origin : 'asal',
      destination : 'tujuan',
      providers : 'penyedia jasa',
      delivery_estimate : 'Perkiraan durasi pengiriman',
      price_total : 'total biaya',
      valid_until : 'berlaku hingga',
      creator : 'dibuat oleh',
      action : ''
    },
    content : {
      empty : 'tidak ada penawaran tersimpan',
      delivery_estimate : {
        title : '%{min} - %{max} hari'
      },
      price : {
        title : '%{currency} %{smart_count}'
      },
      freight_mode : {
        air : 'udara',
        ocean : 'laut',
        express : 'ekspres'
      },
      action : {
        book : 'pesan',
        search : '',
        delete : '',
        load_more : 'lihat lainnya...'
      }
    }
  },
  book: {
    title: 'Ringkasan Pesanan',
    loading_validation: 'kami sedang memvalidasi penawaran ini. Mohon tunggu...',
    invalid_validation : 'Penawaran ini tidak berlaku',
    loading_booking : 'Kami sedang memproses pesanan anda. Mohon tunggu...',
    booking_successful : {
      title : 'Selamat!',
      description : 'Pemesanan Anda Telah Berhasil'
    },
    booking_contact : {
      title : 'Jika anda membutuhkan bantuan untuk pengiriman ini,',
      description : 'anda dapat langsung menghubungi tim support kami',
      email : {
        marvinus : 'marvinus@andalin.com',
        bobby : 'bobby@andalin.com'
      },
      whatsapp : {
        marvinus : '(62) 821 1026 7895',
        bobby : '(62) 856 9201 4841'
      },
      action : {
        go_to_shipment : 'lihat daftar pengiriman untuk mengelola pengiriman anda'
      }
    },
    providers: 'penyedia jasa',
    valid_until: 'berlaku hingga',
    services: 'jasa',
    currency : 'pemesanan dengan kurs',
    load: {
      title: 'jumlah kiriman',
      lcl: {
        qty: '%{smart_count} unit |||| %{smart_count} unit',
        volume: '%{smart_count} cbm',
        weight: '%{smart_count} kg |||| %{smart_count} kg'
      },
      fcl: {
        qty: '%{smart_count} unit |||| %{smart_count} unit',
        volume: {
          ft20: '%{smart_count} x 20\' %{type} %{weight}',
          ft40: '%{smart_count} x 40\' %{type} %{weight}',
          hc40: '%{smart_count} x 40\'HC %{type} %{weight}'
        },
        type: {
          dry: 'dry',
          reefer: 'reefer'
        },
        weight: {
          standard: 'standar',
          overweight: 'muatan lebih'
        }
      }
    },
    optional_services: {
      title: 'layanan Pilihan',
      yes: 'ya',
      no: 'tidak',
      insurance: {
        title: 'asuransi',
        goods_value: {
          title: 'nilai barang',
          value: '%{smart_count} %{currency}'
        }
      },
      custom: {
        title: 'Layanan Kustom'
      }
    },
    action : {
      show_price_detail : 'lihat rincian harga'
    },
    total_shipment: 'total pengiriman',
    date: 'tanggal kesiapan barang',
    delivery_estimate: 'Perkiraan durasi pengiriman',
    days: 'hari',
    payment: {
      title: 'total biaya',
      total: '%{currency} %{smart_count}',
      information : {
        title : 'informasi pesanan',
        description : 'Harga berdasarkan detail barang yang anda berikan. Jika terdapat perbedaan ukuran atau perbedaan lainnya, maka akan terdapat biaya yang ditambahkan ke dalam rincian pesanan.'
      },
      action: {
        pay: 'pesan sekarang'
      }
    },
    shipment_detail: {
      title: 'Masukkan detail pengiriman',
      commodities : {
        title : 'Detail Komoditas',
        hscode : {
          label : 'HS code Barang',
          placeholder : 'HS Code Barang'
        },
        product_desc : {
          label : 'deskripsi barang',
          placeholder : 'masukkan deskripsi barang'
        },
        product_class : {
          label : 'kelas produk',
          placeholder : 'masukkan kelas produk',
          options : {
            GENCO : 'GENERAL CARGO',
            DG : 'DANGEROUS GOODS'
          }
        },
        product_type : {
          label : 'jenis produk',
          placeholder : 'masukkan jenis produk',
          options : {
            CLOTHES : 'baju',
            FURNITURE : 'furnitur',
            FOODBEVERAGES : 'makanan& minuman',
            OTHERS : 'lainnya'
          }
        }
      },
      shipper: {
        title: 'Detail Pengirim',
        company : {
          label : 'nama perusahaan pengirim',
          placeholder : 'masukkan nama perusahaan pengirim'
        },
        name: {
          label: 'nama PIC pengirim',
          placeholder: "masukkan nama pengirim"
        },
        address: {
          label: 'alamat pengirim',
          placeholder: "masukkan alamat pengirim"
        },
        phone: {
          label: 'no telepon pengirim',
          placeholder: "masukkan no telp pengirim"
        },
        fax: {
          label: 'no fax pengirim',
          placeholder: "masukkan no fax pengirimr"
        },
        email : {
          label : 'alamat email pengirim',
          placeholder : 'masukkan email pengirim'
        },
        note: {
          optional: 'pilihan',
          label: 'catatan pengiriman',
          placeholder: 'tinggalkan pesan untuk penyedia jasa'
        }
      },
      consignee: {
        title: 'Detail Penerima',
        company : {
          label : 'nama perusahaan penerima',
          placeholder : 'masukkan nama perusahaan penerima'
        },
        name: {
          label: 'nama PIC penerima',
          placeholder: "masukkan nama penerima"
        },
        address: {
          label: 'alamat penerima',
          placeholder: "masukkan alamat penerima"
        },
        phone: {
          label: 'no telepon penerima',
          placeholder: "nasukkan nomor telepon penerima"
        },
        fax: {
          label: 'no fax penerima',
          placeholder: "masukkan nomor fax penerima"
        },
        email : {
          label : 'alamat email penerima',
          placeholder : 'masukkan alamat email penerima'
        }
      }
    },
    additional_request: {
      title: 'permintaan tambahan',
      request: 'permintaan',
      optional: 'opsional',
      action: {
        add: 'permintaan'
      },
      type: {
        title: 'permintaan',
        value: 'jumlah',
        note: 'catatan',
        options: {
          free_time: {
            title: 'maximum free time',
            unit: 'hari',
            placeholder: 'hari',
            description: 'max. free time akan dikonfirmasi kembali setelah pemesanan'
          },
          min_temperature: {
            title: 'suhu minimum',
            unit: '℃elcius',
            placeholder: 'derajat',
            description: 'hanya untuk reefer'
          },
          max_temperature: {
            title: 'suhu maksimum',
            unit: '℃elcius',
            placeholder: 'derajat',
            description: 'hanya untuk reefer'
          },
          shipping_line : {
            title: 'karier/ kapal',
            unit: 'nama',
            placeholder: 'preferensi',
            description: 'preferensi kapal/ karier atau preferensi jadwal'
          },
          vessel : {
            title: 'kapal',
            unit: 'nama',
            placeholder: 'preferensi',
            description: 'preferensi nama/ bendera / spesifikasi kapal lainnya'
          }
        }
      }
    },
    terms_of_booking: {
      title: 'terms of booking',
      description: 'Items’ description on your order must exactly be matching with the physical items and your documents e.g. commercial invoice, packing list, etc. If the actual weight is more than what you’ve declared on your order, Andalin will charge you for the difference, and vice versa. Andalin do NOT accept illegal items e.g. illegal drugs, narcotics, weapons, etc. You accept all legal consequences for whatever items you are to export/import and hold Andalin harmless against any legal claim whatsoever. The price may change from time to time due to wrong HS-Code, random check by Custom Officer, incomplete licenses or any other reasons that may only arise at a later day.'
    },
    cancellation_policy: {
      title: 'cancellation policy',
      description: ' An order cannot be cancelled by a User when it is confirmed for processing by Freight Forwarder. If for some reasons, such order is cancelled by a User after getting a confirmation from Freight Forwarder for further process, a 25% penalty of freight cost may apply. A compensation will then be paid to such Freight Forwarder based on this penalty. A penalty will not apply if it’s caused by a Force Majeur.'
    },
    validation : {
      email_invalid : 'email is not valid',
      required : 'Required'
    }
  },
  settings : {
    tabs : {
      user : 'akun saya',
      company : 'pengaturan perusahaan',
      company_documents : 'dokumen perusahaan',
      company_users : 'daftar pengguna perusahaan'
    },
    docs : {
      type : {
        title : 'tipe dokumen',
        label : 'tipe dokumen',
        placeholder : 'Dokumen apakah ini?',
        options : {
          ktp : 'ktp',
          npwp : 'npwp',
          siup : 'siup',
          tdp : 'tdp',
          passport : 'passport',
          license : 'license',
          nikexport : 'nik export',
          nikimport : 'nik import',
        }
      },
      uploaded : {
        title : 'terunggah?',
        options : {
          exists : 'Ya',
          not_exists : 'Tidak'
        }
      },
      action : {
        new_upload : 'unggah dokumen',
        upload : 'unggah',
        uploading : 'sedang mengunggah',
        download : 'unduh'
      },
      form : {
        title : 'Unggah Dokumen Baru',
        description : 'Dengan mengupload dokumen baru, anda akan menghapus dokumen yang lama.',
        loading : 'sedang mengunggah...',
        close : 'tutup',
        success : '%{fileType} anda sudah berhasil diunggah',
        file_input : {
          label : 'unggah file'
        },
        validation : {
          required : 'diperlukan'
        }
      }
    },
    user : {
      photo : 'foto',
      form : {
        loading : 'sedang mengunggah...',
        file_input : {
          label : 'unggah foto'
        }
      },
      action : {
        upload : 'unggah foto profil',
        uploading : 'mengunggah foto profil anda'
      }
    },
    users : {
      username : 'username',
      gender : 'J/K',
      fullname : 'nama lengkap',
      statverif : 'aktif?',
      codeusertype : 'tipe',
      action : {
        'updating' : 'memperbaharui...',
        'update_users' : 'Perbaharui',
        'invite' : 'undang pengguna baru'
      },
      options : {
        'USRA' : 'Admin Perusahaan',
        'USRS' : 'Petugas',
        'VDRA' : 'Admin Freight Forwarder',
        'VDRS' : 'Petugas',
        'SADM' : 'system admin'
      }
    }
  },
  shipment : {
    loading : 'Sedang mengambil seluruh data. Silahkan Tunggu...',
    form : {
      filtered_results : 'menampilkan %{smart_count} hasil |||| menampilkan %{smart_count} hasil ',
      total_results : ' dari %{smart_count} jumlah pengiriman |||| dari %{smart_count} jumlah pengiriman',
      order_number : {
        label : 'cari nomor pesanan',
        placeholder : 'masukkan nomor pesanan'
      },
      order_by_direction : {
        label : 'ASC/DESC',
        options : {
          asc : 'ASC',
          desc : 'DESC'
        }
      },
      order_by: {
        label: 'sort by',
        options: { //object key based on the json field
          created_at: 'Tanggal Pesanan',
          shipment_date: 'Tanggal Pengiriman',
          carrier_name: 'Karier',
          vendor_name: 'Freight Forwarder',
          order_stat: 'status pesanan'
        }
      }
    },
    tab : {
      shipment : 'Detail Pengiriman',
      price : 'Detail Harga',
      carrier : 'Jadwal Karier',
      documents : 'Dokumen',
      messages : 'Pesan',
      history : 'Riwayat'
    },
    detail: {
      title : 'Informasi Pesanan',
      edit_order : 'Edit Status Pesanan',
      order_number: 'Nomor Pesanan',
      close : 'Tutup Tab Ini',
      order_stat: 'Status Pesanan',
      message : {
        title : 'Kanal Pesan untuk #',
        label : 'Tinggalkan Pesan untuk pesanan #',
        placeholder : 'masukkan pesan anda',
        file_description : 'deskripsi',
        action : {
          post : 'Kirim Pesan',
          posting : 'sedang mengirim...',
          download : 'unduh %{fileName}'
        },
        validation : {
          required : 'Wajib Diisi'
        }
      },
      shipper: {
        title : 'Informasi Pengirim&',
        edit : 'Ubah Pengirim',
        name: 'Nama Pengirim',
        company: 'Perusahaan Pengirim',
        address: 'Alamat Pengirim',
        phone: 'Telp Pengirim',
        email: 'Email Pengirim',
        fax: 'Fax Pengirim',
        notes: 'Catatan'
      },
      consignee: {
        title: 'Penerima',
        edit : 'Penerima',
        name: 'Nama Penerima',
        company: 'Perusahaan Penerima',
        address: 'Alamat Penerima',
        phone: 'Telp Penerima',
        email: 'Email Penerima',
        fax: 'Fax Penerima'
      },
      commodities: {
        title : 'Informasi Komoditas',
        edit_commodities : 'Edit Komoditas',
        class: {
          title : 'kelas komoditas',
          options : {
            GENCO: 'General Cargo',
            DG: 'Dangerous Goods'
          }
        },
        type: 'Tipe Komoditas',
        hscode: 'HS Code',
        product: 'Deskripsi Produk'
      },
      custom: 'Layanan Kustom',
      insurance: {
        title: 'asuransi',
        goods_value: 'Nilai Barang',
        goods_value_currency: 'Kurs',
        pcg: '% Premi Asuransi',
        price_currency_rate: 'Kurs',
        price_sum: 'Total Biaya Asuransi'
      },
      trucking: 'Jasa Truk',
      stat: {
        yes: 'Ya',
        no: 'Tidak'
      },
      freight: {
        mode: 'Jalur Pengiriman',
        type: 'Tipe Pengiriman'
      },
      vendor: {
        name: 'Freight Forwarder'
      },
      shipment_date: 'Tgl Kesiapan Barang',
      book_price: {
        total: 'Total Harga',
        currency_code: 'Kurs',
        currency_rate: 'Rate'
      },
      carrier: {
        title : 'Informasi Karier',
        edit_carrier : 'Ubah Informasi Karier',
        name: 'Nama Karier',
        do_awb: 'BL/AWB',
        contract: 'Kontrak Servis'
      },
      origin: {
        country: 'Negara Asal',
        city: 'Kota Asal',
        title: 'Asal',
        pol: 'Port of Loading'
      },
      destination: {
        country: 'Negara Tujuan',
        city: 'Kota Tujuan',
        title: 'Tujuan',
        pod: 'Port of Discharge'
      },
      transittime: 'Estimasi Durasi Pengiriman',
      requests: {
        title: 'Permintaan Tambahan',
        edit : 'Ubah Permintaan',
        header: {
          type: 'tipe',
          value: 'nilai',
          stat: 'status pengajuan',
          unit: 'Unit',
          note : 'catatan',
          created_at: 'Tgl',
          created_by: 'Dibuat oleh'
        },
        display: {
          stat: {
            U: 'belum dikonfirmasi',
            Y: 'diterima',
            N: 'ditolak'
          },
          type: {
            free_time: 'Free Time',
            min_temperature: 'Min Temperature',
            max_temperature: 'Max Temperature',
            shipping_line : 'Karier/ Kapal',
            vessel : 'Spesifikasi Kapal'
          }
        }
      },
      form: {
        close : 'tutup',
        validation : {
          required : 'Diperlukan',
          email_invalid : 'Email Tidak Valid'
        },
        consignee_shipper : {
          title : 'Ubah Profil Pengirim& Penerima',
          order_number : 'nomor pesanan #',
          loading : 'mohon tunggu...',
          success : 'Informasi Pengirim dan Penerima Sudah Berhasil Diubah!',
          action : {
            edit : 'Ubah Profil Pengirim& Penerima'
          }
        },
        order_stat : {
          title : 'Ubah Status Order',
          order_number : 'nomor pesanan #',
          loading : 'mohon tunggu...',
          success : 'Status Pesanan Sudah Berhasil Diubah!',
          label : 'Status Pesanan',
          placeholder : 'Status Pesanan',
          options : {
            book : 'DIPESAN',
            confirm : 'TERKONFIRMASI',
            scheduled : 'DIJADWALKAN',
            dispatch : 'DIBERANGKATKAN',
            discharge : 'SAMPAI TUJUAN',
            waiting_payment : 'MENUNGGU PEMBAYARAN',
            complete : 'SELESAI',
            all : 'SEMUA'
          },
          vessel_name : {
            label : 'Nama Kapal',
            placeholder : 'Masukkan Nama Kapal'
          },
          date_arrival : {
            label : 'Tanggal Kedatangan (%{cityCode},%{countryCode})',
            placeholder : 'Enter arrival date'
          },
          date_departure : {
            label : 'Tanggal Keberangkatan (%{cityCode},%{countryCode})',
            placeholder : 'Enter departure date'
          },
          port_open_time : {
            label : 'Port Open Time',
            placeholder : 'Port Open Time'
          },
          port_cutoff_time : {
            label : 'Port Cutoff Time',
            placeholder : 'Port Cut Off Time'
          },
          inland_cutoff_time : {
            label : 'Inland Cutoff Time',
            placeholder : 'Inland CutOff Time'
          },
          pol : {
            label : 'Port of Loading',
            placeholder : 'Port of Loading'
          },
          pod : {
            label : 'Port of Discharge',
            placeholder : 'Port of Discharge'
          },
          origin_city : {
            label : 'Kota Asal',
            placeholder : 'Kota Asal',
            loading : 'mohon tunggu...'
          },
          origin_country : {
            label : 'Negara Asal',
            placeholder : 'Negara Asal',
            loading : 'mohon tunggu...'
          },
          destination_city : {
            label : 'Kota Tujuan',
            placeholder : 'Kota Tujuan',
            loading : 'mohon tunggu...'
          },
          destination_country : {
            label : 'Negara Tujuan',
            placeholder : 'Negara Tujuan',
            loading : 'mohon tunggu...'
          },
          due_date : {
            label : 'Tgl Jatuh Tempo Penagihan',
            placeholder : 'DD/MM/YY'
          },
          payment_method : {
            label : 'Metode Pembayaran',
            placeholder : 'Masukkan Metode Pembayaran',
            options : {
              transfer : 'Transfer',
              giro : 'GIRO Transfer',
              wire_transfer : 'Wire Transfer'
            }
          },
          payment_to_bank : {
            label : 'Tujuan Pembaran',
            placeholder : 'Tujuan Pembayaran'
          },
          currency_from_code : {
            label : 'Kurs Basis',
            placeholder : 'Masukkan Basis Kurs'
          },
          currency_rate : {
            label : 'Rate Kurs',
            placeholder : 'Masukkan Rate Kurs'
          },
          ppn : {
            label : `% PPN`,
            placeholder : 'Masukkan %PPN'
          },
          note : {
            label : 'Catatan Tagihan',
            placeholder : 'Masukkan Catatan tagihan'
          },
          shipper_bank_name : {
            label : 'Nama Bank',
            placeholder : 'Masukkan Nama Bank'
          },
          shipper_bank_account_name : {
            label : 'Nama Akun Bank',
            placeholder : 'masukkan Nama Akun Bank'
          },
          shipper_bank_account_number : {
            label : 'Nomor Rekening',
            placeholder : 'masukkan Nomor Rekening'
          },
          payment_reference_number : {
            label : 'Nomor Referensi',
            placeholder : 'Masukkan Nomor Referensi'
          },
          payment_date : {
            label : 'Tanggal Pembayaran',
            placeholder : 'DD/MM/YY'
          },
          payment_total_paid : {
            label : 'Total Pembayaran',
            placeholder : 'Total Pembayaran'
          },
          payment_currency_code : {
            label : 'Kurs',
            placeholder : 'Kurs'
          },
          action : {
            edit : 'Ubah Status Pesanan',
            add_payment : 'Tambah Pembayaran',
            add_schedule : 'Tambah Jadwal'
          }
        },
        commodities : {
          title : 'Ubah Komoditas',
          order_number : 'Nomor Pesanan #',
          loading : 'mohon tunggu...',
          success : 'Komoditas Sudah Berhasil Diperbaharui!',
          action : {
            edit : 'Ubah Komoditas'
          }
        },
        order_number: {
          title: 'nomor pesanan',
          placeholder: 'nomor pesanan'
        },
        order_status: {
          options: {
            BOOK: 'DIPESAN',
            CONFIRM: 'TERKONFIRMASI',
            DISPATCH: 'DIBERANGKATKAN',
            DISCHARGE: 'SAMPAI TUJUAN',
            WAITING_PAYMENT : 'MENUNGGU PEMBAYARAN',
            COMPLETE: 'SELESAI'
          }
        },
        carrier: {
          title : 'Ubah Karier',
          loading : 'mohon tunggu...',
          order_number : 'Nomor Pesanan #',
          success : 'Karier Berhasil Diubah!',
          action : {
            edit : 'Ubah Karier'
          },
          name: {
            label: 'Nama Karier',
            placeholder: 'Nama Karier'
          },
          bl_awb: {
            label: 'BL/AWB',
            placeholder: 'Masukkan Nomor BL/AWB'
          },
          contract: {
            label: 'Kontrak Servis',
            placeholder: 'Masukkan Nomor Kontrak Servis'
          }
        },
        freights : {
          title : 'Ubah Kiriman',
          order_number : 'Nomor Pesanan #',
          loading : 'mohon tunggu...',
          success : 'Kiriman Sudah Berhasil Diubah!',
          action : {
            edit : 'Ubah Kiriman'
          },
          qty : 'jml',
          volume : 'volume',
          weight : 'berat',
          tracking_number : 'nomor lacak',
          service_type : 'tipe'
        },
        requests : {
          title : 'Ubah Permintaan',
          order_number : 'Nomor Pesanan #',
          loading : 'mohon tunggu...',
          success : 'Permintaan Sudah Berhasil Diubah!',
          action : {
            edit : 'Ubah Permintaan',
            add : 'Tambah Permintaan'
          },
          value : 'nilai',
          note : 'catatan',
          type : 'tipe',
          approved : {
            label : 'disetujui?',
            options : {
              U : 'BELUM TERKONFIRMASI',
              Y : 'DITERIMA',
              N : 'DITOLAK'
            }
          }
        },
        prices : {
          title : 'Ubah Harga',
          loading : 'mohon tunggu...',
          order_number : 'Nomor Pesanan #',
          success : 'Komponen Harga Sudah Ditambahkan!',
          action : {
            add : 'Tambah Komponen Harga',
            edit : 'Ubah Komponen Harga'
          },
          component_type : {
            label : 'Tipe Harga',
            placeholder : 'Tipe Harga',
            options : {
              freight : 'freight',
              custom : 'custom',
              document : 'document',
              trucking : 'trucking',
              insurance : 'insurance'
            }
          },
          description : {
            label : 'Deskripsi',
            placeholder : 'Deskripsi'
          },
          price_uom :{
            label : 'UOM',
            placeholder : 'uom',
            options : {
              w : 'weight',
              v : 'volume',
              vw : 'volumetric',
              container : 'cont',
              all : 'kiriman'
            }
          },
          qty : {
            label : 'jml',
            placeholder : 'jml'
          },
          price_sum : {
            label : 'Total Harga dalam %{currency}',
            placeholder : 'Total Harga'
          }
        },
        shipper: {
          title: 'Detail Pengirim',
          company: {
            label: 'Perusahaan Pengirim',
            placeholder: 'Masukkan Nama Perusahaan Pengirim'
          },
          name: {
            label: 'Nama Pengirim',
            placeholder: "Masukkan Nama Pengirim"
          },
          address: {
            label: 'Alamat Pengirim',
            placeholder: "Masukkan Alamat Pengirim"
          },
          phone: {
            label: 'Telp Pengirim',
            placeholder: "Masukkan Telp Pengirim"
          },
          fax: {
            label: 'Fax Pengirim',
            placeholder: "Masukkan Fax Pengirimr"
          },
          email: {
            label: 'Email Pengirim',
            placeholder: 'masukkan Email Pengirim'
          },
          note: {
            optional: 'optional',
            label: 'Catatan Pengirim',
            placeholder: 'Tinggalkan Pesan Untuk Penyedia Jasa'
          }
        },
        consignee: {
          title: 'Detail Penerima',
          company: {
            label: 'Perusahaan Penerima',
            placeholder: 'Masukkan Nama Perusahaan Penerima'
          },
          name: {
            label: 'Nama Penerima',
            placeholder: "Masukkan Nama Penerima"
          },
          address: {
            label: 'Alamat Penerima',
            placeholder: "Masukkan Alamat Penerima"
          },
          phone: {
            label: 'Telp Penerima',
            placeholder: "Masukkan Telp Penerima"
          },
          fax: {
            label: 'Fax Penerima',
            placeholder: "Masukkan Fax Penerimar"
          },
          email: {
            label: 'Email Penerima',
            placeholder: 'masukkan Email Penerima'
          }
        },
      },
      document : {
        title : 'Dokumen Terunggah',
        edit : 'Unggah Dokumen Baru',
        header : {
          created_at : 'tgl',
          file_category : 'kategori',
          file_description : 'deskripsi',
          file_origin : 'diunggah melalui',
          created_by_username : 'diunggah oleh'
        },
        form : {
          title  : 'Unggah Dokumen Baru',
          order_number : 'Nomo Pesanan #',
          loading : 'Mohon tunggu...',
          close : 'tutup',
          success : '%{fileCategory} telah berhasil diunggah',
          category : {
            label : 'kategori dokumen',
            placeholder : 'dokumen apakah ini?',
            options : {
              bl : 'BL - BILL OF LADING',
              pl : 'PL - PACKING LIST',
              bl_draft : 'BL DRAFT - BILL OF LADING DRAFT',
              photo : 'GOODS PHOTO',
              awb : 'AWB - AIRWAY BILL',
              api : 'API - IMPORT LICENSE (API)',
              certificate : 'CERTIFICATE',
              others : 'OTHERS'
            }
          },
          description : {
            label : 'deskripsi',
            placeholder : 'Masukkan deskripsi'
          },
          file_input : {
            label : 'Pilih Dokumen'
          },
          action : {
            uploading : 'sedang mengunggah...',
            upload : 'Unggah Dokumen'
          },
          validation : {
            required : 'diperlukan'
          }
        }
      },
      history : {
        title : 'Riwayat Pesanan',
        header : {
          created_at : 'tgl',
          description : 'deskripsi',
          created_by_comptp : 'dibuat oleh',
          created_by_username : 'Nama'
        }
      },
      price: {
        title: 'Detail Harga',
        edit : 'Ubah Detail Harga',
        download_invoice : 'Unduh Tagihan',
        header: {
          code_fee: 'Kode Biaya',
          service_type: 'tipe',
          qty: 'jml',
          price_description : 'Deskripsi',
          weight : 'Berat',
          volume : 'Volume',
          price_uom: 'unit',
          price_sum: 'Total Harga',
          insurance : {
            title : 'Detail Asuransi',
            goods_value : 'nilai Barang',
            currency_rate : 'nilai Kurs',
            pcg : '%Premi',
            price_sum : 'total Premi',
            commodities_type : 'Tipe Produk',
            commodities_product : 'Deskripsi Produk'
          }
        },
        display: {
          uom_options: {
            volume: 'm3',
            weight: 'kg',
            shipment: 'pengiriman',
            container: 'kontainer',
            volumetric: 'volmtr'
          }
        }
      },
      schedules : {
        title : 'Jadwal Kapal/Karier',
        port_open_time : 'waktu buka(P)',
        port_cutoff_time : 'waktu cutoff(P)',
        inland_cutoff_time : 'waktu cutoff(I)',
        vessel_name : 'Kapal',
        origin : 'Asal/Tgl Keberangkatan',
        destination : 'Tujuan/Tgl Kedatangan',
        scheduled : 'Aktif?',
        created_at : 'Tgl Penjadwalan'
      },
      freights: {
        title: 'Ringkasan Kiriman',
        header: {
          index: '#',
          type: 'Tipe',
          tracking_number: 'Nomor Lack',
          qty: 'Jml',
          weight: 'Berat',
          volume: 'Volume'
        },
        display: {
          service_type: {
            SEAFCL20FT: '20\' DRY',
            SEAFCL40FT: '40\' DRY',
            SEAFCL40FTHC: '40\'HC DRY',
            SEAFCL20FTREF: '20\' REEFER',
            SEAFCL40FTREF: '40\' REEFER',
            SEAFCL20FTVENT: '20\' VENTILATE',
            SEAFCL40FTVENT: '40\' VENTILATE',
            SEALCL: 'OCEAN LCL',
            AIRCARGO: 'AIR CARGO',
            AIRCOURIER: 'AIR COURIER'
          },
          additional_info: {
            standard: 'Standar',
            box: 'kotak/ peti kayu',
            pallets: 'pallet',
            overweight: 'Muatan Lebih'
          },
          unit: '%{smart_count} unit |||| %{smart_count} unit'
        }
      }
    }
  }
}
