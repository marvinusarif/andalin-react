import { createStore, applyMiddleware, compose } from 'redux';
import { createEpicMiddleware } from 'redux-observable';
import { createPolyglotMiddleware } from 'redux-polyglot';
import { persistStore, autoRehydrate } from 'redux-persist';
import { URL_WEBSOCKET, SWITCH_LANGUAGE } from '../actions';
import rootReducer from '../reducers';
import rootEpic from '../epics';
import { enLang, idnLang } from '../language';
import socketIO from 'socket.io-client'
import socketIoMiddleware from 'redux-socket.io-middleware'

export default function configureStore (){
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  const epicMiddleware = createEpicMiddleware(rootEpic);
  const polyglotMiddleware = createPolyglotMiddleware(
      SWITCH_LANGUAGE,
      (action) => action.payload.locale,
      (locale) => new Promise(resolve => {
          let phrase = null;
          switch(locale){
              case 'idn' :
                phrase = idnLang;
                break;
              case 'en' :
                phrase = enLang;
                break;
              default :
                phrase = enLang;
            }
          phrase !== null && resolve(phrase)
      })
  )
  const io = socketIO.connect(URL_WEBSOCKET)
  const middlewares = [epicMiddleware,polyglotMiddleware, socketIoMiddleware(io)]
  const enhancers = [ applyMiddleware(...middlewares), autoRehydrate()]
  return new Promise((resolve, reject) => {
    try {
      const store = createStore( rootReducer, undefined, composeEnhancers(...enhancers) );
      persistStore(store, {
        whitelist : ['user'],
        debounce : 100
      }, () => resolve(store));
    } catch (err) {
      reject(err)
    }
  })
}
