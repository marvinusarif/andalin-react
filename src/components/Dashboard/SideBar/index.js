import _ from 'lodash';
import React from 'react';
import { Link } from 'react-router-dom';
import translate from 'redux-polyglot/translate';

const SideBarItems = (props) => {
  const { items } = props;
  return (
    <div>
      <ul className="menu-items">
        { _.map(items, (item) => {
            return (
              <li key={props.p.t(item.label)} className="">
                <Link to={item.url} className="detailed">
                  <span className="title" style={{fontSize:'12px'}}>{props.p.tc(item.label)}</span>
                  { item.detail && (
                    <span className="details">{item.detail}</span>
                  )}
                </Link>
                <span className="icon-thumbnail "><i className={`fa fa-${item.icon}`}></i></span>
              </li>
            )
          })
        }
      </ul>
      <div className="clearfix"></div>
    </div>
  )
}

export default translate({polyglotScope : 'sidebar'})(SideBarItems)
