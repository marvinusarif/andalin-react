import React from 'react';
import { Container, Row, Col, Card, CardBlock} from 'reactstrap';

export const ReactLoader = ({message}) => {
  return (
      <Container className="m-t-20 bg-transparent">
        <Row className="bg-transparent">
          <Col className="bg-transparent">
            <Card className="bg-transparent">
              <CardBlock className="bg-transparent">
                <div className="cssload-thecube">
                	<div className="cssload-cube cssload-c1"></div>
                	<div className="cssload-cube cssload-c2"></div>
                	<div className="cssload-cube cssload-c4"></div>
                	<div className="cssload-cube cssload-c3"></div>
                </div>
                <div className="text-center m-t-20">
                  {message}
                </div>
              </CardBlock>
            </Card>
          </Col>
        </Row>
      </Container>
  )
}
