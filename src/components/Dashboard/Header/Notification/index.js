import React from 'react';
import { Link } from 'react-router-dom';

export const NotificationFooter = (props) => {
  return (
    <div>
      {/*<!-- START Notification Footer-->*/}
      <div className="notification-footer text-center">
        <a className="">Read all notifications</a>
        <a data-toggle="refresh" className="portlet-refresh text-black pull-right">
          <i className="pg-refresh_new"></i>
        </a>
      </div>
      {/*<!-- START Notification Footer-->*/}
    </div>
  )
}
export const NotificationItemScrollable = (props) => {
  return (
    <div>
        {/*<!-- START Notification Item-->*/}
        <div className="notification-item unread clearfix">
          {/*<!-- START Notification Item-->*/}
          <div className="heading open">
            <a className="text-primary pull-left">
              <i className="pg-map fs-16 m-r-10"></i>
              <span className="bold">Carrot Design</span>
              <span className="fs-12 m-l-10">David Nester</span>
            </a>
            <div className="pull-right">
              <div className="thumbnail-wrapper d16 circular inline m-t-15 m-r-10 toggle-more-details">
                <div><i className="fa fa-angle-left"></i>
                </div>
              </div>
              <span className=" time">few sec ago</span>
            </div>
            <div className="more-details">
              <div className="more-details-inner">
                <h5 className="semi-bold fs-16">“Apple’s Motivation - Innovation <br/>
                                              distinguishes between <br/>
                                              A leader and a follower.”</h5>
                <p className="small hint-text">
                  Commented on john Smiths wall.
                  <br/> via pages framework.
                </p>
              </div>
            </div>
          </div>
          {/*<!-- END Notification Item-->*/}
          {/*<!-- START Notification Item Right Side-->*/}
          <div className="option" data-toggle="tooltip" data-placement="left" title="mark as read">
            <Link className="mark" to="#"></Link>
          </div>
          {/*<!-- END Notification Item Right Side-->*/}
        </div>
    </div>
  )
}
export const NotificationItem = (props) => {
  return (
    <div>
      {/*<!-- START Notification Item-->*/}
      <div className="notification-item unread clearfix">
        <div className="heading">
          <div className="thumbnail-wrapper d24 circular b-white m-r-5 b-a b-white m-t-10 m-r-10">
            <img width="30" height="30" data-src-retina="assets/img/profiles/1x.jpg" data-src="assets/img/profiles/1.jpg" alt="" src="assets/img/profiles/1.jpg"/>
          </div>
          <a className="text-primary pull-left">
            <span className="bold">Revox Design Labs</span>
            <span className="fs-12 m-l-10">Owners</span>
          </a>
          <span className="pull-right time">11:00pm</span>
        </div>
        {/*<!-- START Notification Item Right Side-->*/}
        <div className="option" data-toggle="tooltip" data-placement="left" title="mark as read">
          <Link className="mark" to="#"></Link>
        </div>
        {/*<!-- END Notification Item Right Side-->*/}
      </div>
      {/*<!-- END Notification Item-->*/}
    </div>
  )
}
