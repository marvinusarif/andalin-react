import 'react-datepicker/dist/react-datepicker.css';
import 'react-input-range/lib/css/index.css';
import React from 'react';
import DatePicker from 'react-datepicker';
import Moment from 'moment';
import {TextField as TextFieldMaterialUI} from 'material-ui';
import InputRange from 'react-input-range';
import NumberFormat from 'react-number-format';
import Select from 'react-select';

export const HiddenField = (field) => {
  return (
    <input type="hidden" {...field.input}/>
  )
}
export const SelectField = (field) => {
  const showLabel = field.label ? field.label : '';
  const disabled = field.disabled ? 'disabled' : '';
  return (
    <div className={`form-group ${field.required}`}>
      { field.label && (
        <label htmlFor={field.id}>{showLabel}</label>
      )}
      <select className={field.className} {...field.input} disabled={disabled}>
        { field.items.map((item,index) => {
            return <option key={index} value={item.value}>{item.label}</option>
          })
        }
      </select>
    </div>
  )
}

export const SelectFieldReact = (field) => {
  const showLabel = field.label ? field.label : '';
  function handleChange(value) {
      field.input.onChange(value);
  }
  return (
    <div className={`form-group ${field.required}`}>
      <label htmlFor={field.id}>{showLabel}</label>
      <Select
        {...field}
        simpleValue={field.simpleValue ? true : false }
        autofocus
        onChange={handleChange}
        options={field.items}
        value={field.input.value}
        onBlur={() => field.input.onBlur(field.input.value)}
        onBlurResetsInput={true}
        required={ field.required === 'required' ? true : false}/>
    </div>
  )
}

export const CheckBox = (field) => {
    const error_class = (field.meta.touched && field.meta.error) ? 'has-danger' : '';
    const disabled = field.disabled ? 'disabled' : '';
    return (
      <div className={`form-check ${error_class}`}>
        <label className="form-check-label" style={{fontSize : '12px'}}>
          <input type="checkbox" className="form-check-input" checked={field.input.value ? 'true' : ''} {...field.input} disabled={disabled}/>
          {field.label}
        </label>
        {field.meta.touched && field.meta.error && (
          <small className="form-control-feedback">{field.meta.error}</small>
        )}
      </div>
    )
}

export const TextField = (field) => {
  const showLabel = field.label ? field.label : '';
  const error_class = (field.meta.touched && field.meta.error) ? 'has-danger' : '';
  const disabled = field.disabled ? 'disabled' : '';
  return (
    <div className={`form-group ${field.required} ${error_class}`}>
      { field.label && (
        <label htmlFor={field.id}>{showLabel}</label>
      )}
      <input placeholder={field.placeholder} style={{height:'38px'}} className={`${field.className}`} type={field.type ? field.type : 'text'} {...field.input} disabled={disabled}/>
      {field.meta.touched && field.meta.error && (
        <small className="form-control-feedback">{field.meta.error}</small>
      )}
    </div>
  )
}

export const TextArea = (field) => {
  const showLabel = field.label ? field.label : '';
  const error_class = (field.meta.touched && field.meta.error) ? 'has-danger' : '';
  const disabled = field.disabled ? 'disabled' : '';
  return (
    <div className={`form-group ${field.required} ${error_class}`}>
      <label htmlFor={field.id}>{showLabel}</label>
      <textarea placeholder={field.placeholder} style={{height:'76px'}} className={`${field.className}`} type={field.type ? field.type : 'text'} {...field.input} disabled={disabled}/>
      {field.meta.touched && field.meta.error && (
        <small className="form-control-feedback">{field.meta.error}</small>
      )}
    </div>
  )
}

export const ButtonV = (field) => {
  return (
          <button type={field.type ? field.type : 'button'} {...field}>
            <span className="p-t-5 p-b-5">
              <i className={`fa fa-${field.icon} fs-15`}></i>
            </span>
            <br/>
            <span className="fs-11 font-montserrat text-uppercase">{field.value}</span>
          </button>
  )
}
export const Button = (field) => {
  return (
    <button type={field.type ? field.type : 'button'} {...field}>
      { field.icon ? <i className={`fa fa-${field.icon}`}></i> : ''}
      {' '}
      {field.value} </button>
  )
}

export const DateField = (field) => {
  const showLabel = field.label ? field.label : '';
  const error_class = (field.meta.touched && field.meta.error) ? 'has-danger' : '';
  return (
    <div className={`form-group ${field.required} ${error_class}`}>
      <label htmlFor={field.id}>{showLabel}</label>
      <DatePicker
        showTimeSelect={field.showTimeSelect ? true : false}
        className={field.className}
        timeFormat={field.timeFormat}
        dateFormat={`${ field.dateFormat ? field.dateFormat : 'DD/MM/YY'}`}
        minDate={field.minDate}
        maxDate={field.maxDate}
        selected={field.input.value ? Moment(field.input.value, `${ field.dateFormat ? field.dateFormat : 'DD/MM/YY' } ${ field.timeFormat ? field.timeFormat : 'HH:mm'}`) : null }
        {...field.input} />
      {field.meta.touched && field.meta.error && (
        <small className="form-control-feedback">{field.meta.error}</small>
      )}
    </div>
  )
}

export const NumberField = (field) => {
  const showLabel = field.label ? field.label : '';
  const error_class = (field.meta.touched && field.meta.error) ? 'has-danger' : '';
  return (
    <div className={`form-group ${field.required} ${error_class}`}>
      <label htmlFor={field.id}>{showLabel}</label>
      <NumberFormat customInput={TextFieldMaterialUI} hintText={field.placeholder} thousandSeparator={true} {...field.input}/>
      {field.meta.touched && field.meta.error && (
        <small className="form-control-feedback">{field.meta.error}</small>
      )}
    </div>
  )
}
export const SliderField = (field) => {
  const showLabel = field.label ? field.label : '';
  const error_class = (field.meta.touched && field.meta.error) ? 'has-danger' : '';
  return (
    <div className={`form-group ${field.required} ${error_class}`}>
      <label htmlFor={field.id}>{showLabel}</label>
      <InputRange
        formatLabel={value => {
            let newValue = 0.0;
            let units = '';
            let prefix = '';
            let toFixed = 0;
            if(field.currencyBase !== field.currency.select ){
              prefix = field.currency.select;
              newValue = value * field.currency.rates[field.currencyBase][field.currency.select].rate;
            }else{
              prefix = field.currencyBase;
              newValue = value;
            }
            if(newValue >= 1000000000){
              newValue = newValue/1000000000;
              units = 'B'
            } else if(newValue >= 1000000){
              newValue = newValue/1000000;
              units = 'M'
            } else if(newValue >= 1000){
              newValue = newValue/1000;
              units = 'K'
            } else {
              newValue = value;
              units = ''
            }

            if(field.currency.select === 'IDR'){
              toFixed = 2;
            } else {
              toFixed = 1;
            }

            return `${prefix} ${newValue.toFixed(toFixed)}${units}`}}
        step={field.step}
        maxValue={field.max}
        minValue={field.min}
        {...field.input}/>
    </div>
  )
}

export const FileInputField = (field) => {
  const {
    input: {
      value: omitValue,
      onChange,
      onBlur,
      ...inputProps,
    },
    meta: omitMeta,
    ...props,
  } = field;

  const adaptFileEventToValue = delegate =>
  e => delegate(e.target.files[0]);

  const showLabel = field.label ? field.label : '';
  const error_class = (field.meta.touched && field.meta.error) ? 'has-danger' : '';

  return (
    <div className={`form-group ${field.required} ${error_class}`}>
      { field.label && (
        <label htmlFor={field.id}>{showLabel}</label>
      )}
      <input
        style={{height : '38px'}}
        onChange={adaptFileEventToValue(onChange)}
        onBlur={adaptFileEventToValue(onBlur)}
        type="file"
        {...inputProps}
        {...props}/>
      {field.meta.touched && field.meta.error && (
        <small className="form-control-feedback">{field.meta.error}</small>
      )}
    </div>
  )
}
