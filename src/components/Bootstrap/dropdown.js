import React from 'react';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem} from 'reactstrap';

export const DropDown = (props) => {
  const { isOpen, toggle, label, options, onItemClick, p } = props;
  return (
    <Dropdown isOpen={isOpen} size="sm" toggle={toggle}>
        <DropdownToggle caret>
          <span>{p ? p.tc(`${label}`) : label }<i className="fa fa-caret-down"></i> </span>
        </DropdownToggle>
        <DropdownMenu>
          { options.map( opt => {
            return(
              <DropdownItem key={opt.action} onClick={() => onItemClick(opt)}> {p ? p.tc(`${opt.label}`) : opt.label } </DropdownItem>
            )
          })}
        </DropdownMenu>
      </Dropdown>
  )
}

export const DropDownCustom = (props) => {
  const { icon, isOpen, toggle, label, options, onItemClick, p } = props;
  return (
  <Dropdown isOpen={isOpen} toggle={toggle} size="sm">
      <DropdownToggle>
        <span><i className={`fa fa-${icon}`}></i>{p ? p.tc(`${label}`) : label }<i className="fa fa-caret-down"></i> </span>
      </DropdownToggle>
        <DropdownMenu>
          {options.map( (opt,index) => {
            return (!opt.thread ?
                      (<DropdownItem key={index} onClick={() => onItemClick(opt)}>
                        <i className={`fa fa-${opt.icon}`}></i> {p ? p.tc(`${opt.label}`) : opt.label }
                      </DropdownItem>)
                     :
                      (<DropdownItem key={index} className="clearfix bg-master-lighter" onClick={ () => onItemClick(opt)}>
                        <span className="pull-left">{p ? p.tc(`${opt.label}`) : opt.label }</span>
                        <span className="pull-right"><i className={`fa fa-${opt.icon}`}></i></span>
                      </DropdownItem>))
            })}
        </DropdownMenu>
    </Dropdown>)
}
