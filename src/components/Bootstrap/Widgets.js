import React from 'react';
import { Alert } from 'reactstrap';

export const Widgets = (props) => {
  const { title, content } = props;
  return (
    <Alert color="primary">
      <h2>{title}</h2>
      <h5>{content}</h5>
    </Alert>
  )
}
