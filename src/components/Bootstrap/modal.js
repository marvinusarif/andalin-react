import React from 'react';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

export const ModalStrap = (props) => {
  const { isOpen, title, toggle, className, size, content, footer } = props;
  return (
    <Modal isOpen={isOpen} toggle={toggle} className={className} size={size}>
         { title && (
           <ModalHeader toggle={toggle}>{title}</ModalHeader>
         )}
         <ModalBody>
           {content}
         </ModalBody>
         { footer && (
           <ModalFooter>
             {footer}
           </ModalFooter>
         )}

       </Modal>
  )
}
