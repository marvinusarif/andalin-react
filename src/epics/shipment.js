import { URL_REST,
         URL_FILES,
         URL_SHIPMENT,
         URL_SHIPMENT_DOWNLOAD,
         URL_SHIPMENT_INVOICE_DOWNLOAD,
         URL_SHIPMENT_PRICE, URL_SHIPMENT_DETAIL,
         URL_SHIPMENT_DOCUMENT,
         URL_SHIPMENT_CARRIER,
         URL_SHIPMENT_HISTORY,
         URL_SHIPMENT_UPLOAD_DOCUMENT,
         URL_SHIPMENT_DETAIL_UPDATE_CARRIER,
         URL_SHIPMENT_DETAIL_UPDATE_SHIPPER_CONSIGNEE,
         URL_SHIPMENT_DETAIL_UPDATE_ORDER_STAT,
         URL_SHIPMENT_DETAIL_UPDATE_COMMODITIES,
         URL_SHIPMENT_DETAIL_UPDATE_FREIGHT,
         URL_SHIPMENT_DETAIL_UPDATE_REQUEST,
         URL_SHIPMENT_DETAIL_UPDATE_PRICE,
         FETCH_SHIPMENTS,
         FETCH_SHIPMENT_DETAIL,
         FETCH_SHIPMENT_PRICE,
         FETCH_SHIPMENT_DOCUMENT,
         FETCH_SHIPMENT_CARRIER,
         FETCH_SHIPMENT_HISTORY,
         UPDATE_SHIPMENT_SHIPPER_CONSIGNEE,
         UPDATE_SHIPMENT_ORDER_STAT,
         UPDATE_SHIPMENT_COMMODITIES,
         UPDATE_SHIPMENT_CARRIER,
         UPDATE_SHIPMENT_FREIGHT,
         UPDATE_SHIPMENT_ORDER_REQUEST,
         UPDATE_SHIPMENT_ORDER_PRICE,
         DOWNLOAD_SHIPMENT_INVOICE,
         POST_UPDATE_SHIPMENT_DOCUMENT,
         DOWNLOAD_SHIPMENT_DOC
         } from '../actions';
import { ajax } from 'rxjs/observable/dom/ajax';
import { receiveFetchShipments, rejectFetchShipments,
         receiveFetchShipmentDetail, rejectFetchShipmentDetail,
         receiveFetchShipmentPrice, rejectFetchShipmentPrice,
         receiveFetchShipmentDocument, rejectFetchShipmentDocument,
         receiveFetchShipmentCarrier, rejectFetchShipmentCarrier,
         receiveFetchShipmentHistory, rejectFetchShipmentHistory,
         receiveShipmentShipperConsignee, rejectShipmentShipperConsignee,
         receiveShipmentOrderStat, rejectShipmentOrderStat,
         receiveShipmentCommodities, rejectShipmentCommodities,
         receiveShipmentCarrier, rejectShipmentCarrier,
         receiveShipmentFreight, rejectShipmentFreight,
         receiveShipmentOrderRequest, rejectShipmentOrderRequest,
         receiveShipmentOrderPrice, rejectShipmentOrderPrice,
         updateProgressBarUpdateShipmentDocs,
         receivePostUpdateShipmentDocs, rejectPostUpdateShipmentDocs,
         receiveDownloadShipmentDoc, rejectDownloadShipmentDoc,
         receiveDownloadShipmentInvoice, rejectDownloadShipmentInvoice,
       } from '../actions/shipment';
import { Observable } from 'rxjs';
import axios from 'axios';
const fileSaver = require('file-saver');

const _axios = {
    upload : (url, formdata, store) => {
      const request = axios.post( url,
                                 formdata,
                                 { headers : {'Content-Type' : 'multipart/form-data',
                                              'Authorization' : `Bearer ${store.getState().user.session.token}`},
                                  onUploadProgress: progressEvent => {
                                        const percentCompleted = Math.floor((progressEvent.loaded * 100) / progressEvent.total);
                                        store.dispatch(updateProgressBarUpdateShipmentDocs(percentCompleted))
                               }})
                      .then(response => response)
      return Observable.from(request);
    },
    download : (url, fileName, store) => {
      const request = axios({
                                url,
                                method : 'get',
                                responseType : 'blob',
                                headers : {'Content-Type' : 'application/json',
                                  'Authorization' : `Bearer ${store.getState().user.session.token}`}
                      }).then(response => {
                        const downloadedFile = new Blob([response.data]);
                        fileSaver.saveAs(downloadedFile,`${fileName.saveAsFileName}${fileName.extFileName}` );
                        return response;
                      })
      return Observable.from(request);
    }
  };

export const fetchShipmentsEpic = (action$, store) =>
    action$.ofType(FETCH_SHIPMENTS)
    .mergeMap(action => ajax.get(`${URL_REST}${URL_SHIPMENT}`,
                        { 'Content-Type': 'application/json',
                          'Authorization' : `Bearer ${ store.getState().user.session.token}`})
                        .map(response => receiveFetchShipments(response))
                        .catch(err => Observable.of( rejectFetchShipments(err)))
    );
export const fetchShipmentDetailEpic = (action$, store) =>
    action$.ofType(FETCH_SHIPMENT_DETAIL)
    .mergeMap(action => ajax.get(`${URL_REST}${URL_SHIPMENT}${URL_SHIPMENT_DETAIL}/${action.payload.shipmentId}/${action.payload.orderNumber.split('/').join('-')}`,
                        { 'Content-Type': 'application/json',
                          'Authorization' : `Bearer ${ store.getState().user.session.token}`})
                        .map(response => receiveFetchShipmentDetail({response, shipmentId : action.payload.shipmentId}))
                        .catch(err => Observable.of( rejectFetchShipmentDetail({error :err, shipmentId : action.payload.shipmentId})))
    );

export const fetchShipmentPriceEpic = (action$, store) =>
    action$.ofType(FETCH_SHIPMENT_PRICE)
    .mergeMap(action => ajax.get(`${URL_REST}${URL_SHIPMENT}${URL_SHIPMENT_PRICE}/${action.payload.shipmentId}/${action.payload.orderNumber.split('/').join('-')}`,
                        { 'Content-Type': 'application/json',
                          'Authorization' : `Bearer ${ store.getState().user.session.token}`})
                        .map(response => receiveFetchShipmentPrice({response, shipmentId : action.payload.shipmentId}))
                        .catch(err => Observable.of( rejectFetchShipmentPrice({error : err, shipmentId : action.payload.shipmentId})))
    );

export const fetchShipmentDocumentEpic = (action$, store) =>
    action$.ofType(FETCH_SHIPMENT_DOCUMENT)
    .mergeMap(action => ajax.get(`${URL_REST}${URL_SHIPMENT}${URL_SHIPMENT_DOCUMENT}/${action.payload.shipmentId}/${action.payload.orderNumber.split('/').join('-')}`,
                        { 'Content-Type': 'application/json',
                          'Authorization' : `Bearer ${ store.getState().user.session.token}`})
                        .map(response => receiveFetchShipmentDocument({response, shipmentId : action.payload.shipmentId}))
                        .catch(err => Observable.of( rejectFetchShipmentDocument({error : err, shipmentId : action.payload.shipmentId})))
    );

export const fetchShipmentCarrierEpic = (action$, store) =>
    action$.ofType(FETCH_SHIPMENT_CARRIER)
    .mergeMap(action => ajax.get(`${URL_REST}${URL_SHIPMENT}${URL_SHIPMENT_CARRIER}/${action.payload.shipmentId}/${action.payload.orderNumber.split('/').join('-')}`,
                        { 'Content-Type': 'application/json',
                          'Authorization' : `Bearer ${ store.getState().user.session.token}`})
                        .map(response => receiveFetchShipmentCarrier({response, shipmentId : action.payload.shipmentId}))
                        .catch(err => Observable.of( rejectFetchShipmentCarrier({error : err, shipmentId : action.payload.shipmentId})))
    );

export const fetchShipmentHistoryEpic = (action$, store) =>
    action$.ofType(FETCH_SHIPMENT_HISTORY)
    .mergeMap(action => ajax.get(`${URL_REST}${URL_SHIPMENT}${URL_SHIPMENT_HISTORY}/${action.payload.shipmentId}/${action.payload.orderNumber.split('/').join('-')}`,
                        { 'Content-Type': 'application/json',
                          'Authorization' : `Bearer ${ store.getState().user.session.token}`})
                        .map(response => receiveFetchShipmentHistory({response, shipmentId : action.payload.shipmentId}))
                        .catch(err => Observable.of( rejectFetchShipmentHistory({error : err, shipmentId : action.payload.shipmentId})))
    );

export const updateShipmentShipperConsigneeEpic = (action$, store) =>
    action$.ofType(UPDATE_SHIPMENT_SHIPPER_CONSIGNEE)
    .mergeMap(action => ajax.put(`${URL_REST}${URL_SHIPMENT}${URL_SHIPMENT_DETAIL}${URL_SHIPMENT_DETAIL_UPDATE_SHIPPER_CONSIGNEE}/${action.payload.shipmentId}/${action.payload.orderNumber.split('/').join('-')}`,
                        action.payload.form,
                        { 'Content-Type': 'application/json',
                              'Authorization' : `Bearer ${ store.getState().user.session.token}`})
                        .map(response => receiveShipmentShipperConsignee({response, shipmentId : action.payload.shipmentId}))
                        .catch(err => Observable.of( rejectShipmentShipperConsignee({error : err, shipmentId : action.payload.shipmentId})))
    );

export const updateShipmentOrderStatEpic = (action$, store) =>
    action$.ofType(UPDATE_SHIPMENT_ORDER_STAT)
    .mergeMap(action => ajax.put(`${URL_REST}${URL_SHIPMENT}${URL_SHIPMENT_DETAIL}${URL_SHIPMENT_DETAIL_UPDATE_ORDER_STAT}/${action.payload.shipmentId}/${action.payload.orderNumber.split('/').join('-')}`,
                        action.payload.form,
                        { 'Content-Type': 'application/json',
                              'Authorization' : `Bearer ${ store.getState().user.session.token}`})
                        .map(response => receiveShipmentOrderStat({response, shipmentId : action.payload.shipmentId}))
                        .catch(err => Observable.of( rejectShipmentOrderStat({error : err, shipmentId : action.payload.shipmentId})))
    );

export const updateShipmentCommoditiesEpic = (action$, store) =>
    action$.ofType(UPDATE_SHIPMENT_COMMODITIES)
    .mergeMap(action => ajax.put(`${URL_REST}${URL_SHIPMENT}${URL_SHIPMENT_DETAIL}${URL_SHIPMENT_DETAIL_UPDATE_COMMODITIES}/${action.payload.shipmentId}/${action.payload.orderNumber.split('/').join('-')}`,
                        action.payload.form,
                        { 'Content-Type': 'application/json',
                              'Authorization' : `Bearer ${ store.getState().user.session.token}`})
                        .map(response => receiveShipmentCommodities({response, shipmentId : action.payload.shipmentId}))
                        .catch(err => Observable.of( rejectShipmentCommodities({error : err, shipmentId : action.payload.shipmentId})))
    );

export const updateShipmentCarrierEpic = (action$, store) =>
    action$.ofType(UPDATE_SHIPMENT_CARRIER)
    .mergeMap(action => ajax.put(`${URL_REST}${URL_SHIPMENT}${URL_SHIPMENT_DETAIL}${URL_SHIPMENT_DETAIL_UPDATE_CARRIER}/${action.payload.shipmentId}/${action.payload.orderNumber.split('/').join('-')}`,
                        action.payload.form,
                        { 'Content-Type': 'application/json',
                              'Authorization' : `Bearer ${ store.getState().user.session.token}`})
                        .map(response => receiveShipmentCarrier({response, shipmentId : action.payload.shipmentId}))
                        .catch(err => Observable.of( rejectShipmentCarrier({error : err, shipmentId : action.payload.shipmentId})))
    );

export const updateShipmentFreightEpic = (action$, store) =>
    action$.ofType(UPDATE_SHIPMENT_FREIGHT)
    .mergeMap(action => ajax.put(`${URL_REST}${URL_SHIPMENT}${URL_SHIPMENT_DETAIL}${URL_SHIPMENT_DETAIL_UPDATE_FREIGHT}/${action.payload.shipmentId}/${action.payload.orderNumber.split('/').join('-')}`,
                        action.payload.form,
                        { 'Content-Type': 'application/json',
                              'Authorization' : `Bearer ${ store.getState().user.session.token}`})
                        .map(response => receiveShipmentFreight({response, shipmentId : action.payload.shipmentId}))
                        .catch(err => Observable.of( rejectShipmentFreight({error : err, shipmentId : action.payload.shipmentId})))
    );

export const updateShipmentOrderRequestEpic = (action$, store) =>
    action$.ofType(UPDATE_SHIPMENT_ORDER_REQUEST)
    .mergeMap(action => ajax.put(`${URL_REST}${URL_SHIPMENT}${URL_SHIPMENT_DETAIL}${URL_SHIPMENT_DETAIL_UPDATE_REQUEST}/${action.payload.shipmentId}/${action.payload.orderNumber.split('/').join('-')}`,
                        action.payload.form,
                        { 'Content-Type': 'application/json',
                            'Authorization' : `Bearer ${ store.getState().user.session.token}`})
                        .map(response => receiveShipmentOrderRequest({response, shipmentId : action.payload.shipmentId}))
                        .catch(err => Observable.of( rejectShipmentOrderRequest({error : err, shipmentId : action.payload.shipmentId})))
    );

export const updateShipmentOrderPriceEpic = (action$, store) =>
      action$.ofType(UPDATE_SHIPMENT_ORDER_PRICE)
      .mergeMap(action => ajax.put(`${URL_REST}${URL_SHIPMENT}${URL_SHIPMENT_DETAIL_UPDATE_PRICE}/${action.payload.shipmentId}/${action.payload.orderNumber.split('/').join('-')}`,
                          action.payload.form,
                          { 'Content-Type': 'application/json',
                              'Authorization' : `Bearer ${ store.getState().user.session.token}`})
                          .map(response => receiveShipmentOrderPrice({response, shipmentId : action.payload.shipmentId}))
                          .catch(err => Observable.of( rejectShipmentOrderPrice({error : err, shipmentId : action.payload.shipmentId})))
    );
export const postUpdateShipmentDocsEpic = (action$,store) =>
    action$.ofType(POST_UPDATE_SHIPMENT_DOCUMENT)
    .mergeMap( action => _axios.upload(`${URL_REST}${URL_FILES}${URL_SHIPMENT}${URL_SHIPMENT_UPLOAD_DOCUMENT}`,
                         action.payload.form, store)
                         .map(response => receivePostUpdateShipmentDocs({response, shipmentId : action.payload.shipmentId}))
                         .catch(error => Observable.of(rejectPostUpdateShipmentDocs({error, shipmentId : action.payload.shipmentId})))
    );

export const downloadShipmentDocsEpic = (action$,store) =>
    action$.ofType(DOWNLOAD_SHIPMENT_DOC)
    .mergeMap( action => _axios.download(`${URL_REST}${URL_FILES}${URL_SHIPMENT}${URL_SHIPMENT_DOWNLOAD}/stream/${action.payload.shipmentId}/${action.payload.orderNumber.split('/').join('-')}/${action.payload.fileId}/${action.payload.fileCategory}`,
                          action.payload.fileName, store)
                         .map(response => receiveDownloadShipmentDoc(response))
                         .catch(error => Observable.of(rejectDownloadShipmentDoc(error)))
    );

export const downloadShipmentInvoiceEpic = (action$,store) =>
    action$.ofType(DOWNLOAD_SHIPMENT_INVOICE)
    .mergeMap( action => _axios.download(`${URL_REST}${URL_SHIPMENT}${URL_SHIPMENT_INVOICE_DOWNLOAD}/stream/${action.payload.shipmentId}/${action.payload.orderNumber.split('/').join('-')}`,
                          action.payload.fileName, store)
                          .map(response => receiveDownloadShipmentInvoice(response))
                          .catch(error => Observable.of(rejectDownloadShipmentInvoice(error)))
    );
