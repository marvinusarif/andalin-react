import { URL_REST, URL_BOOK, URL_POST_BOOK_VALIDATE, URL_POST_BOOK_ORDER, POST_BOOK_VALIDATE_QUOTE,
         POST_BOOK_ORDER } from '../actions';
import { ajax } from 'rxjs/observable/dom/ajax';
import { receivePostBookValidateQuote, rejectPostBookValidateQuote,
         receivePostBookOrder, rejectPostBookOrder } from '../actions/book';
import { Observable } from 'rxjs';

export const fetchBookValidateQuoteEpic = (action$, store) =>
    action$.ofType(POST_BOOK_VALIDATE_QUOTE)
    .mergeMap(action => ajax.post(`${URL_REST}${URL_BOOK}${URL_POST_BOOK_VALIDATE}`,
                          action.payload,
                        { 'Content-Type': 'application/json',
                          'Authorization' : `Bearer ${ store.getState().user.session.token}`})
                        .map(response => receivePostBookValidateQuote(response))
                        .catch(err => Observable.of( rejectPostBookValidateQuote(err)))
    );

    
export const fetchBookOrderEpic = (action$, store) =>
    action$.ofType(POST_BOOK_ORDER)
    .mergeMap(action => ajax.post(`${URL_REST}${URL_BOOK}${URL_POST_BOOK_ORDER}`,
                              action.payload,
                            { 'Content-Type': 'application/json',
                              'Authorization' : `Bearer ${ store.getState().user.session.token}`})
                            .map(response => receivePostBookOrder(response))
                            .catch(err => Observable.of( rejectPostBookOrder(err)))
        );
