import { URL_REST,
         URL_ROUTE,
         URL_COUNTRY,
         URL_USER,
         URL_VERIFICATION_EMAIL,
         URL_CONTACTS,
         URL_USER_VERIFY,
         URL_USER_RESET_PASSWORD_MAIL,
         URL_SHIPMENT,
         URL_SHIPMENT_BANK_ACC,
         FETCH_ALL_COUNTRIES,
         POST_USER_REGISTER, POST_VERIFICATION_EMAIL,
         RESET_VERIFICATION_EMAIL_TIMER, REDUCE_VERIFICATION_EMAIL_TIMER,
         POST_CONTACT, VERIFY_USER, RESET_PASSWORD_MAIL,
         FETCH_SHIPMENT_BANK_ACC
       } from '../actions';
import { receiveFetchAllCountries, rejectFetchAllCountries,
         receiveVerificationEmail, rejectVerificationEmail,
         resetVerificationTimer, reduceVerificationTimer,
         receivePostContact, rejectPostContact,
         receiveVerifyUser, rejectVerifyUser,
         receiveResetPasswordMail, rejectResetPasswordMail,
         receiveFetchShipmentBankAcc, rejectFetchShipmentBankAcc,
       } from '../actions/page';
import { ajax } from 'rxjs/observable/dom/ajax';
import { Observable } from 'rxjs';

export const fetchAllCountriesEpic = (action$) =>
    action$.ofType(FETCH_ALL_COUNTRIES)
    .mergeMap(action => ajax.get(`${URL_REST}${URL_ROUTE}${URL_COUNTRY}`)
                          .map(response => receiveFetchAllCountries(response))
                          .catch(err =>  Observable.of(rejectFetchAllCountries(err)))
      );
export const postVerificationEmailEpic = (action$) =>
    action$.ofType(POST_VERIFICATION_EMAIL)
    .mergeMap(action => ajax.post(`${URL_REST}${URL_USER}${URL_VERIFICATION_EMAIL}`,
                        action.payload.user,
                        { 'Content-Type': 'application/json' })
                        .map(response => receiveVerificationEmail(response))
                        .catch(error => Observable.of(rejectVerificationEmail(error)))
      );
export const postContactEpic = (action$) =>
    action$.ofType(POST_CONTACT)
    .mergeMap(action => ajax.post(`${URL_REST}${URL_CONTACTS}`,
                        action.payload.form,
                        { 'Content-Type': 'application/json' })
                        .map(response => receivePostContact(response))
                        .catch(error => Observable.of(rejectPostContact(error)))
      );
export const resetVerificationTimerEpic = (action$, store) =>
    action$.ofType(POST_USER_REGISTER, POST_VERIFICATION_EMAIL)
    .filter(() => store.getState().page.modal.register.mail.emailSent > 1)
    .mapTo(resetVerificationTimer());

export const reduceVerificationTimerEpic = (action$, store) =>
    action$.ofType(RESET_VERIFICATION_EMAIL_TIMER, REDUCE_VERIFICATION_EMAIL_TIMER)
    .filter(() => store.getState().page.modal.register.mail.timer > 0)
    .delay(1000)
    .mapTo(reduceVerificationTimer());

export const userVerifyEpic = (action$) =>
    action$.ofType(VERIFY_USER)
    .mergeMap(action => ajax.put(`${URL_REST}${URL_USER}${URL_USER_VERIFY}`,
                        action.payload,
                        { 'Content-Type' : 'application/json'})
                        .map(response => receiveVerifyUser(response))
                        .catch(error => Observable.of(rejectVerifyUser(error)))
    );
export const resetPasswordMailEpic = (action$) =>
    action$.ofType(RESET_PASSWORD_MAIL)
    .mergeMap(action => ajax.post(`${URL_REST}${URL_USER}${URL_USER_RESET_PASSWORD_MAIL}`,
                        action.payload.form,
                        { 'Content-Type' : 'application/json'})
                        .map(response => receiveResetPasswordMail(response))
                        .catch(error => Observable.of(rejectResetPasswordMail(error)))
    );
export const fetchShipmentBankAccEpic = (action$, store) =>
    action$.ofType(FETCH_SHIPMENT_BANK_ACC)
    .mergeMap(action => ajax.get(`${URL_REST}${URL_SHIPMENT}${URL_SHIPMENT_BANK_ACC}`,
                        { 'Content-Type': 'application/json',
                          'Authorization' : `Bearer ${ store.getState().user.session.token}`})
                        .map(response => receiveFetchShipmentBankAcc(response))
                        .catch(err => Observable.of( rejectFetchShipmentBankAcc(err)))
    );
