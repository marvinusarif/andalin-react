import { combineEpics } from 'redux-observable';
import { fetchAllCountriesEpic,
         resetVerificationTimerEpic,
         postVerificationEmailEpic,
         reduceVerificationTimerEpic,
         postContactEpic,
         userVerifyEpic,
         resetPasswordMailEpic,
         fetchShipmentBankAccEpic, } from './page';
import { postUserLoginEpic,
         postUserRegisterEpic,
         fetchUserEpic,
         updateUserEpic,
         resetPasswordEpic,
         updateUserPhotoEpic } from './user';
import { fetchCompanyEpic,
         fetchCompanyUsersEpic,
         fetchCompanyDocsEpic,
         updateCompanyEpic,
         updateCompanyUsersEpic,
         updateCompanyDocsEpic,
         downloadCompanyDocEpic,
         postCompanyUsersInviteMailEpic,
         postCompanyUsersJoinEpic } from './company';
import { fetchCountriesDestinationEpic,
         fetchCountriesOriginEpic,
         fetchCitiesDestinationEpic,
         fetchCitiesOriginEpic,
         postQueryEpic } from './search';
import { postQuoteEpic,
         fetchQuotesEpic,
         deleteQuoteEpic } from './quote';
import { fetchBookValidateQuoteEpic,
         fetchBookOrderEpic } from './book';
import { fetchCurrencyRateEpic } from './currency';
import { fetchShipmentsEpic,
         fetchShipmentDetailEpic,
         fetchShipmentPriceEpic,
         fetchShipmentDocumentEpic,
         fetchShipmentCarrierEpic,
         fetchShipmentHistoryEpic,
         updateShipmentShipperConsigneeEpic,
         updateShipmentOrderStatEpic,
         updateShipmentCommoditiesEpic,
         updateShipmentCarrierEpic,
         updateShipmentFreightEpic,
         updateShipmentOrderRequestEpic,
         updateShipmentOrderPriceEpic,
         postUpdateShipmentDocsEpic,
         downloadShipmentDocsEpic,
         downloadShipmentInvoiceEpic
       } from './shipment';
import {
        postSubscribeChannelEpic,
        fetchSubscribersByShipmentEpic,
        postShipmentMessageEpic,
        fetchShipmentMessagesHeadEpic,
        fetchShipmentMessagesBodyEpic,
        fetchUnreadNotificationsEpic,
        updateNotificationsEpic
       } from './message';
import {
        joinNewSubscribedChannelEpic,
        receiveWebsocketUpdateShipmentDocumentEpic,
        receiveWebsocketUpdateShipmentMessageEpic } from './websocket'
export default combineEpics(
  /**
  */
  fetchAllCountriesEpic,
  postVerificationEmailEpic,
  resetVerificationTimerEpic,
  reduceVerificationTimerEpic,
  postContactEpic,
  userVerifyEpic,
  resetPasswordMailEpic,
  fetchShipmentBankAccEpic,
  /**
    user
  */
  postUserLoginEpic,
  postUserRegisterEpic,
  fetchUserEpic,
  updateUserEpic,
  resetPasswordEpic,
  updateUserPhotoEpic,
  /**
    company
  */
  fetchCompanyEpic,
  fetchCompanyUsersEpic,
  fetchCompanyDocsEpic,
  updateCompanyEpic,
  updateCompanyUsersEpic,
  updateCompanyDocsEpic,
  downloadCompanyDocEpic,
  postCompanyUsersInviteMailEpic,
  postCompanyUsersJoinEpic,
  /**
    search Epics
  */
  fetchCountriesDestinationEpic,
  fetchCountriesOriginEpic,
  fetchCitiesDestinationEpic,
  fetchCitiesOriginEpic,
  postQueryEpic,
  /**
    quote Epics
  */
  postQuoteEpic,
  fetchQuotesEpic,
  deleteQuoteEpic,
  /**
    book Epics
  */
  fetchBookValidateQuoteEpic,
  fetchBookOrderEpic,
  /**
    currency Epics
  */
  fetchCurrencyRateEpic,
  /**
    shipments Epics
  */
  fetchShipmentsEpic,
  fetchShipmentDetailEpic,
  fetchShipmentPriceEpic,
  fetchShipmentDocumentEpic,
  fetchShipmentCarrierEpic,
  fetchShipmentHistoryEpic,

  updateShipmentShipperConsigneeEpic,
  updateShipmentOrderStatEpic,
  updateShipmentCommoditiesEpic,
  updateShipmentCarrierEpic,
  updateShipmentFreightEpic,
  updateShipmentOrderRequestEpic,
  updateShipmentOrderPriceEpic,

  postUpdateShipmentDocsEpic,
  downloadShipmentDocsEpic,
  downloadShipmentInvoiceEpic,
  /**
    message Epics
  */
  postSubscribeChannelEpic,
  fetchSubscribersByShipmentEpic,
  postShipmentMessageEpic,
  fetchShipmentMessagesHeadEpic,
  fetchShipmentMessagesBodyEpic,
  fetchUnreadNotificationsEpic,
  updateNotificationsEpic,
  /**
    websocket Epics
  */
  joinNewSubscribedChannelEpic,
  receiveWebsocketUpdateShipmentDocumentEpic,
  receiveWebsocketUpdateShipmentMessageEpic
);
