import { URL_REST, URL_USER, URL_USER_LOGIN, URL_USER_REGISTER, URL_USER_RESET_PASSWORD, URL_FILES, URL_USER_PHOTO_UPLOAD,
        POST_USER_LOGIN,
        POST_USER_REGISTER,
        FETCH_USER,
        UPDATE_USER,
        UPDATE_USER_PHOTO,
        RESET_PASSWORD } from '../actions';
import { ajax } from 'rxjs/observable/dom/ajax';
import { receiveUserLogin, rejectUserLogin,
         receiveUserRegister, rejectUserRegister,
         receiveFetchUser, rejectFetchUser,
         receiveUpdateUser, rejectUpdateUser,
         receiveResetPassword, rejectResetPassword,
         receiveUpdateUserPhoto, rejectUpdateUserPhoto } from '../actions/user';
import { showNotifications } from '../actions/notifications';
import { Observable } from 'rxjs';
import axios from 'axios';

const _axios = {
    upload : (url, formdata, store) => {
      const request = axios.post( url,
                                 formdata,
                                 { headers : {'Content-Type' : 'multipart/form-data',
                                              'Authorization' : `Bearer ${store.getState().user.session.token}`} })
                      .then(response => response)
      return Observable.from(request);
    }
  }

export const postUserLoginEpic = (action$) =>
    action$.ofType(POST_USER_LOGIN)
    .mergeMap(action => ajax.post(`${URL_REST}${URL_USER}${URL_USER_LOGIN}`,
                          action.payload.form,
                        { 'Content-Type': 'application/json' })
                        .map(response => receiveUserLogin(response))
                        .catch(error => Observable.of(rejectUserLogin(error)))
    );

export const postUserRegisterEpic = (action$) =>
    action$.ofType(POST_USER_REGISTER)
    .mergeMap(action => ajax.post(`${URL_REST}${URL_USER}${URL_USER_REGISTER}`,
                        action.payload.form,
                        { 'Content-Type': 'application/json' })
                        .map(response => receiveUserRegister(response))
                        .catch(error => Observable.of(rejectUserRegister(error)))
    );
export const fetchUserEpic = (action$, store) =>
    action$.ofType(FETCH_USER)
    .mergeMap(action => ajax.get(`${URL_REST}${URL_USER}`,
                        { 'Content-Type': 'application/json',
                          'Authorization' : `Bearer ${ store.getState().user.session.token}`})
                        .map(response => receiveFetchUser(response))
                        .catch(error => Observable.of(rejectFetchUser(error)))
    );
export const updateUserEpic = (action$, store) =>
    action$.ofType(UPDATE_USER)
    .mergeMap(action => ajax.put(`${URL_REST}${URL_USER}`,
                        action.payload.form,
                        { 'Content-Type': 'application/json',
                          'Authorization' : `Bearer ${ store.getState().user.session.token}`})
                        .flatMap(response => Observable.concat(
                          Observable.of(receiveUpdateUser(response)),
                          Observable.of(showNotifications({
                            title: store.getState().polyglot.phrases.notifications.user.update.title,
                            message: store.getState().polyglot.phrases.notifications.user.update.message,
                            position: 'br',
                            autoDismiss: 3
                          }, 'success'))
                        ))
                        .catch(error => Observable.concat(
                          Observable.of(rejectUpdateUser(error)),
                          Observable.of(showNotifications({
                            title : store.getState().polyglot.phrase.error.title,
                            message : !error.xhr.response ? store.getState().polyglot.phrases.error.message.connection_lost : store.getState().polyglot.phrases.error[error.xhr.response],
                          }, 'error'))))
    );
export const resetPasswordEpic = (action$) =>
action$.ofType(RESET_PASSWORD)
.mergeMap(action => ajax.post(`${URL_REST}${URL_USER}${URL_USER_RESET_PASSWORD}`,
                    action.payload.form,
                    { 'Content-Type': 'application/json' })
                    .map(response => receiveResetPassword(response))
                    .catch(error => Observable.of(rejectResetPassword(error)))
);

export const updateUserPhotoEpic = (action$,store) =>
    action$.ofType(UPDATE_USER_PHOTO)
    .mergeMap(action => _axios.upload(`${URL_REST}${URL_FILES}${URL_USER}${URL_USER_PHOTO_UPLOAD}`,
                                      action.payload.form, store)
                        .map(response => receiveUpdateUserPhoto(response))
                        .catch(error => Observable.of(rejectUpdateUserPhoto(error)))
    );
