import { URL_REST, URL_CURRENCY, FETCH_CURRENCY_RATE} from '../actions';
import { ajax } from 'rxjs/observable/dom/ajax';
import { receiveFetchCurrencyRate, rejectFetchCurrencyRate } from '../actions/currency';
import { Observable } from 'rxjs';

export const fetchCurrencyRateEpic = (action$, store) =>
    action$.ofType(FETCH_CURRENCY_RATE)
    .mergeMap(action => ajax.get(`${URL_REST}${URL_CURRENCY}`,
                        { 'Content-Type': 'application/json',
                          'Authorization' : `Bearer ${ store.getState().user.session.token}`})
                        .map(response => receiveFetchCurrencyRate(response))
                        .catch(err => Observable.of( rejectFetchCurrencyRate(err)))
    );
