import { RECEIVE_POST_SUBSCRIBE_SHIPMENT_CHANNEL } from '../actions'
import { RECEIVE_WEBSOCKET_UPDATE_SHIPMENT_DOCUMENTS, RECEIVE_WEBSOCKET_UPDATE_SHIPMENT_MESSAGES } from '../actions/websocket'
import { fetchShipmentMessagesHead } from '../actions/message'
import { updateShipmentMessage, noUpdateShipmentMessage, socketJoinRoom } from '../actions/websocket'
import { Observable } from 'rxjs'

export const joinNewSubscribedChannelEpic = (action$, store) =>
      action$.ofType(RECEIVE_POST_SUBSCRIBE_SHIPMENT_CHANNEL)
      .flatMap((action) => Observable.concat(
                              Observable.of(socketJoinRoom(store.getState().user.session.token, action.payload.response.orderNumber)),
                              Observable.of(fetchShipmentMessagesHead({shipmentId : action.payload.response.shipmentId, orderNumber : action.payload.response.orderNumber}))
                            ));

export const receiveWebsocketUpdateShipmentDocumentEpic = (action$, store) =>
  action$.ofType(RECEIVE_WEBSOCKET_UPDATE_SHIPMENT_DOCUMENTS)
    .map( action => action.payload.response.message.sender.username !== store.getState().user.profile.username ?
    updateShipmentMessage({
      type : 'document',
      response : {
        message : action.payload.response.message,
        notification : action.payload.response.notification
      }
    }) : noUpdateShipmentMessage())

export const receiveWebsocketUpdateShipmentMessageEpic = (action$, store) =>
  action$.ofType(RECEIVE_WEBSOCKET_UPDATE_SHIPMENT_MESSAGES)
    .map( action => action.payload.response.message.sender.username !== store.getState().user.profile.username ?
    updateShipmentMessage({
      type : 'message',
      response : {
        message : action.payload.response.message,
        notification : action.payload.response.notification
      }
    }) : noUpdateShipmentMessage())
