import { URL_REST, URL_COMPANY, URL_COMPANY_USERS, URL_COMPANY_DOCS, URL_COMPANY_UPLOAD_DOC_SINGLE, URL_COMPANY_DOWNLOAD_DOC, URL_FILES, URL_COMPANY_USERS_JOIN,
        FETCH_COMPANY, UPDATE_COMPANY, UPDATE_COMPANY_USERS, UPDATE_COMPANY_DOCS,
        POST_COMPANY_USERS_INVITE_MAIL, POST_COMPANY_USERS_JOIN, DOWNLOAD_COMPANY_DOC } from '../actions';
import { ajax } from 'rxjs/observable/dom/ajax';
import { receiveFetchCompany, rejectFetchCompany,
         receiveFetchCompanyUsers, rejectFetchCompanyUsers,
         receiveFetchCompanyDocs, rejectFetchCompanyDocs,
         receiveUpdateCompany, rejectUpdateCompany,
         receiveUpdateCompanyUsers, rejectUpdateCompanyUsers,
         receiveUpdateCompanyDocs, rejectUpdateCompanyDocs, updateProgressBarUpdateCompanyDocs,
         receivePostCompanyUsersInviteMail, rejectPostCompanyUsersInviteMail,
         receivePostCompanyUsersJoin, rejectPostCompanyUsersJoin,
         receiveDownloadCompanyDoc, rejectDownloadCompanyDoc } from '../actions/company';
import { showNotifications } from '../actions/notifications';
import { Observable } from 'rxjs';
import axios from 'axios';
const fileSaver = require('file-saver');

const _axios = {
    upload : (url, formdata, store) => {
      const request = axios.post( url,
                                 formdata,
                                 { headers : {'Content-Type' : 'multipart/form-data',
                                              'Authorization' : `Bearer ${store.getState().user.session.token}`},
                                  onUploadProgress: progressEvent => {
                                        const percentCompleted = Math.floor((progressEvent.loaded * 100) / progressEvent.total);
                                        store.dispatch(updateProgressBarUpdateCompanyDocs(percentCompleted))
                               }})
                      .then(response => response)
      return Observable.from(request);
    },
    download : (url, fileName, store) => {
      const request = axios({
                                url,
                                method : 'get',
                                responseType : 'blob',
                                headers : {'Content-Type' : 'application/json',
                                  'Authorization' : `Bearer ${store.getState().user.session.token}`}
                      }).then(response => {
                        const downloadedFile = new Blob([response.data]);
                        fileSaver.saveAs(downloadedFile,`${fileName.saveAsFileName}${fileName.extFileName}` );
                        return response;
                      })
      return Observable.from(request);
    }
  };

export const fetchCompanyEpic = (action$, store) =>
    action$.ofType(FETCH_COMPANY)
    .mergeMap(action => ajax.get(`${URL_REST}${URL_COMPANY}`,
                        { 'Content-Type': 'application/json',
                          'Authorization' : `Bearer ${ store.getState().user.session.token}`})
                        .map(response => receiveFetchCompany(response))
                        .catch(error => Observable.of(rejectFetchCompany(error)))
    );

export const fetchCompanyUsersEpic = (action$, store) =>
    action$.ofType(FETCH_COMPANY)
    .mergeMap(action => ajax.get(`${URL_REST}${URL_COMPANY}${URL_COMPANY_USERS}`,
                        { 'Content-Type': 'application/json',
                          'Authorization' : `Bearer ${ store.getState().user.session.token}`})
                        .map(response => receiveFetchCompanyUsers(response))
                        .catch(error => Observable.of(rejectFetchCompanyUsers(error)))
    );

export const fetchCompanyDocsEpic = (action$, store) =>
    action$.ofType(FETCH_COMPANY)
    .mergeMap(action => ajax.get(`${URL_REST}${URL_COMPANY}${URL_COMPANY_DOCS}`,
                        { 'Content-Type': 'application/json',
                          'Authorization' : `Bearer ${ store.getState().user.session.token}`})
                        .map(response => receiveFetchCompanyDocs(response))
                        .catch(error => Observable.of(rejectFetchCompanyDocs(error)))
    );
    
export const updateCompanyEpic = (action$,store) =>
    action$.ofType(UPDATE_COMPANY)
    .mergeMap(action => ajax.put(`${URL_REST}${URL_COMPANY}`,
                        action.payload.form,
                        { 'Content-Type': 'application/json',
                          'Authorization' : `Bearer ${ store.getState().user.session.token}`})
                        .flatMap(response => Observable.concat(
                          Observable.of(receiveUpdateCompany(response)),
                          Observable.of(showNotifications({
                            title: store.getState().polyglot.phrases.notifications.company.profile.update.title,
                            message: store.getState().polyglot.phrases.notifications.company.profile.update.message,
                            position: 'br',
                            autoDismiss: 3
                          },'success'))
                        ))
                        .catch(error => Observable.concat(
                          Observable.of(rejectUpdateCompany(error)),
                          Observable.of(showNotifications({
                            title: store.getState().polyglot.phrases.error.title,
                            message: !error.xhr.response ? store.getState().polyglot.phrases.error.message.connection_lost : store.getState().polyglot.phrases.error[error.xhr.response],
                            position: 'br',
                            autoDismiss: 3
                          },'error'))))
    );

export const updateCompanyUsersEpic = (action$,store) =>
    action$.ofType(UPDATE_COMPANY_USERS)
    .mergeMap(action => ajax.put(`${URL_REST}${URL_COMPANY}${URL_COMPANY_USERS}`,
                        action.payload.form,
                        { 'Content-Type': 'application/json',
                          'Authorization' : `Bearer ${ store.getState().user.session.token}`})
                        .flatMap(response => Observable.concat(
                          Observable.of(receiveUpdateCompanyUsers(response)),
                          Observable.of(showNotifications({
                            title: store.getState().polyglot.phrases.notifications.company.users.update.title,
                            message: store.getState().polyglot.phrases.notifications.company.users.update.message,
                            position: 'br',
                            autoDismiss: 3
                          },'success'))
                        ))
                        .catch(error => Observable.concat(
                          Observable.of(rejectUpdateCompanyUsers(error)),
                          Observable.of(showNotifications({
                            title: store.getState().polyglot.phrases.error.title,
                            message: !error.xhr.response ? store.getState().polyglot.phrases.error.message.connection_lost : store.getState().polyglot.phrases.error[error.xhr.response],
                            position: 'br',
                            autoDismiss: 3
                          }, 'error'))))
    );

export const updateCompanyDocsEpic = (action$,store) =>
    action$.ofType(UPDATE_COMPANY_DOCS)
    .mergeMap(action => _axios.upload(`${URL_REST}${URL_FILES}${URL_COMPANY}${URL_COMPANY_UPLOAD_DOC_SINGLE}`,
                                      action.payload.form, store)
                        .map(response => receiveUpdateCompanyDocs(response))
                        .catch(error => Observable.of(rejectUpdateCompanyDocs(error)))
    );
export const downloadCompanyDocEpic = (action$, store) =>
    action$.ofType(DOWNLOAD_COMPANY_DOC)
    .mergeMap(action => _axios.download(`${URL_REST}${URL_FILES}${URL_COMPANY}${URL_COMPANY_DOWNLOAD_DOC}/stream/${action.payload.fileName.saveAsFileName}`,
                                      action.payload.fileName, store)
                        .map(response => receiveDownloadCompanyDoc(response))
                        .catch(error => Observable.of(rejectDownloadCompanyDoc(error)))
    );
export const postCompanyUsersInviteMailEpic = (action$,store) =>
  action$.ofType(POST_COMPANY_USERS_INVITE_MAIL)
  .mergeMap( action => ajax.post(`${URL_REST}${URL_COMPANY}${URL_COMPANY_USERS}`,
                       action.payload.form,
                       {'Content-Type': 'application/json',
                         'Authorization' : `Bearer ${ store.getState().user.session.token}`})
                       .map(response => receivePostCompanyUsersInviteMail(response))
                       .catch(error => Observable.of(rejectPostCompanyUsersInviteMail(error)))
    );
export const postCompanyUsersJoinEpic = (action$,store) =>
  action$.ofType(POST_COMPANY_USERS_JOIN)
  .mergeMap( action => ajax.post(`${URL_REST}${URL_COMPANY}${URL_COMPANY_USERS}${URL_COMPANY_USERS_JOIN}`,
                       action.payload.form,
                      {'Content-Type': 'application/json'})
                      .map(response => receivePostCompanyUsersJoin(response))
                      .catch(error => Observable.of(rejectPostCompanyUsersJoin(error)))
    );
