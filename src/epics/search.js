import { URL_REST, URL_SEARCH,
         URL_ROUTE, URL_COUNTRY_DESTINATION, URL_COUNTRY_ORIGIN,
         FETCH_COUNTRIES_DESTINATION, FETCH_COUNTRIES_ORIGIN,
         FETCH_CITIES_DESTINATION, FETCH_CITIES_ORIGIN,
         POST_QUERY } from '../actions';
import { ajax } from 'rxjs/observable/dom/ajax';
import { receiveFetchCountriesDestination, rejectFetchCountriesDestination,
        receiveFetchCountriesOrigin, rejectFetchCountriesOrigin,
         receiveFetchCitiesDestination, rejectFetchCitiesDestination,
         receiveFetchCitiesOrigin, rejectFetchCitiesOrigin,
         receivePostQuery, rejectPostQuery } from '../actions/search';
import { Observable } from 'rxjs';

export const fetchCountriesDestinationEpic = (action$) =>
    action$.ofType(FETCH_COUNTRIES_DESTINATION)
    .mergeMap(action => ajax.get(`${URL_REST}${URL_ROUTE}${URL_COUNTRY_DESTINATION}`)
                          .map(response => receiveFetchCountriesDestination(response))
                          .catch(err =>  Observable.of(rejectFetchCountriesDestination(err)))
      );

export const fetchCitiesDestinationEpic = (action$) =>
      action$.ofType(FETCH_CITIES_DESTINATION)
      .mergeMap(action => ajax.get(`${URL_REST}${URL_ROUTE}${URL_COUNTRY_DESTINATION}/${action.payload.country}`)
                            .map(response => receiveFetchCitiesDestination(response))
                            .catch(err =>  Observable.of(rejectFetchCitiesDestination(err)))
      );

export const fetchCountriesOriginEpic = (action$) =>
      action$.ofType(FETCH_COUNTRIES_ORIGIN)
      .mergeMap(action => ajax.get(`${URL_REST}${URL_ROUTE}${URL_COUNTRY_ORIGIN}`)
                            .map(response => receiveFetchCountriesOrigin(response))
                            .catch(err =>  Observable.of(rejectFetchCountriesOrigin(err)))
      );

export const fetchCitiesOriginEpic = (action$) =>
      action$.ofType(FETCH_CITIES_ORIGIN)
      .mergeMap(action => ajax.get(`${URL_REST}${URL_ROUTE}${URL_COUNTRY_ORIGIN}/${action.payload.country}`)
                            .map(response => receiveFetchCitiesOrigin(response))
                            .catch(err =>  Observable.of(rejectFetchCitiesOrigin(err)))
      );

export const postQueryEpic = (action$, store) =>
    action$.ofType(POST_QUERY)
    .mergeMap(action => ajax.post(`${URL_REST}${URL_SEARCH}`,
                          action.payload.query,
                          { 'Content-Type': 'application/json',
                            'Authorization' : `Bearer ${store.getState().user.session.token}`})
                          .map(response => receivePostQuery(response))
                          .catch(err =>  Observable.of(rejectPostQuery(err)))
      );
