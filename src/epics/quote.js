import { URL_REST, URL_QUOTES,
         POST_QUOTE, FETCH_QUOTES, DELETE_QUOTE } from '../actions';
import { receivePostQuote, rejectPostQuote,
         receiveFetchQuotes, rejectFetchQuotes,
         receiveDeleteQuote, rejectDeleteQuote } from '../actions/quote';
import { showNotifications } from '../actions/notifications';
import { ajax } from 'rxjs/observable/dom/ajax';
import { Observable } from 'rxjs';

export const postQuoteEpic = (action$, store) =>
      action$.ofType(POST_QUOTE)
      .mergeMap(action => ajax.post(`${URL_REST}${URL_QUOTES}`,
                          action.payload,
                          { 'Content-Type': 'application/json',
                            'Authorization' : `Bearer ${store.getState().user.session.token}`})
                          .flatMap(response => Observable.concat(
                              Observable.of(receivePostQuote(response)),
                              Observable.of(showNotifications({
                                title: store.getState().polyglot.phrases.notifications.quote.receive.title,
                                message: store.getState().polyglot.phrases.notifications.quote.receive.message,
                                position: 'br',
                                autoDismiss: 3
                              }, 'success'))
                            ))
                          .catch(error => Observable.concat(
                            Observable.of(rejectPostQuote(error)),
                            Observable.of(showNotifications({
                              title: store.getState().polyglot.phrases.error.title,
                              message: !error.xhr.response ? store.getState().polyglot.phrases.error.message.connection_lost : store.getState().polyglot.phrases.error[error.xhr.response],
                              position: 'br',
                              autoDismiss: 3
                            },'error'))))
      );

export const fetchQuotesEpic = (action$, store) =>
    action$.ofType(FETCH_QUOTES)
    .mergeMap(action => ajax.get(`${URL_REST}${URL_QUOTES}/${action.payload.pageState ? action.payload.pageState : ''}`,
                        { 'Content-Type' : 'application/json',
                          'Authorization' : `Bearer ${store.getState().user.session.token}`})
                        .map(response => receiveFetchQuotes(response))
                        .catch(error => Observable.of( rejectFetchQuotes(error) ))
    );

export const deleteQuoteEpic = (action$, store) =>
    action$.ofType(DELETE_QUOTE)
    .mergeMap(action => ajax.delete(`${URL_REST}${URL_QUOTES}/${action.payload.quoteId}/${action.payload.createdAt}`,
                        { 'Content-Type' : 'application/json',
                          'Authorization' : `Bearer ${store.getState().user.session.token}`})
                        .map(response => receiveDeleteQuote(response))
                        .catch(error => Observable.of( rejectDeleteQuote(error) ))
    );
