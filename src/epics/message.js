import { URL_REST, URL_MESSAGES, URL_MESSAGE_HEAD, URL_MESSAGE_BODY, URL_CHANNELSUBSCRIBER, URL_NOTIFICATIONS,
         UPDATE_NOTIFICATIONS_SHIPMENT_MESSAGE,
         FETCH_UNREAD_NOTIFICATIONS,
         FETCH_SUBSCRIBERS_BY_SHIPMENT,
         POST_SUBSCRIBE_SHIPMENT_CHANNEL,
         POST_SHIPMENT_MESSAGE,
         FETCH_SHIPMENT_MESSAGES_HEAD,
         FETCH_SHIPMENT_MESSAGES_BODY } from '../actions';
import { ajax } from 'rxjs/observable/dom/ajax';
import { receivePostShipmentMessage, rejectPostShipmentMessage,
         receiveFetchShipmentMessagesHead, rejectFetchShipmentMessagesHead,
         receiveFetchShipmentMessagesBody, rejectFetchShipmentMessagesBody,
         receiveFetchSubscribersByShipment, rejectFetchSubscribersByShipment,
         receivePostSubscribeChannel, rejectPostSubscribeChannel,
         receiveFetchUnreadNotifications, rejectFetchUnreadNotifications,
         receiveUpdateNotifications, rejectUpdateNotifications } from '../actions/message';
import { Observable } from 'rxjs';

export const fetchUnreadNotificationsEpic = (action$, store) =>
      action$.ofType(FETCH_UNREAD_NOTIFICATIONS)
      .mergeMap(action => ajax.get(`${URL_REST}${URL_MESSAGES}${URL_NOTIFICATIONS}`,
                          { 'Content-Type': 'application/json',
                            'Authorization' : `Bearer ${ store.getState().user.session.token}`})
                          .map(response => receiveFetchUnreadNotifications(response))
                          .catch(err => Observable.of( rejectFetchUnreadNotifications(err)))
      );
export const updateNotificationsEpic = (action$, store) =>
    action$.ofType(UPDATE_NOTIFICATIONS_SHIPMENT_MESSAGE)
    .mergeMap(action => ajax.put(`${URL_REST}${URL_MESSAGES}${URL_NOTIFICATIONS}/${action.payload.shipmentId}`,
                        action.payload.form,
                        { 'Content-Type': 'application/json',
                          'Authorization' : `Bearer ${ store.getState().user.session.token}`})
                        .map(response => receiveUpdateNotifications({response, shipmentId : action.payload.shipmentId}))
                        .catch(err => Observable.of( rejectUpdateNotifications({error : err, shipmentId : action.payload.shipmentId})))
    );
export const fetchSubscribersByShipmentEpic = (action$, store) =>
      action$.ofType(FETCH_SUBSCRIBERS_BY_SHIPMENT)
      .mergeMap(action => ajax.get(`${URL_REST}${URL_MESSAGES}${URL_CHANNELSUBSCRIBER}/${action.payload.shipmentId}/${action.payload.orderNumber.split('/').join('-')}`,
                          { 'Content-Type': 'application/json',
                            'Authorization' : `Bearer ${ store.getState().user.session.token}`})
                          .map(response => receiveFetchSubscribersByShipment({shipmentId : action.payload.shipmentId, response : response.response }))
                          .catch(err => Observable.of( rejectFetchSubscribersByShipment({error : err, shipmentId : action.payload.shipmentId})))
      );
export const postSubscribeChannelEpic = (action$, store) =>
    action$.ofType(POST_SUBSCRIBE_SHIPMENT_CHANNEL)
    .mergeMap(action => ajax.post(`${URL_REST}${URL_MESSAGES}${URL_CHANNELSUBSCRIBER}`,
                          action.payload.form,
                          { 'Content-Type': 'application/json',
                            'Authorization' : `Bearer ${store.getState().user.session.token}`})
                          .map(response => receivePostSubscribeChannel(response))
                          .catch(err =>  Observable.of(rejectPostSubscribeChannel(err)))
      );

export const postShipmentMessageEpic = (action$, store) =>
    action$.ofType(POST_SHIPMENT_MESSAGE)
    .mergeMap(action => ajax.post(`${URL_REST}${URL_MESSAGES}`,
                          action.payload.form,
                          { 'Content-Type': 'application/json',
                            'Authorization' : `Bearer ${store.getState().user.session.token}`})
                          .map(response => receivePostShipmentMessage(response))
                          .catch(err =>  Observable.of(rejectPostShipmentMessage(err)))
      );

export const fetchShipmentMessagesHeadEpic = (action$, store) =>
    action$.ofType(FETCH_SHIPMENT_MESSAGES_HEAD)
    .mergeMap(action => ajax.get(`${URL_REST}${URL_MESSAGES}${URL_MESSAGE_HEAD}/${action.payload.shipmentId}/${action.payload.orderNumber.split('/').join('-')}`,
                        { 'Content-Type': 'application/json',
                          'Authorization' : `Bearer ${ store.getState().user.session.token}`})
                        .map(response => receiveFetchShipmentMessagesHead({response, shipmentId : action.payload.shipmentId}))
                        .catch(err => Observable.of( rejectFetchShipmentMessagesHead({ error : err, shipmentId : action.payload.shipmentId})))
    );

export const fetchShipmentMessagesBodyEpic = (action$, store) =>
      action$.ofType(FETCH_SHIPMENT_MESSAGES_BODY)
      .mergeMap(action => ajax.get(`${URL_REST}${URL_MESSAGES}${URL_MESSAGE_BODY}/${action.payload.shipmentId}/${action.payload.orderNumber.split('/').join('-')}/${action.payload.messageDateBucket}`,
                        { 'Content-Type': 'application/json',
                          'Authorization' : `Bearer ${ store.getState().user.session.token}`})
                        .map(response => receiveFetchShipmentMessagesBody({response, shipmentId : action.payload.shipmentId, messageDateBucket : action.payload.messageDateBucket}))
                        .catch(err => Observable.of( rejectFetchShipmentMessagesBody({ error : err, shipmentId : action.payload.shipmentId, messageDateBucket : action.payload.messageDateBucket})))
    );
