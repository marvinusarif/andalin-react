import { SWITCH_LANGUAGE } from './';

export const setLanguage = (locale='en') => {
  return {
    type : SWITCH_LANGUAGE,
    payload : { locale }
  }
}
