import { FETCH_COMPANY, RECEIVE_FETCH_COMPANY, REJECT_FETCH_COMPANY,
         FETCH_COMPANY_USERS, RECEIVE_FETCH_COMPANY_USERS, REJECT_FETCH_COMPANY_USERS,
         FETCH_COMPANY_DOCS, RECEIVE_FETCH_COMPANY_DOCS, REJECT_FETCH_COMPANY_DOCS,
         UPDATE_COMPANY, RECEIVE_UPDATE_COMPANY, REJECT_UPDATE_COMPANY,
         UPDATE_COMPANY_USERS, RECEIVE_UPDATE_COMPANY_USERS, REJECT_UPDATE_COMPANY_USERS,
         UPDATE_COMPANY_DOCS, RECEIVE_UPDATE_COMPANY_DOCS, REJECT_UPDATE_COMPANY_DOCS,UPDATE_PROGRESS_BAR_UPLOAD_COMPANY_DOCS,
         POST_COMPANY_USERS_INVITE_MAIL, RECEIVE_POST_COMPANY_USERS_INVITE_MAIL, REJECT_POST_COMPANY_USERS_INVITE_MAIL,
         POST_COMPANY_USERS_JOIN, RECEIVE_POST_COMPANY_USERS_JOIN, REJECT_POST_COMPANY_USERS_JOIN,
         DOWNLOAD_COMPANY_DOC, RECEIVE_DOWNLOAD_COMPANY_DOC, REJECT_DOWNLOAD_COMPANY_DOC
       } from './';

export const fetchCompany = (isFetching = true) => {
  return {
    type : FETCH_COMPANY,
    payload : null,
    isFetching
  }
}

export const receiveFetchCompany = (result, isFetching = false) => {
  const { response } = result;
  return {
    type : RECEIVE_FETCH_COMPANY,
    payload : { response },
    isFetching
  }
}

export const rejectFetchCompany = (error, isFetching = false) => {
  const { xhr } = error;
  return {
    type : REJECT_FETCH_COMPANY,
    payload : { xhr },
    isFetching
  }
}

export const updateCompany = (form, isFetching = true) => {
  return {
    type : UPDATE_COMPANY,
    payload : { form },
    isFetching
  }
}

export const receiveUpdateCompany = (result, isFetching = false) => {
  const { response } = result;
  return {
    type : RECEIVE_UPDATE_COMPANY,
    payload : { response },
    isFetching
  }
}

export const rejectUpdateCompany = (error, isFetching = false) => {
  const { xhr } = error;
  return {
    type : REJECT_UPDATE_COMPANY,
    payload : { xhr },
    isFetching
  }
}

export const fetchCompanyUsers = (isFetching = true) => {
  return {
    type : FETCH_COMPANY_USERS,
    payload : null,
    isFetching
  }
}

export const receiveFetchCompanyUsers = (result, isFetching = false) => {
  const { response } = result;
  return {
    type : RECEIVE_FETCH_COMPANY_USERS,
    payload : { response },
    isFetching
  }
}

export const rejectFetchCompanyUsers = (error, isFetching = false) => {
  const { xhr } = error;
  return {
    type : REJECT_FETCH_COMPANY_USERS,
    payload : { xhr },
    isFetching
  }
}
export const updateCompanyUsers = ( form, isFetching = true) => {
  return {
    type : UPDATE_COMPANY_USERS,
    payload : { form },
    isFetching
  }
}

export const receiveUpdateCompanyUsers = (result, isFetching = false) => {
  const { response } = result;
  return {
    type : RECEIVE_UPDATE_COMPANY_USERS,
    payload : { response },
    isFetching
  }
}

export const rejectUpdateCompanyUsers = (error, isFetching = false) => {
  const { xhr } = error;
  return {
    type : REJECT_UPDATE_COMPANY_USERS,
    payload : { xhr },
    isFetching
  }
}

export const fetchCompanyDocs = (isFetching = true) => {
  return {
    type : FETCH_COMPANY_DOCS,
    payload : null,
    isFetching
  }
}

export const receiveFetchCompanyDocs = (result, isFetching = false) => {
  const { response } = result;
  return {
    type : RECEIVE_FETCH_COMPANY_DOCS,
    payload : { response },
    isFetching
  }
}

export const rejectFetchCompanyDocs = (error, isFetching = false) => {
  const { xhr } = error;
  return {
    type : REJECT_FETCH_COMPANY_DOCS,
    payload : { xhr },
    isFetching
  }
}

export const updateCompanyDocs = (form, isFetching = true, isSuccessful=false) => {
  return {
    type : UPDATE_COMPANY_DOCS,
    payload : { form },
    isFetching,
    isSuccessful
  }
}
export const updateProgressBarUpdateCompanyDocs = (percentCompleted) => {
  return {
    type : UPDATE_PROGRESS_BAR_UPLOAD_COMPANY_DOCS,
    payload : { percentCompleted }
  }
}
export const receiveUpdateCompanyDocs = (result, isFetching = false, isSuccessful=true) => {
  const { data } = result;
  const response = data;
  return {
    type : RECEIVE_UPDATE_COMPANY_DOCS,
    payload : { response },
    isFetching,
    isSuccessful
  }
}

export const rejectUpdateCompanyDocs = (error, isFetching = false, isSuccessful=false) => {
  const { data } = error.response;
  const xhr = {
    response : data
  }
  return {
    type : REJECT_UPDATE_COMPANY_DOCS,
    payload : { xhr },
    isFetching,
    isSuccessful
  }
}

export const downloadCompanyDoc = (saveAsFileName, extFileName) => {
  return {
    type : DOWNLOAD_COMPANY_DOC,
    payload : { fileName : { saveAsFileName, extFileName} }
  }
}

export const receiveDownloadCompanyDoc = (result) => {
  const { data } = result;
  const response = data;
  return {
    type : RECEIVE_DOWNLOAD_COMPANY_DOC,
    payload : { response }
  }
}

export const rejectDownloadCompanyDoc = (error) => {
  const { data } = error.response;
  const xhr = {
    response : data
  }
  return {
    type : REJECT_DOWNLOAD_COMPANY_DOC,
    payload : { xhr }
  }
}

export const postCompanyUsersInviteMail = (form, isFetching = true, isSuccessful=false) => {
  return {
    type : POST_COMPANY_USERS_INVITE_MAIL,
    payload : { form },
    isFetching,
    isSuccessful
  }
}

export const receivePostCompanyUsersInviteMail = (result, isFetching = false, isSuccessful = true) => {
  const { response } = result;
  return {
    type : RECEIVE_POST_COMPANY_USERS_INVITE_MAIL,
    payload : { response },
    isFetching,
    isSuccessful
  }
}

export const rejectPostCompanyUsersInviteMail = (error, isFetching = false, isSuccessful = false) => {
  const { xhr } = error;
  return {
    type : REJECT_POST_COMPANY_USERS_INVITE_MAIL,
    payload : { xhr },
    isFetching,
    isSuccessful
  }
}


export const postCompanyUsersJoin = (form, isFetching = true, isSuccessful = false) => {
  return {
    type : POST_COMPANY_USERS_JOIN,
    payload : { form },
    isFetching,
    isSuccessful
  }
}

export const receivePostCompanyUsersJoin = (result, isFetching = false, isSuccessful = true) => {
  const { response } = result;
  return {
    type : RECEIVE_POST_COMPANY_USERS_JOIN,
    payload : { response },
    isFetching,
    isSuccessful
  }
}

export const rejectPostCompanyUsersJoin = (error, isFetching = false, isSuccessful = false) => {
  const { xhr } = error;
  return {
    type : REJECT_POST_COMPANY_USERS_JOIN,
    payload : { xhr },
    isFetching,
    isSuccessful
  }
}
