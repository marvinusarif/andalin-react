import { POST_USER_LOGIN, RECEIVE_POST_USER_LOGIN, REJECT_POST_USER_LOGIN,
         POST_USER_REGISTER, RECEIVE_POST_USER_REGISTER, REJECT_POST_USER_REGISTER,
         FETCH_USER, RECEIVE_FETCH_USER, REJECT_FETCH_USER,
         UPDATE_USER, RECEIVE_UPDATE_USER, REJECT_UPDATE_USER,
         USER_LOGOUT,
         UPDATE_USER_PHOTO, RECEIVE_UPDATE_USER_PHOTO, REJECT_UPDATE_USER_PHOTO,
         RESET_PASSWORD, RECEIVE_RESET_PASSWORD, REJECT_RESET_PASSWORD,
        } from './';

export const postUserLogin = (form, isFetching = true) => {
  return {
    type : POST_USER_LOGIN,
    payload : { form },
    isFetching
  }
}

export const receiveUserLogin = (result, isFetching = false) => {
  const { response } = result;
  return {
    type : RECEIVE_POST_USER_LOGIN,
    payload : { response },
    isFetching,
    isOpen : false
  }
}

export const rejectUserLogin = (error, isFetching = false) => {
  const { xhr } = error;
  return {
    type : REJECT_POST_USER_LOGIN,
    payload : { xhr },
    isFetching
  }
}

export const postUserRegister = (form, isFetching = true, isSuccessful = false) => {
  return {
    type : POST_USER_REGISTER,
    payload : { form },
    isFetching,
    isSuccessful
  }
}

export const receiveUserRegister = (result, isFetching = false, isSuccessful = true) => {
  const { response } = result;
  return {
    type : RECEIVE_POST_USER_REGISTER,
    payload : { response },
    isFetching,
    isSuccessful
  }
}

export const rejectUserRegister = (error, isFetching = false, isSuccessful = false) => {
  const { xhr } = error;
  return {
    type : REJECT_POST_USER_REGISTER,
    payload : { xhr },
    isFetching,
    isSuccessful
  }
}


export const fetchUser = (form, isFetching = true) => {
  return {
    type : FETCH_USER,
    payload : { form },
    isFetching
  }
}

export const receiveFetchUser = (result, isFetching = false) => {
  const { response } = result;
  return {
    type : RECEIVE_FETCH_USER,
    payload : { response },
    isFetching
  }
}

export const rejectFetchUser = (error, isFetching = false) => {
  const { xhr } = error;
  return {
    type : REJECT_FETCH_USER,
    payload : { xhr },
    isFetching
  }
}


export const updateUser = (form, isFetching = true) => {
  return {
    type : UPDATE_USER,
    payload : { form },
    isFetching
  }
}

export const receiveUpdateUser = (result, isFetching = false) => {
  const { response } = result;
  return {
    type : RECEIVE_UPDATE_USER,
    payload : { response },
    isFetching
  }
}

export const rejectUpdateUser = (error, isFetching = false) => {
  const { xhr } = error;
  return {
    type : REJECT_UPDATE_USER,
    payload : { xhr },
    isFetching
  }
}

export const resetPassword = (form, isFetching = true, isSuccessful=false) => {
  return {
    type : RESET_PASSWORD,
    payload : { form },
    isFetching,
    isSuccessful
  }
}

export const receiveResetPassword = (result, isFetching = false,isSuccessful=true) => {
  const { response } = result;
  return {
    type : RECEIVE_RESET_PASSWORD,
    payload : { response },
    isFetching,
    isSuccessful
  }
}

export const rejectResetPassword = (error, isFetching = false,isSuccessful=false) => {
  const { xhr } = error;
  return {
    type : REJECT_RESET_PASSWORD,
    payload : { xhr },
    isFetching,
    isSuccessful
  }
}

export const userLogout = () => {
  return {
    type : USER_LOGOUT,
    payload : null
  }
}


export const updateUserPhoto = (form, isFetching = true, isSuccessful=false) => {
  return {
    type : UPDATE_USER_PHOTO,
    payload : { form },
    isFetching,
    isSuccessful
  }
}

export const receiveUpdateUserPhoto = (result, isFetching = false, isSuccessful=true) => {
  const { data } = result;
  const response = data;
  return {
    type : RECEIVE_UPDATE_USER_PHOTO,
    payload : { response },
    isFetching,
    isSuccessful
  }
}

export const rejectUpdateUserPhoto = (error, isFetching = false, isSuccessful=false) => {
  const { data } = error.response;
  const xhr = {
    response : data
  }
  return {
    type : REJECT_UPDATE_USER_PHOTO,
    payload : { xhr },
    isFetching,
    isSuccessful
  }
}
