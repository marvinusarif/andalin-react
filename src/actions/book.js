import { POST_BOOK_VALIDATE_QUOTE, RECEIVE_POST_BOOK_VALIDATE_QUOTE, REJECT_POST_BOOK_VALIDATE_QUOTE,
         POST_BOOK_ORDER, RECEIVE_POST_BOOK_ORDER, REJECT_POST_BOOK_ORDER } from './';

export const postBookValidateQuote = ({ quote, query}, isValidating=true, isSuccessful = false) => {
  const validity = false;
  return {
    type : POST_BOOK_VALIDATE_QUOTE,
    payload : { quote, query, validity },
    isValidating,
    isSuccessful
  }
}

export const receivePostBookValidateQuote = (result, isValidating=false, isSuccessful = true) => {
  const { response : { quote, query, validity } } = result;
  return {
    type : RECEIVE_POST_BOOK_VALIDATE_QUOTE,
    payload : { quote, query, validity },
    isValidating,
    isSuccessful
  }
}

export const rejectPostBookValidateQuote = (error, isValidating=false, isSuccessful = false) => {
  const { xhr } = error;
  return {
    type : REJECT_POST_BOOK_VALIDATE_QUOTE,
    payload : { xhr },
    isValidating,
    isSuccessful
  }
}


export const postBookOrder = ({ form, quote, query }, isBooking=true, isSuccessful=false) => {
  return {
    type : POST_BOOK_ORDER,
    payload : { form, quote, query },
    isBooking,
    isSuccessful
  }
}

export const receivePostBookOrder = (result, isBooking=false, isSuccessful=true) => {
  const { response } = result;
  return {
    type : RECEIVE_POST_BOOK_ORDER,
    payload : { response },
    isSuccessful,
    isBooking
  }
}

export const rejectPostBookOrder = (error, isBooking=false, isSuccessful=false) => {
  const { xhr } = error;
  return {
    type : REJECT_POST_BOOK_ORDER,
    payload : { xhr },
    isSuccessful,
    isBooking
  }
}
