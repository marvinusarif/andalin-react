import { RESULT_SORT } from './'

export const sortResultBy = (sortBy='orderPriceTotal') => {
  return {
    type : RESULT_SORT,
    payload : { sortBy }
  }
}
