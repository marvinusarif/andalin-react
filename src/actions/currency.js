import { SWITCH_CURRENCY, FETCH_CURRENCY_RATE, RECEIVE_FETCH_CURRENCY_RATE, REJECT_FETCH_CURRENCY_RATE } from './';


export const switchCurrency = (currency) => {
  return {
    type : SWITCH_CURRENCY,
    payload : { currency }
  }
}
export const fetchCurrencyRate = (isFetching=true) => {
  return {
    type : FETCH_CURRENCY_RATE,
    payload : null,
    isFetching
  }
}

export const receiveFetchCurrencyRate = (result, isFetching=false) => {
  const { response } = result;
  return {
    type : RECEIVE_FETCH_CURRENCY_RATE,
    payload : {response},
    isFetching
  }
}

export const rejectFetchCurrencyRate = (error, isFetching = false) => {
  const { xhr } = error;
  return {
    type : REJECT_FETCH_CURRENCY_RATE,
    payload : { xhr }
  }
}
