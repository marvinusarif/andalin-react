import {
  POST_SHIPMENT_MESSAGE, RECEIVE_POST_SHIPMENT_MESSAGE, REJECT_POST_SHIPMENT_MESSAGE,
  FETCH_SHIPMENT_MESSAGES_HEAD, RECEIVE_FETCH_SHIPMENT_MESSAGES_HEAD, REJECT_FETCH_SHIPMENT_MESSAGES_HEAD,
  FETCH_SHIPMENT_MESSAGES_BODY, RECEIVE_FETCH_SHIPMENT_MESSAGES_BODY, REJECT_FETCH_SHIPMENT_MESSAGES_BODY,
  POST_SUBSCRIBE_SHIPMENT_CHANNEL, RECEIVE_POST_SUBSCRIBE_SHIPMENT_CHANNEL, REJECT_POST_SUBSCRIBE_SHIPMENT_CHANNEL,
  FETCH_UNREAD_NOTIFICATIONS, RECEIVE_FETCH_UNREAD_NOTIFICATIONS,  REJECT_FETCH_UNREAD_NOTIFICATIONS,
  FETCH_SUBSCRIBERS_BY_SHIPMENT, RECEIVE_FETCH_SUBSCRIBERS_BY_SHIPMENT, REJECT_FETCH_SUBSCRIBERS_BY_SHIPMENT,
  UPDATE_NOTIFICATIONS_SHIPMENT_MESSAGE, RECEIVE_UPDATE_NOTIFICATIONS_SHIPMENT_MESSAGE, REJECT_UPDATE_NOTIFICATIONS_SHIPMENT_MESSAGE
} from './';

export const postShipmentMessage =(form,isFetching=true) => {
  return {
    type : POST_SHIPMENT_MESSAGE,
    payload : { form }
  }
}

export const receivePostShipmentMessage = (result, isFetching=false) => {
  const { response } = result;
  return {
    type : RECEIVE_POST_SHIPMENT_MESSAGE,
    payload : { response }
  }
}

export const rejectPostShipmentMessage = (error, isFetching=false) => {
  const { xhr } = error;
  return {
    type : REJECT_POST_SHIPMENT_MESSAGE,
    payload : { xhr }
  }
}


export const fetchShipmentMessagesHead =({ shipmentId, orderNumber },isFetching=true) => {
  return {
    type : FETCH_SHIPMENT_MESSAGES_HEAD,
    payload : { shipmentId, orderNumber }
  }
}

export const receiveFetchShipmentMessagesHead = (result, isFetching=false) => {
  const { response : { response }, shipmentId } = result;
  return {
    type : RECEIVE_FETCH_SHIPMENT_MESSAGES_HEAD,
    payload : { response, shipmentId }
  }
}

export const rejectFetchShipmentMessagesHead = ({error, shipmentId}, isFetching=false) => {
  const { xhr } = error;
  return {
    type : REJECT_FETCH_SHIPMENT_MESSAGES_HEAD,
    payload : { xhr, shipmentId }
  }
}


export const fetchShipmentMessagesBody =({ shipmentId, orderNumber, messageDateBucket },isFetching=true) => {
  return {
    type : FETCH_SHIPMENT_MESSAGES_BODY,
    payload : { shipmentId, orderNumber, messageDateBucket }
  }
}

export const receiveFetchShipmentMessagesBody = (result, isFetching=false) => {
  const { response : { response }, shipmentId, messageDateBucket } = result;
  return {
    type : RECEIVE_FETCH_SHIPMENT_MESSAGES_BODY,
    payload : { response, messageDateBucket, shipmentId }
  }
}

export const rejectFetchShipmentMessagesBody = ({error, shipmentId, messageDateBucket}, isFetching=false) => {
  const { xhr } = error;
  return {
    type : REJECT_FETCH_SHIPMENT_MESSAGES_BODY,
    payload : { xhr, shipmentId, messageDateBucket }
  }
}

export const fetchSubscribersByShipment = (shipmentId, orderNumber) => {
  return {
    type : FETCH_SUBSCRIBERS_BY_SHIPMENT,
    payload : {
      shipmentId,
      orderNumber
    }
  }
}

export const receiveFetchSubscribersByShipment = ({response, shipmentId}) => {
  return {
    type : RECEIVE_FETCH_SUBSCRIBERS_BY_SHIPMENT,
    payload : {
      response,
      shipmentId
    }
  }
}

export const rejectFetchSubscribersByShipment = ({error, shipmentId}) => {
  const { xhr } = error
  return {
    type : REJECT_FETCH_SUBSCRIBERS_BY_SHIPMENT,
    payload : {
      xhr,
      shipmentId
    }
  }
}
export const postSubscribeChannel = (shipmentId, orderNumber) => {
  return {
    type : POST_SUBSCRIBE_SHIPMENT_CHANNEL,
    payload : {
      form : {
        shipmentId, orderNumber
      }
    }
  }
}

export const receivePostSubscribeChannel = ({response}) => {
  return {
    type : RECEIVE_POST_SUBSCRIBE_SHIPMENT_CHANNEL,
    payload : {
      response
    }
  }
}

export const rejectPostSubscribeChannel = (error) => {
  const { xhr } = error
  return {
    type : REJECT_POST_SUBSCRIBE_SHIPMENT_CHANNEL,
    payload : {
        xhr
    }
  }
}

export const fetchUnreadNotifications = () => {
  return {
    type : FETCH_UNREAD_NOTIFICATIONS
  }
}

export const receiveFetchUnreadNotifications = ({response}) => {
  return {
    type : RECEIVE_FETCH_UNREAD_NOTIFICATIONS,
    payload : {
      response
    }
  }
}

export const rejectFetchUnreadNotifications = (err) => {
  const { xhr } = err
  return {
    type : REJECT_FETCH_UNREAD_NOTIFICATIONS,
    payload : {
      xhr
    }
  }
}

export const updateNotifications = (shipmentId, notifications) => {
  return {
    type : UPDATE_NOTIFICATIONS_SHIPMENT_MESSAGE,
    payload : {
      shipmentId,
      form : {
        notifications
      }
    }
  }
}

export const receiveUpdateNotifications = ({response, shipmentId}) => {
  return {
    type : RECEIVE_UPDATE_NOTIFICATIONS_SHIPMENT_MESSAGE,
    payload : {
      response,
      shipmentId
    }
  }
}

export const rejectUpdateNotifications = (err) => {
  const { xhr } = err
  return {
    type : REJECT_UPDATE_NOTIFICATIONS_SHIPMENT_MESSAGE,
    payload : {
      xhr
    }
  }
}
