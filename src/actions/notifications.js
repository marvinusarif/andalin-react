import { RNS_SHOW_NOTIFICATION, RNS_HIDE_NOTIFICATION, RNS_REMOVE_ALL_NOTIFICATIONS } from './';

export const showNotifications = (opts = {}, level = 'success') => {
  return {
    type: RNS_SHOW_NOTIFICATION,
    ...opts,
    uid: opts.uid || Date.now(),
    level
  };
}

export function hide(uid) {
  return {
    type: RNS_HIDE_NOTIFICATION,
    uid
  };
}

export function removeAll() {
  return { type: RNS_REMOVE_ALL_NOTIFICATIONS };
}
