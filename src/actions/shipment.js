import { FETCH_SHIPMENTS, RECEIVE_FETCH_SHIPMENTS, REJECT_FETCH_SHIPMENTS,
         FETCH_SHIPMENT_DETAIL, RECEIVE_FETCH_SHIPMENT_DETAIL, REJECT_FETCH_SHIPMENT_DETAIL,
         FETCH_SHIPMENT_PRICE, RECEIVE_FETCH_SHIPMENT_PRICE, REJECT_FETCH_SHIPMENT_PRICE,
         FETCH_SHIPMENT_CARRIER, RECEIVE_FETCH_SHIPMENT_CARRIER, REJECT_FETCH_SHIPMENT_CARRIER,
         FETCH_SHIPMENT_DOCUMENT, RECEIVE_FETCH_SHIPMENT_DOCUMENT, REJECT_FETCH_SHIPMENT_DOCUMENT,
         FETCH_SHIPMENT_HISTORY, RECEIVE_FETCH_SHIPMENT_HISTORY, REJECT_FETCH_SHIPMENT_HISTORY,
         UPDATE_SHIPMENT_SHIPPER_CONSIGNEE, RECEIVE_UPDATE_SHIPMENT_SHIPPER_CONSIGNEE, REJECT_UPDATE_SHIPMENT_SHIPPER_CONSIGNEE,
         UPDATE_SHIPMENT_ORDER_STAT, RECEIVE_UPDATE_SHIPMENT_ORDER_STAT, REJECT_UPDATE_SHIPMENT_ORDER_STAT,
         UPDATE_SHIPMENT_COMMODITIES, RECEIVE_UPDATE_SHIPMENT_COMMODITIES, REJECT_UPDATE_SHIPMENT_COMMODITIES,
         UPDATE_SHIPMENT_CARRIER, RECEIVE_UPDATE_SHIPMENT_CARRIER, REJECT_UPDATE_SHIPMENT_CARRIER,
         UPDATE_SHIPMENT_FREIGHT, RECEIVE_UPDATE_SHIPMENT_FREIGHT, REJECT_UPDATE_SHIPMENT_FREIGHT,
         UPDATE_SHIPMENT_ORDER_REQUEST, RECEIVE_UPDATE_SHIPMENT_ORDER_REQUEST, REJECT_UPDATE_SHIPMENT_ORDER_REQUEST,
         UPDATE_SHIPMENT_ORDER_PRICE, RECEIVE_UPDATE_SHIPMENT_ORDER_PRICE, REJECT_UPDATE_SHIPMENT_ORDER_PRICE,
         POST_UPDATE_SHIPMENT_DOCUMENT, RECEIVE_POST_UPDATE_SHIPMENT_DOCUMENT, REJECT_POST_UPDATE_SHIPMENT_DOCUMENT, UPDATE_PROGRESS_BAR_UPLOAD_SHIPMENT_DOCS,
         DOWNLOAD_SHIPMENT_DOC, RECEIVE_DOWNLOAD_SHIPMENT_DOC, REJECT_DOWNLOAD_SHIPMENT_DOC,
         DOWNLOAD_SHIPMENT_INVOICE, RECEIVE_DOWNLOAD_SHIPMENT_INVOICE, REJECT_DOWNLOAD_SHIPMENT_INVOICE,
       } from '../actions';

export const fetchShipments = (isFetching=true) => {
  return {
    type : FETCH_SHIPMENTS,
    payload : null,
    isFetching
  }
}

export const receiveFetchShipments = (result,isFetching=false) => {
  const { response } = result;
  return {
    type : RECEIVE_FETCH_SHIPMENTS,
    payload : {response},
    isFetching
  }
}

export const rejectFetchShipments = (error,isFetching=false) => {
  const { xhr } = error;
  return {
    type : REJECT_FETCH_SHIPMENTS,
    payload : {xhr},
    isFetching
  }
}

export const fetchShipmentDetail = ({shipmentId, orderNumber}, isFetching=true) => {
  return {
    type : FETCH_SHIPMENT_DETAIL,
    payload : {shipmentId, orderNumber},
    isFetching
  }
}

export const receiveFetchShipmentDetail = (result,isFetching=false) => {
  const { response : { response }, shipmentId } = result;
  return {
    type : RECEIVE_FETCH_SHIPMENT_DETAIL,
    payload : {response, shipmentId},
    isFetching
  }
}

export const rejectFetchShipmentDetail = ({error, shipmentId},isFetching=false) => {
  const { xhr } = error;
  return {
    type : REJECT_FETCH_SHIPMENT_DETAIL,
    payload : {shipmentId, xhr},
    isFetching
  }
}

export const fetchShipmentPrice = ({shipmentId, orderNumber}, isFetching=true) => {
  return {
    type : FETCH_SHIPMENT_PRICE,
    payload : {shipmentId, orderNumber},
    isFetching
  }
}

export const receiveFetchShipmentPrice = (result,isFetching=false) => {
  const { response : { response }, shipmentId } = result;
  return {
    type : RECEIVE_FETCH_SHIPMENT_PRICE,
    payload : {response, shipmentId},
    isFetching
  }
}

export const rejectFetchShipmentPrice = ({error, shipmentId},isFetching=false) => {
  const { xhr } = error;
  return {
    type : REJECT_FETCH_SHIPMENT_PRICE,
    payload : {shipmentId, xhr},
    isFetching
  }
}

export const fetchShipmentDocument = ({shipmentId, orderNumber}, isFetching=true) => {
  return {
    type : FETCH_SHIPMENT_DOCUMENT,
    payload : {shipmentId, orderNumber},
    isFetching
  }
}

export const receiveFetchShipmentDocument = (result,isFetching=false) => {
  const { response : { response }, shipmentId } = result;
  return {
    type : RECEIVE_FETCH_SHIPMENT_DOCUMENT,
    payload : {response, shipmentId},
    isFetching
  }
}

export const rejectFetchShipmentDocument = ({error, shipmentId},isFetching=false) => {
  const { xhr } = error;
  return {
    type : REJECT_FETCH_SHIPMENT_DOCUMENT,
    payload : {shipmentId, xhr},
    isFetching
  }
}

export const fetchShipmentCarrier = ({shipmentId, orderNumber}, isFetching=true) => {
  return {
    type : FETCH_SHIPMENT_CARRIER,
    payload : {shipmentId, orderNumber},
    isFetching
  }
}

export const receiveFetchShipmentCarrier = (result,isFetching=false) => {
  const { response : { response }, shipmentId } = result;
  return {
    type : RECEIVE_FETCH_SHIPMENT_CARRIER,
    payload : {response, shipmentId},
    isFetching
  }
}

export const rejectFetchShipmentCarrier = ({error, shipmentId},isFetching=false) => {
  const { xhr } = error;
  return {
    type : REJECT_FETCH_SHIPMENT_CARRIER,
    payload : {shipmentId, xhr},
    isFetching
  }
}


export const fetchShipmentHistory = ({shipmentId, orderNumber}, isFetching=true) => {
  return {
    type : FETCH_SHIPMENT_HISTORY,
    payload : {shipmentId, orderNumber},
    isFetching
  }
}

export const receiveFetchShipmentHistory = (result,isFetching=false) => {
  const { response : { response }, shipmentId } = result;
  return {
    type : RECEIVE_FETCH_SHIPMENT_HISTORY,
    payload : {response, shipmentId},
    isFetching
  }
}

export const rejectFetchShipmentHistory = ({error, shipmentId},isFetching=false) => {
  const { xhr } = error;
  return {
    type : REJECT_FETCH_SHIPMENT_HISTORY,
    payload : {shipmentId, xhr},
    isFetching
  }
}

export const updateShipmentShipperConsignee = ({shipmentId,form,orderNumber}, isFetching=true, isSuccessful=false) => {
  return {
    type : UPDATE_SHIPMENT_SHIPPER_CONSIGNEE,
    payload : { shipmentId, form, orderNumber},
    isFetching,
    isSuccessful
  }
}

export const receiveShipmentShipperConsignee = (result, isFetching=false, isSuccessful=true) => {
  const { response : { response }, shipmentId } = result;
  return {
    type : RECEIVE_UPDATE_SHIPMENT_SHIPPER_CONSIGNEE,
    payload : { response, shipmentId },
    isFetching,
    isSuccessful
  }
}

export const rejectShipmentShipperConsignee = ({error, shipmentId }, isFetching=false, isSuccessful=false) => {
  const { xhr } = error;
  return {
    type : REJECT_UPDATE_SHIPMENT_SHIPPER_CONSIGNEE,
    payload : { shipmentId, xhr },
    isFetching,
    isSuccessful
  }
}

export const updateShipmentOrderStat = ({shipmentId,form,orderNumber}, isFetching=true, isSuccessful=false) => {
  return {
    type : UPDATE_SHIPMENT_ORDER_STAT,
    payload : { shipmentId, form, orderNumber},
    isFetching,
    isSuccessful
  }
}

export const receiveShipmentOrderStat = (result, isFetching=false, isSuccessful=true) => {
  const { response : { response }, shipmentId } = result;
  return {
    type : RECEIVE_UPDATE_SHIPMENT_ORDER_STAT,
    payload : { response, shipmentId },
    isFetching,
    isSuccessful
  }
}

export const rejectShipmentOrderStat = ({error, shipmentId }, isFetching=false, isSuccessful=false) => {
  const { xhr } = error;
  return {
    type : REJECT_UPDATE_SHIPMENT_ORDER_STAT,
    payload : { shipmentId, xhr },
    isFetching,
    isSuccessful
  }
}

export const updateShipmentCommodities = ({shipmentId,form,orderNumber}, isFetching=true, isSuccessful=false) => {
  return {
    type : UPDATE_SHIPMENT_COMMODITIES,
    payload : { shipmentId, form, orderNumber},
    isFetching,
    isSuccessful
  }
}

export const receiveShipmentCommodities = (result, isFetching=false, isSuccessful=true) => {
  const { response : { response }, shipmentId } = result;
  return {
    type : RECEIVE_UPDATE_SHIPMENT_COMMODITIES,
    payload : { response, shipmentId },
    isFetching,
    isSuccessful
  }
}

export const rejectShipmentCommodities = ({error, shipmentId }, isFetching=false, isSuccessful=false) => {
  const { xhr } = error;
  return {
    type : REJECT_UPDATE_SHIPMENT_COMMODITIES,
    payload : { shipmentId, xhr },
    isFetching,
    isSuccessful
  }
}

export const updateShipmentCarrier = ({shipmentId,form,orderNumber}, isFetching=true, isSuccessful=false) => {
  return {
    type : UPDATE_SHIPMENT_CARRIER,
    payload : { shipmentId, form, orderNumber},
    isFetching,
    isSuccessful
  }
}

export const receiveShipmentCarrier = (result, isFetching=false, isSuccessful=true) => {
  const { response : { response }, shipmentId } = result;
  return {
    type : RECEIVE_UPDATE_SHIPMENT_CARRIER,
    payload : { response, shipmentId },
    isFetching,
    isSuccessful
  }
}

export const rejectShipmentCarrier = ({error, shipmentId }, isFetching=false, isSuccessful=false) => {
  const { xhr } = error;
  return {
    type : REJECT_UPDATE_SHIPMENT_CARRIER,
    payload : { shipmentId, xhr },
    isFetching,
    isSuccessful
  }
}

export const updateShipmentFreight = ({shipmentId,form,orderNumber}, isFetching=true, isSuccessful=false) => {
  return {
    type : UPDATE_SHIPMENT_FREIGHT,
    payload : { shipmentId, form, orderNumber},
    isFetching,
    isSuccessful
  }
}

export const receiveShipmentFreight = (result, isFetching=false, isSuccessful=true) => {
  const { response : { response }, shipmentId } = result;
  return {
    type : RECEIVE_UPDATE_SHIPMENT_FREIGHT,
    payload : { response, shipmentId },
    isFetching,
    isSuccessful
  }
}

export const rejectShipmentFreight = ({error, shipmentId }, isFetching=false, isSuccessful=false) => {
  const { xhr } = error;
  return {
    type : REJECT_UPDATE_SHIPMENT_FREIGHT,
    payload : { shipmentId, xhr },
    isFetching,
    isSuccessful
  }
}

export const updateShipmentOrderRequest = ({shipmentId,form,orderNumber}, isFetching=true, isSuccessful=false) => {
  return {
    type : UPDATE_SHIPMENT_ORDER_REQUEST,
    payload : { shipmentId, form, orderNumber},
    isFetching,
    isSuccessful
  }
}

export const receiveShipmentOrderRequest = (result, isFetching=false, isSuccessful=true) => {
  const { response : { response }, shipmentId } = result;
  return {
    type : RECEIVE_UPDATE_SHIPMENT_ORDER_REQUEST,
    payload : { response, shipmentId },
    isFetching,
    isSuccessful
  }
}

export const rejectShipmentOrderRequest = ({error, shipmentId }, isFetching=false, isSuccessful=false) => {
  const { xhr } = error;
  return {
    type : REJECT_UPDATE_SHIPMENT_ORDER_REQUEST,
    payload : { shipmentId, xhr },
    isFetching,
    isSuccessful
  }
}

export const updateShipmentOrderPrice = ({shipmentId,form,orderNumber}, isFetching=true, isSuccessful=false) => {
  return {
    type : UPDATE_SHIPMENT_ORDER_PRICE,
    payload : { shipmentId, form, orderNumber},
    isFetching,
    isSuccessful
  }
}

export const receiveShipmentOrderPrice = (result, isFetching=false, isSuccessful=true) => {
  const { response : { response }, shipmentId } = result;
  return {
    type : RECEIVE_UPDATE_SHIPMENT_ORDER_PRICE,
    payload : { response, shipmentId },
    isFetching,
    isSuccessful
  }
}

export const rejectShipmentOrderPrice = ({error, shipmentId }, isFetching=false, isSuccessful=false) => {
  const { xhr } = error;
  return {
    type : REJECT_UPDATE_SHIPMENT_ORDER_PRICE,
    payload : { shipmentId, xhr },
    isFetching,
    isSuccessful
  }
}

export const postUpdateShipmentDocs = ({shipmentId,form}, isFetching=true, isSuccessful=false) => {
  return {
    type : POST_UPDATE_SHIPMENT_DOCUMENT,
    payload : { shipmentId, form},
    isFetching,
    isSuccessful
  }
}

export const receivePostUpdateShipmentDocs = (result, isFetching=false, isSuccessful=true) => {
  const { response : { data } , shipmentId } = result;
  return {
    type : RECEIVE_POST_UPDATE_SHIPMENT_DOCUMENT,
    payload : { response : data, shipmentId },
    isFetching,
    isSuccessful
  }
}

export const rejectPostUpdateShipmentDocs = ({error, shipmentId }, isFetching=false, isSuccessful=false) => {
  const { data } = error.response;
  const xhr = {
    response : data
  }
  return {
    type : REJECT_POST_UPDATE_SHIPMENT_DOCUMENT,
    payload : { shipmentId, xhr },
    isFetching,
    isSuccessful
  }
}

export const updateProgressBarUpdateShipmentDocs = (percentCompleted) => {
  return {
    type : UPDATE_PROGRESS_BAR_UPLOAD_SHIPMENT_DOCS,
    payload : { percentCompleted }
  }
}


export const downloadShipmentDoc = ({shipmentId, orderNumber, fileId, fileCategory, saveAsFileName, extFileName}) => {
  return {
    type : DOWNLOAD_SHIPMENT_DOC,
    payload : { shipmentId, orderNumber, fileCategory, fileId, fileName : { saveAsFileName, extFileName }}
  }
}

export const receiveDownloadShipmentDoc = (result) => {
  const { data } = result;
  const response = data;
  return {
    type : RECEIVE_DOWNLOAD_SHIPMENT_DOC,
    payload : { response }
  }
}

export const rejectDownloadShipmentDoc = (error) => {
  const { data } = error.response;
  const xhr = {
    response : data
  }
  return {
    type : REJECT_DOWNLOAD_SHIPMENT_DOC,
    payload : { xhr }
  }
}

export const downloadShipmentInvoice = ({shipmentId, orderNumber, saveAsFileName, extFileName}) => {
  return {
    type : DOWNLOAD_SHIPMENT_INVOICE,
    payload : { shipmentId, orderNumber, fileName : { saveAsFileName, extFileName} }
  }
}

export const receiveDownloadShipmentInvoice = (result) => {
  const { data } = result;
  const response = data;
  return {
    type : RECEIVE_DOWNLOAD_SHIPMENT_INVOICE,
    payload : { response }
  }
}

export const rejectDownloadShipmentInvoice = (error) => {
  const { data } = error.response;
  const xhr = {
    response : data
  }
  return {
    type : REJECT_DOWNLOAD_SHIPMENT_INVOICE,
    payload : { xhr }
  }
}
