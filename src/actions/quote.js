import { POST_QUOTE, RECEIVE_POST_QUOTE, REJECT_POST_QUOTE,
         FETCH_QUOTES, RECEIVE_FETCH_QUOTES, REJECT_FETCH_QUOTES,
         DELETE_QUOTE, RECEIVE_DELETE_QUOTE, REJECT_DELETE_QUOTE } from './'

export const postQuote = ({quote, query}, isFetching = true) => {
  return {
    type : POST_QUOTE,
    payload : { quote, query },
    isFetching
  }
}
export const receivePostQuote = (result, isFetching = false) => {
  const { response } = result;
  return {
    type : RECEIVE_POST_QUOTE,
    payload : { response },
    isFetching
  }
}
export const rejectPostQuote = (error, isFetching = false) => {
  const { xhr } = error;
  return {
    type : REJECT_POST_QUOTE,
    payload : { xhr },
    isFetching
  }
}

export const fetchQuotes = (pageState = null, isFetching = true) => {
  return {
    type : FETCH_QUOTES,
    payload : { pageState },
    isFetching
  }
}
export const receiveFetchQuotes = (result, isFetching = false) => {
  const { response : { pageState, results } } = result;
  return {
    type : RECEIVE_FETCH_QUOTES,
    payload : { pageState, results },
    isFetching
  }
}
export const rejectFetchQuotes = (error, isFetching = false) => {
  const { xhr } = error;
  return {
    type : REJECT_FETCH_QUOTES,
    payload : { xhr }
  }
}

export const deleteQuote = (quoteId, createdAt, isFetching = true) => {
  return {
    type : DELETE_QUOTE,
    payload : { quoteId, createdAt },
    isFetching
  }
}
export const receiveDeleteQuote = ( result, isFetching = false) => {
  const { response } = result;
  return {
    type : RECEIVE_DELETE_QUOTE,
    payload : { response },
    isFetching
  }
}
export const rejectDeleteQuote = (error, isFetching = false) => {
  const { xhr } = error;
  return {
    type : REJECT_DELETE_QUOTE,
    payload : { xhr },
    isFetching
  }
}
