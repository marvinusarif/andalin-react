import { OPEN_LOGIN_MODAL, CLOSE_LOGIN_MODAL,
         OPEN_INVITATION_MODAL, CLOSE_INVITATION_MODAL,
         OPEN_LOGOUT_MODAL, CLOSE_LOGOUT_MODAL,
         OPEN_RESET_PASSWORD_MODAL, CLOSE_RESET_PASSWORD_MODAL,
         OPEN_REGISTER_MODAL, CLOSE_REGISTER_MODAL,
         OPEN_CONTACT_MODAL, CLOSE_CONTACT_MODAL,
         OPEN_UPLOADFILE_MODAL, CLOSE_UPLOADFILE_MODAL,
         OPEN_UPDATE_SHIPPER_CONSIGNEE_MODAL, CLOSE_UPDATE_SHIPPER_CONSIGNEE_MODAL,
         OPEN_UPDATE_ORDER_REQUEST_MODAL, CLOSE_UPDATE_ORDER_REQUEST_MODAL,
         OPEN_UPDATE_COMMODITIES_MODAL, CLOSE_UPDATE_COMMODITIES_MODAL,
         OPEN_UPDATE_CARRIER_MODAL, CLOSE_UPDATE_CARRIER_MODAL,
         OPEN_UPDATE_ORDER_STAT_MODAL, CLOSE_UPDATE_ORDER_STAT_MODAL,
         OPEN_UPDATE_FREIGHT_MODAL, CLOSE_UPDATE_FREIGHT_MODAL,
         OPEN_UPDATE_ORDER_PRICE_MODAL, CLOSE_UPDATE_ORDER_PRICE_MODAL,
         OPEN_UPLOADFILE_SHIPMENT_DOC_MODAL, CLOSE_UPLOADFILE_SHIPMENT_DOC_MODAL,
         FETCH_ALL_COUNTRIES, RECEIVE_ALL_COUNTRIES, REJECT_ALL_COUNTRIES,
         POST_VERIFICATION_EMAIL, RECEIVE_POST_VERIFICATION_EMAIL, REJECT_POST_VERIFICATION_EMAIL,
         RESET_VERIFICATION_EMAIL_TIMER, REDUCE_VERIFICATION_EMAIL_TIMER,
         POST_CONTACT, RECEIVE_POST_CONTACT, REJECT_POST_CONTACT,
         VERIFY_USER, RECEIVE_VERIFY_USER, REJECT_VERIFY_USER,
         RESET_PASSWORD_MAIL, RECEIVE_RESET_PASSWORD_MAIL, REJECT_RESET_PASSWORD_MAIL,
         FETCH_SHIPMENT_BANK_ACC, RECEIVE_FETCH_SHIPMENT_BANK_ACC, REJECT_FETCH_SHIPMENT_BANK_ACC,
       } from './';

export const openLoginModal = (isOpen=true, isFetching = false) => {
  return {
    type : OPEN_LOGIN_MODAL,
    payload : null,
    isOpen,
    isFetching
  }
}

export const closeLoginModal = (isOpen=false, isFetching = false) => {
  return {
    type : CLOSE_LOGIN_MODAL,
    payload : null,
    isOpen,
    isFetching
  }
}

export const openInvitationModal = (isOpen=true, isFetching = false, isSuccessful=false) => {
  return {
    type : OPEN_INVITATION_MODAL,
    payload : null,
    isOpen,
    isFetching,
    isSuccessful
  }
}

export const closeInvitationModal = (isOpen=false, isFetching = false, isSuccessful=false) => {
  return {
    type : CLOSE_INVITATION_MODAL,
    payload : null,
    isOpen,
    isFetching,
    isSuccessful
  }
}

export const openResetPasswordModal = (isOpen=true, isFetching=false,isSuccessful=false) => {
  return {
    type : OPEN_RESET_PASSWORD_MODAL,
    payload : null,
    isOpen,
    isFetching,
    isSuccessful
  }
}

export const closeResetPasswordModal = (isOpen=false, isFetching=false, isSuccessful=false) => {
  return {
    type : CLOSE_RESET_PASSWORD_MODAL,
    payload : null,
    isOpen,
    isFetching,
    isSuccessful
  }
}

export const openRegisterModal = (isOpen=true) => {
  return {
    type : OPEN_REGISTER_MODAL,
    payload : null,
    isOpen
  }
}

export const closeRegisterModal = (isOpen=false) => {
  return {
    type : CLOSE_REGISTER_MODAL,
    payload : null,
    isOpen
  }
}

export const openUploadFileModal = (isOpen=true, isFetching=false, isSuccessful=false) => {
  return {
    type : OPEN_UPLOADFILE_MODAL,
    payload : null,
    isOpen,
    isFetching,
    isSuccessful
  }
}

export const closeUploadFileModal = (isOpen=false, isFetching=false,isSuccessful=false) => {
  return {
    type : CLOSE_UPLOADFILE_MODAL,
    payload : null,
    isOpen,
    isFetching,
    isSuccessful
  }
}

export const openLogoutModal = (isOpen=true) => {
  return {
    type : OPEN_LOGOUT_MODAL,
    payload : null,
    isOpen
  }
}

export const closeLogoutModal = (isOpen=false) => {
  return {
    type : CLOSE_LOGOUT_MODAL,
    payload : null,
    isOpen
  }
}

export const fetchAllCountries = (result, isFetching = true) => {
  return {
    type : FETCH_ALL_COUNTRIES,
    payload : null,
    isFetching
  }
}

export const receiveFetchAllCountries = (result,isFetching = false) => {
  const { response } = result;
  return {
    type : RECEIVE_ALL_COUNTRIES,
    payload : { response },
    isFetching
  }
}
export const rejectFetchAllCountries = (error, isFetching = false) => {
  const { xhr } = error;
  return {
    type : REJECT_ALL_COUNTRIES,
    payload : { xhr },
    isFetching
  }
}
export const openContactModal = (isOpen=true, isSuccessful=false) => {
  return {
    type : OPEN_CONTACT_MODAL,
    payload : null,
    isOpen,
    isSuccessful
  }
}

export const closeContactModal = (isOpen=false) => {
  return {
    type : CLOSE_CONTACT_MODAL,
    payload : null,
    isOpen
  }
}


export const postVerificationEmail = (user, isFetching = true) => {
  return {
    type : POST_VERIFICATION_EMAIL,
    payload : { user },
    isFetching
  }
}

export const receiveVerificationEmail = (result, isFetching = false) => {
  const { response } = result;
  return {
    type : RECEIVE_POST_VERIFICATION_EMAIL,
    payload : { response },
    isFetching
  }
}

export const rejectVerificationEmail = (error, isFetching = false) => {
  const { xhr } = error;
  return {
    type : REJECT_POST_VERIFICATION_EMAIL,
    payload : { xhr },
    isFetching
  }
}

export const resetVerificationTimer = (timer=30) => {
  return {
    type : RESET_VERIFICATION_EMAIL_TIMER,
    payload : null,
    timer
  }
}

export const reduceVerificationTimer = (reduceTimerBy=1) => {
  return {
    type : REDUCE_VERIFICATION_EMAIL_TIMER,
    payload : null,
    reduceTimerBy
  }
}

export const postContact = (form, isFetching=true) => {
  return {
    type : POST_CONTACT,
    payload : { form },
    isFetching
  }
}

export const receivePostContact = (result, isFetching=false, isSuccessful=true) => {
  const { response } = result;
  return {
    type : RECEIVE_POST_CONTACT,
    payload : { response },
    isFetching,
    isSuccessful
  }
}

export const rejectPostContact = (error, isFetching=false, isSuccessful=false) => {
  const { xhr } = error;
  return {
    type : REJECT_POST_CONTACT,
    payload : { xhr },
    isFetching,
    isSuccessful
  }
}


export const verifyUser = (verifcode, isFetching = true, isSuccessful=false) => {
  return {
    type : VERIFY_USER,
    payload : { verifcode },
    isFetching,
    isSuccessful
  }
}

export const receiveVerifyUser = (result, isFetching = false, isSuccessful=true) => {
  const { response } = result;
  return {
    type : RECEIVE_VERIFY_USER,
    payload : { response },
    isFetching,
    isSuccessful
  }
}

export const rejectVerifyUser = (error, isFetching = false, isSuccessful=false) => {
  const { xhr } = error;
  return {
    type : REJECT_VERIFY_USER,
    payload : { xhr },
    isFetching,
    isSuccessful
  }
}

export const resetPasswordMail = (form, isFetching = true, isSuccessful = false) => {
  return {
    type : RESET_PASSWORD_MAIL,
    payload : { form },
    isFetching,
    isSuccessful
  }
}


export const receiveResetPasswordMail = (result, isFetching = false, isSuccessful=true) => {
  const { response } = result;
  return {
    type : RECEIVE_RESET_PASSWORD_MAIL,
    payload : { response },
    isFetching,
    isSuccessful
  }
}

export const rejectResetPasswordMail = (error, isFetching = false, isSuccessful=false) => {
  const { xhr } = error;
  return {
    type : REJECT_RESET_PASSWORD_MAIL,
    payload : { xhr },
    isFetching,
    isSuccessful
  }
}

export const openUpdateShipperConsigneeModal = (shipmentId, isOpen=true, isFetching = false) => {
  return {
    type : OPEN_UPDATE_SHIPPER_CONSIGNEE_MODAL,
    payload : {shipmentId},
    isOpen,
    isFetching
  }
}

export const closeUpdateShipperConsigneeModal = (shipmentId, isOpen=false, isFetching = false) => {
  return {
    type : CLOSE_UPDATE_SHIPPER_CONSIGNEE_MODAL,
    payload : {shipmentId},
    isOpen,
    isFetching
  }
}

export const openUpdateOrderRequestModal = (shipmentId, isOpen=true, isFetching = false) => {
  return {
    type : OPEN_UPDATE_ORDER_REQUEST_MODAL,
    payload : {shipmentId},
    isOpen,
    isFetching
  }
}

export const closeUpdateOrderRequestModal = (shipmentId, isOpen=false, isFetching = false) => {
  return {
    type : CLOSE_UPDATE_ORDER_REQUEST_MODAL,
    payload : {shipmentId},
    isOpen,
    isFetching
  }
}

export const openUpdateCommoditiesModal = (shipmentId, isOpen=true, isFetching = false) => {
  return {
    type : OPEN_UPDATE_COMMODITIES_MODAL,
    payload : {shipmentId},
    isOpen,
    isFetching
  }
}

export const closeUpdateCommoditiesModal = (shipmentId, isOpen=false, isFetching = false) => {
  return {
    type : CLOSE_UPDATE_COMMODITIES_MODAL,
    payload : {shipmentId},
    isOpen,
    isFetching
  }
}

export const openUpdateCarrierModal = (shipmentId, isOpen=true, isFetching = false) => {
  return {
    type : OPEN_UPDATE_CARRIER_MODAL,
    payload : {shipmentId},
    isOpen,
    isFetching
  }
}

export const closeUpdateCarrierModal = (shipmentId, isOpen=false, isFetching = false) => {
  return {
    type : CLOSE_UPDATE_CARRIER_MODAL,
    payload : {shipmentId},
    isOpen,
    isFetching
  }
}

export const openUpdateOrderStatModal = (shipmentId, isOpen=true, isFetching = false) => {
  return {
    type : OPEN_UPDATE_ORDER_STAT_MODAL,
    payload : {shipmentId},
    isOpen,
    isFetching
  }
}

export const closeUpdateOrderStatModal = (shipmentId, isOpen=false, isFetching = false) => {
  return {
    type : CLOSE_UPDATE_ORDER_STAT_MODAL,
    payload : {shipmentId},
    isOpen,
    isFetching
  }
}

export const openUpdateFreightModal = (shipmentId, isOpen=true, isFetching = false) => {
  return {
    type : OPEN_UPDATE_FREIGHT_MODAL,
    payload : {shipmentId},
    isOpen,
    isFetching
  }
}

export const closeUpdateFreightModal = (shipmentId, isOpen=false, isFetching = false) => {
  return {
    type : CLOSE_UPDATE_FREIGHT_MODAL,
    payload : {shipmentId},
    isOpen,
    isFetching
  }
}

export const openUpdateOrderPriceModal = (shipmentId, isOpen=true, isFetching = false) => {
  return {
    type : OPEN_UPDATE_ORDER_PRICE_MODAL,
    payload : {shipmentId},
    isOpen,
    isFetching
  }
}

export const closeUpdateOrderPriceModal = (shipmentId, isOpen=false, isFetching = false) => {
  return {
    type : CLOSE_UPDATE_ORDER_PRICE_MODAL,
    payload : {shipmentId},
    isOpen,
    isFetching
  }
}

export const openPostUpdateShipmentDocs = (shipmentId, isOpen=true, isFetching = false) => {
  return {
    type : OPEN_UPLOADFILE_SHIPMENT_DOC_MODAL,
    payload : {shipmentId},
    isOpen,
    isFetching
  }
}

export const closePostUpdateShipmentDocs = (shipmentId, isOpen=false, isFetching = false) => {
  return {
    type : CLOSE_UPLOADFILE_SHIPMENT_DOC_MODAL,
    payload : {shipmentId},
    isOpen,
    isFetching
  }
}

export const fetchShipmentBankAcc = (isFetching=true) => {
  return {
    type : FETCH_SHIPMENT_BANK_ACC,
    payload : null,
    isFetching
  }
}

export const receiveFetchShipmentBankAcc = (result, isFetching=false) => {
  const { response } = result;
  return {
    type : RECEIVE_FETCH_SHIPMENT_BANK_ACC,
    payload : { response },
    isFetching
  }
}

export const rejectFetchShipmentBankAcc = (error, isFetching=false) => {
  const { xhr } = error;
  return {
    type : REJECT_FETCH_SHIPMENT_BANK_ACC,
    payload : { xhr },
    isFetching
  }
}
