import { SEARCH_SWITCH_TAB,
        FETCH_COUNTRIES_DESTINATION, RECEIVE_COUNTRIES_DESTINATION, REJECT_COUNTRIES_DESTINATION,
        FETCH_COUNTRIES_ORIGIN, RECEIVE_COUNTRIES_ORIGIN, REJECT_COUNTRIES_ORIGIN,
        FETCH_CITIES_DESTINATION, RECEIVE_CITIES_DESTINATION, REJECT_CITIES_DESTINATION,
        FETCH_CITIES_ORIGIN, RECEIVE_CITIES_ORIGIN, REJECT_CITIES_ORIGIN,
        POST_QUERY, RECEIVE_POST_QUERY, REJECT_POST_QUERY } from './';

export const switchTab = (activeTab) => {
  return {
    type : SEARCH_SWITCH_TAB,
    payload : { activeTab }
  }
}
export const fetchCountriesDestination = () => {
  return {
    type : FETCH_COUNTRIES_DESTINATION,
    payload : null
  }
}
export const fetchCountriesOrigin = () => {
  return {
    type : FETCH_COUNTRIES_ORIGIN,
    payload : null
  }
}
export const receiveFetchCountriesDestination = (result) => {
  const { response } = result;
  return {
    type : RECEIVE_COUNTRIES_DESTINATION,
    payload : { response }
  }
}
export const rejectFetchCountriesDestination = (error) => {
  const { xhr } = error;
  return {
    type : REJECT_COUNTRIES_DESTINATION,
    payload : { xhr }
  }
}
export const receiveFetchCountriesOrigin = (result) => {
  const { response } = result;
  return {
    type : RECEIVE_COUNTRIES_ORIGIN,
    payload : { response }
  }
}
export const rejectFetchCountriesOrigin = (error) => {
  const { xhr } = error;
  return {
    type : REJECT_COUNTRIES_ORIGIN,
    payload : { xhr }
  }
}
export const fetchCitiesDestination = (country) => {
  return {
    type : FETCH_CITIES_DESTINATION,
    payload : { country }
  }
}
export const fetchCitiesOrigin = (country) => {
  return {
    type : FETCH_CITIES_ORIGIN,
    payload : { country }
  }
}
export const receiveFetchCitiesDestination = (result) => {
  const { response } = result;
  return {
    type : RECEIVE_CITIES_DESTINATION,
    payload : { response }
  }
}
export const rejectFetchCitiesDestination = (error) => {
  const { xhr } = error;
  return {
    type : REJECT_CITIES_DESTINATION,
    payload : { xhr }
  }
}
export const receiveFetchCitiesOrigin = (result) => {
  const { response } = result;
  return {
    type : RECEIVE_CITIES_ORIGIN,
    payload : { response }
  }
}
export const rejectFetchCitiesOrigin = (error) => {
  const { xhr } = error;
  return {
    type : REJECT_CITIES_ORIGIN,
    payload : { xhr }
  }
}
export const postQuery = (query, isFetching = true) => {
  return {
    type : POST_QUERY,
    payload : { query },
    isFetching
  }
}
export const receivePostQuery = (result, isFetching = false) => {
  const { response } = result;
  return {
    type : RECEIVE_POST_QUERY,
    payload : { response },
    isFetching
  }
}
export const rejectPostQuery = (error) => {
  const { xhr } = error;
  return {
    type : REJECT_POST_QUERY,
    payload : { xhr }
  }
}
