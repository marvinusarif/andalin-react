import "./assets/plugins/bootstrap/css/bootstrap.min.css";
import "./assets/plugins/font-awesome/css/font-awesome.css";
import "./assets/pages/css/pages-icons.css";
import "./assets/pages/css/themes/corporate.css";
import "./assets/css/loader.css";
import "./assets/plugins/pace/pace-theme-flash.css";
import "./assets/plugins/bootstrap-datepicker/css/datepicker3.css";
import "react-select/dist/react-select.css";
import "./index.css";
import "rxjs";

import React from 'react';
import ReactDOM from 'react-dom';
import App from './containers/App';
import registerServiceWorker from './registerServiceWorker';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import configureStore from './stores/configureStore';

async function init(){
    const store = await configureStore()
    ReactDOM.render(
                  <Provider store={store}>
                    <BrowserRouter>
                      <MuiThemeProvider>
                        <App/>
                      </MuiThemeProvider>
                    </BrowserRouter>
                  </Provider>
                  , document.getElementById('root'));
}

init()


//registerServiceWorker();
